package com.ct.tpcalculator.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.calculator.data.MainData;
import com.ct.tpcalculator.helpers.FileCache;
import com.ct.tpcalculator.helpers.L;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by marat on 11/27/2015.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    public final static String ACTION_SYNC_COMPLETE = "com.ct.odp.SYNC_COMPLETE";

    public static final String ARG_FORCE_SYNC = "com.ct.ARG_FORCE_SYNC";
    public static final String ARG_FILE_NAME = "com.ct.ARG_FILE_NAME";
    public static final String ARG_EXTRA_RESULT_DATA = "com.ct.ARG_EXTRA_RESULT_DATA";
    public static final String ARG_EXTRA_RESULT_IS_COMPLETE = "com.ct.ARG_EXTRA_RESULT_IS_COMPLETE";

    public SyncAdapter(Context context) {
        super(context, true);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        boolean forceSync = extras.getBoolean(ARG_FORCE_SYNC);
        String fileName = extras.getString(ARG_FILE_NAME);

        if (TextUtils.isEmpty(fileName)){
            fileName = getFileName();
        }

        L.e("onPerformSync - filename = " + fileName);

        if (!TextUtils.isEmpty(fileName) && (forceSync || checkNewDataAvailable(fileName))) {
            downloadData(fileName);
        } else {
            sendSyncComplete("", false);
        }
    }

    private String getFileName() {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        return preferences.getString(getContext().getString(R.string.preferences_selected_country_region_file_name), "");
    }

    private boolean checkNewDataAvailable(String fileName) {
        byte[] data = null;

        // нет сети - нет данных
        if (!com.ct.tpcalculator.application.Application.isNetworkAvailable()) {
            return false;
        }

        try {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

            String fileNameWithOutExt = fileName.replaceFirst("[.][^.]+$", "");
            String path = String.format("%1$s%2$s.txt", getContext().getString(R.string.server_url), fileNameWithOutExt);

            L.e("checkNewDataAvailable - path = " + path);

            URL url = new URL(path);
            URLConnection urlConnection = url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            try {
                data = FileCache.readFull(in);
                String text = new String(data, "UTF-8");

                String currentFileVersion = preferences.getString(getContext().getString(R.string.preferences_file_version), "");

                L.e("old file version = " + currentFileVersion);
                L.e("new file version = " + text);

                if (!currentFileVersion.equalsIgnoreCase(text)) {
                    return true;
                }
            } finally {
                in.close();
            }
        } catch (MalformedURLException ex) {

        } catch (IOException ex) {

        }
        return false;
    }

    private void downloadData(String fileName) {
        byte[] data = null;

        // нет сети - нет данных
        if (!com.ct.tpcalculator.application.Application.isNetworkAvailable()) {
            sendSyncComplete("", false);
            return;
        }

        L.e("downloadData");
        try {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
//            String fileName = preferences.getString(getContext().getString(R.string.preferences_selected_country_region_file_name), "");
            String path = String.format("%1$s%2$s", getContext().getString(R.string.server_url), fileName);
            L.e("downloadData - path = " + path);
            URL url = new URL(path);
            URLConnection urlConnection = url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            try {
                data = FileCache.readFull(in);

                XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
                parser.setInput(new ByteArrayInputStream(data), null);

                MainData mainData = new MainData(parser);
                Application.getInstance().setMainData(mainData);
                FileCache.setContent(FileCache.REGION_FILE_KEY, data, FileCache.NEVER_EXPIRES);

                SharedPreferences.Editor e = preferences.edit();
                e.putString(getContext().getString(R.string.preferences_file_version), mainData.getFileVersion());
                e.apply();

                String textData = new String(data, "UTF-8");
                sendSyncComplete(textData, true);

                L.e(textData);

            } catch (XmlPullParserException e) {
                sendSyncComplete("", false);
            } finally {
                in.close();
            }
        } catch (MalformedURLException ex) {
            sendSyncComplete("", false);
        } catch (IOException ex) {
            sendSyncComplete("", false);
        }
    }

    private void sendSyncComplete(String data, boolean isComplete) {
        final Intent intent = new Intent(ACTION_SYNC_COMPLETE);
        intent.putExtra(ARG_EXTRA_RESULT_DATA, data);
        intent.putExtra(ARG_EXTRA_RESULT_IS_COMPLETE, isComplete);
        getContext().sendBroadcast(intent);
    }
}
