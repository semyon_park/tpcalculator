package com.ct.tpcalculator.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by marat on 11/30/2015.
 */
public class SyncService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return new SyncAdapter(getApplicationContext()).getSyncAdapterBinder();
    }

}
