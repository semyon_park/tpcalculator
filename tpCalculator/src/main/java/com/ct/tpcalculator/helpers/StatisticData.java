package com.ct.tpcalculator.helpers;

import android.content.Context;

import com.ct.tpcalculator.calculator.data.DataRegionTypes;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticCall;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticCalls;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticInternetTraffic;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticInternetTraffics;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticLogs;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticMessage;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticMessages;
import com.ct.tpcalculator.calculator.statisticAgent.callstat.CallStatCore;
import com.ct.tpcalculator.calculator.statisticAgent.msgstat.MsgCore;
import com.ct.tpcalculator.calculator.statisticAgent.netstat.NetStatCore;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by USER on 06.07.2015.
 */
public class StatisticData {

    private static StatisticData instance;

    private StatisticData() {
    }

    public static StatisticData getInstance() {
        if (instance == null) {
            instance = new StatisticData();
        }
        return instance;
    }


    public StatisticLogs getStatisticData(ArrayList<String> homeCountryCodes, ArrayList<String> homeRegionCodes,
                                          int minDigits, int maxDigits,
                                          Date startDate, Date endDate, Context context) {
        StatisticCalls calls = getStatisticCalls(homeCountryCodes, homeRegionCodes, minDigits, maxDigits, startDate, endDate, context);
        StatisticMessages smsMessages = getStatisticMessages(homeCountryCodes, homeRegionCodes, minDigits, maxDigits, StatisticMessage.TYPE_MSG_SMS, startDate, endDate, context);
        StatisticMessages mmsMessages = getStatisticMessages(homeCountryCodes, homeRegionCodes, minDigits, maxDigits, StatisticMessage.TYPE_MSG_MMS, startDate, endDate, context);
        StatisticInternetTraffics internetTraffics = getStatisticInternetTraffic(context, startDate, endDate);

        return new StatisticLogs(calls, smsMessages, mmsMessages, internetTraffics);
    }

    public StatisticCalls getStatisticCalls(ArrayList<String> homeCountryCodes,
                                            ArrayList<String> homeRegionCodes,
                                            int minDigits,
                                            int maxDigits,
                                            Date startDate,
                                            Date endDate,
                                            Context context) {


        ArrayList<StatisticCall> allCalls = CallStatCore.GetStatisticCalls(context, startDate, endDate);
        return getStatisticCalls(allCalls, homeCountryCodes, homeRegionCodes, minDigits, maxDigits, startDate, endDate);
    }

    public StatisticCalls getStatisticCalls(ArrayList<StatisticCall> allCalls,
                                            ArrayList<String> homeCountryCodes,
                                            ArrayList<String> homeRegionCodes,
                                            int minDigits,
                                            int maxDigits,
                                            Date startDate,
                                            Date endDate) {

        ArrayList<StatisticCall> inRegionCalls = new ArrayList<>();
        ArrayList<StatisticCall> interRegionCalls = new ArrayList<>();
        ArrayList<StatisticCall> interCountriesCalls = new ArrayList<>();

        for (StatisticCall call : allCalls) {
            if (call.getCallDate().after(endDate) || call.getCallDate().before(startDate)){
                continue;
            }
            int regionType = Utils.GetPhoneNumberRegionType(call.getPhoneNumber(), homeCountryCodes, homeRegionCodes, minDigits, maxDigits);

            if (regionType == DataRegionTypes.InRegion) {
                inRegionCalls.add(call);
            } else if (regionType == DataRegionTypes.InterRegion) {
                interRegionCalls.add(call);
            } else if (regionType == DataRegionTypes.InterCountries) {
                interCountriesCalls.add(call);
            }
        }

        return new StatisticCalls(inRegionCalls, interRegionCalls, interCountriesCalls);
    }

    public StatisticMessages getStatisticMessages( ArrayList<StatisticMessage> allMessages,
                                                    ArrayList<String> homeCountryCodes,
                                                    ArrayList<String> homeRegionCodes,
                                                   int minDigits, int maxDigits, Date startDate, Date endDate) {

        ArrayList<StatisticMessage> inRegionMessages = new ArrayList<>();
        ArrayList<StatisticMessage> interRegionMessages = new ArrayList<>();
        ArrayList<StatisticMessage> interCountriesMessages = new ArrayList<>();

        for (StatisticMessage message : allMessages) {
            if (message.getMessageDate().after(endDate) || message.getMessageDate().before(startDate)){
                continue;
            }
            int regionType = Utils.GetPhoneNumberRegionType(message.getPhoneNumber(), homeCountryCodes, homeRegionCodes, minDigits, maxDigits);
            if (regionType == DataRegionTypes.InRegion) {
                inRegionMessages.add(message);
            } else if (regionType == DataRegionTypes.InterRegion) {
                interRegionMessages.add(message);
            } else if (regionType == DataRegionTypes.InterCountries) {
                interCountriesMessages.add(message);
            }
        }

        return new StatisticMessages(inRegionMessages, interRegionMessages, interCountriesMessages);
    }

    public StatisticMessages getStatisticMessages(ArrayList<String> homeCountryCodes,
                                            ArrayList<String> homeRegionCodes,
                                            int minDigits,
                                            int maxDigits,
                                                  int msgType,
                                            Date startDate,
                                            Date endDate,
                                            Context context) {

        ArrayList<StatisticMessage> allMessages = MsgCore.GetStatisticMessageStatistic(context, startDate, endDate, msgType);
        return getStatisticMessages(allMessages, homeCountryCodes, homeRegionCodes, minDigits, maxDigits, startDate, endDate);
    }

    public StatisticInternetTraffics getStatisticInternetTraffic(Context context, Date startDate, Date endDate) {
        ArrayList<StatisticInternetTraffic> wifiTraffic = NetStatCore.GetInternetStatisticData(context, startDate, endDate, StatisticInternetTraffic.TYPE_WIFI);
        ArrayList<StatisticInternetTraffic> mobileTraffic = NetStatCore.GetInternetStatisticData(context, startDate, endDate, StatisticInternetTraffic.TYPE_MOBILE);

        StatisticInternetTraffics result = new StatisticInternetTraffics();
        result.setWifiInternetTraffic(wifiTraffic);
        result.setMobileInternetTraffic(mobileTraffic);

        return result;
    }

}
