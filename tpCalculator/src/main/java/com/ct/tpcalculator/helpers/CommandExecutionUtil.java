package com.ct.tpcalculator.helpers;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.activities.BaseActivity;
import com.ct.tpcalculator.calculator.data.BaseCommand;
import com.ct.tpcalculator.calculator.data.Command;
import com.ct.tpcalculator.calculator.data.IvrBaseCommand;
import com.ct.tpcalculator.calculator.data.LinkBaseCommand;
import com.ct.tpcalculator.calculator.data.SmsBaseCommand;
import com.ct.tpcalculator.calculator.data.UssdBaseCommand;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;

/**
 * Created by marat on 11/26/2015.
 */
public class CommandExecutionUtil {

    public final static String SENT_INTENT_ACTION = "com.ct.ussdmenu.SMS_SENT";
    public final static String DELIVER_INTENT_ACTION = "com.ct.ussdmenu.SMS_DELIVERED";
    public final static String SENT_INTENT_ADDRESS = "address";
    public static final int CALL_PHONE_PERMISSION_REQUEST = 100;

    private static CommandExecutionUtil mInstance;

    private static Handler handler = new Handler(Looper.getMainLooper());

    public static CommandExecutionUtil getInstance() {
        if (mInstance == null) {
            mInstance = new CommandExecutionUtil();
        }
        return mInstance;
    }

    public void executeOfflineCommand(BaseActivity context, Command command, String gaLogCategory) {

        BaseCommand baseCommand = command.getBaseCommands().get(0);
        String action = null;
        String logCommand = null;

        if (baseCommand instanceof IvrBaseCommand) {
            IvrBaseCommand ivrBaseCommand = (IvrBaseCommand) baseCommand;
            call(context, ivrBaseCommand.getPhoneNumber());
            action = "CALL";
            logCommand = ivrBaseCommand.getPhoneNumber();
        } else if (baseCommand instanceof UssdBaseCommand) {
            UssdBaseCommand ussdBaseCommand = (UssdBaseCommand) baseCommand;
            call(context, ussdBaseCommand.getUssdCommand());
            action = "USSD";
            logCommand = ussdBaseCommand.getUssdCommand();
        } else if (baseCommand instanceof SmsBaseCommand) {
            SmsBaseCommand smsBaseCommand = (SmsBaseCommand) baseCommand;
            sendSms(context, smsBaseCommand.getSmsNumber(), smsBaseCommand.getSmsText());
            action = "SMS";
            logCommand = "NUMBER=" + smsBaseCommand.getSmsNumber() + "; TEXT=" + smsBaseCommand.getSmsText();
        } else if (baseCommand instanceof LinkBaseCommand) {
            LinkBaseCommand linkBaseCommand = (LinkBaseCommand) baseCommand;
            openLink(context, linkBaseCommand.getLink());
            action = "LINK";
            logCommand = linkBaseCommand.getLink();
        }

        if (action != null) {
            Dictionary<Integer, String> customVariables = new Hashtable<>();
            customVariables.put(new Integer(GoogleAnalyticsHelper.CUSTOM_VARIABLE_COMMAND), logCommand);

            GoogleAnalyticsHelper.sendLog(gaLogCategory, action, customVariables);
        }
    }

    private void call(final BaseActivity activity, final String number) {
        final Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + Uri.encode(number)));
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            activity.startActivity(intent);
        }
    }

    public static void requestAllPermissions(final Activity activity) {
        CommandExecutionUtil.makeActionWithPermissions(activity, Manifest.permission.CALL_PHONE,
                Manifest.permission.GET_ACCOUNTS,
//               Manifest.permission.ACCOUNT_MANAGER,
                Manifest.permission.READ_CALL_LOG,
                Manifest.permission.READ_SMS,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_WIFI_STATE,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.WAKE_LOCK,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECEIVE_BOOT_COMPLETED,
                Manifest.permission.WRITE_SYNC_SETTINGS);

    }

    public static void makeActionWithPermissions(final Activity activity, final String... permissions) {
        if (!hasPermissions(activity, permissions)) {
            if (activity instanceof ActivityCompat.OnRequestPermissionsResultCallback) {
                ActivityCompat.requestPermissions(activity, permissions, 100);
            }
        }
    }

    public static boolean hasPermissions(final Context context, final String... permissions) {
        for (final String permission : permissions) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    /***
     * Отправка SMS
     */
    public void sendSms(Context context, String number, String text) {
        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> smsText = smsManager.divideMessage(text);
        ArrayList<PendingIntent> sentItents = new ArrayList<PendingIntent>();
        ArrayList<PendingIntent> deliverItents = new ArrayList<PendingIntent>();
        for (int i = 0; i < smsText.size(); i++) {
            Intent intent = new Intent(SENT_INTENT_ACTION);
            intent.putExtra(SENT_INTENT_ADDRESS, number);
            sentItents.add(PendingIntent.getBroadcast(context, 0, intent,
                    PendingIntent.FLAG_CANCEL_CURRENT));

            Intent di = new Intent(DELIVER_INTENT_ACTION);
            di.putExtra(SENT_INTENT_ADDRESS, number);
            deliverItents.add(PendingIntent.getBroadcast(context, 0, di,
                    PendingIntent.FLAG_CANCEL_CURRENT));
        }
        smsManager.sendMultipartTextMessage(number, null, smsText, sentItents, deliverItents);
    }

    /***
     * Открытие линка
     *
     * @param context
     * @param uri
     */
    public static void openLink(Context context, String uri) {
        try {
            final Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(uri));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
