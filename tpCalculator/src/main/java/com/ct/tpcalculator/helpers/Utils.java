package com.ct.tpcalculator.helpers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.support.v4.util.Pair;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.calculator.data.DataRegionTypes;
import com.ct.tpcalculator.calculator.data.LocalizedStrings;
import com.ct.tpcalculator.calculator.data.Service;

import java.security.InvalidParameterException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by USER on 23.06.2015.
 */
public class Utils {


    public static int convertDipToPixels(int dip, Resources r) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dip, r.getDisplayMetrics());
    }

    /**
     * Отрезок времени помесячно с учетом шага. Например: startDate = 28.03.2015; endDate = 5.05.2015, step = 2, Возвратит 2 отрезка (28.03.2015-30.04.2015); (01.05.2015-05.05.2015)
     */
    public static ArrayList<Pair<Date, Date>> getMonthPeriod(Date startDate, Date endDate, int step) {

        ArrayList<Pair<Date, Date>> result = new ArrayList<>();

        Calendar startDateCalendar = Calendar.getInstance();
        startDateCalendar.setTime(startDate);

        Calendar endDateCalendar = Calendar.getInstance();
        endDateCalendar.setTime(endDate);

        int sMonth = startDateCalendar.get(Calendar.MONTH);
        int sYear = startDateCalendar.get(Calendar.YEAR);

        int eMonth = endDateCalendar.get(Calendar.MONTH);
        int eYear = endDateCalendar.get(Calendar.YEAR);

        int stepIndex = 1;
        Calendar periodStartDate = Calendar.getInstance();
        Calendar periodEndDate = Calendar.getInstance();
        while (sYear * 12 + sMonth <= eYear * 12 + eMonth) {
            if (stepIndex == 1) {
                int minDay = 1;
                if (sMonth == startDateCalendar.get(Calendar.MONTH) &&
                        sYear == startDateCalendar.get(Calendar.YEAR)) {
                    minDay = startDateCalendar.get(Calendar.DAY_OF_MONTH);
                } else {
                    minDay = 1;
                }
                periodStartDate.set(sYear, sMonth, minDay);
                setDateToStartOfDay(periodStartDate);
            }
            if (stepIndex == step) {
                int maxDay;
                if (sMonth == eMonth && sYear == eYear) {
                    maxDay = endDateCalendar.get(Calendar.DAY_OF_MONTH);
                } else {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(sYear, sMonth, 1);
                    maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                }
                periodEndDate.set(sYear, sMonth, maxDay);

                setDateToEndOfDay(periodEndDate);

                result.add(new Pair<>(periodStartDate.getTime(), periodEndDate.getTime()));

                stepIndex = 1;
            } else if (sMonth == eMonth && sYear == eYear) {
                int maxDay;
                maxDay = endDateCalendar.get(Calendar.DAY_OF_MONTH);
                periodEndDate.set(sYear, sMonth, maxDay);

                setDateToEndOfDay(periodEndDate);

                result.add(new Pair<>(periodStartDate.getTime(), periodEndDate.getTime()));
            } else {
                stepIndex++;
            }


            if (sMonth == Calendar.DECEMBER) {
                sMonth = Calendar.JANUARY;
                sYear++;
            } else {
                sMonth++;
            }
        }

        return result;
    }

    /**
     * Возвращает количество недель в промежутке времени.
     */
    public static ArrayList<Pair<Date, Date>> getWeeksPeriod(Date a, Date b, int step) {

        if (b.before(a)) {
            throw new InvalidParameterException("b cannot be less than a");
        }

        ArrayList<Pair<Date, Date>> result = new ArrayList<>();

        Calendar cal = Calendar.getInstance();
        cal.setTime(a);
        cal.setFirstDayOfWeek(Calendar.MONDAY);

        int stepIndex = 1;
        Calendar startDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();
        while (cal.getTime().before(b)) {

            if (stepIndex == 1) {
                if (cal.getTime().equals(a)) {
                    startDate.setTime(a);
                } else {
                    cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                    startDate.setTime(cal.getTime());
                }
                setDateToStartOfDay(startDate);
            }

            if (stepIndex == step) {
                cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

                if (b.before(cal.getTime())) {
                    endDate.setTime(b);
                } else {
                    endDate.setTime(cal.getTime());
                }

                setDateToEndOfDay(endDate);

                result.add(new Pair<>(startDate.getTime(), endDate.getTime()));

                stepIndex = 1;
            } else {
                stepIndex++;
            }

            if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) { //Почему то когда день недели воскресенье и установить календарь на понедельник, то возврощается число со следующей недели.
                cal.add(Calendar.DAY_OF_MONTH, -1);
            }
            cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

            // add another week
            cal.add(Calendar.WEEK_OF_YEAR, 1);
        }
        return result;
    }

    /**
     * Возвращает дни в промежутке времени.
     */
    public static ArrayList<Pair<Date, Date>> getDaysPeriod(Date a, Date b, int step) {

        if (b.before(a)) {
            throw new InvalidParameterException("b cannot be less than a");
        }

        ArrayList<Pair<Date, Date>> result = new ArrayList<>();

        Calendar cal = Calendar.getInstance();
        cal.setTime(a);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        int stepIndex = 1;
        Calendar calB = Calendar.getInstance();
        calB.setTime(b);

        Calendar startDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();
        while (cal.getTime().before(b)) {
            if (stepIndex == 1) {
                startDate.setTime(cal.getTime());
                setDateToStartOfDay(startDate);
            }

            if (stepIndex == step) {
                endDate.setTime(cal.getTime());
                setDateToEndOfDay(endDate);
                result.add(new Pair<>(startDate.getTime(), endDate.getTime()));
                stepIndex = 1;
            } else if (cal.get(Calendar.YEAR) == calB.get(Calendar.YEAR) &&
                    cal.get(Calendar.DAY_OF_YEAR) == calB.get(Calendar.DAY_OF_YEAR)) {
                endDate.setTime(cal.getTime());
                setDateToEndOfDay(endDate);
                result.add(new Pair<>(startDate.getTime(), endDate.getTime()));
            } else {
                stepIndex++;
            }

            // add another day
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        return result;
    }

    public static Pair<Date, Date> getMonthOfTheDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        int startDay = 1;
        int endDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        Calendar startDate = Calendar.getInstance();
        startDate.set(year, month, startDay, 0, 0, 0);

        Calendar endDate = Calendar.getInstance();
        endDate.set(year, month, endDay, 23, 59, 59);

        return new Pair<>(startDate.getTime(), endDate.getTime());
    }

    public static Pair<Date, Date> getDayOfTheDate(Date date) {
        Date startDate = getStartOfDay(date);
        Date endDate = getEndOfDay(date);
        return new Pair<>(startDate, endDate);
    }

    public static Pair<Date, Date> getWeekOfTheDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        int startDay = calendar.getActualMinimum(Calendar.DAY_OF_WEEK);
        int endDay = calendar.getActualMaximum(Calendar.DAY_OF_WEEK);

        Calendar startDate = Calendar.getInstance();
        startDate.set(year, month, startDay, 0, 0, 0);

        Calendar endDate = Calendar.getInstance();
        endDate.set(year, month, endDay, 23, 59, 59);

        return new Pair<>(startDate.getTime(), endDate.getTime());
    }

    public static Date getStartOfDay(Date startDate) {
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        setDateToStartOfDay(c);
        return c.getTime();
    }

    private static void setDateToStartOfDay(Calendar date) {
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
    }

    public static Date getEndOfDay(Date startDate) {
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        setDateToEndOfDay(c);
        return c.getTime();
    }

    public static void setDateToEndOfDay(Calendar date) {
        date.set(Calendar.HOUR_OF_DAY, 23);
        date.set(Calendar.MINUTE, 59);
        date.set(Calendar.SECOND, 59);
        date.set(Calendar.MILLISECOND, 59);
    }

    /**
     * Возвращает кол-во минут относительно секунд. Например seconds = 62 Вернет 2 минуты
     */
    public static int getMinutes(int seconds) {
        if (seconds % 60 == 0) {
            return seconds / 60;
        } else {
            return (seconds / 60) + 1;
        }
    }

    /**
     * Возвращает кол-во МБ относительно Байт. Например seconds = 500Б Вернет 1 МБ. (Округление идет в большую сторону)
     */
    public static int getMegaBytes(long bytes) {
        if (bytes % (1024 * 1024) == 0) {
            return (int) (bytes / (1024 * 1024));
        }
        return (int) ((bytes / (1024 * 1024)) + 1);
    }

    public static boolean isPhoneNumberStartWith(String phoneNumber, String numberPrefix) {
        if (TextUtils.isEmpty(numberPrefix)) {
            return false;
        }

        int phoneNumberStartSubstringIndex = 0;
        if (phoneNumber.startsWith("+")) {
            phoneNumberStartSubstringIndex = 1;
        } else if (phoneNumber.startsWith("810")) {
            phoneNumberStartSubstringIndex = 3;
        }

        try {
            if (numberPrefix.contains("-")) {
                String[] numberPrefixes = numberPrefix.split("-");
                int numberA = Integer.parseInt(numberPrefixes[0]);
                int numberB = Integer.parseInt(numberPrefixes[1]);
                int digitsCount = numberPrefixes[0].length();
                int phoneNumberPrefix = Integer.parseInt(phoneNumber.substring(phoneNumberStartSubstringIndex, phoneNumberStartSubstringIndex + digitsCount));
                return numberA <= phoneNumberPrefix && numberB >= phoneNumberPrefix;
            } else if (phoneNumber.substring(phoneNumberStartSubstringIndex).startsWith(numberPrefix)) {
                return true;
            }
        }catch (Exception ex){
            L.e(ex.toString());
        }


        return false;
    }

    public static boolean IsTimeAIsAfterOrEqualsTimeB(Date timeA, Date timeB) {
        Calendar calA = Calendar.getInstance();
        calA.setTime(timeA);

        int hourA = calA.get(Calendar.HOUR_OF_DAY);
        int minuteA = calA.get(Calendar.MINUTE);

        Calendar calB = Calendar.getInstance();
        calB.setTime(timeB);

        int hourB = calB.get(Calendar.HOUR_OF_DAY);
        int minuteB = calB.get(Calendar.MINUTE);

        if (hourA > hourB) {
            return true;
        } else if (hourA == hourB) {
            return minuteA >= minuteB;
        }
        return false;
    }

    public static ArrayList<String> cloneStringArrayList(ArrayList<String> list) {
        ArrayList<String> clone = new ArrayList<>();
        for (String value : list) clone.add(new String(value));
        return clone;
    }

    public static String DeleteUnUsableCharactersFromPhoneNumber(String phoneNumber) {
        String result = phoneNumber.replace("+", "");
        result = result.replace(" ", "");
        result = result.replace("-", "");
        result = result.replace("(", "");
        result = result.replace(")", "");

        return result;
    }

    public static int GetPhoneNumberRegionType(String phoneNumber, ArrayList<String> homeCountryCodes, ArrayList<String> homeRegionCodes, int minDigits, int maxDigits) {
        if (phoneNumber.length() < minDigits) {
            return DataRegionTypes.NONE;
        }
        if (phoneNumber.startsWith("*")) {
            return DataRegionTypes.NONE;
        }
        // Если есть хоть одна буква в номере, то этот номер игнорируем
        if (phoneNumber.matches(".*[a-zA-Z]+.*")) {
            return DataRegionTypes.NONE;
        }
        // Если кол-во цифр в номере телефона меньше либо равно, то этот номер телефона будем считать, как внутри региона.
        if (phoneNumber.length() >= minDigits && phoneNumber.length() <= maxDigits) {
            return DataRegionTypes.InRegion;
        } else if (!isPhoneNumberAtHomeCountry(phoneNumber, homeCountryCodes)) { // Если номер телефона не определен, как внутри страны, то будем считать его международным
            return DataRegionTypes.InterCountries;
        } else if (!isPhoneNumberAtHomeRegion(phoneNumber, homeRegionCodes)) {// Если номер телефона не определен, как внутри региона, то будем считать его мажрегиональным
            return DataRegionTypes.InterRegion;
        } else {
            return DataRegionTypes.InRegion;
        }
    }

    public static boolean isPhoneNumberAtHomeCountry(String phoneNumber, ArrayList<String> homeCountryCodes) {
        if (phoneNumber.startsWith("8") && !phoneNumber.startsWith("810")) {
            return true;
        }
        for (String countryCode : homeCountryCodes) {
            if (TextUtils.isEmpty(countryCode)) {
                continue;
            }
            try {
                String testPhoneNumber = Utils.DeleteUnUsableCharactersFromPhoneNumber(phoneNumber);

                boolean result = Utils.isPhoneNumberStartWith(testPhoneNumber, countryCode);

                if (result) {
                    return true;
                }
            } catch (Exception ex) {
                Log.e("PhonesSorter", ex.getMessage());
            }
        }
        return false;
    }

    /**
     * @param phoneNumber     Номер телефона
     * @param homeRegionCodes префиксы кодов телеыфонов домашнего региона
     * @return Входит ли номер телефона в домашний регион
     */
    public static boolean isPhoneNumberAtHomeRegion(String phoneNumber, ArrayList<String> homeRegionCodes) {
        if (homeRegionCodes == null || homeRegionCodes.isEmpty()) {
            return true;
        }
        for (String regionCode : homeRegionCodes) {
            if (TextUtils.isEmpty(regionCode)) {
                continue;
            }
            try {
                String testPhoneNumber = Utils.DeleteUnUsableCharactersFromPhoneNumber(phoneNumber);

                boolean result = Utils.isPhoneNumberStartWith(testPhoneNumber, regionCode);

                if (result) {
                    return true;
                }
            } catch (Exception ex) {
                Log.e("PhonesSorter", ex.getMessage());
            }
        }
        return false;
    }

    public static void showNotification(Context context, Intent intent, int notifyId,
                                        String statusBarText, String notifyTitle, String notifyText, int iconResId) {

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Resources res = context.getResources();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentIntent(contentIntent)
                .setSmallIcon(iconResId)
                .setTicker(statusBarText)
                .setAutoCancel(true)
                .setLargeIcon(BitmapFactory.decodeResource(res, iconResId))
                .setDefaults(Notification.DEFAULT_ALL) // requires VIBRATE permission
                .setContentTitle(notifyTitle)
                .setContentText(notifyText)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(notifyText));

        notificationManager.notify(notifyId, builder.build());
    }

    public static String arrayListToString(ArrayList<String> data) {
        StringBuilder result = new StringBuilder();
        for (String item : data) {
            if (result.length() == 0) {
                result.append(item);
            } else {
                result.append(';');
                result.append(item);
            }
        }
        return result.toString();
    }

    public static boolean getIsCalendarDateAEqualsDateB(Date dateA, Date dateB) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateA);
        int yearA = calendar.get(Calendar.YEAR);
        int monthA = calendar.get(Calendar.MONTH);
        int dayA = calendar.get(Calendar.DAY_OF_MONTH);

        calendar.setTime(dateB);
        int yearB = calendar.get(Calendar.YEAR);
        int monthB = calendar.get(Calendar.MONTH);
        int dayB = calendar.get(Calendar.DAY_OF_MONTH);

        return yearA == yearB && monthA == monthB && dayA == dayB;
    }

    public static String getLocalizedString(LocalizedStrings strings) {
        return strings.getStringByLanguage(Application.getInstance().getSelectedLanguageCode());
    }


    public static String getDuration(Context context, int seconds) {
        DecimalFormat formater = new DecimalFormat("00");
        double minutes = Math.floor(seconds / 60);
        double secondsRemain = seconds - (minutes * 60);
        double hours = Math.floor(minutes / 60);
        if (hours == 0) {
            return String.format("%1$s:%2$s", formater.format(minutes), formater.format(secondsRemain));
        } else {
            return String.format("%1$s:%2$s:%3$s", formater.format(hours), formater.format(minutes), formater.format(secondsRemain));
        }


//        if (minutes == 0 && seconds == 0) {
//            return "";
//        } else if (minutes == 0) {
//            return String.format("%1$s %2$s", formater.format(secondsRemain), context.getString(R.string.seconds));
//        } else if (secondsRemain == 0) {
//            return String.format("%1$s %2$s", formater.format(minutes), context.getString(R.string.minutes));
//        } else {
//            return String.format("%1$s %2$s %3$s %4$s", formater.format(minutes), context.getString(R.string.minutes), formater.format(secondsRemain), context.getString(R.string.seconds));
//        }

    }

    public static String getFormattedDate_DayMonth(Date date) {
        SimpleDateFormat formater = new SimpleDateFormat("dd MMM");
        return formater.format(date);
    }

    public static String getFormattedDate_DayMonthYear(Date date){
        SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy");
        return formater.format(date);
    }

    public static Bitmap getPhotoUri(Context context, String phoneNumber) {
        Uri uri = Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        String[] PHOTO_ID_PROJECTION = new String[]{
                ContactsContract.Contacts.PHOTO_ID
        };

        Cursor cursor = context.getContentResolver().query(uri, PHOTO_ID_PROJECTION, null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC");
        int thumbnailId = 0;
        try {
            if (cursor.moveToFirst()) {
                thumbnailId = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_ID));
            }
        } finally {
            cursor.close();
        }

        String[] PHOTO_BITMAP_PROJECTION = new String[]{
                ContactsContract.CommonDataKinds.Photo.PHOTO
        };

        uri = ContentUris.withAppendedId(ContactsContract.Data.CONTENT_URI, thumbnailId);
        cursor = context.getContentResolver().query(uri, PHOTO_BITMAP_PROJECTION, null, null, null);

        try {
            Bitmap thumbnail = null;
            if (cursor.moveToFirst()) {
                final byte[] thumbnailBytes = cursor.getBlob(0);
                if (thumbnailBytes != null) {
                    thumbnail = BitmapFactory.decodeByteArray(thumbnailBytes, 0, thumbnailBytes.length);
                }
            }
            return thumbnail;
        } finally {
            cursor.close();
        }
    }

    public static Uri getPhotoUri(Context context, long contactId) {
        ContentResolver contentResolver = context.getContentResolver();

        try {
            Cursor cursor = contentResolver
                    .query(ContactsContract.Data.CONTENT_URI,
                            null,
                            ContactsContract.Data.CONTACT_ID
                                    + "="
                                    + contactId
                                    + " AND "

                                    + ContactsContract.Data.MIMETYPE
                                    + "='"
                                    + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
                                    + "'", null, null);

            if (cursor != null) {
                if (!cursor.moveToFirst()) {
                    cursor.close();
                    return null; // no photo
                }
                cursor.close();
            } else {
                return null; // error in cursor process
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        Uri person = ContentUris.withAppendedId(
                ContactsContract.Contacts.CONTENT_URI, contactId);
        return Uri.withAppendedPath(person,
                ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
    }


    public static ShapeDrawable getColoredCircle(int color){
        ShapeDrawable shape = new ShapeDrawable(new OvalShape());
        shape.getPaint().setColor(color);
        return shape;
    }


    public static Service findServiceById(ArrayList<Service> services, String id) {
        for (Service service : services) {
            if (service.getId().equalsIgnoreCase(id)) {
                return service;
            }
        }
        return null;
    }

    public static int convertDipToPixels(Context context, float dip) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dip, context.getResources().getDisplayMetrics());
    }
}

