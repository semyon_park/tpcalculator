package com.ct.tpcalculator.helpers;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.ct.tpcalculator.calculator.data.MainData;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Locale;

/***
 * Реализация файлового кеша по ключу. Про сохранение указываем массив байт,
 * ключ и время окончания действия. Обратно извлекаем по ключу массив байт или
 * null, если элемент не найден или просрочен.
 */
public class FileCache {

	public final static String REGION_FILE_KEY = "region.key";
	public final static String COUNTRIES_REGIONS_FILE_KEY = "countries_regions.key";

	// hide from other components to avoid direct using - should use getIconKet
	// instead
	private final static String ICON_KEY = "icon.key.%d";

	private final static String OFFER_FILE_KEY = "offer.%s";

	private final static String TAG = FileCache.class.getName();

	// храним сслылку на экземпляр. Паттерн sigleton.
	private volatile static FileCache instance;
	// ссылка на кеш приложения. Используем систему, чтобы кеш чистился при
	// удалении.
	private File cacheDir;
	private volatile static Context context;

	public final static long ONE_HOUR = 1000L * 60L * 60L;
	public final static long ONE_DAY = ONE_HOUR * 24L;
	// для Шахерезады это была вечность
	public final static long NEVER_EXPIRES = System.currentTimeMillis()
			+ ONE_DAY * 1001L;

	/**
	 * Статические метод для инициализации файлового кеша. Инициализир
	 *
	 * @param context
	 *            - контекст приложения
	 */
	public static void initCache(Context context) {
		instance = new FileCache(context);
	}

	/***
	 * Конструктор. Требует контекст приложения для получения ссылки на файловый
	 * кеш приложения.
	 *
	 * @param context
	 *            - контекст приложения
	 */
	private FileCache(Context context) {
		super();
		FileCache.context = context;
		cacheDir = context.getCacheDir();
		clearCache();
	}

	/***
	 * Очистка кеша от старых элементов
	 */
	private void clearCache() {
		// текущее время в миллисекундах
		long current = Calendar.getInstance(Locale.US).getTimeInMillis();
		// сравниваем со временем модификации (используем как время просрочки)
		// файлов.
		for (File f : cacheDir.listFiles())
			if (f.lastModified() < current)
				f.delete();

	}

	/***
	 * Выдаем массив байт по указанному ключу. Если ключ не найден возвращаем
	 * null
	 *
	 * @param key
	 *            ключ элемента
	 * @return массив байт или null
	 */
	private byte[] getFileData(String key) {
		File f = new File(cacheDir, key);
		if (f.exists()) {
			if (f.lastModified() < Calendar.getInstance(Locale.US).getTimeInMillis()) {
				f.delete();
				return null;
			} else {

				try {
					FileInputStream fis = new FileInputStream(f);
					byte[] data = readFull(fis);
					fis.close();
					fis = null;
					return data;

				} catch (Exception e) {
					Log.e(TAG, Log.getStackTraceString(e));
					return null;
				}
			}
		} else {
			return null;
		}
	}

	public static MainData getMainData(){
		MainData mainData = null;
		try {
			byte[] data = FileCache.getContent(FileCache.REGION_FILE_KEY);
			if (data == null) {
				return null;
			}
			InputStream is = new ByteArrayInputStream(data);
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
			xpp.setInput(is, "utf-8");
			mainData = new MainData(xpp);
			is.close();
		} catch (IOException e) {
			L.e("CheckLimits exception: " + e.getMessage());
		} catch (XmlPullParserException ex) {
			L.e("CheckLimits exception: " + ex.getMessage());
		} catch (Exception e) {
			L.e(e.getMessage());
		}
		return mainData;
	}

	public static MainData getCountriesAndRegions(){
		MainData mainData = null;
		try {
			byte[] data = FileCache.getContent(FileCache.COUNTRIES_REGIONS_FILE_KEY);
			if (data == null) {
				return null;
			}
			InputStream is = new ByteArrayInputStream(data);
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
			xpp.setInput(is, "utf-8");
			mainData = new MainData(xpp);
			is.close();
		} catch (IOException e) {
			L.e("CheckLimits exception: " + e.getMessage());
		} catch (XmlPullParserException ex) {
			L.e("CheckLimits exception: " + ex.getMessage());
		} catch (Exception e) {
			L.e(e.getMessage());
		}
		return mainData;
	}

	private boolean isFileExists(String key) {
		File f = new File(cacheDir, key);
		if (f.exists()) {
			if (f.lastModified() < Calendar.getInstance(Locale.US).getTimeInMillis()) {
				f.delete();
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	/***
	 * Выдает дескриптор файла по указанному ключу.Если ключ не найден или файл
	 * устарел возвращаем null
	 *
	 * @param key
	 *            ключ элемента
	 * @return дескриптор файла или null
	 */
	private File getFileHandle(String key) {
		File f = new File(cacheDir, key);
		if (f.exists()) {
			return f;
		}
		return null;
	}

	/***
	 * Сохраняем массив байт под указанным ключом. Учитываем время устаревания -
	 * сохраняем в last modified
	 *
	 * @param key
	 *            - ключ
	 * @param data
	 *            - массив байт
	 * @param expireTime
	 *            - время устаревания данных
	 */
	private File storeFileData(String key, byte[] data, long expireTime) {
		File f = new File(cacheDir, key);
		try {
			FileOutputStream fos = new FileOutputStream(f);
			fos.write(data);
			fos.close();
			f.setLastModified(expireTime);
		} catch (Exception e) {
			Log.e(TAG, Log.getStackTraceString(e));
		}
		return f;
	}

	/***
	 * Статический метод (удобно использовать) для получения контента по ключу
	 *
	 * @param key
	 *            - ключ элемента
	 * @return массив байт или null, если элемент не найден или устарел
	 */
	public static byte[] getContent(String key) {
//		if (key.equalsIgnoreCase(REGION_FILE_KEY)){
//			AssetManager assetManager = context.getAssets();
//			try {
//				InputStream is = assetManager.open("XML_data_sample_uz.xml");
//				byte[] data = readFull(is);
//				is.close();
//				return data;
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
		return instance.getFileData(key);
	}

	/***
	 * Статический метод (для удобства) для получение дескриптора вайла по ключу
	 *
	 * @param key
	 * @return дескриптор файла или null
	 */
	public static File getFile(String key) {
		return instance.getFileHandle(key);
	}

	/***
	 * Статический метод (удобно использовать) для сохранения контента
	 *
	 * @param key
	 *            - ключ элемента
	 * @param data
	 *            - массив байт контента элемента
	 * @param expireTime
	 *            - время устаревания в миллисекундах
	 */
	public static File setContent(String key, byte[] data, long expireTime) {
		return instance.storeFileData(key, data, expireTime);
	}

	public static boolean removeContent(String key) {
		File f = instance.getFileHandle(key);
		if (f != null) {
			try {
				return f.delete();
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}

	/***
	 * Статический метод (не зависит от экземпляра, вспомогательный) для
	 * загрузки всех данных потока в массив байт
	 *
	 * @param is
	 *            - поток с данными
	 * @return массив байт с контентом потока
	 */
	static public byte[] readFull(InputStream is) {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		byte[] buf = new byte[1020];

		int numRead = 0;

		try {
			while ((numRead = is.read(buf)) != -1) {
				b.write(buf, 0, numRead);
			}
		} catch (Exception e) {
			Log.e(TAG, Log.getStackTraceString(e));
		}
		return b.toByteArray();
	}


	public static String getIconKey(int id) {
		return String.format(Locale.getDefault(), ICON_KEY, id);
	}

	public static String getImageKey(String guid) {
		return "image." + guid;
	}

	public static String getImageKey(String guid, int width, int height) {
		return String.format("image.%s.%dx%d", guid, width, height);
	}

	public static String getNewsImageKey(String guid) {
		return "newsimage." + guid;
	}

	public static String getLicenseFilename(String lang) {
		return String.format(OFFER_FILE_KEY, lang);
	}

	/***
	 * Хранение постоянных файлов
	 * 
	 * @param name
	 * @param data
	 */
	public static void storePermanentFile(String name, byte[] data) {
		try {
			FileOutputStream fos = context.openFileOutput(name,
					Context.MODE_PRIVATE);
			fos.write(data);
			fos.close();
		} catch (Exception e) {
			Log.e(TAG, Log.getStackTraceString(e));
		}
	}

	/***
	 * Stores license file to $app_data/files directory
	 * 
	 * @param lang
	 * @param license
	 */
	public static void storeLicense(String lang, String license) {
		try {
			FileOutputStream fos = context.openFileOutput(
					getLicenseFilename(lang), Context.MODE_PRIVATE);
			fos.write(license.getBytes());
			fos.close();
		} catch (Exception e) {
			Log.e(TAG, Log.getStackTraceString(e));
		}
	}

	public static byte[] getPermanentFile(String name) {
		try {
			FileInputStream fis = context.openFileInput(name);
			byte[] data = readFull(fis);
			fis.close();
			return data;
		} catch (Exception e) {
			L.e("missing file: " + name);
		}

		// if not found, trying old cache
		return getContent(name);
	}

	public static boolean isPermanentFileExists(String name) {
		FileInputStream fis = null;
		try {
			fis = context.openFileInput(name);
			fis.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean deletePermanentFile(String name) {
		try {

			return context.deleteFile(name);
		} catch (Exception e) {
			return false;
		}
	}

	/***
	 * Load license file from $app_data/files location.
	 * 
	 * @param lang
	 * @return
	 */
	public static String getLicense(String lang) {
		try {
			FileInputStream fis = context
					.openFileInput(getLicenseFilename(lang));
			byte[] data = readFull(fis);
			fis.close();
			return new String(data);
		} catch (Exception e) {
//			Log.e(TAG, Log.getStackTraceString(e));
		}

		// try to use old store method, if current is fail
		try {
			String license = new String(getContent(getLicenseFilename(lang)));
			if (!TextUtils.isEmpty(license)) {
				// copy to current location if success
				storeLicense(lang, license);
				return license;
			}
		} catch (Exception e) {
		}

		return null;

	}
}
