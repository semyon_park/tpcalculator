package com.ct.tpcalculator.helpers;

import android.text.TextUtils;

import com.ct.tpcalculator.application.Application;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.Dictionary;
import java.util.Enumeration;

/**
 * Created by marat on 3/3/2016.
 */
public class GoogleAnalyticsHelper {
    public static final int CUSTOM_VARIABLE_COUNTRY = 1;
    public static final int CUSTOM_VARIABLE_REGION = 2;
    public static final int CUSTOM_VARIABLE_OPERATOR = 3;
    public static final int CUSTOM_VARIABLE_LANG = 4;
    public static final int CUSTOM_VARIABLE_TP = 5;
    public static final int CUSTOM_VARIABLE_SERVICES = 6;
    public static final int CUSTOM_VARIABLE_COMMAND = 7;

    public static void sendLog(String pageName, String action) {
        sendLog(pageName, action, null);
    }

    public static void sendLog(String pageName, String action, Dictionary<Integer, String> customVariables) {
        Tracker tracker = Application.getInstance().getDefaultTracker();
        if (TextUtils.isEmpty(action)) {
            tracker.setScreenName(pageName);
            tracker.send(new HitBuilders.ScreenViewBuilder().build());

        } else {
            if (customVariables == null) {
                tracker.send(new HitBuilders.EventBuilder()
                        .setCategory(pageName)
                        .setAction(action)
                        .build());
            } else {
                HitBuilders.EventBuilder eventBuilder = new HitBuilders.EventBuilder();
                eventBuilder.setCategory(pageName);
                eventBuilder.setAction(action);

                Enumeration<Integer> e = customVariables.keys();
                while (e.hasMoreElements()) {
                    Integer key = e.nextElement();
                    String value = customVariables.get(key);
                    eventBuilder.setCustomDimension(key.intValue(), value);
                }

                tracker.send(eventBuilder.build());
            }
        }
    }
}
