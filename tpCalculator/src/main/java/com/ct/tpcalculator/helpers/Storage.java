package com.ct.tpcalculator.helpers;

import com.ct.tpcalculator.calculator.data.MainData;
import com.ct.tpcalculator.calculator.data.MobileOperator;

/**
 * Created by USER on 22.06.2015.
 */
public class Storage {

    private static Storage instance;

    private  MainData countries_regions_langs;

    private MainData mobileOperators;

    private MobileOperator mobileOperator;

    public static Storage getInstance(){
        if (instance == null){
            instance = new Storage();
        }
        return instance;
    }

    public MainData getCountries_regions_langs(){
        return countries_regions_langs;
    }

    public void setCountriesRegionsLanguages(MainData data){
        countries_regions_langs = data;
    }

    public MainData getMobileOperators(){
        return mobileOperators;
    }

    public void setMobileOperators(MainData data){
        mobileOperators = data;
    }

    public MobileOperator getMobileOperator() {
        return mobileOperator;
    }

    public void setMobileOperator(MobileOperator mobileOperator) {
        this.mobileOperator = mobileOperator;
    }
}
