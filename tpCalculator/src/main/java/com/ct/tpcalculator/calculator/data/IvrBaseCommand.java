package com.ct.tpcalculator.calculator.data;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by USER on 17.06.2015.
 */
public class IvrBaseCommand extends BaseCommand implements Serializable {

    private String phoneNumber;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public IvrBaseCommand(XmlPullParser parser) throws XmlPullParserException, IOException {
        super();

        for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
            final String attrName = parser.getAttributeName(i);
            final String attrValue = parser.getAttributeValue(i);
            if (XML_TAGS.XML_ATTRIBUTE_PHONE_NUMBER.equalsIgnoreCase(attrName)) {
                phoneNumber = attrValue;
            } else if (XML_TAGS.XML_ATTRIBUTE_ID.equalsIgnoreCase(attrName)) {
                id = attrValue;
            }
        }
        parser.next();

    }
}
