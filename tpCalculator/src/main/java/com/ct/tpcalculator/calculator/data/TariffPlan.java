package com.ct.tpcalculator.calculator.data;

import android.support.v4.util.Pair;
import com.ct.tpcalculator.calculator.data.conditions.CallsCondition;
import com.ct.tpcalculator.calculator.data.conditions.InternetCondition;
import com.ct.tpcalculator.calculator.data.conditions.MmsCondition;
import com.ct.tpcalculator.calculator.data.conditions.SmsCondition;
import com.ct.tpcalculator.helpers.Utils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by USER on 16.06.2015.
 */
public class TariffPlan extends Package implements Serializable {

    private ArrayList<String> availableServices;

    public ArrayList<String> getAvailableServices() {
        return availableServices;
    }

    private ArrayList<String> mustHaveServices;

    public ArrayList<String> getMustHaveServices(){
        return mustHaveServices;
    }

    private boolean isArchive;

    public boolean getIsArchive(){
        return isArchive;
    }

    public TariffPlan(XmlPullParser parser) throws XmlPullParserException, IOException {
        priceType = new SubscriptionPriceType();
        titles = new LocalizedStrings();
        descriptions = new LocalizedStrings();

        final String tag = parser.getName();
        if (XML_TAGS.XML_TAG_TARIFF_PLAN.equalsIgnoreCase(tag)) {
            for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
                final String attrName = parser.getAttributeName(i);
                final String attrValue = parser.getAttributeValue(i);
                if (XML_TAGS.XML_ATTRIBUTE_ID.equalsIgnoreCase(attrName)) {
                    id = attrValue;
                } else if (attrName.startsWith(XML_TAGS.XML_ATTRIBUTE_TITLE) && attrName.length() > XML_TAGS.XML_ATTRIBUTE_TITLE.length()) {
                    getTitles().put(attrName.substring(XML_TAGS.XML_ATTRIBUTE_TITLE.length() + 1), attrValue);
                } else if (attrName.startsWith(XML_TAGS.XML_ATTRIBUTE_DESCRIPTION) && attrName.length() > XML_TAGS.XML_ATTRIBUTE_DESCRIPTION.length()) {
                    getDescriptions().put(attrName.substring(XML_TAGS.XML_ATTRIBUTE_DESCRIPTION.length() + 1), attrValue);
                } else if (XML_TAGS.XML_ATTRIBUTE_PRICE_TYPE.equalsIgnoreCase(attrName)) {
                    getPriceType().setPriceType(attrValue);
                } else if (XML_TAGS.XML_ATTRIBUTE_PRICE.equalsIgnoreCase(attrName)) {
                    price = Double.parseDouble(attrValue);
                } else if (XML_TAGS.XML_ATTRIBUTE_CHANGE_PRICE.equalsIgnoreCase(attrName)) {
                    changePrice = Double.parseDouble(attrValue);
                } else if (XML_TAGS.XML_ATTRIBUTE_PERIOD.equalsIgnoreCase(attrName)) {
                    getPriceType().setPeriodCount(Integer.parseInt(attrValue));
                } else if (XML_TAGS.XML_ATTRIBUTE_AVAILABLE_SERVICES.equalsIgnoreCase(attrName)) {
                    availableServices = new ArrayList<>(Arrays.asList(attrValue.split(";")));
                }else if (XML_TAGS.XML_ATTRIBUTE_ENABLED_SERVICES.equalsIgnoreCase(attrName)){
                    mustHaveServices = new ArrayList<>(Arrays.asList(attrValue.split(";")));
                }else if (XML_TAGS.XML_ATTRIBUTE_IS_ARCHIVE.equalsIgnoreCase(attrName)){
                    isArchive = Boolean.parseBoolean(attrValue);
                }
            }
        }
        parser.next();

        int eventType = parser.getEventType();
        do {
            if (eventType == XmlPullParser.START_TAG) {
                final String name = parser.getName();
                if (XML_TAGS.XML_TAG_CALLS.equalsIgnoreCase(name)) {
                    callsCondition = new CallsCondition(parser);
                } else if (XML_TAGS.XML_TAG_SMS.equalsIgnoreCase(name)) {
                    smsCondition = new SmsCondition(parser);
                } else if (XML_TAGS.XML_TAG_MMS.equalsIgnoreCase(name)) {
                    mmsCondition = new MmsCondition(parser);
                } else if (XML_TAGS.XML_TAG_INTERNET.equalsIgnoreCase(name)) {
                    internetCondition = new InternetCondition(parser);
                } else if (XML_TAGS.XML_TAG_COMMANDS.equalsIgnoreCase(name)) {
                    commands = new Commands(parser);
                }
            }
            eventType = parser.next();
        }
        while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_TARIFF_PLAN.equals(parser.getName()))
                && (eventType != XmlPullParser.END_DOCUMENT));

    }

    public Pair<Date, Date> getIntervalPeriodByDate(Date date){
        Pair<Date, Date> period = null;
        if (getPriceType().getPriceType() == SubscriptionPriceType.MONTH) {
            period = Utils.getMonthOfTheDate(date);
        } else if (getPriceType().getPriceType() == SubscriptionPriceType.WEEK) {
            period = Utils.getWeekOfTheDate(date);
        } else if (getPriceType().getPriceType() == SubscriptionPriceType.DAY) {
            period = Utils.getDayOfTheDate(date);
        }

        return period;
    }
}