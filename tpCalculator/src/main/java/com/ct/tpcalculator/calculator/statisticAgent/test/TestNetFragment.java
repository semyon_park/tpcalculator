package com.ct.tpcalculator.calculator.statisticAgent.test;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticInternetTraffic;
import com.ct.tpcalculator.calculator.statisticAgent.Consts;
import com.ct.tpcalculator.calculator.statisticAgent.base.SQLHelper;
import com.ct.tpcalculator.calculator.statisticAgent.netstat.NetStatCore;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class TestNetFragment extends Fragment {

    public final static String TAG = TestNetFragment.class.getName();

    public TestNetFragment() {
        // Required empty public constructor
    }

    EditText edQuery;
    Button btnClear;
    Button btnCopy;
    Button btnExecQuery;
    ListView lvData;
    MyAdap adap;
    ArrayList<String> list = new ArrayList<String>();

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.net_test_btn_copy_base:
                    NetStatCore.baseCopy(v.getContext());
                    break;
                case R.id.net_test_btn_clear_base:
                    SQLiteDatabase db = SQLHelper.getInstance(v.getContext()).getWritableDatabase();
                    db.execSQL("DROP TABLE IF EXISTS " + StatisticInternetTraffic.KEY_CLASS_TYPE);

                    String create_script = "CREATE TABLE " + StatisticInternetTraffic.KEY_CLASS_TYPE + " ( "
//                                    + " _id integer PRIMARY KEY AUTOINCREMENT, "
                            + StatisticInternetTraffic.KEY_DATE + " INTEGER PRIMARY KEY, "
                            + StatisticInternetTraffic.KEY_TYPE + " INTEGER, "
                            + StatisticInternetTraffic.KEY_DOWNLOAD + " INTEGER, "
                            + StatisticInternetTraffic.KEY_UPLOAD + " INTEGER " + " ) ";
                    db.execSQL(create_script);

                    break;
                case R.id.net_test_btn_exec_query:
                    String text = edQuery.getText().toString();
                    if (text.length() == 0) {
                        NetStatCore.base(v.getContext());
                    } else {
                        exec(v.getContext(), text);
                    }
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_test_net, container, false);

        btnCopy = (Button) v.findViewById(R.id.net_test_btn_copy_base);
        btnClear = (Button) v.findViewById(R.id.net_test_btn_clear_base);
        edQuery = (EditText) v.findViewById(R.id.net_test_ed_query);
        btnExecQuery = (Button) v.findViewById(R.id.net_test_btn_exec_query);

        btnCopy.setOnClickListener(clickListener);
        btnClear.setOnClickListener(clickListener);
        btnExecQuery.setOnClickListener(clickListener);

//        lvData = (ListView) v.findViewById(R.id.net_test_btn_base_items);
//        adap = new MyAdap(v.getContext(), R.layout.net_test_list_item, list);
//        lvData.setAdapter(adap);
        return v;
    }

    private void exec(Context context, String query) {
        //------------------------------------------------------------------------------------------
        SimpleDateFormat newFormat = new SimpleDateFormat("dd.MM HH:mm:ss");
        String timeS = newFormat.format(System.currentTimeMillis());

        e("BaseInet: Query....................................................................");
        e("BaseInet: Query LogData: " + timeS);
        SQLiteDatabase db = SQLHelper.getInstance(context).getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        StatisticInternetTraffic traff;
        long d = 0;
        long dm = 0;
        long dw = 0;
        long u = 0;
        long um = 0;
        long uw = 0;

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {

                traff = new StatisticInternetTraffic(cursor.getInt(cursor.getColumnIndex(StatisticInternetTraffic.KEY_TYPE)));
                traff.setDate(new Date(cursor.getLong(cursor.getColumnIndex(StatisticInternetTraffic.KEY_DATE))));
                traff.setReceivedBytes(cursor.getLong(cursor.getColumnIndex(StatisticInternetTraffic.KEY_DOWNLOAD)));
                traff.setTransmittedBytes(cursor.getLong(cursor.getColumnIndex(StatisticInternetTraffic.KEY_UPLOAD)));

                e("BaseInet: Query #" + cursor.getPosition() + " " + traff.toLog());

                if (traff.getType() == StatisticInternetTraffic.TYPE_MOBILE) {
                    dm += traff.getReceivedBytes();
                    um += traff.getTransmittedBytes();
                } else {
                    dw += traff.getReceivedBytes();
                    uw += traff.getTransmittedBytes();
                }
            }
        }

        if (cursor != null) cursor.close();
//        if (db != null && db.isOpen()) db.close();
        e("BaseInet: Query All: download: " + d + "; upload: " + u + "; total: " + (d+u));
        e("BaseInet: Query Mobile: download: " + dm + "; upload: " + um + "; total: " + (dm+um));
        e("BaseInet: Query Wifi: download: " + dw + "; upload: " + uw + "; total: " + (dw+uw));
        e("BaseInet: Query....................................................................");
//        e("BaseInet Query: list.size: " + list.size());
    }

    private class MyAdap extends ArrayAdapter<String> {
        ArrayList<String> list;

        public MyAdap(Context context, int resource, ArrayList<String> data) {
            super(context, resource);
            list = data;
//            e("BaseInet Query: MyAdap.Constructor: list.size: " + list.size());
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.net_test_list_item, parent, false);
//            e("BaseInet Query: MyAdap.getView: " + list.get(position));
            TextView tx = (TextView) convertView.findViewById(R.id.net_test_text);
            tx.setText(list.get(position));

            return convertView;
        }
    }

    //------------------------------------------------------------------------------------------
    private static boolean debug = Consts.DEBUG && true;
    private static String className = "NetTestFragment.";

    private static void e(String message) {
        if (debug) {
            Log.e(Consts.APP_TAG, className + message);
        }
    }


}
