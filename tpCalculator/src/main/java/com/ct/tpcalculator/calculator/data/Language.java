package com.ct.tpcalculator.calculator.data;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;

public class Language implements Serializable {

	private String locale;
	private String name;

	public Language(String name, String locale) {
		this.name = name;
		this.locale = locale;
	}

	public Language(XmlPullParser parser) throws XmlPullParserException, IOException {

		int eventType = parser.getEventType();
		do {
			if (eventType == XmlPullParser.START_TAG) {
				final String tag = parser.getName();
				if (XML_TAGS.XML_TAG_LANGUAGE.equalsIgnoreCase(tag)) {
					for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
						final String attrName = parser.getAttributeName(i);
						final String attrValue = parser.getAttributeValue(i);
						if ("code".equalsIgnoreCase(attrName)) {
							locale = attrValue;
						} else if ("title".equalsIgnoreCase(attrName)) {
							name = attrValue;
						}
					}
				}
			}
			eventType = parser.next();
		} while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_LANGUAGE.equals(parser.getName()))
						&& (eventType != XmlPullParser.END_DOCUMENT));
	}

	public String getName() {
		return name;
	}

	public String getLocale() {
		return locale;
	}

}
