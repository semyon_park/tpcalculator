package com.ct.tpcalculator.calculator.data.calculationData;

import com.ct.tpcalculator.calculator.data.statisticData.StatisticInternetTraffic;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticInternetTraffics;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by USER on 10.07.2015.
 */
public class StatisticInternetTrafficsForCalculation implements Serializable {

    public ArrayList<StatisticInternetTrafficForCalculation> getMobileInternetTraffic() {
        return mobileInternetTraffic;
    }

    public ArrayList<StatisticInternetTrafficForCalculation> getWifiInternetTraffic() {
        return wifiInternetTraffic;
    }

    private ArrayList<StatisticInternetTrafficForCalculation> mobileInternetTraffic;
    private ArrayList<StatisticInternetTrafficForCalculation> wifiInternetTraffic;

    public StatisticInternetTrafficsForCalculation(StatisticInternetTraffics internetTraffics) {
        mobileInternetTraffic = new ArrayList<>();
        if (internetTraffics.getMobileInternetTraffic() != null) {
            for (StatisticInternetTraffic internetTraffic : internetTraffics.getMobileInternetTraffic()) {
                mobileInternetTraffic.add(new StatisticInternetTrafficForCalculation(internetTraffic));
            }
        }

        wifiInternetTraffic = new ArrayList<>();
        if (internetTraffics.getWifiInternetTraffic() != null) {
            for (StatisticInternetTraffic internetTraffic : internetTraffics.getWifiInternetTraffic()) {
                wifiInternetTraffic.add(new StatisticInternetTrafficForCalculation(internetTraffic));
            }
        }
    }

    private StatisticInternetTrafficsForCalculation() {
        super();
        mobileInternetTraffic = new ArrayList<>();
        wifiInternetTraffic = new ArrayList<>();
    }

    public StatisticInternetTrafficsForCalculation clone() throws CloneNotSupportedException {
        StatisticInternetTrafficsForCalculation result = new StatisticInternetTrafficsForCalculation();
        for (StatisticInternetTrafficForCalculation internetTraffic : this.getMobileInternetTraffic()) {
            result.getMobileInternetTraffic().add(internetTraffic.clone());
        }
        for (StatisticInternetTrafficForCalculation internetTraffic : this.getWifiInternetTraffic()) {
            result.getWifiInternetTraffic().add(internetTraffic.clone());
        }
        return result;
    }
}
