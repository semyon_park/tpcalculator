package com.ct.tpcalculator.calculator.data.statisticData;

import java.io.Serializable;

/**
 * Created by USER on 24.06.2015.
 */
public class StatisticLogs implements Serializable {

    private final StatisticCalls calls;
    private final StatisticMessages smsMessages;
    private final StatisticMessages mmsMessages;
    private final StatisticInternetTraffics internetTraffics;

    public StatisticCalls getCalls() {
        return calls;
    }

    public StatisticMessages getSmsMessages() {
        return smsMessages;
    }

    public StatisticInternetTraffics getInternetTraffics() {
        return internetTraffics;
    }

    public StatisticMessages getMmsMessages() {
        return mmsMessages;
    }

    public StatisticLogs(StatisticCalls calls, StatisticMessages smsMessages, StatisticMessages mmsMessages, StatisticInternetTraffics internetTraffics) {
        this.calls = calls;
        this.smsMessages = smsMessages;
        this.internetTraffics = internetTraffics;
        this.mmsMessages = mmsMessages;
    }

}
