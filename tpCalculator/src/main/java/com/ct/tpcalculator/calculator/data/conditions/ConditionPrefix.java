package com.ct.tpcalculator.calculator.data.conditions;

import com.ct.tpcalculator.calculator.data.XML_TAGS;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticCallForCalculation;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticMessageForCalculation;
import com.ct.tpcalculator.helpers.Utils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by USER on 16.06.2015.
 */
public class ConditionPrefix extends Condition implements Serializable {

    private ArrayList<String> numberPrefix;

    private Condition childCondition; //  Дочернее доп. учсловие может быть conditionTime или conditionMinute или conditionDay

    public Condition getChildCondition() {
        return childCondition;
    }

    public ArrayList<String> getNumberPrefix() {
        return numberPrefix;
    }

    public ConditionPrefix(XmlPullParser parser) throws XmlPullParserException, IOException {
        super(parser);
        numberPrefix = new ArrayList<>();
        final String tag = parser.getName();
        if (XML_TAGS.XML_TAG_CONDITION_PREFIX.equalsIgnoreCase(tag)) {
            for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
                final String attrName = parser.getAttributeName(i);
                final String attrValue = parser.getAttributeValue(i);
                if (XML_TAGS.XML_ATTRIBUTE_NUMBER_PREFIX.equalsIgnoreCase(attrName)) {
                    String[] values = attrValue.split(";");
                    numberPrefix = new ArrayList<>(Arrays.asList(values));
                }
            }
        }
//        parser.next();
//
        int eventType = parser.getEventType();
        do {
            if (eventType == XmlPullParser.START_TAG) {
                final String name = parser.getName();
                if (XML_TAGS.XML_TAG_CONDITION_MINUTE.equalsIgnoreCase(name)) {
                    childCondition = new ConditionMinute(parser);
                } else if (XML_TAGS.XML_TAG_CONDITION_TIME.equalsIgnoreCase(name)) {
                    childCondition = new ConditionTime(parser);
                } else if (XML_TAGS.XML_TAG_CONDITION_DAY.equalsIgnoreCase(name)) {
                    childCondition = new ConditionDay(parser);
                }
            }
            eventType = parser.next();
        }
        while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_CONDITION_PREFIX.equals(parser.getName()))
                && (eventType != XmlPullParser.END_DOCUMENT));
    }

    @Override
    public void calculatePrice(StatisticCallForCalculation call) {
        if (isPhoneNumberContainsPrefixes(call.getPhoneNumber())) {
            if (childCondition != null) {
                childCondition.calculatePrice(call);
                if (call.isCalculated())
                    return;
            }
            call.setPrice(call.getNotCalculatedMinutes() * getPrice());
            call.setCalculated(true);
        }
    }

    public void calculatePrice(StatisticMessageForCalculation message) {
        if (isPhoneNumberContainsPrefixes(message.getPhoneNumber())) {
            if (childCondition != null) {
                childCondition.calculatePrice(message);
                if (message.isCalculated())
                    return;
            }
            message.setPrice(getPrice());
            message.setCalculated(true);
        }
    }

    private boolean isPhoneNumberContainsPrefixes(String phoneNumber) {
        for (String prefix : numberPrefix) {
            if (Utils.isPhoneNumberStartWith(phoneNumber, prefix)) {
                return true;
            }
        }
        return false;
    }

}
