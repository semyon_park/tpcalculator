package com.ct.tpcalculator.calculator.statisticAgent.base;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ct.tpcalculator.calculator.data.statisticData.StatisticCall;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticInternetTraffic;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticMessage;
import com.ct.tpcalculator.calculator.statisticAgent.Consts;

/**
 * Created by Andrey on 08.07.2015.
 */
public class SQLHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "TpCalculator.db";


    public SQLHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    private static SQLHelper instance;

    public static SQLHelper getInstance(Context context) {
        if (instance == null) instance = new SQLHelper(context, DATABASE_NAME, null, DATABASE_VERSION);

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        e("-----------onCreate Database-------------");

        String create_script = "CREATE TABLE " + StatisticInternetTraffic.KEY_CLASS_TYPE + " ( "
                                    + " id INTEGER PRIMARY KEY AUTOINCREMENT, "
                                    + StatisticInternetTraffic.KEY_DATE + " INTEGER, "
                                    + StatisticInternetTraffic.KEY_TYPE + " INTEGER, "
                                    + StatisticInternetTraffic.KEY_DOWNLOAD + " INTEGER, "
                                    + StatisticInternetTraffic.KEY_UPLOAD + " INTEGER " + " ) ";

        db.execSQL(create_script);

        // TODO: Disable creating
        if (Consts.DEBUG) {
            create_script = "CREATE TABLE " + StatisticInternetTraffic.KEY_CLASS_TYPE + "_TEST" + " ( "
                    + " id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + StatisticInternetTraffic.KEY_DATE + " INTEGER, "
                    + StatisticInternetTraffic.KEY_TYPE + " INTEGER, "
                    + StatisticInternetTraffic.KEY_DOWNLOAD + " INTEGER, "
                    + StatisticInternetTraffic.KEY_UPLOAD + " INTEGER " + " ) ";
            db.execSQL(create_script);
        }

        create_script = "CREATE TABLE " + StatisticCall.KEY_CLASS_TYPE + " ( "
//                + " _id integer PRIMARY KEY AUTOINCREMENT, "
                + StatisticCall.KEY_DATE + " INTEGER PRIMARY KEY, "
                + StatisticCall.KEY_NAME + " TEXT, "
                + StatisticCall.KEY_NUMBER + " TEXT, "
                + StatisticCall.KEY_TYPE + " INTEGER, "
                + StatisticCall.KEY_DURATION + " INTEGER, "
                + StatisticCall.KEY_CONTACT_ID + " INTEGER"+ " ) ";

        db.execSQL(create_script);

        create_script = "CREATE TABLE " + StatisticMessage.KEY_CLASS_TYPE + " ( "
//                + " _id integer PRIMARY KEY AUTOINCREMENT, "
                + StatisticMessage.KEY_DATE + " INTEGER PRIMARY KEY, "
                + StatisticMessage.KEY_NAME + " TEXT, "
                + StatisticMessage.KEY_NUMBER + " TEXT, "
                + StatisticMessage.KEY_TYPE + " INTEGER, "
                + StatisticMessage.KEY_MSG_TYPE + " INTEGER " + " ) ";

        db.execSQL(create_script);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        e("----------------onUpgrade called----------------------");
        e("onUpgrade(): From " + oldVersion + " to " + newVersion + " version");
    }

    //------------------------------------------------------------------------------------------
    private static boolean debug = Consts.DEBUG && true;
    private static String className = "SQLHelper.";

    private static void e(String message) {
        if (debug) {
            Log.e(Consts.APP_TAG, className + message);
        }
    }
    //------------------------------------------------------------------------------------------

}
