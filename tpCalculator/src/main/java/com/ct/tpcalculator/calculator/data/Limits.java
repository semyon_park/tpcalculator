package com.ct.tpcalculator.calculator.data;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class Limits extends ArrayList<Limit> implements Serializable {

	public Limits(XmlPullParser parser) throws XmlPullParserException, IOException {

		int eventType = parser.getEventType();
		do {
			if (eventType == XmlPullParser.START_TAG) {
				final String name = parser.getName();
				if (XML_TAGS.XML_TAG_LIMIT.equalsIgnoreCase(name)) {
					add(new Limit(parser));
				}
			}
			eventType = parser.next();
		} while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_LIMITS.equals(parser.getName()))
						&& (eventType != XmlPullParser.END_DOCUMENT));
	}

}
