package com.ct.tpcalculator.calculator.data;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Semyon on 15.06.2015.
 * mainData - header xml data
 */
public class MainData implements Serializable {

    public final static String XML_TAG = "ctn";

    private ArrayList<Country> countries;
    private Languages languages;
    private Error error;
    private String mFileVersion;

    public ArrayList<Country> getCountries() {
        return countries;
    }

    public Languages getLanguages() {
        return languages;
    }

    public Error getError() {
        return error;
    }

    public String getFileVersion() {
        return mFileVersion;
    }

    public MainData(XmlPullParser parser) throws XmlPullParserException, IOException {
        countries = new ArrayList<>();

        for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
            final String attrName = parser.getAttributeName(i);
            final String attrValue = parser.getAttributeValue(i);
            if (attrName.equalsIgnoreCase(XML_TAGS.XML_ATTRIBUTE_FILE_VERSION)){
                mFileVersion = attrValue;
            }
        }

        int eventType = parser.getEventType();
        do {
            if (eventType == XmlPullParser.START_TAG) {
                final String name = parser.getName();
                if (XML_TAGS.XML_TAG_ERROR.equalsIgnoreCase(name)) {
                    error = new Error(parser);
                } else if (XML_TAGS.XML_TAG_COUNTRY.equalsIgnoreCase(name)) {
                    getCountries().add(new Country(parser));
                } else if (XML_TAGS.XML_TAG_LANGUAGES.equalsIgnoreCase(name)) {
                    languages = new Languages(parser);
                }
            }
            eventType = parser.next();
        } while (!(eventType == XmlPullParser.END_TAG && XML_TAG.equals(parser.getName()))
                && (eventType != XmlPullParser.END_DOCUMENT));

    }

    public Country getCountryById(String countryId) {
        for (Country country : countries) {
            if (country.getId().equalsIgnoreCase(countryId)) {
                return country;
            }
        }
        return null;
    }
}
