package com.ct.tpcalculator.calculator.data.statisticData;

import android.content.ContentValues;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by USER on 18.06.2015.
 */
public class StatisticMessage implements Serializable {

    public static final int TYPE_MSG_SMS = 0;
    public static final int TYPE_MSG_MMS = 1;

    public static final String KEY_CLASS_TYPE = "msgModel";
    public static final String KEY_NAME = "msgName";
    public static final String KEY_NUMBER = "msgNumber";
    public static final String KEY_DATE = "msgDate";
    public static final String KEY_TYPE = "directionType";
    public static final String KEY_MSG_TYPE = "msgType";

    private final String name;
    private final String phoneNumber;
    private final int directionType;
    private final Date messageDate;
    private final int messageType;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getDirectionType() {
        return directionType;
    }

    public Date getMessageDate() {
        return messageDate;
    }

    public String getName(){
        return name;
    }

    public int getMessageType(){
        return messageType;
    }

    public StatisticMessage(String name, String phoneNumber, int directionType, Date messageDate, int msgType) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.directionType = directionType;
        this.messageDate = messageDate;
        this.messageType = msgType;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();

        values.put(KEY_NAME, this.getName());
        values.put(KEY_NUMBER, this.getPhoneNumber());
        values.put(KEY_DATE, this.getMessageDate().getTime());
        values.put(KEY_TYPE, this.getDirectionType());
        values.put(KEY_MSG_TYPE, this.getMessageType());
        return values;
    }
}
