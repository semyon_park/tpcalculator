package com.ct.tpcalculator.calculator;

import android.support.v4.util.Pair;
import android.text.TextUtils;

import com.ct.tpcalculator.calculator.data.*;
import com.ct.tpcalculator.calculator.data.Package;
import com.ct.tpcalculator.calculator.data.calculationData.*;
import com.ct.tpcalculator.calculator.data.conditions.Condition;
import com.ct.tpcalculator.calculator.data.conditions.Conditions;
import com.ct.tpcalculator.helpers.Utils;

import java.util.*;

/**
 * Created by USER on 10.07.2015.
 */
public class PackageCalculator {

    public Hashtable<Limit, Integer> getUsedLimits() {
        return usedLimits;
    }

    // Хранит в себе данные о использованых лимитах: ключ - лимит, значение - кол-во использованного лимита
    private final Hashtable<Limit, Integer> usedLimits;

    // Расчитываемый опции или тарифный план
    private final Package serviceOrTp;

    // Дата активации опции или тарифного плана
    private final Date dateOfActivation;

    public PackageCalculator(Package serviceOrTp, Date dateOfActivation) {
        this.serviceOrTp = serviceOrTp;
        this.dateOfActivation = dateOfActivation;
        usedLimits = new Hashtable<>();
    }

    public Date getDateOfDeactivation() {
        Calendar endDateCalendar = Calendar.getInstance();
        endDateCalendar.setTime(dateOfActivation);

        switch (serviceOrTp.getPriceType().getPriceType()) {
            case SubscriptionPriceType.MONTH:
                endDateCalendar.add(Calendar.MONTH, serviceOrTp.getPriceType().getPeriodCount());
                break;
            case SubscriptionPriceType.WEEK:
                endDateCalendar.add(Calendar.WEEK_OF_YEAR, serviceOrTp.getPriceType().getPeriodCount());
                break;
            case SubscriptionPriceType.DAY:
            default:
                endDateCalendar.add(Calendar.DAY_OF_MONTH, serviceOrTp.getPriceType().getPeriodCount());
                break;
        }
        Date endDate = endDateCalendar.getTime();
        endDate = Utils.getEndOfDay(endDate);

        return endDate;
    }

    /**
     * Подсчитывает стоимость израсходванных минут, смс, интернет
     *
     * @param startDate - дата начала выборки
     * @param endDate   - дата конца выборки
     * @param logs      - логи - израсходованные минуты, смс, интернет
     * @return цену
     */
    public double CalculateTheCost(Date startDate, Date endDate, StatisticLogsForCalculation logs) {
        ArrayList<Pair<Date, Date>> datesPeriod;
        if (serviceOrTp instanceof Service) {
            datesPeriod = splitOnPeriods(startDate, endDate, ((Service) serviceOrTp).getServiceType());
        } else {
            datesPeriod = splitOnPeriods(startDate, endDate, serviceOrTp.getPriceType());
        }

        calculatePricesInPackage(datesPeriod, logs);

        return datesPeriod.size() * serviceOrTp.getPrice();
    }

    /**
     * @param startDate   - дата начала выборки
     * @param endDate     - дата конца выборки
     * @param serviceType - тип опции
     * @return промежутки времени
     */
    protected ArrayList<Pair<Date, Date>> splitOnPeriods(Date startDate, Date endDate, int serviceType) {
        ArrayList<Pair<Date, Date>> datesPeriod;
        if (serviceType == Service.SERVICE_TYPE_ONE_TIME) {
            datesPeriod = new ArrayList<>();
            Date dateOfDeactivation = getDateOfDeactivation();
            if (dateOfDeactivation.after(endDate)) {
                dateOfDeactivation = endDate;
            }
            datesPeriod.add(new Pair<>(dateOfActivation, dateOfDeactivation));
        } else {
            return splitOnPeriods(startDate, endDate, serviceOrTp.getPriceType());
        }
        return datesPeriod;
    }

    /**
     * @param startDate - дата начала выборки
     * @param endDate   - дата конца выборки
     * @param priceType - тип подписки
     * @return промежутки времени
     */
    public static ArrayList<Pair<Date, Date>> splitOnPeriods(Date startDate, Date endDate, SubscriptionPriceType priceType) {
        ArrayList<Pair<Date, Date>> datesPeriod;
        if ((priceType.getPriceType() == SubscriptionPriceType.MONTH) ||
                (priceType.getPriceType() == SubscriptionPriceType.DAY && priceType.getPeriodCount() == 30) ||
                (priceType.getPriceType() == SubscriptionPriceType.WEEK && priceType.getPeriodCount() == 4)) {
            datesPeriod = Utils.getMonthPeriod(startDate, endDate, priceType.getPeriodCount());
        } else if (priceType.getPriceType() == SubscriptionPriceType.WEEK) {
            datesPeriod = Utils.getWeeksPeriod(startDate, endDate, priceType.getPeriodCount());
        } else {
            datesPeriod = Utils.getDaysPeriod(startDate, endDate, priceType.getPeriodCount());

        }
        return datesPeriod;
    }

    /**
     * Подсчитывает стоимость израсходванных минут, смс, интернет
     *
     * @param datesPeriod - промежутки времени
     * @param logs        - логи - израсходованные минуты, смс, интернет                   -
     */
    protected void calculatePricesInPackage(ArrayList<Pair<Date, Date>> datesPeriod, StatisticLogsForCalculation logs) {
        for (Pair<Date, Date> date : datesPeriod) {
            if (serviceOrTp.getCallsCondition() != null) {
                if (serviceOrTp.getCallsCondition().getInRegionConditions() != null) {
                    calculatePricesInPackageByRegions(serviceOrTp.getCallsCondition().getInRegionConditions(),
                            logs.getCallsForCalculation().getInRegionCallsForCalculation(), date.first, date.second);
                }
                if (serviceOrTp.getCallsCondition().getInterRegionsConditions() != null) {
                    calculatePricesInPackageByRegions(serviceOrTp.getCallsCondition().getInterRegionsConditions(),
                            logs.getCallsForCalculation().getInterRegionCallsForCalculation(), date.first, date.second);
                }
                if (serviceOrTp.getCallsCondition().getInterCountriesConditions() != null) {
                    calculatePricesInPackageByRegions(serviceOrTp.getCallsCondition().getInterCountriesConditions(),
                            logs.getCallsForCalculation().getInternationalCallsForCalculation(), date.first, date.second);
                }
            }
            if (serviceOrTp.getSmsCondition() != null) {
                if (serviceOrTp.getSmsCondition().getInRegionConditions() != null) {
                    calculateMessagesPricesInPackageByRegions(serviceOrTp.getSmsCondition().getInRegionConditions(),
                            logs.getSmsMessagesForCalculation().getInRegionMessagesForCalculation(), date.first, date.second);
                }
                if (serviceOrTp.getSmsCondition().getInterRegionsConditions() != null) {
                    calculateMessagesPricesInPackageByRegions(serviceOrTp.getSmsCondition().getInterRegionsConditions(),
                            logs.getSmsMessagesForCalculation().getInterRegionMessagesForCalculation(), date.first, date.second);
                }
                if (serviceOrTp.getSmsCondition().getInterCountriesConditions() != null) {
                    calculateMessagesPricesInPackageByRegions(serviceOrTp.getSmsCondition().getInterCountriesConditions(),
                            logs.getSmsMessagesForCalculation().getInternationalMessagesForCalculation(), date.first, date.second);
                }
            }
            if (serviceOrTp.getMmsCondition() != null) {
                if (serviceOrTp.getMmsCondition().getInRegionConditions() != null) {
                    calculateMessagesPricesInPackageByRegions(serviceOrTp.getMmsCondition().getInRegionConditions(),
                            logs.getMmsMessagesForCalculation().getInRegionMessagesForCalculation(), date.first, date.second);
                }
                if (serviceOrTp.getMmsCondition().getInterRegionsConditions() != null) {
                    calculateMessagesPricesInPackageByRegions(serviceOrTp.getMmsCondition().getInterRegionsConditions(),
                            logs.getMmsMessagesForCalculation().getInterRegionMessagesForCalculation(), date.first, date.second);
                }
                if (serviceOrTp.getMmsCondition().getInterCountriesConditions() != null) {
                    calculateMessagesPricesInPackageByRegions(serviceOrTp.getMmsCondition().getInterCountriesConditions(),
                            logs.getMmsMessagesForCalculation().getInternationalMessagesForCalculation(), date.first, date.second);
                }
            }
            if (serviceOrTp.getInternetCondition() != null) {
                if (serviceOrTp.getInternetCondition().getInRegionConditions() != null) {
                    calculateInternetTraffic(serviceOrTp.getInternetCondition().getInRegionConditions(),
                            logs.getInternetTrafficsForCalculation(), date.first, date.second);
                }
            }
        }
    }

    /**
     * Подсчитывает стоимость израсходванных минут в зависимости от региона
     *
     * @param conditionsByRegion - условия в зависимотси от региона
     * @param callsByRegion      - звонки в зависимотси от региона
     * @param startDate          - дата начала выборки
     * @param endDate            - дата конца выборки
     */
    private void calculatePricesInPackageByRegions(Conditions conditionsByRegion, ArrayList<StatisticCallForCalculation> callsByRegion,
                                                   Date startDate, Date endDate) {
        for (StatisticCallForCalculation statisticCall : callsByRegion) {
            if (statisticCall.getCallDate().before(startDate) ||
                    statisticCall.getCallDate().after(endDate)) {
                continue;
            }
            if (statisticCall.isCalculated()) {
                continue;
            }
            //��������� �� ������� � ������
            calculateCallInPackageLimit(statisticCall, conditionsByRegion.getLimits());

            //��������� �� ��������
            if (statisticCall.getCallType() == DataDirectionTypes.INCOMING) {
                calculateCallByConditions(statisticCall, conditionsByRegion.getIncomingConditions());
            } else if (statisticCall.getCallType() == DataDirectionTypes.OUTGOING) {
                calculateCallByConditions(statisticCall, conditionsByRegion.getOutgoingConditions());
            }
        }
    }

    /**
     * Подсчитывает стоимость израсходванных минут в зависимости от условий
     *
     * @param statisticCall - звонок
     * @param conditions    - условия
     */
    private void calculateCallByConditions(StatisticCallForCalculation statisticCall, ArrayList<Condition> conditions) {
        if (conditions != null) {
            for (Condition condition : conditions) {
                condition.calculatePrice(statisticCall);
                if (statisticCall.isCalculated()) {
                    break;
                }
            }
        }
    }

    /**
     * Подсчитывает кол-во израсходованного лимита
     *
     * @param statisticCall - звонок
     * @param limits        - лимиты
     */
    private void calculateCallInPackageLimit(StatisticCallForCalculation statisticCall, Limits limits) {
        if (limits != null) {
            for (Limit limit : limits) {
                if (isLimitIsUp(limit)) {
                    break;
                }
                if ((limit.getLimitType() == Limit.LIMIT_TYPE_OUTGOING && statisticCall.getCallType() == DataDirectionTypes.INCOMING) ||
                        (limit.getLimitType() == Limit.LIMIT_TYPE_INCOMING && statisticCall.getCallType() == DataDirectionTypes.OUTGOING)) {
                    break;
                }

                boolean phoneNumberStartsWithPrefix = false;
                if (TextUtils.isEmpty(limit.getNumberPrefix())) {
                    phoneNumberStartsWithPrefix = true;
                } else {
                    ArrayList<String> limitPrefixes = new ArrayList<>(Arrays.asList(limit.getNumberPrefix().split(";")));
                    for (String prefix : limitPrefixes) {
                        phoneNumberStartsWithPrefix = Utils.isPhoneNumberStartWith(statisticCall.getPhoneNumber(), prefix);
                        if (phoneNumberStartsWithPrefix) {
                            break;
                        }
                    }
                }
                if (phoneNumberStartsWithPrefix) {
                    int remainderOfTheLimit = getRemainderOfTheLimit(limit);
                    if (remainderOfTheLimit >= statisticCall.getNotCalculatedMinutes()) {
                        addUsedLimit(limit, statisticCall.getNotCalculatedMinutes());
                        statisticCall.setCalculated(true);
                    } else {
                        addUsedLimit(limit, remainderOfTheLimit);
                        statisticCall.addCalculatedMinutes(remainderOfTheLimit);
                    }
                }
            }
        }
    }

    /**
     * Подсчитывает стоимость израсходванных смс в зависимости от региона
     *
     * @param conditionsByRegion - усдовия в зависимости от региона
     * @param messagesByRegion   - сообщения в зависимости от региона
     * @param startDate          - дата начала выборки
     * @param endDate            - дата конца выборки
     */
    private void calculateMessagesPricesInPackageByRegions(Conditions conditionsByRegion, ArrayList<StatisticMessageForCalculation> messagesByRegion,
                                                           Date startDate, Date endDate) {
        for (StatisticMessageForCalculation statisticMessage : messagesByRegion) {
            if (statisticMessage.getMessageDate().before(startDate) ||
                    statisticMessage.getMessageDate().after(endDate)) {
                continue;
            }
            if (statisticMessage.isCalculated()) {
                continue;
            }
            //��������� �� ������� � ������
            calculateMessageInPackageLimit(statisticMessage, conditionsByRegion.getLimits());

            if (statisticMessage.isCalculated()) {
                continue;
            }

            //��������� �� ��������
            if (statisticMessage.getDirectionType() == DataDirectionTypes.INCOMING) {
                calculateMessageByConditions(statisticMessage, conditionsByRegion.getIncomingConditions());
            } else if (statisticMessage.getDirectionType() == DataDirectionTypes.OUTGOING) {
                calculateMessageByConditions(statisticMessage, conditionsByRegion.getOutgoingConditions());
            }
        }
    }

    /**
     * Подсчитывает стоимость израсходванных смс в зависимости от условий
     *
     * @param statisticMessage - сообщения
     * @param conditions       - условия
     */
    private void calculateMessageByConditions(StatisticMessageForCalculation statisticMessage, ArrayList<Condition> conditions) {
        if (conditions != null) {
            for (Condition condition : conditions) {
                condition.calculatePrice(statisticMessage);
                if (statisticMessage.isCalculated()) {
                    break;
                }
            }
        }
    }

    /**
     * Подсчитывает кол-во израсходованного лимита
     *
     * @param statisticMessage - сообщение
     * @param limits           - лимиты
     */
    private void calculateMessageInPackageLimit(StatisticMessageForCalculation statisticMessage, Limits limits) {
        if (limits != null) {
            for (Limit limit : limits) {
                if (isLimitIsUp(limit)) {
                    break;
                }
                if ((limit.getLimitType() == Limit.LIMIT_TYPE_OUTGOING && statisticMessage.getDirectionType() == DataDirectionTypes.INCOMING) ||
                        (limit.getLimitType() == Limit.LIMIT_TYPE_INCOMING && statisticMessage.getDirectionType() == DataDirectionTypes.OUTGOING)) {
                    break;
                }

                boolean phoneNumberStartsWithPrefix = false;
                if (TextUtils.isEmpty(limit.getNumberPrefix())) {
                    phoneNumberStartsWithPrefix = true;
                } else {
                    ArrayList<String> limitPrefixes = new ArrayList<>(Arrays.asList(limit.getNumberPrefix().split(";")));
                    for (String prefix : limitPrefixes) {
                        phoneNumberStartsWithPrefix = Utils.isPhoneNumberStartWith(statisticMessage.getPhoneNumber(), prefix);
                        if (phoneNumberStartsWithPrefix) {
                            break;
                        }
                    }
                }

                if (phoneNumberStartsWithPrefix) {
                    addUsedLimit(limit, 1);
                    statisticMessage.setPrice(0);
                    statisticMessage.setCalculated(true);
                }
            }
        }
    }

    /**
     * Подсчитывает стоимость израсходванного интернет трафика
     *
     * @param conditions       - условия
     * @param internetTraffics - интернет трафик
     * @param startDate        - дата старта выборки
     * @param endDate          - дата конца выборки
     */
    private void calculateInternetTraffic(Conditions conditions, StatisticInternetTrafficsForCalculation internetTraffics,
                                          Date startDate, Date endDate) {
        for (StatisticInternetTrafficForCalculation internetTraffic : internetTraffics.getMobileInternetTraffic()) {
            if (internetTraffic.getDate().before(startDate) ||
                    internetTraffic.getDate().after(endDate)) {
                continue;
            }
            //Учитываем трафик в лимите
            calculateInternetTrafficInPackageLimit(internetTraffic, conditions.getLimits());

            if (internetTraffic.isCalculated())
                continue;

            //Подсчитать в условиях
            calculateInternetTrafficByConditions(internetTraffic, conditions.getIncomingConditions(), DataDirectionTypes.INCOMING);
            calculateInternetTrafficByConditions(internetTraffic, conditions.getOutgoingConditions(), DataDirectionTypes.OUTGOING);
        }
    }

    /**
     * Подсчитывает  кол-во потраченого интернет трафика в лимите
     *
     * @param internetTraffic - интернет трафик
     * @param limits          - лимиты
     */
    private void calculateInternetTrafficInPackageLimit(StatisticInternetTrafficForCalculation internetTraffic, Limits limits) {
        if (limits != null) {
            for (Limit limit : limits) {
                if (isLimitIsUp(limit)) {
                    break;
                }

                int remainderOfTheLimit = getRemainderOfTheLimit(limit);

                if (limit.getLimitType() == Limit.LIMIT_TYPE_OUTGOING) {
                    calculateTransmittedInternetTrafficLimit(internetTraffic, limit, remainderOfTheLimit);
                } else if (limit.getLimitType() == Limit.LIMIT_TYPE_INCOMING) {
                    calculateReceivedInternetTrafficLimit(internetTraffic, limit, remainderOfTheLimit);
                } else {
                    calculateTransmittedInternetTrafficLimit(internetTraffic, limit, remainderOfTheLimit);
                    remainderOfTheLimit = getRemainderOfTheLimit(limit); // ����������� ���-�� ����������� ������
                    calculateReceivedInternetTrafficLimit(internetTraffic, limit, remainderOfTheLimit);
                }
            }
        }
    }

    /**
     * Подсчитывает кол-во потраченого исходящего интернет трафика в лимите
     *
     * @param internetTraffic     - интернет трафик
     * @param limit               - лимит
     * @param remainderOfTheLimit - остаток лимита
     */
    private void calculateTransmittedInternetTrafficLimit(StatisticInternetTrafficForCalculation internetTraffic, Limit limit, int remainderOfTheLimit) {
        if (remainderOfTheLimit >= internetTraffic.getMobileTransmittedNotCalculatedMegaBytes()) {
            addUsedLimit(limit, internetTraffic.getMobileTransmittedNotCalculatedMegaBytes());
            internetTraffic.addMobileTransmittedCalculatedMegaBytes(internetTraffic.getMobileTransmittedNotCalculatedMegaBytes());
        } else {
            addUsedLimit(limit, remainderOfTheLimit);
            internetTraffic.addMobileTransmittedCalculatedMegaBytes(remainderOfTheLimit);
        }
    }

    /**
     * Подсчитывает кол-во потраченного входящего интернет трафика в лимите
     *
     * @param internetTraffic
     * @param limit
     * @param remainderOfTheLimit
     */
    private void calculateReceivedInternetTrafficLimit(StatisticInternetTrafficForCalculation internetTraffic, Limit limit, int remainderOfTheLimit) {
        if (remainderOfTheLimit >= internetTraffic.getMobileReceivedNotCalculatedMegaBytes()) {
            addUsedLimit(limit, internetTraffic.getMobileReceivedNotCalculatedMegaBytes());
            internetTraffic.addMobileReceivedCalculatedMegaBytes(internetTraffic.getMobileReceivedNotCalculatedMegaBytes());
        } else {
            addUsedLimit(limit, remainderOfTheLimit);
            internetTraffic.addMobileReceivedCalculatedMegaBytes(remainderOfTheLimit);
        }
    }

    /**
     * Подсчитывает стоимость интернет трафика по условиям
     *
     * @param internetTraffic   - интернет трафик
     * @param conditions        - условия
     * @param dataDirectionType - направление интернет трафика (входящие или исходящие)
     */
    private void calculateInternetTrafficByConditions(StatisticInternetTrafficForCalculation internetTraffic, ArrayList<Condition> conditions, int dataDirectionType) {
        if (conditions != null) {
            for (Condition condition : conditions) {
                condition.calculatePrice(internetTraffic, dataDirectionType);
                if (internetTraffic.isCalculated()) {
                    break;
                }
            }
        }
    }

    /**
     * Возращет остаток лимита
     *
     * @param limit - лимит
     * @return остаток лимита
     */
    public int getRemainderOfTheLimit(Limit limit) {
        if (usedLimits.containsKey(limit)) {
            return limit.getValue() - usedLimits.get(limit);
        }
        return limit.getValue();
    }

    // Лиимит исчерпан
    public boolean isLimitIsUp(Limit limit) {
        if (usedLimits.containsKey(limit)) {
            return limit.getValue() <= usedLimits.get(limit);
        }
        return limit.getValue() <= 0;
    }

    /**
     * Учесть использованый лимит
     *
     * @param limit          - лимит
     * @param usedLimitValue - кол-во использованного лимита
     */
    private void addUsedLimit(Limit limit, int usedLimitValue) {
        int limitValue = usedLimitValue;
        if (usedLimits.containsKey(limit)) {
            limitValue += usedLimits.get(limit);
        }
        usedLimits.put(limit, limitValue);
    }

    /**
     * Подсчитать кол-во израсходованных минут в лимите
     *
     * @param callsForCalculation - звонки
     * @param limits              - лимиты
     * @param startDate           - дата начало выборки
     * @param endDate             - дата конца выборки
     */
    public void CalculateCallsLimits(ArrayList<StatisticCallForCalculation> callsForCalculation, Limits limits, Date startDate, Date endDate) {
        for (StatisticCallForCalculation statisticCall : callsForCalculation) {
            if (statisticCall.getCallDate().before(startDate) ||
                    statisticCall.getCallDate().after(endDate)) {
                continue;
            }
            if (statisticCall.isCalculated()) {
                continue;
            }
            // Учесть звонок в лимите
            calculateCallInPackageLimit(statisticCall, limits);
        }
    }

    /**
     * Подсчитать кол-во израсходованных сообщений в лимите
     *
     * @param messagesForCalculation - сообщения
     * @param limits                 - лимиты
     * @param startDate              - дата начала выборки
     * @param endDate                - дата конца выборки
     */
    public void CalculateMessagesLimits(ArrayList<StatisticMessageForCalculation> messagesForCalculation, Limits limits, Date startDate, Date endDate) {
        for (StatisticMessageForCalculation statisticMessage : messagesForCalculation) {
            if (statisticMessage.getMessageDate().before(startDate) ||
                    statisticMessage.getMessageDate().after(endDate)) {
                continue;
            }
            if (statisticMessage.isCalculated()) {
                continue;
            }
            // Учесть сообщение в лимите
            calculateMessageInPackageLimit(statisticMessage, limits);
        }
    }

    /**
     * Подсчитать кол-во израсходованных МБ в лимите
     *
     * @param internetTraffics - интернет трафик
     * @param limits           - лимиты
     * @param startDate        - дата начала выборки
     * @param endDate          - дата конца выборки
     */
    public void CalculateInternetTrafficLimits(ArrayList<StatisticInternetTrafficForCalculation> internetTraffics, Limits limits, Date startDate, Date endDate) {
        for (StatisticInternetTrafficForCalculation internetTraffic : internetTraffics) {
            if (internetTraffic.getDate().before(startDate) ||
                    internetTraffic.getDate().after(endDate)) {
                continue;
            }
            if (internetTraffic.isCalculated()) {
                continue;
            }
            calculateInternetTrafficInPackageLimit(internetTraffic, limits);
        }
    }
}
