package com.ct.tpcalculator.calculator.data.conditions;

import com.ct.tpcalculator.calculator.data.DataDirectionTypes;
import com.ct.tpcalculator.calculator.data.XML_TAGS;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticCallForCalculation;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticInternetTrafficForCalculation;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticMessageForCalculation;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by USER on 16.06.2015.
 */
public class Condition implements Serializable {

    public void setName(String name) {
        this.name = name;
    }

    private String name;
    private double price;
    private String id;

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getId(){
        return id;
    }

    public Condition(XmlPullParser parser) throws XmlPullParserException, IOException {
        final String tag = parser.getName();
//        if (XML_TAGS.XML_TAG_CONDITION_DEFAULT.equalsIgnoreCase(tag)) {
            for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
                final String attrName = parser.getAttributeName(i);
                final String attrValue = parser.getAttributeValue(i);
                if (XML_TAGS.XML_ATTRIBUTE_NAME.equalsIgnoreCase(attrName)) {
                    name = attrValue;
                } else if (XML_TAGS.XML_ATTRIBUTE_PRICE.equalsIgnoreCase(attrName)) {
                    price = Double.parseDouble(attrValue);
                }else if (XML_TAGS.XML_ATTRIBUTE_ID.equalsIgnoreCase(attrName)){
                    id = attrValue;
                }
            }
//        }
    }

    public Condition(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public void calculatePrice(StatisticCallForCalculation call) {
        call.setPrice(call.getNotCalculatedMinutes() * getPrice());
        call.setCalculated(true);
    }

    public void calculatePrice(StatisticMessageForCalculation message) {
        message.setPrice(getPrice());
        message.setCalculated(true);
    }

    public void calculatePrice(StatisticInternetTrafficForCalculation internetTraffic, int dataDirectionType) {
        int traffic = 0;
        if (dataDirectionType == DataDirectionTypes.INCOMING) {
            traffic = internetTraffic.getMobileReceivedMegaBytes() - internetTraffic.getMobileReceivedCalculatedMegaBytes();
            internetTraffic.addMobileReceivedCalculatedMegaBytes(traffic);
        } else if (dataDirectionType == DataDirectionTypes.OUTGOING) {
            traffic = internetTraffic.getMobileTransmittedMegaBytes() - internetTraffic.getMobileTransmittedCalculatedMegaBytes();
            internetTraffic.addMobileTransmittedCalculatedMegaBytes(traffic);
        }
        internetTraffic.setCalculatedPrice(internetTraffic.getCalculatedPrice() + (traffic * getPrice()));
    }

}
