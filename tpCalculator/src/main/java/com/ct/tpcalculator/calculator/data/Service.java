package com.ct.tpcalculator.calculator.data;

import com.ct.tpcalculator.calculator.data.conditions.CallsCondition;
import com.ct.tpcalculator.calculator.data.conditions.InternetCondition;
import com.ct.tpcalculator.calculator.data.conditions.MmsCondition;
import com.ct.tpcalculator.calculator.data.conditions.SmsCondition;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by USER on 16.06.2015.
 */
public class Service extends com.ct.tpcalculator.calculator.data.Package implements Serializable {

    public static final int SERVICE_TYPE_ONE_TIME = 0;
    public static final int SERVICE_TYPE_PERIODICALLY = 1;
    public static final int SERVICE_TYPE_MIX = 2;

    private int serviceType;

    public int getServiceType() {
        return serviceType;
    }

    public Service(XmlPullParser parser) throws XmlPullParserException, IOException {
        priceType = new SubscriptionPriceType();
        titles = new LocalizedStrings();
        descriptions = new LocalizedStrings();

        final String tag = parser.getName();
        if (XML_TAGS.XML_TAG_SERVICE.equalsIgnoreCase(tag)) {
            for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
                final String attrName = parser.getAttributeName(i);
                final String attrValue = parser.getAttributeValue(i);
                if (XML_TAGS.XML_ATTRIBUTE_ID.equalsIgnoreCase(attrName)) {
                    id = attrValue;
                } else if (attrName.startsWith(XML_TAGS.XML_ATTRIBUTE_TITLE) && attrName.length() > XML_TAGS.XML_ATTRIBUTE_TITLE.length()) {
                    titles.put(attrName.substring(XML_TAGS.XML_ATTRIBUTE_TITLE.length() + 1), attrValue);
                } else if (attrName.startsWith(XML_TAGS.XML_ATTRIBUTE_DESCRIPTION) && attrName.length() > XML_TAGS.XML_ATTRIBUTE_DESCRIPTION.length()) {
                    getDescriptions().put(attrName.substring(XML_TAGS.XML_ATTRIBUTE_DESCRIPTION.length() + 1), attrValue);
                } else if (XML_TAGS.XML_ATTRIBUTE_PRICE_TYPE.equalsIgnoreCase(attrName)) {
                    priceType.setPriceType(attrValue);
                } else if (XML_TAGS.XML_ATTRIBUTE_PRICE.equalsIgnoreCase(attrName)) {
                    price = Double.parseDouble(attrValue);
                } else if (XML_TAGS.XML_ATTRIBUTE_CHANGE_PRICE.equalsIgnoreCase(attrName)) {
                    changePrice = Double.parseDouble(attrValue);
                } else if (XML_TAGS.XML_ATTRIBUTE_PERIOD.equalsIgnoreCase(attrName)) {
                    priceType.setPeriodCount(Integer.parseInt(attrValue));
                } else if (XML_TAGS.XML_ATTRIBUTE_SERVICE_TYPE.equalsIgnoreCase(attrName)) {
                    if (attrValue.equalsIgnoreCase("oneTime")) {
                        serviceType = SERVICE_TYPE_ONE_TIME;
                    } else if (attrValue.equalsIgnoreCase("periodically")) {
                        serviceType = SERVICE_TYPE_PERIODICALLY;
                    } else if (attrValue.equalsIgnoreCase("mix")) {
                        serviceType = SERVICE_TYPE_MIX;
                    }
                }
            }
        }
        parser.next();

        int eventType = parser.getEventType();
        do {
            if (eventType == XmlPullParser.START_TAG) {
                final String name = parser.getName();
                if (XML_TAGS.XML_TAG_CALLS.equalsIgnoreCase(name)) {
                    callsCondition = new CallsCondition(parser);
                } else if (XML_TAGS.XML_TAG_SMS.equalsIgnoreCase(name)) {
                    smsCondition = new SmsCondition(parser);
                } else if (XML_TAGS.XML_TAG_MMS.equalsIgnoreCase(name)) {
                    mmsCondition = new MmsCondition(parser);
                } else if (XML_TAGS.XML_TAG_INTERNET.equalsIgnoreCase(name)) {
                    internetCondition = new InternetCondition(parser);
                } else if (XML_TAGS.XML_TAG_COMMANDS.equalsIgnoreCase(name)) {
                    commands = new Commands(parser);
                }
            }
            eventType = parser.next();
        }
        while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_SERVICE.equals(parser.getName()))
                && (eventType != XmlPullParser.END_DOCUMENT));
    }


}