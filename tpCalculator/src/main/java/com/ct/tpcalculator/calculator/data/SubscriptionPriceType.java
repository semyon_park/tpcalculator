package com.ct.tpcalculator.calculator.data;

import java.io.Serializable;

/**
 * Created by USER on 16.06.2015.
 */
public class SubscriptionPriceType implements Serializable {

    public static final int DAY = 0;
    public static final int WEEK = 1;
    public static final int MONTH = 2;

    private int priceType;

    private int periodCount = 1;

    public int getPeriodCount() {
        return periodCount;
    }

    public void setPeriodCount(int periodCount) {
        if (periodCount <= 0){
            throw new IllegalArgumentException("periodCount can not be less or equal to zero");
        }
        this.periodCount = periodCount;
    }

    public int getPriceType() {
        return priceType;
    }

    public void setPriceType(int priceType) {
        this.priceType = priceType;
    }

    public void setPriceType(String priceType){
        if (priceType.equalsIgnoreCase("day")) {
            this.setPriceType(SubscriptionPriceType.DAY);
        } else if (priceType.equalsIgnoreCase("week")) {
            this.setPriceType(SubscriptionPriceType.WEEK);
        } else if (priceType.equalsIgnoreCase("month")) {
            this.setPriceType(SubscriptionPriceType.MONTH);
        } else {
            this.setPriceType(SubscriptionPriceType.DAY);
        }
    }

//    public SubscriptionPriceType(String priceType) {
//       setPriceType(priceType);
//    }


    public SubscriptionPriceType() {
        setPeriodCount(1);
        setPriceType(SubscriptionPriceType.DAY);
    }

}
