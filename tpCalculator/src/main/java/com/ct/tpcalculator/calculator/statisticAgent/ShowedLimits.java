package com.ct.tpcalculator.calculator.statisticAgent;

import com.ct.tpcalculator.helpers.L;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Semyon on 20.08.2015.
 * Класс служит для сохранения отображенных лимитов
 */
public class ShowedLimits extends ArrayList<ShowedLimit> {

    private static final String KEY_LIMITS = "limits";

    public ShowedLimits(){
        super();
    }

    public ShowedLimits(String data) throws JSONException {
        super();
        JSONObject jsonObject = new JSONObject(data);
        JSONArray jsonArray = jsonObject.getJSONArray(KEY_LIMITS);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            ShowedLimit limit= null;
            try {
                limit = new ShowedLimit(obj);
            } catch (JSONException ex) {
                L.e(ex.getMessage());
            }
            if (limit != null){
                add(limit);
            }
        }
    }

    public String GetJSon(){
        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();
            for (ShowedLimit showedLimit:this) {
                jsonArray.put(showedLimit.getJsonObject());
            }
            jsonObject.put(KEY_LIMITS, jsonArray);
        } catch (JSONException e) {
            L.e(e.getMessage());
        }
        return jsonObject.toString();
    }

    public ShowedLimit findShowedLimitById(String id){
        for (ShowedLimit showedLimit :
                this) {
            if (showedLimit.getId().equalsIgnoreCase(id)){
                return showedLimit;
            }
        }
        return null;
    }

}
