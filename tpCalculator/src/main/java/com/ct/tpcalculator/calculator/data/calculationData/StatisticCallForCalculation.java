package com.ct.tpcalculator.calculator.data.calculationData;

import com.ct.tpcalculator.calculator.data.statisticData.StatisticCall;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by USER on 10.07.2015.
 */
public class StatisticCallForCalculation implements Serializable {

    private StatisticCall call;

    private int calculatedMinutes;
    private double price;
    private boolean isCalculated;

    public StatisticCallForCalculation(StatisticCall call){
        this.call = call;
    }

    public int getCalculatedMinutes() { // ������������ ������
        return calculatedMinutes;
    }

    public int getNotCalculatedMinutes() {// �� ������������ ������
        return call.getCallDurationInMinutes() - calculatedMinutes;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isCalculated() {
        return isCalculated;
    }

    public void setCalculated(boolean isCalculated) {
        this.isCalculated = isCalculated;
        calculatedMinutes = call.getCallDurationInMinutes();
    }

    public void addCalculatedMinutes(int calculatedMinutes) {
        this.calculatedMinutes += calculatedMinutes;
        if (this.calculatedMinutes >= call.getCallDurationInMinutes()) {
            setCalculated(true);
        }
    }

    public Date getCallDate(){
        return call.getCallDate();
    }

    public int getCallType(){
        return call.getCallType();
    }

    public String getPhoneNumber(){
        return call.getPhoneNumber();
    }

    @Override
    protected StatisticCallForCalculation clone() {
        return new StatisticCallForCalculation(call);
    }
}
