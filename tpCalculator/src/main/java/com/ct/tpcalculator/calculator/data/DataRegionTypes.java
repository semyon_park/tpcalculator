package com.ct.tpcalculator.calculator.data;

import java.io.Serializable;

/**
 * Created by USER on 24.06.2015.
 */
public class DataRegionTypes implements Serializable {

    public static int NONE = 0;
    public static int InRegion = 1;
    public static int InterRegion = 2;
    public static int InterCountries = 3;
}
