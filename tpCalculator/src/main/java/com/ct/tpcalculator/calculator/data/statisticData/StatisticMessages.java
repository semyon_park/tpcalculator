package com.ct.tpcalculator.calculator.data.statisticData;


import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by USER on 18.06.2015.
 */
public class StatisticMessages implements Serializable {

    private ArrayList<StatisticMessage> mInRegionMessages;
    private ArrayList<StatisticMessage> mInterRegionMessages;
    private ArrayList<StatisticMessage> mInternationalMessages;


    public StatisticMessages(ArrayList<StatisticMessage> inRegionMessages, ArrayList<StatisticMessage> interRegionMessages, ArrayList<StatisticMessage> internationalMessages) {
        mInRegionMessages = inRegionMessages;
        mInterRegionMessages = interRegionMessages;
        mInternationalMessages = internationalMessages;
    }

    public ArrayList<StatisticMessage> getInRegionMessages() {
        return mInRegionMessages;
    }

    public ArrayList<StatisticMessage> getInterRegionMessages() {
        return mInterRegionMessages;
    }

    public ArrayList<StatisticMessage> getInternationalMessages() {
        return mInternationalMessages;
    }

}
