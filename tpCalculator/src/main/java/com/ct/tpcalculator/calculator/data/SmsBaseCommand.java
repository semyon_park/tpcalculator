package com.ct.tpcalculator.calculator.data;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by USER on 17.06.2015.
 */
public class SmsBaseCommand extends BaseCommand implements Serializable {

    private String smsNumber;
    private String smsText;

    public String getSmsNumber() {
        return smsNumber;
    }

    public String getSmsText() {
        return smsText;
    }

    public SmsBaseCommand(XmlPullParser parser) throws XmlPullParserException, IOException {
        super();

        for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
            final String attrName = parser.getAttributeName(i);
            final String attrValue = parser.getAttributeValue(i);
            if (XML_TAGS.XML_ATTRIBUTE_SMS_NUMBER.equalsIgnoreCase(attrName)) {
                smsNumber = attrValue;
            } else if (XML_TAGS.XML_ATTRIBUTE_SMS_TEXT.equalsIgnoreCase(attrName)) {
                smsText = attrValue;
            }else if (XML_TAGS.XML_ATTRIBUTE_ID.equalsIgnoreCase(attrName)){
                id = attrValue;
            }
        }
        parser.next();

    }
}
