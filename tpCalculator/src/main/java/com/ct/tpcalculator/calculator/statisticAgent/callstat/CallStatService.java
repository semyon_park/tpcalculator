package com.ct.tpcalculator.calculator.statisticAgent.callstat;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.ct.tpcalculator.calculator.statisticAgent.Consts;

public class CallStatService extends IntentService {

    public CallStatService() {
        super("CallStatService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        e("onHandleIntent: call");
        AlarmCallReceiver.setAlarm(getApplicationContext());

        CallStatCore.check(this.getApplicationContext());

        AlarmCallReceiver.completeWakefulIntent(intent);
    }

    //------------------------------------------------------------------------------------------
    private boolean debug = Consts.DEBUG && true;
    private String className = "CallStatService.";

    private void e(String message) {
        if (debug) Log.e(Consts.APP_TAG, className + message);
    }

    private void d(String message) {
        if (debug) Log.d(Consts.APP_TAG, className + message);
    }
    //------------------------------------------------------------------------------------------
}
