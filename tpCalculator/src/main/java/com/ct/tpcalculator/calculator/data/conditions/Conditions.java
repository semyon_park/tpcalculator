package com.ct.tpcalculator.calculator.data.conditions;

import com.ct.tpcalculator.calculator.data.Limits;
import com.ct.tpcalculator.calculator.data.XML_TAGS;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by USER on 16.06.2015.
 */
public class Conditions  implements Serializable {

    private ArrayList<Condition> incomingConditions;
    private ArrayList<Condition> outgoingConditions;
    private Limits limits;

    public ArrayList<Condition> getIncomingConditions() {
        return incomingConditions;
    }

    public ArrayList<Condition> getOutgoingConditions() {
        return outgoingConditions;
    }

    public Limits getLimits() {
        return limits;
    }

    public Conditions(XmlPullParser parser, String parentElementName) throws XmlPullParserException, IOException {
        incomingConditions = new ArrayList<>();
        outgoingConditions = new ArrayList<>();

        int eventType = parser.getEventType();
        do {
            if (eventType == XmlPullParser.START_TAG) {
                final String name = parser.getName();
                if (XML_TAGS.XML_TAG_INCOMING.equalsIgnoreCase(name)) {

                    int subEventType = parser.getEventType();
                    do {
                        if (subEventType == XmlPullParser.START_TAG) {
                            final String subName = parser.getName();
                            if (XML_TAGS.XML_TAG_CONDITION_DEFAULT.equalsIgnoreCase(subName)) {
                                getIncomingConditions().add(new Condition(parser));
                                parser.next();
                            } else if (XML_TAGS.XML_TAG_CONDITION_PREFIX.equalsIgnoreCase(subName)) {
                                getIncomingConditions().add(new ConditionPrefix(parser));
                            } else if (XML_TAGS.XML_TAG_CONDITION_TIME.equalsIgnoreCase(subName)) {
                                getIncomingConditions().add(new ConditionTime(parser));
                            } else if (XML_TAGS.XML_TAG_CONDITION_MINUTE.equalsIgnoreCase(subName)) {
                                getIncomingConditions().add(new ConditionMinute(parser));
                            } else if (XML_TAGS.XML_TAG_CONDITION_DAY.equalsIgnoreCase(subName)) {
                                getIncomingConditions().add(new ConditionDay(parser));
                            }
                        }
                        subEventType = parser.next();
                    }
                    while (!(subEventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_INCOMING.equals(parser.getName()))
                            && (subEventType != XmlPullParser.END_DOCUMENT));

                } else if (XML_TAGS.XML_TAG_OUTGOING.equalsIgnoreCase(name)) {

                    int subEventType = parser.getEventType();
                    do {
                        if (subEventType == XmlPullParser.START_TAG) {
                            final String subName = parser.getName();
                            if (XML_TAGS.XML_TAG_CONDITION_DEFAULT.equalsIgnoreCase(subName)) {
                                getOutgoingConditions().add(new Condition(parser));
                                parser.next();
                            } else if (XML_TAGS.XML_TAG_CONDITION_PREFIX.equalsIgnoreCase(subName)) {
                                getOutgoingConditions().add(new ConditionPrefix(parser));
                            } else if (XML_TAGS.XML_TAG_CONDITION_TIME.equalsIgnoreCase(subName)) {
                                getOutgoingConditions().add(new ConditionTime(parser));
                            } else if (XML_TAGS.XML_TAG_CONDITION_MINUTE.equalsIgnoreCase(subName)) {
                                getOutgoingConditions().add(new ConditionMinute(parser));
                            } else if (XML_TAGS.XML_TAG_CONDITION_DAY.equalsIgnoreCase(subName)) {
                                getOutgoingConditions().add(new ConditionDay(parser));
                            }
                        }
                        subEventType = parser.next();
                    }
                    while (!(subEventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_OUTGOING.equals(parser.getName()))
                            && (subEventType != XmlPullParser.END_DOCUMENT));
                } else if (XML_TAGS.XML_TAG_LIMITS.equalsIgnoreCase(name)) {
                    limits = new Limits(parser);
                }
            }
            eventType = parser.next();
        }
        while (!(eventType == XmlPullParser.END_TAG && parentElementName.equals(parser.getName()))
                && (eventType != XmlPullParser.END_DOCUMENT));
    }

}
