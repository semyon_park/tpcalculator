package com.ct.tpcalculator.calculator.data.calculationData;

import com.ct.tpcalculator.calculator.data.statisticData.StatisticInternetTraffic;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by USER on 10.07.2015.
 */
public class StatisticInternetTrafficForCalculation implements Serializable {

    public void setMobileReceivedMegaBytes(int mobileReceivedMegaBytes) {
        this.mobileReceivedMegaBytes = mobileReceivedMegaBytes;
    }

    public void setMobileTransmittedMegaBytes(int mobileTransmittedMegaBytes) {
        this.mobileTransmittedMegaBytes = mobileTransmittedMegaBytes;
    }

    public int getMobileReceivedMegaBytes() {
        return mobileReceivedMegaBytes;
    }

    public int getMobileTransmittedMegaBytes() {
        return mobileTransmittedMegaBytes;
    }

    private int mobileReceivedMegaBytes;

    private int mobileTransmittedMegaBytes;

    public Date getDate() {
        return date;
    }

    private final Date date;

    private int mobileReceivedCalculatedMegaBytes;

    private int mobileTransmittedCalculatedMegaBytes;

    private double calculatedPrice;

    private boolean calculated;

    public int getMobileReceivedCalculatedMegaBytes() {
        return mobileReceivedCalculatedMegaBytes;
    }

    public int getMobileReceivedNotCalculatedMegaBytes(){
        return mobileReceivedMegaBytes - mobileReceivedCalculatedMegaBytes;
    }

    public int getMobileTransmittedCalculatedMegaBytes() {
        return mobileTransmittedCalculatedMegaBytes;
    }

    public int getMobileTransmittedNotCalculatedMegaBytes(){
        return mobileTransmittedMegaBytes - mobileTransmittedCalculatedMegaBytes;
    }

    public void addMobileTransmittedCalculatedMegaBytes(long mobileTransmittedCalculatedMegaBytes) {
        this.mobileTransmittedCalculatedMegaBytes += mobileTransmittedCalculatedMegaBytes;
        if (this.mobileReceivedCalculatedMegaBytes >= mobileReceivedMegaBytes &&
                this.mobileTransmittedCalculatedMegaBytes >= mobileTransmittedCalculatedMegaBytes){
            calculated = true;
        }
    }

    public void addMobileReceivedCalculatedMegaBytes(int mobileReceivedCalculatedMegaBytes) {
        this.mobileReceivedCalculatedMegaBytes += mobileReceivedCalculatedMegaBytes;
        if (this.mobileReceivedCalculatedMegaBytes >= mobileReceivedMegaBytes &&
                this.mobileTransmittedCalculatedMegaBytes >= mobileTransmittedCalculatedMegaBytes){
            calculated = true;
        }
    }

    public double getCalculatedPrice() {
        return calculatedPrice;
    }

    public void setCalculatedPrice(double calculatedPrice) {
        this.calculatedPrice = calculatedPrice;
    }

    public boolean isCalculated() {
        return calculated;
    }

    public StatisticInternetTrafficForCalculation(StatisticInternetTraffic internetTraffic){
        mobileReceivedMegaBytes = internetTraffic.getReceivedMegaBytes();
        mobileTransmittedMegaBytes = internetTraffic.getTransmittedMegaBytes();
        date = internetTraffic.getDate();
    }

    private StatisticInternetTrafficForCalculation(int mobileReceivedMegaBytes, int mobileTransmittedMegaBytes, Date date){
        this.mobileReceivedMegaBytes = mobileReceivedMegaBytes;
        this.mobileTransmittedMegaBytes = mobileTransmittedMegaBytes;
        this.date = date;
    }

    @Override
    protected StatisticInternetTrafficForCalculation clone(){
        return new StatisticInternetTrafficForCalculation(mobileReceivedMegaBytes, mobileTransmittedMegaBytes, date);
    }
}
