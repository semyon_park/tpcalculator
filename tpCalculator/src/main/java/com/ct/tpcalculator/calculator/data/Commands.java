package com.ct.tpcalculator.calculator.data;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by USER on 17.06.2015.
 */
public class Commands extends ArrayList<Command> implements Serializable {

    public Commands(XmlPullParser parser) throws XmlPullParserException, IOException {

        int eventType = parser.getEventType();
        do {
            if (eventType == XmlPullParser.START_TAG) {
                final String name = parser.getName();
                if (XML_TAGS.XML_TAG_COMMAND_BLOCK.equalsIgnoreCase(name)) {
                    add(new Command(parser));
                }
            }
            eventType = parser.next();
        } while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_COMMANDS.equals(parser.getName()))
                && (eventType != XmlPullParser.END_DOCUMENT));
    }

    public Command getFirstCommandByCommandType(int commandType) {
        for (Command command:this){
            if (command.getCommandType() == commandType){
                return command;
            }
        }
        return null;
    }
}
