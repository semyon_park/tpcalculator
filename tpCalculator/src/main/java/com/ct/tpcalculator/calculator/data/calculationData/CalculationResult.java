package com.ct.tpcalculator.calculator.data.calculationData;

import com.ct.tpcalculator.calculator.data.MobileOperator;
import com.ct.tpcalculator.calculator.data.TariffPlan;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by marat on 9/2/2015.
 */
public class CalculationResult implements Serializable {

    private final MobileOperator mMobileOperator;
    private final ArrayList<PackagesToOffer> mPackagesToOffer;
    private final double mCurrentPrice;
    private final TariffPlan mTariffPlan;

    public ArrayList<PackagesToOffer> getPackagesToOffer() {
        return mPackagesToOffer;
    }

    public double getPrice() {
        return mCurrentPrice;
    }

    public String getFormattedPrice() {
        NumberFormat format = NumberFormat.getNumberInstance();
        format.setMaximumFractionDigits(2);
        return format.format(mCurrentPrice);
    }

    public String getFormattedPriceInYear() {
        NumberFormat format = NumberFormat.getNumberInstance();
        format.setMaximumFractionDigits(2);
        return format.format(mCurrentPrice * 12);
    }

    public TariffPlan getTariffPlan() {
        return mTariffPlan;
    }

    public MobileOperator getMobileOperator() {
        return mMobileOperator;
    }

    public CalculationResult(ArrayList<PackagesToOffer> packagesToOffer, MobileOperator mobileOperator, TariffPlan tariffPlan, double currentPrice) {
        mPackagesToOffer = packagesToOffer;
        mMobileOperator = mobileOperator;
        mTariffPlan = tariffPlan;
        mCurrentPrice = currentPrice;
    }
}
