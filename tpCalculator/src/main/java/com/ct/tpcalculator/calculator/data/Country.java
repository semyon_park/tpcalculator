package com.ct.tpcalculator.calculator.data;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by USER on 15.06.2015.
 */
public class Country implements Serializable {

    private LocalizedStrings titles; // key - language code(ru, uz, en ...); value - title by language
    private String numberPrefix;
    private ArrayList<Region> regions;
    private String id;
    private String currency;

    public String getId() {
        return id;
    }

    public LocalizedStrings getTitles() {
        return titles;
    }

    public String getNumberPrefix() {
        return numberPrefix;
    }

    public ArrayList<Region> getRegions() {
        return regions;
    }

    public String getCurrency() {
        return currency;
    }

    public Country(XmlPullParser parser) throws XmlPullParserException, IOException {
        titles = new LocalizedStrings();
        regions = new ArrayList<>();

        for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
            final String attrName = parser.getAttributeName(i);
            final String attrValue = parser.getAttributeValue(i);

            if (attrName.startsWith(XML_TAGS.XML_ATTRIBUTE_TITLE) && attrName.length() > XML_TAGS.XML_ATTRIBUTE_TITLE.length()) {
                String key = attrName.substring(XML_TAGS.XML_ATTRIBUTE_TITLE.length() + 1);
                titles.put(key, attrValue);
            } else if (XML_TAGS.XML_ATTRIBUTE_NUMBER_PREFIX.equalsIgnoreCase(attrName)) {
                numberPrefix = attrValue;
            } else if (XML_TAGS.XML_ATTRIBUTE_ID.equalsIgnoreCase(attrName)) {
                id = attrValue;
            } else if (XML_TAGS.XML_ATTRIBUTE_CURRENCY.equalsIgnoreCase(attrName)){
                currency = attrValue;
            }
        }
        parser.next();

        int eventType = parser.getEventType();
        do {
            if (eventType == XmlPullParser.START_TAG) {
                final String name = parser.getName();
                if (XML_TAGS.XML_TAG_REGION.equalsIgnoreCase(name)) {
                    regions.add(new Region(parser));
                }
            }
            eventType = parser.next();
        } while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_COUNTRY.equals(parser.getName()))
                && (eventType != XmlPullParser.END_DOCUMENT));
    }

    public Region getRegionById(String regionId) {
        for (Region region : regions) {
            if (region.getId().equalsIgnoreCase(regionId)) {
                return region;
            }
        }
        return null;
    }
}
