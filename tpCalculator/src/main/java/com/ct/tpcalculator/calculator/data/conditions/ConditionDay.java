package com.ct.tpcalculator.calculator.data.conditions;

import com.ct.tpcalculator.calculator.data.DataDirectionTypes;
import com.ct.tpcalculator.calculator.data.XML_TAGS;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticCallForCalculation;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticInternetTrafficForCalculation;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticMessageForCalculation;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by USER on 16.06.2015.
 */
public class ConditionDay extends Condition  implements Serializable {

    private ArrayList<Day> days;
    public ArrayList<Day> getDays() {
        return days;
    }

    public ConditionDay(XmlPullParser parser) throws XmlPullParserException, IOException {
        super(parser);
        days = new ArrayList<>();

        int eventType = parser.getEventType();
        do {
            if (eventType == XmlPullParser.START_TAG) {
                final String name = parser.getName();
                if (XML_TAGS.XML_TAG_DAY.equalsIgnoreCase(name)) {
                    getDays().add(new Day(parser));
                }
            }
            eventType = parser.next();
        }
        while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_CONDITION_DAY.equals(parser.getName()))
                && (eventType != XmlPullParser.END_DOCUMENT));
    }

    @Override
    public void calculatePrice(StatisticCallForCalculation call) {
        Calendar callDate = Calendar.getInstance();
        callDate.setTime(call.getCallDate());
        for (Day conditionDay:days){
            if (conditionDay.getWeekday() == callDate.get(Calendar.DAY_OF_WEEK)){
                call.setPrice(call.getNotCalculatedMinutes() * conditionDay.getPrice());
                call.setCalculated(true);
                return;
            }
        }
    }

    @Override
    public void calculatePrice(StatisticMessageForCalculation message) {
        Calendar messageDate = Calendar.getInstance();
        messageDate.setTime(message.getMessageDate());
        for (Day conditionDay:days){
            if (conditionDay.getWeekday() == messageDate.get(Calendar.DAY_OF_WEEK)){
                message.setPrice(conditionDay.getPrice());
                message.setCalculated(true);
                return;
            }
        }
    }

    @Override
    public void calculatePrice(StatisticInternetTrafficForCalculation internetTraffic, int dataDirectionType) {
        Calendar internetTrafficDate = Calendar.getInstance();
        internetTrafficDate.setTime(internetTraffic.getDate());
        for (Day conditionDay:days){
            if (conditionDay.getWeekday() == internetTrafficDate.get(Calendar.DAY_OF_WEEK)){
                int traffic = 0;
                if (dataDirectionType == DataDirectionTypes.INCOMING){
                    traffic = internetTraffic.getMobileReceivedMegaBytes() - internetTraffic.getMobileReceivedCalculatedMegaBytes();
                    internetTraffic.addMobileReceivedCalculatedMegaBytes(traffic);
                }else if (dataDirectionType == DataDirectionTypes.OUTGOING){
                    traffic = internetTraffic.getMobileTransmittedMegaBytes() - internetTraffic.getMobileTransmittedCalculatedMegaBytes();
                    internetTraffic.addMobileTransmittedCalculatedMegaBytes(traffic);
                }
                internetTraffic.setCalculatedPrice(internetTraffic.getCalculatedPrice() + (traffic * conditionDay.getPrice()));
                return;
            }
        }
    }

    public class Day  implements Serializable{

        private String name;
        private double price;
        private int weekday; //Calendar.DAY_OF_WEEK; Sunday = 1, Monday = 2 ...

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }

        public int getWeekday() {
            return weekday;
        }

        public Day(XmlPullParser parser)throws XmlPullParserException, IOException {
            final String tag = parser.getName();
            if (XML_TAGS.XML_TAG_DAY.equalsIgnoreCase(tag)) {
                for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
                    final String attrName = parser.getAttributeName(i);
                    final String attrValue = parser.getAttributeValue(i);
                    if (XML_TAGS.XML_ATTRIBUTE_NAME.equalsIgnoreCase(attrName)) {
                        name = attrValue;
                    }else if (XML_TAGS.XML_ATTRIBUTE_PRICE.equalsIgnoreCase(attrName)){
                        price = Double.parseDouble(attrValue);
                    }else if (XML_TAGS.XML_ATTRIBUTE_WEEKDAY.equalsIgnoreCase(attrName)){
                        weekday = Integer.parseInt(attrValue);
                    }
                }
            }
            parser.next();
        }
    }

}
