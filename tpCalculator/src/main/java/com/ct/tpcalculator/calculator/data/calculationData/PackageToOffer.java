package com.ct.tpcalculator.calculator.data.calculationData;

import android.support.v4.util.Pair;

import com.ct.tpcalculator.calculator.PackageCalculator;
import com.ct.tpcalculator.calculator.data.Service;
import com.ct.tpcalculator.calculator.data.TariffPlan;
import com.ct.tpcalculator.helpers.Utils;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by USER on 06.07.2015.
 */
public class PackageToOffer implements Serializable {

    private TariffPlan tariffPlan;

    private ArrayList<ServiceToOffer> services;

    private ArrayList<ServiceToOffer> enabledServices;

    private final double mPreviousCost;

    private double sumOfCost;

    public PackageToOffer(double previousCost) {
        services = new ArrayList<>();
        enabledServices = new ArrayList<>();
        mPreviousCost = previousCost;
    }

    public ArrayList<ServiceToOffer> getServices() {
        return services;
    }

    public ArrayList<ServiceToOffer> getEnabledServices() {
        return enabledServices;
    }

    public TariffPlan getTariffPlan() {
        return tariffPlan;
    }

    public void setTariffPlan(TariffPlan tariffPlan) {
        this.tariffPlan = tariffPlan;
    }


    public int getServicesCount() {
        return services.size();
    }

    public double getSumOfCost() {
        return sumOfCost;
    }

    public String getFormattedSumOfCost() {
        return String.format("%.2f", sumOfCost);
    }

    public String getFormattedDiffOfCost() {

        NumberFormat format = NumberFormat.getNumberInstance();
        format.setMaximumFractionDigits(2);
        return format.format(getDiffOfCost());
//        return String.format("%.2f", getDiffOfCost());
    }

    public String getFormattedPreviousCost() {
        NumberFormat format = NumberFormat.getNumberInstance();
        format.setMaximumFractionDigits(2);
        return format.format(mPreviousCost);
    }

    public String getFormattedPreviousCostInYear() {
        NumberFormat format = NumberFormat.getNumberInstance();
        format.setMaximumFractionDigits(2);
        return format.format(mPreviousCost * 12);
    }

    public double getPreviousCost() {
        return mPreviousCost;
    }

    public double getDiffOfCost() {
        return mPreviousCost - sumOfCost;
    }

    public String getFormattedDiffOfCostInYear() {
        NumberFormat format = NumberFormat.getNumberInstance();
        format.setMaximumFractionDigits(2);
        return format.format(getDiffOfCost() * 12);
    }

    public void setSumCost(double cost) {
        sumOfCost = cost;
    }

    /**
     * Добавить опцию при условии, что опция является более выгодной по сравнению с текущим списком опций
     * @param service - опция
     */
    public void addService(ServiceToOffer service) {
        boolean addToServiceList = true;

        if (services.isEmpty()) {
            addToServiceList = true;
        } else {
            for (ServiceToOffer serviceB : services) {
                if (isServicesComparable(service, serviceB)) {
                    if (serviceB.getPriceDifferenceWithTp() >= service.getPriceDifferenceWithTp()) {
                        addToServiceList = false;
                        break;
                    } else {
                        services.remove(serviceB);
                        addToServiceList = true;
                        break;
                    }
                }
            }
        }
        if (addToServiceList) {
            services.add(service);
        }
    }

    public void addService(Service service, Date startDate, Date endDate){
        ArrayList<Pair<Date, Date>> datesPeriod = PackageCalculator.splitOnPeriods(startDate, endDate, service.getPriceType());

        for (Pair<Date, Date> datePeriod : datesPeriod) {
            ServiceToOffer serviceToOffer = new ServiceToOffer(service, null, 0, 0,  Utils.getStartOfDay(datePeriod.first));
            services.add(serviceToOffer);
        }
    }

    public void addEnabledService(Service service, Date startDate, Date endDate) {
        ArrayList<Pair<Date, Date>> datesPeriod = PackageCalculator.splitOnPeriods(startDate, endDate, service.getPriceType());

        for (Pair<Date, Date> datePeriod : datesPeriod) {
            ServiceToOffer serviceToOffer = new ServiceToOffer(service, null, 0, 0,  Utils.getStartOfDay(datePeriod.first));
            enabledServices.add(serviceToOffer);
        }
    }

    // Сравниваем два сервиса на то что они подобны
    private boolean isServicesComparable(ServiceToOffer serviceA, ServiceToOffer serviceB) {

        Date startDateA = serviceA.getDateOfActivation();
        Date endDateA = serviceA.getDateOfDeactivation();

        Date startDateB = serviceB.getDateOfActivation();
        Date endDateB = serviceB.getDateOfDeactivation();

        boolean isDatePeriodsInThemselves = getDatePeriodsAreInThemselves(startDateA, endDateA, startDateB, endDateB);

        if (!isDatePeriodsInThemselves) {
            return false;
        }

        boolean serviceAHaveInRegionCalls = serviceA.getService().getCallsCondition() == null ? false : serviceA.getService().getCallsCondition().getInRegionConditions() != null;
        boolean serviceAHaveInterRegionCalls = serviceA.getService().getCallsCondition() == null ? false : serviceA.getService().getCallsCondition().getInterRegionsConditions() != null;
        boolean serviceAHaveInternationalCalls = serviceA.getService().getCallsCondition() == null ? false : serviceA.getService().getCallsCondition().getInterCountriesConditions() != null;

        boolean serviceBHaveInRegionCalls = serviceB.getService().getCallsCondition() == null ? false : serviceB.getService().getCallsCondition().getInRegionConditions() != null;
        boolean serviceBHaveInterRegionCalls = serviceB.getService().getCallsCondition() == null ? false : serviceB.getService().getCallsCondition().getInterRegionsConditions() != null;
        boolean serviceBHaveInternationalCalls = serviceB.getService().getCallsCondition() == null ? false : serviceB.getService().getCallsCondition().getInterCountriesConditions() != null;

        if ((serviceAHaveInRegionCalls && serviceBHaveInRegionCalls) || (serviceAHaveInterRegionCalls && serviceBHaveInterRegionCalls)
                || (serviceAHaveInternationalCalls && serviceBHaveInternationalCalls)) {
            return true;
        }

        boolean serviceAHaveInRegionSmsMessages = serviceA.getService().getSmsCondition() == null ? false : serviceA.getService().getSmsCondition().getInRegionConditions() != null;
        boolean serviceAHaveInterRegionSmsMessages = serviceA.getService().getSmsCondition() == null ? false : serviceA.getService().getSmsCondition().getInterRegionsConditions() != null;
        boolean serviceAHaveInternationalSmsMessages = serviceA.getService().getSmsCondition() == null ? false : serviceA.getService().getSmsCondition().getInterCountriesConditions() != null;

        boolean serviceBHaveInRegionSmsMessages = serviceB.getService().getSmsCondition() == null ? false : serviceB.getService().getSmsCondition().getInRegionConditions() != null;
        boolean serviceBHaveInterRegionSmsMessages = serviceB.getService().getSmsCondition() == null ? false : serviceB.getService().getSmsCondition().getInterRegionsConditions() != null;
        boolean serviceBHaveInternationalSmsMessages = serviceB.getService().getSmsCondition() == null ? false : serviceB.getService().getSmsCondition().getInterCountriesConditions() != null;

        if ((serviceAHaveInRegionSmsMessages && serviceBHaveInRegionSmsMessages) || (serviceAHaveInterRegionSmsMessages && serviceBHaveInterRegionSmsMessages)
                || (serviceAHaveInternationalSmsMessages && serviceBHaveInternationalSmsMessages)) {
            return true;
        }

        boolean serviceAHaveInRegionMmsMessages = serviceA.getService().getMmsCondition() == null ? false : serviceA.getService().getMmsCondition().getInRegionConditions() != null;
        boolean serviceAHaveInterRegionMmsMessages = serviceA.getService().getMmsCondition() == null ? false : serviceA.getService().getMmsCondition().getInterRegionsConditions() != null;
        boolean serviceAHaveInternationalMmsMessages = serviceA.getService().getMmsCondition() == null ? false : serviceA.getService().getMmsCondition().getInterCountriesConditions() != null;

        boolean serviceBHaveInRegionMmsMessages = serviceB.getService().getMmsCondition() == null ? false : serviceB.getService().getMmsCondition().getInRegionConditions() != null;
        boolean serviceBHaveInterRegionMmsMessages = serviceB.getService().getMmsCondition() == null ? false : serviceB.getService().getMmsCondition().getInterRegionsConditions() != null;
        boolean serviceBHaveInternationalMmsMessages = serviceB.getService().getMmsCondition() == null ? false : serviceB.getService().getSmsCondition().getInterCountriesConditions() != null;

        if ((serviceAHaveInRegionMmsMessages && serviceBHaveInRegionMmsMessages) || (serviceAHaveInterRegionMmsMessages && serviceBHaveInterRegionMmsMessages)
                || (serviceAHaveInternationalMmsMessages && serviceBHaveInternationalMmsMessages)) {
            return true;
        }

        boolean serviceAHaveInternetTraffic = serviceA.getService().getInternetCondition() != null;
        boolean serviceBHaveInternetTraffic = serviceB.getService().getInternetCondition() != null;

        return serviceAHaveInternetTraffic && serviceBHaveInternetTraffic;

    }

    private boolean getDatePeriodsAreInThemselves(Date startDateA, Date endDateA, Date startDateB, Date endDateB) {
        return ((startDateA.before(startDateB) || startDateA.equals(startDateB)) && (endDateA.after(endDateB) || endDateA.equals(endDateB))) ||
                ((startDateB.equals(startDateA) || startDateB.before(startDateA)) && (endDateB.equals(endDateA) || endDateB.after(endDateA)));
    }
}
