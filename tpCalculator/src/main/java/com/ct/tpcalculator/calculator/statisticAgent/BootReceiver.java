package com.ct.tpcalculator.calculator.statisticAgent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.ct.tpcalculator.calculator.statisticAgent.callstat.AlarmCallReceiver;
import com.ct.tpcalculator.calculator.statisticAgent.msgstat.AlarmMsgReceiver;
import com.ct.tpcalculator.calculator.statisticAgent.netstat.AlarmNetReceiver;

/**
 * Created by Andrey on 08.07.2015.
 */
public class BootReceiver extends BroadcastReceiver{

    private static final String KEY_LAST_TOTAL_TX = "KEY_LAST_TOTAL_TX";
    private static final String KEY_LAST_TOTAL_RX = "KEY_LAST_TOTAL_RX";
    private static final String KEY_LAST_MOB_TX = "KEY_LAST_MOB_TX";
    private static final String KEY_LAST_MOB_RX = "KEY_LAST_MOB_RX";

    private static final String KEY_COUNT_WIFI_TX_TRAFFIC = "KEY_COUNT_WIFI_TX_TRAFFIC";
    private static final String KEY_COUNT_MOB_TX_TRAFFIC = "KEY_COUNT_MOB_TX_TRAFFIC";
    private static final String KEY_COUNT_WIFI_RX_TRAFFIC = "KEY_COUNT_WIFI_RX_TRAFFIC";
    private static final String KEY_COUNT_MOB_RX_TRAFFIC = "KEY_COUNT_MOB_RX_TRAFFIC";

    private static final long DELAY_START = 10 * 1000;

    public void onReceive(Context context, Intent intent) {
        e("onReceive(): called");

//        Проверка значений после перезагрузки произходит в NetStatCore.check() методе
//
//        SharedPreferences mSettings = context.getSharedPreferences(Consts.APP_PACKAGE, Context.MODE_PRIVATE);
//        long lastTotalTxBytes = mSettings.getLong(KEY_LAST_TOTAL_TX, 0);
//        long lastTotalRxBytes = mSettings.getLong(KEY_LAST_TOTAL_RX, 0);
//        long lastMobileTxBytes;
//        long lastMobileRxBytes;
//
//        long lastSnapTime = System.currentTimeMillis();
//
//        long currTotalTxBytes = TrafficStats.getTotalTxBytes();
//        long currTotalRxBytes = TrafficStats.getTotalRxBytes();
//        long currMobileTxBytes = TrafficStats.getMobileTxBytes();
//        long currMobileRxBytes = TrafficStats.getMobileRxBytes();
//
//        if ((currTotalTxBytes <= lastTotalTxBytes)
//                || (currTotalRxBytes <= lastTotalRxBytes)) {
//            e("onReceive(): ReInit to null");
//
//            lastTotalTxBytes = currTotalTxBytes;
//            lastTotalRxBytes = currTotalRxBytes;
//            lastMobileTxBytes = currMobileTxBytes;
//            lastMobileRxBytes = currMobileRxBytes;
//
//            SharedPreferences.Editor e = mSettings.edit();
//            e.putLong(KEY_LAST_TOTAL_RX, lastTotalRxBytes);
//            e.putLong(KEY_LAST_TOTAL_TX, lastTotalTxBytes);
//            e.putLong(KEY_LAST_MOB_RX, lastMobileRxBytes);
//            e.putLong(KEY_LAST_MOB_TX, lastMobileTxBytes);
//            e.putLong(KEY_LAST_SNAP_TIME, lastSnapTime);
//            e.commit();
//        }

        //------------------------------/ Set alarms /-------------------------------
        AlarmNetReceiver.setAlarm(context);
        AlarmCallReceiver.setAlarm(context);
        AlarmMsgReceiver.setAlarm(context);

    }

    //------------------------------------------------------------------------------------------
    private static boolean debug = Consts.DEBUG;
    private static String className = "BootReceiver.";

    private static void e(String message) {
        if (debug) {
            Log.e(Consts.APP_TAG, className + message);
        }
    }
    //------------------------------------------------------------------------------------------
}
