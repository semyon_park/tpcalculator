package com.ct.tpcalculator.calculator;

import android.support.v4.util.Pair;

import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.calculator.data.MobileOperator;
import com.ct.tpcalculator.calculator.data.Service;
import com.ct.tpcalculator.calculator.data.TariffPlan;
import com.ct.tpcalculator.calculator.data.calculationData.*;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticLogs;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by USER on 24.06.2015.
 */
public class ExpenseController {

    private static ExpenseController instance;

    public static ExpenseController getInstance() {
        if (instance == null) {
            instance = new ExpenseController();
        }
        return instance;
    }

    public ExpenseController() {
        super();
    }


    /**
     * Подсчитывает кол-во потраченых денег
     *
     * @param startDate  - начальная дата.
     * @param endDate    - конечная дата.
     * @param tariffPlan - тарифный план
     * @param services   - подключенный дополнительные опции
     * @param logs       - логи (звонки, смс, интернет)
     * @return - кол-во потраченых денег
     */
    public double calculateExpense(Date startDate,
                                   Date endDate,
                                   TariffPlan tariffPlan,
                                   ArrayList<Pair<Service, Date>> services,
                                   StatisticLogs logs) {

        StatisticLogsForCalculation statisticLogsForCalculation = new StatisticLogsForCalculation(logs);
        double price = 0;

        for (Pair<Service, Date> service : services) {
            PackageCalculator serviceCalculator = new PackageCalculator(service.first, service.second);
            price += serviceCalculator.CalculateTheCost(startDate, endDate, statisticLogsForCalculation);
        }

        PackageCalculator tpCalculator = new PackageCalculator(tariffPlan, startDate);
        price += tpCalculator.CalculateTheCost(startDate, endDate, statisticLogsForCalculation);

        price += SumAllLogs(statisticLogsForCalculation);

        System.out.println(price);

        return price;
    }

    /**
     * Подсчитать расход за определенное время при выбраном тарифе и доп. опций
     * @param startDate дата начала
     * @param endDate дата клнца
     * @param logs - логи за период времени startDate - endDate
     * @param tariffPlan тарифный план
     * @param services - доп. опции
     * @param mustHaveServices - опции которые подключаются вместе с тарифом (обязательные подключенные опции)
     * @return расход (стоимость)
     */
    public double calculateExpense(Date startDate,
                                    Date endDate,
                                    StatisticLogsForCalculation logs,
                                    TariffPlan tariffPlan,
                                    ArrayList<ServiceToOffer> services,
                                    ArrayList<ServiceToOffer> mustHaveServices) {
        double price = 0;

        for (ServiceToOffer service : mustHaveServices) {
            PackageCalculator serviceCalculator = new PackageCalculator(service.getService(), service.getDateOfActivation());
            price += serviceCalculator.CalculateTheCost(startDate, endDate, logs);
        }

        for (ServiceToOffer service : services) {
            PackageCalculator serviceCalculator = new PackageCalculator(service.getService(), service.getDateOfActivation());
            price += serviceCalculator.CalculateTheCost(startDate, endDate, logs);
        }

        PackageCalculator tpCalculator = new PackageCalculator(tariffPlan, startDate);
        price += tpCalculator.CalculateTheCost(startDate, endDate, logs);

     price += SumAllLogs(logs);

        return price;
    }

    /**
     * Выбирает более дешевый тарифный план с дополнительными опциями относительно данной суммы.
     *
     * @param startDate   - начальная дата.
     * @param endDate     - конечная дата.
     * @param currentCost - текущие затраты
     * @param logs        - логи
     * @return - Возращает список более дешевых тарифных планов с дополнительными опциями
     */
    public ArrayList<PackagesToOffer> calculatePackagesToOffer(Date startDate,
                                                               Date endDate,
                                                               double currentCost,
                                                               StatisticLogs logs) {
        ArrayList<PackagesToOffer> calculatePackagesToOffer = new ArrayList<>();
        for (MobileOperator mobileOperator : Application.getInstance().getRegion().getMobileOperators()) {

            ArrayList<PackageToOffer> packageToOfferArrayList = new ArrayList<>();

            for (TariffPlan tariffPlan : mobileOperator.getTariffPlans()) {
                // Если тарифный план в архиве то не предлагать его.
                if (tariffPlan.getIsArchive()){
                    continue;
                }

                ArrayList<Service> availableServices = mobileOperator.getAvailableServices(tariffPlan.getAvailableServices());
                ArrayList<Service> mustHaveServices = mobileOperator.getAvailableServices(tariffPlan.getMustHaveServices());

                PackageToOffer packageToOffer = new PackageToOffer(currentCost);
                packageToOffer.setTariffPlan(tariffPlan);
                for (Service service : mustHaveServices) {
                    packageToOffer.addEnabledService(service, startDate, endDate);
                }

                for (Service service : availableServices) {
                    if (mustHaveServices.contains(service)){
                        continue;
                    }
                    ArrayList<Pair<Date, Date>> datesPeriod = PackageCalculator.splitOnPeriods(startDate, endDate, service.getPriceType());

                    for (Pair<Date, Date> datePeriod : datesPeriod) {
                        PackageCalculator serviceCalculator = new PackageCalculator(service, datePeriod.first);

                        StatisticLogsForCalculation serviceLogs = new StatisticLogsForCalculation(logs);
                        double price = serviceCalculator.CalculateTheCost(datePeriod.first, datePeriod.second, serviceLogs);
                        serviceLogs.saveOnlyCalculatedLogs();
                        price += SumAllLogs(serviceLogs);

                        StatisticLogsForCalculation tpLogs = null;
                        try {
                            tpLogs = serviceLogs.clone();
                        } catch (CloneNotSupportedException e) {
                            e.printStackTrace();
                        }
                        if (tpLogs == null) {
                            continue;
                        }

                        double tpPrice = 0;
                        // Расчитываем стоимость в доп. оциях, которые идут вместе с ТП (обязательные опции)
                        for (Service mustHaveService : mustHaveServices) {
                            PackageCalculator enabledServiceCalculator = new PackageCalculator(mustHaveService, datePeriod.first);
                            enabledServiceCalculator.CalculateTheCost(datePeriod.first, datePeriod.second, tpLogs);
                        }
                        // Расчитываем стоимость ТП за период действия опции
                        PackageCalculator tpCalculator = new PackageCalculator(tariffPlan, datePeriod.first);
                        tpCalculator.CalculateTheCost(datePeriod.first, datePeriod.second, tpLogs);
                        tpPrice += SumAllLogs(tpLogs);

                        if (price < tpPrice) {
                            ServiceToOffer serviceToOffer = new ServiceToOffer(service, serviceLogs, price, tpPrice - price, datePeriod.first);
                            packageToOffer.addService(serviceToOffer);
                        }
                    }
                }

                StatisticLogsForCalculation packageLogs = new StatisticLogsForCalculation(logs);
                packageToOffer.setSumCost(calculateExpense(startDate, endDate, packageLogs, packageToOffer.getTariffPlan(), packageToOffer.getServices(), packageToOffer.getEnabledServices()));

                if (packageToOffer.getSumOfCost() < currentCost) {
                    packageToOfferArrayList.add(packageToOffer);
                }
            }

            if (!packageToOfferArrayList.isEmpty()) {
                PackagesToOffer packagesToOffer = new PackagesToOffer(packageToOfferArrayList, mobileOperator);
                calculatePackagesToOffer.add(packagesToOffer);
            }
        }
        return calculatePackagesToOffer;
    }

    /**
     * Суммирует стоимость всех логов которые вышли за пределы лимита
     */
    private double SumAllLogs(StatisticLogsForCalculation statisticLogs) {
        double price = 0;

        ArrayList[] allCalls = new ArrayList[]{statisticLogs.getCallsForCalculation().getInRegionCallsForCalculation(),
                statisticLogs.getCallsForCalculation().getInterRegionCallsForCalculation(), statisticLogs.getCallsForCalculation().getInternationalCallsForCalculation()};
        for (ArrayList<StatisticCallForCalculation> calls : allCalls) {
            for (StatisticCallForCalculation call : calls) {
                price += call.getPrice();
            }
        }

        ArrayList[] allSmsMessages = new ArrayList[]{statisticLogs.getSmsMessagesForCalculation().getInRegionMessagesForCalculation(),
                statisticLogs.getSmsMessagesForCalculation().getInterRegionMessagesForCalculation(), statisticLogs.getSmsMessagesForCalculation().getInternationalMessagesForCalculation()};
        for (ArrayList<StatisticMessageForCalculation> smsMessages : allSmsMessages) {
            for (StatisticMessageForCalculation smsMessage : smsMessages) {
                price += smsMessage.getPrice();
            }
        }

        ArrayList[] allMmsMessages = new ArrayList[]{statisticLogs.getMmsMessagesForCalculation().getInRegionMessagesForCalculation(),
                statisticLogs.getMmsMessagesForCalculation().getInterRegionMessagesForCalculation(), statisticLogs.getMmsMessagesForCalculation().getInternationalMessagesForCalculation()};
        for (ArrayList<StatisticMessageForCalculation> mmsMessages : allMmsMessages) {
            for (StatisticMessageForCalculation mmsMessage : mmsMessages) {
                price += mmsMessage.getPrice();
            }
        }

        for (StatisticInternetTrafficForCalculation internetTraffic : statisticLogs.getInternetTrafficsForCalculation().getMobileInternetTraffic()) {
            price += internetTraffic.getCalculatedPrice();
        }

        return price;
    }

}
