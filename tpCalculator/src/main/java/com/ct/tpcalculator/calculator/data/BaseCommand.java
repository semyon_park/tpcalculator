package com.ct.tpcalculator.calculator.data;

import java.io.Serializable;

/**
 * Created by USER on 17.06.2015.
 */
public class BaseCommand  implements Serializable {

    protected String id;

    public String getId(){
        return id;
    }

    public BaseCommand(){
    }
}
