package com.ct.tpcalculator.calculator.data;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by USER on 16.06.2015.
 */
public class Limit implements Serializable {

    public static final int LIMIT_TYPE_INCOMING = 0;
    public static final int LIMIT_TYPE_OUTGOING = 1;
    public static final int LIMIT_TYPE_MIX = 2;

    private int value;
    private int limitType;
    private String numberPrefix;
    private LocalizedStrings titles; // key - language code(ru, uz, en ...); value - title by language
    private String id;

    public int getValue() {
        return value;
    }

    public int getLimitType() {
        return limitType;
    }

    public String getNumberPrefix() {
        return numberPrefix;
    }

    public LocalizedStrings getTitles() {
        return titles;
    }

    public String getId() {
        return id;
    }

    public Limit(XmlPullParser parser) throws XmlPullParserException, IOException {
        final String tag = parser.getName();
        titles = new LocalizedStrings();
        if (XML_TAGS.XML_TAG_LIMIT.equalsIgnoreCase(tag)) {
            for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
                final String attrName = parser.getAttributeName(i);
                final String attrValue = parser.getAttributeValue(i);
                if (XML_TAGS.XML_ATTRIBUTE_VALUE.equalsIgnoreCase(attrName)) {
                    value = Integer.parseInt(attrValue);
                } else if (XML_TAGS.XML_ATTRIBUTE_TYPE.equalsIgnoreCase(attrName)) {
                    if (attrValue.equalsIgnoreCase("incoming")) {
                        limitType = LIMIT_TYPE_INCOMING;
                    } else if (attrValue.equalsIgnoreCase("outgoing")) {
                        limitType = LIMIT_TYPE_OUTGOING;
                    } else if (attrValue.equalsIgnoreCase("mix")) {
                        limitType = LIMIT_TYPE_MIX;
                    } else {
                        limitType = LIMIT_TYPE_MIX;
                    }
                } else if (XML_TAGS.XML_ATTRIBUTE_NUMBER_PREFIX.equalsIgnoreCase(attrName)) {
                    numberPrefix = attrValue;
                } else if (attrName.startsWith(XML_TAGS.XML_ATTRIBUTE_TITLE) && attrName.length() > XML_TAGS.XML_ATTRIBUTE_TITLE.length()) {
                    String key = attrName.substring(XML_TAGS.XML_ATTRIBUTE_TITLE.length() + 1);
                    titles.put(key, attrValue);
                } else if (XML_TAGS.XML_ATTRIBUTE_ID.equalsIgnoreCase(attrName)){
                    id = attrValue;
                }
            }
        }
        parser.next();
    }
}
