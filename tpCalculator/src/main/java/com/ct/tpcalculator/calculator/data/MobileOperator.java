package com.ct.tpcalculator.calculator.data;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class MobileOperator implements Serializable {

    private String id;
    private LocalizedStrings titles;
    private ArrayList<String> numberPrefix;
    private ArrayList<TariffPlan> tariffPlans;
    private ArrayList<Service> services;
    private Commands commands;
    private String iconPath;

    public String getId() {
        return id;
    }

    public LocalizedStrings getTitles() {
        return titles;
    }

    public ArrayList<String> getNumberPrefix() {
        return numberPrefix;
    }

    public ArrayList<TariffPlan> getTariffPlans() {
        return tariffPlans;
    }

    public ArrayList<Service> getServices() {
        return services;
    }

    public Commands getCommands() {
        return commands;
    }

    public String getIconPath() {
        return iconPath;
    }

    public MobileOperator(XmlPullParser parser) throws XmlPullParserException, IOException {
        titles = new LocalizedStrings();
        tariffPlans = new ArrayList<>();
        services = new ArrayList<>();
        numberPrefix = new ArrayList<>();

        final String tag = parser.getName();
        if (XML_TAGS.XML_TAG_OPERATOR.equalsIgnoreCase(tag)) {
            for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
                final String attrName = parser.getAttributeName(i);
                final String attrValue = parser.getAttributeValue(i);
                if (XML_TAGS.XML_ATTRIBUTE_ID.equalsIgnoreCase(attrName)) {
                    id = attrValue;
                } else if (attrName.startsWith(XML_TAGS.XML_ATTRIBUTE_TITLE) && attrName.length() > XML_TAGS.XML_ATTRIBUTE_TITLE.length()) {
                    getTitles().put(attrName.substring(XML_TAGS.XML_ATTRIBUTE_TITLE.length() + 1), attrValue);
                } else if (XML_TAGS.XML_ATTRIBUTE_NUMBER_PREFIX.equalsIgnoreCase(attrName)) {
                    String[] values = attrValue.split(";");
                    numberPrefix = new ArrayList<>(Arrays.asList(values));
                } else if (attrName.equalsIgnoreCase(XML_TAGS.XML_ATTRIBUTE_ICON)) {
                    iconPath = attrValue;
                }
            }
        }
        parser.next();

        int eventType = parser.getEventType();
        do {
            if (eventType == XmlPullParser.START_TAG) {
                final String name = parser.getName();
                if (XML_TAGS.XML_TAG_TARIFF_PLAN.equalsIgnoreCase(name)) {
                    getTariffPlans().add(new TariffPlan(parser));
                } else if (XML_TAGS.XML_TAG_SERVICE.equalsIgnoreCase(name)) {
                    getServices().add(new Service(parser));
                } else if (XML_TAGS.XML_TAG_COMMANDS.equalsIgnoreCase(name)) {
                    commands = new Commands(parser);
                }
            }
            eventType = parser.next();
        }
        while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_OPERATOR.equals(parser.getName()))
                && (eventType != XmlPullParser.END_DOCUMENT));
    }

    public ArrayList<Service> getAvailableServices(ArrayList<String> serviceCodes) {
        ArrayList<Service> services = new ArrayList<>();
        if (serviceCodes != null) {
            for (String code : serviceCodes) {
                Service service = findService(code);
                if (service != null) {
                    services.add(service);
                }
            }
        }
        return services;
    }

    public Service findService(String serviceId) {
        for (Service s : services) {
            if (s.getId().equalsIgnoreCase(serviceId))
                return s;
        }
        return null;
    }

    public TariffPlan findTariffPlan(String tariffPlainId) {
        for (TariffPlan t : tariffPlans) {
            if (t.getId().equalsIgnoreCase(tariffPlainId))
                return t;
        }
        return null;
    }
}
