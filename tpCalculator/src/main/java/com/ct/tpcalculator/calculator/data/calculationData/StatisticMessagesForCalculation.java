package com.ct.tpcalculator.calculator.data.calculationData;

import com.ct.tpcalculator.calculator.data.statisticData.StatisticMessage;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticMessages;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by USER on 10.07.2015.
 */
public class StatisticMessagesForCalculation implements Serializable {

    public ArrayList<StatisticMessageForCalculation> getInRegionMessagesForCalculation() {
        return inRegionMessagesForCalculation;
    }

    public ArrayList<StatisticMessageForCalculation> getInterRegionMessagesForCalculation() {
        return interRegionMessagesForCalculation;
    }

    public ArrayList<StatisticMessageForCalculation> getInternationalMessagesForCalculation() {
        return internationalMessagesForCalculation;
    }

    private final ArrayList<StatisticMessageForCalculation> inRegionMessagesForCalculation;
    private final ArrayList<StatisticMessageForCalculation> interRegionMessagesForCalculation;
    private final ArrayList<StatisticMessageForCalculation> internationalMessagesForCalculation;

    public StatisticMessagesForCalculation (StatisticMessages messages){
        inRegionMessagesForCalculation = new ArrayList<>();
        for(StatisticMessage message: messages.getInRegionMessages()){
            inRegionMessagesForCalculation.add(new StatisticMessageForCalculation(message));
        }

        interRegionMessagesForCalculation = new ArrayList<>();
        for(StatisticMessage message: messages.getInterRegionMessages()){
            interRegionMessagesForCalculation.add(new StatisticMessageForCalculation(message));
        }

        internationalMessagesForCalculation = new ArrayList<>();
        for (StatisticMessage message: messages.getInternationalMessages()){
            internationalMessagesForCalculation.add(new StatisticMessageForCalculation(message));
        }
    }

    private StatisticMessagesForCalculation(ArrayList<StatisticMessageForCalculation> inRegionMessagesForCalculation,
            ArrayList<StatisticMessageForCalculation> interRegionMessagesForCalculation,
            ArrayList<StatisticMessageForCalculation> internationalMessagesForCalculation){
        this.inRegionMessagesForCalculation = inRegionMessagesForCalculation;
        this.interRegionMessagesForCalculation = interRegionMessagesForCalculation;
        this.internationalMessagesForCalculation = internationalMessagesForCalculation;
    }
    @Override
    public StatisticMessagesForCalculation clone() throws CloneNotSupportedException {
        ArrayList<StatisticMessageForCalculation> inRegionMessagesForCalculation = cloneList(this.inRegionMessagesForCalculation);
        ArrayList<StatisticMessageForCalculation> interRegionMessagesForCalculation = cloneList(this.interRegionMessagesForCalculation);
        ArrayList<StatisticMessageForCalculation> internationalMessagesForCalculation = cloneList(this.internationalMessagesForCalculation);
        return new StatisticMessagesForCalculation(inRegionMessagesForCalculation, interRegionMessagesForCalculation, internationalMessagesForCalculation);
    }

    private ArrayList<StatisticMessageForCalculation> cloneList(ArrayList<StatisticMessageForCalculation> messages) {
        ArrayList<StatisticMessageForCalculation> clone = new ArrayList<>();
        for(StatisticMessageForCalculation message:messages)clone.add(message.clone());
        return clone;
    }

    public ArrayList<StatisticMessageForCalculation> getAllMessages(){
        ArrayList<StatisticMessageForCalculation> result = new ArrayList<>();
        result.addAll(inRegionMessagesForCalculation);
        result.addAll(interRegionMessagesForCalculation);
        result.addAll(internationalMessagesForCalculation);
        return result;
    }
}
