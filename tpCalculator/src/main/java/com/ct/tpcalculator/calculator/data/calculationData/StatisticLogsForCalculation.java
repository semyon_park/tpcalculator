package com.ct.tpcalculator.calculator.data.calculationData;

import com.ct.tpcalculator.calculator.data.statisticData.StatisticLogs;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by USER on 10.07.2015.
 * Класс хранит в себе данные о логах, для расчета более выгодного ТП и доп. опций
 */
public class StatisticLogsForCalculation  implements Serializable {

    public StatisticCallsForCalculation getCallsForCalculation() {
        return callsForCalculation;
    }

    public StatisticMessagesForCalculation getSmsMessagesForCalculation() {
        return smsMessagesForCalculation;
    }

    public StatisticMessagesForCalculation getMmsMessagesForCalculation() {
        return mmsMessagesForCalculation;
    }

    public StatisticInternetTrafficsForCalculation getInternetTrafficsForCalculation() {
        return internetTrafficsForCalculation;
    }

    private final StatisticCallsForCalculation callsForCalculation;
    private final StatisticMessagesForCalculation smsMessagesForCalculation;
    private final StatisticMessagesForCalculation mmsMessagesForCalculation;
    private final StatisticInternetTrafficsForCalculation internetTrafficsForCalculation;

    public StatisticLogsForCalculation(StatisticLogs logs) {
        callsForCalculation = logs.getCalls()== null ? null : new StatisticCallsForCalculation(logs.getCalls());
        smsMessagesForCalculation = logs.getSmsMessages() == null ? null : new StatisticMessagesForCalculation(logs.getSmsMessages());
        mmsMessagesForCalculation = logs.getMmsMessages() == null ? null : new StatisticMessagesForCalculation(logs.getMmsMessages());
        internetTrafficsForCalculation = logs.getInternetTraffics() == null ? null:new StatisticInternetTrafficsForCalculation(logs.getInternetTraffics());
    }

    private  StatisticLogsForCalculation(StatisticCallsForCalculation callsForCalculation, StatisticMessagesForCalculation smsMessagesForCalculation,
            StatisticMessagesForCalculation mmsMessagesForCalculation, StatisticInternetTrafficsForCalculation internetTrafficsForCalculation){
        this.callsForCalculation = callsForCalculation;
        this.smsMessagesForCalculation = smsMessagesForCalculation;
        this.mmsMessagesForCalculation = mmsMessagesForCalculation;
        this.internetTrafficsForCalculation = internetTrafficsForCalculation;
    }
    /*
   // сохранить только те логи, которые мы расчитали
    */
    public void saveOnlyCalculatedLogs() {
        ArrayList[] calls = new ArrayList[]{this.callsForCalculation.getInRegionCallsForCalculation(),
                this.callsForCalculation.getInterRegionCallsForCalculation(), this.callsForCalculation.getInternationalCallsForCalculation()};

        for (ArrayList<StatisticCallForCalculation> statisticCalls : calls) {
            for (int i = statisticCalls.size() - 1; i >= 0; i--) {
                StatisticCallForCalculation statisticCall = statisticCalls.get(i);
                if (!statisticCall.isCalculated()) {
                    statisticCalls.remove(statisticCall);
                }
            }
        }

        ArrayList[] smsMessages = new ArrayList[]{this.smsMessagesForCalculation.getInRegionMessagesForCalculation(),
                this.smsMessagesForCalculation.getInterRegionMessagesForCalculation(), this.smsMessagesForCalculation.getInternationalMessagesForCalculation()};
        for (ArrayList<StatisticMessageForCalculation> statisticSmsMessages : smsMessages) {
            for (int i = statisticSmsMessages.size() - 1; i >= 0; i--) {
                StatisticMessageForCalculation statisticMessage = statisticSmsMessages.get(i);
                if (!statisticMessage.isCalculated()) {
                    statisticSmsMessages.remove(statisticMessage);
                }
            }
        }

        ArrayList[] mmsMessages = new ArrayList[]{this.mmsMessagesForCalculation.getInRegionMessagesForCalculation(),
                this.mmsMessagesForCalculation.getInterRegionMessagesForCalculation(), this.mmsMessagesForCalculation.getInternationalMessagesForCalculation()};
        for (ArrayList<StatisticMessageForCalculation> statisticMmsMessages : mmsMessages) {
            for (int i = statisticMmsMessages.size() - 1; i >= 0; i--) {
                StatisticMessageForCalculation statisticMessage = statisticMmsMessages.get(i);
                if (!statisticMessage.isCalculated()) {
                    statisticMmsMessages.remove(statisticMessage);
                }
            }
        }

        if (internetTrafficsForCalculation != null && internetTrafficsForCalculation.getMobileInternetTraffic() != null) {
            for (int i = internetTrafficsForCalculation.getMobileInternetTraffic().size() - 1; i >= 0; i--) {
                StatisticInternetTrafficForCalculation internetTraffic = internetTrafficsForCalculation.getMobileInternetTraffic().get(i);
                internetTraffic.setMobileReceivedMegaBytes(internetTraffic.getMobileReceivedCalculatedMegaBytes());
                internetTraffic.setMobileTransmittedMegaBytes(internetTraffic.getMobileTransmittedCalculatedMegaBytes());
//                if (internetTraffic.isCalculated()){
//                    getInternetTraffics().remove(internetTraffic);
//                }
            }
        }
    }

    @Override
    public StatisticLogsForCalculation clone() throws CloneNotSupportedException {
        return  new StatisticLogsForCalculation(callsForCalculation.clone(), smsMessagesForCalculation.clone(), mmsMessagesForCalculation.clone(), internetTrafficsForCalculation.clone());
    }
}
