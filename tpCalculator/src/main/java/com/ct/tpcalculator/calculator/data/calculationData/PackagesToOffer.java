package com.ct.tpcalculator.calculator.data.calculationData;

import com.ct.tpcalculator.calculator.data.MobileOperator;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by USER on 06.07.2015.
 */
public class PackagesToOffer implements Serializable {

    private final ArrayList<PackageToOffer> packagesToOffer;
    private final MobileOperator mobileOperator;

    public ArrayList<PackageToOffer> getPackagesToOffer() {
        return packagesToOffer;
    }

    public MobileOperator getMobileOperator() {
        return mobileOperator;
    }

    public PackagesToOffer(ArrayList<PackageToOffer> packagesToOffer, MobileOperator mobileOperator){
        this.packagesToOffer = packagesToOffer;
        this.mobileOperator = mobileOperator;
    }
}
