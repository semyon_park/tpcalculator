package com.ct.tpcalculator.calculator.data.statisticData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by USER on 26.06.2015.
 */
public class StatisticInternetTraffics implements Serializable {

    private ArrayList<StatisticInternetTraffic> wifiInternetTraffic;
    private ArrayList<StatisticInternetTraffic> mobileInternetTraffic;

    public void setWifiInternetTraffic(ArrayList<StatisticInternetTraffic> wifiInternetTraffic) {
        this.wifiInternetTraffic = wifiInternetTraffic;
    }

    public void setMobileInternetTraffic(ArrayList<StatisticInternetTraffic> mobileInternetTraffic) {
        this.mobileInternetTraffic = mobileInternetTraffic;
    }

    public ArrayList<StatisticInternetTraffic> getWifiInternetTraffic() {
        return wifiInternetTraffic;
    }

    public ArrayList<StatisticInternetTraffic> getMobileInternetTraffic() {
        return mobileInternetTraffic;
    }

    public long getMobileInternetTrafficValueInBytes(){

        long result = 0;

        for (StatisticInternetTraffic internetTraffic : mobileInternetTraffic) {
            result += internetTraffic.getReceivedBytes() + internetTraffic.getTransmittedBytes();
        }

        return result;
    }

    public void setMobileInternetTrafficInBytes(Date date, long bytes){
        mobileInternetTraffic.clear();
        StatisticInternetTraffic internetTraffic = new StatisticInternetTraffic(StatisticInternetTraffic.TYPE_MOBILE);
        internetTraffic.setReceivedBytes(bytes);
        internetTraffic.setDate(date);
        mobileInternetTraffic.add(internetTraffic);
    }
}
