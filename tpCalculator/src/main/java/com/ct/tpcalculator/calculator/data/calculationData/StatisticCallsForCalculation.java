package com.ct.tpcalculator.calculator.data.calculationData;

import com.ct.tpcalculator.calculator.data.statisticData.StatisticCall;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticCalls;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by USER on 10.07.2015.
 */
public class StatisticCallsForCalculation implements Serializable {

    public ArrayList<StatisticCallForCalculation> getInRegionCallsForCalculation() {
        return inRegionCallsForCalculation;
    }

    public ArrayList<StatisticCallForCalculation> getInterRegionCallsForCalculation() {
        return interRegionCallsForCalculation;
    }

    public ArrayList<StatisticCallForCalculation> getInternationalCallsForCalculation() {
        return internationalCallsForCalculation;
    }

    private final ArrayList<StatisticCallForCalculation> inRegionCallsForCalculation;
    private final ArrayList<StatisticCallForCalculation> interRegionCallsForCalculation;
    private final ArrayList<StatisticCallForCalculation> internationalCallsForCalculation;

    public StatisticCallsForCalculation(StatisticCalls calls) {
        inRegionCallsForCalculation = new ArrayList<>();
        for (StatisticCall call : calls.getInRegionCalls()) {
            inRegionCallsForCalculation.add(new StatisticCallForCalculation(call));
        }

        interRegionCallsForCalculation = new ArrayList<>();
        for(StatisticCall call: calls.getInterRegionCalls()){
            interRegionCallsForCalculation.add(new StatisticCallForCalculation(call));
        }

        internationalCallsForCalculation = new ArrayList<>();
        for (StatisticCall call: calls.getInternationalCalls()){
            internationalCallsForCalculation.add(new StatisticCallForCalculation(call));
        }
    }

    private  StatisticCallsForCalculation(ArrayList<StatisticCallForCalculation> inRegionCallsForCalculation,
            ArrayList<StatisticCallForCalculation> interRegionCallsForCalculation,
            ArrayList<StatisticCallForCalculation> internationalCallsForCalculation){
        this.inRegionCallsForCalculation = inRegionCallsForCalculation;
        this.interRegionCallsForCalculation = interRegionCallsForCalculation;
        this.internationalCallsForCalculation = internationalCallsForCalculation;
    }

    @Override
    public StatisticCallsForCalculation clone() throws CloneNotSupportedException {
        ArrayList<StatisticCallForCalculation> inRegionCallsForCalculation = cloneList(this.inRegionCallsForCalculation);
        ArrayList<StatisticCallForCalculation> interRegionCallsForCalculation = cloneList(this.interRegionCallsForCalculation);
        ArrayList<StatisticCallForCalculation> internationalCallsForCalculation = cloneList(this.internationalCallsForCalculation);
        return new StatisticCallsForCalculation(inRegionCallsForCalculation, interRegionCallsForCalculation, internationalCallsForCalculation);
    }

    private ArrayList<StatisticCallForCalculation> cloneList(ArrayList<StatisticCallForCalculation> calls) {
        ArrayList<StatisticCallForCalculation> clone = new ArrayList<>();
        for(StatisticCallForCalculation call:calls)clone.add(call.clone());
        return clone;
    }

    public ArrayList<StatisticCallForCalculation> getAllCallsForCalculation(){
        ArrayList<StatisticCallForCalculation> allCalls = new ArrayList<>();
        allCalls.addAll(inRegionCallsForCalculation);
        allCalls.addAll(interRegionCallsForCalculation);
        allCalls.addAll(internationalCallsForCalculation);
        return allCalls;
    }
}
