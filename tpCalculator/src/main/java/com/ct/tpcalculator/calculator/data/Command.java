package com.ct.tpcalculator.calculator.data;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by USER on 17.06.2015.
 */
public class Command implements Serializable {

    public final static int TYPE_CHECK_BALANCE = 0;
    public final static int TYPE_GET_TARIFF_PLAN = 1;
    public final static int TYPE_CHANGE_TARIFF_PLAN = 2;

    private ArrayList<BaseCommand> baseCommands;

    private LocalizedStrings titles;

    private int commandType;

    private LocalizedStrings commandTitles;

    public ArrayList<BaseCommand> getBaseCommands() {
        return baseCommands;
    }

    public LocalizedStrings getTitles() {
        return titles;
    }

    public LocalizedStrings getCommandTitles(){
        return commandTitles;
    }

    public int getCommandType() {
        return commandType;
    }


    public Command(XmlPullParser parser) throws XmlPullParserException, IOException {
        titles = new LocalizedStrings();
        commandTitles = new LocalizedStrings();
        baseCommands = new ArrayList<>();

        final String tag = parser.getName();
        if (XML_TAGS.XML_TAG_COMMAND_BLOCK.equalsIgnoreCase(tag)) {
            for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
                final String attrName = parser.getAttributeName(i);
                final String attrValue = parser.getAttributeValue(i);
                if (attrName.startsWith(XML_TAGS.XML_ATTRIBUTE_TITLE) && attrName.length() > XML_TAGS.XML_ATTRIBUTE_TITLE.length()) {
                    String key = attrName.substring(XML_TAGS.XML_ATTRIBUTE_TITLE.length() + 1);
                    titles.put(key, attrValue);
                } else if (attrName.equalsIgnoreCase(XML_TAGS.XML_ATTRIBUTE_TYPE)) {
                    commandType = Integer.parseInt(attrValue);
                }else if (attrName.startsWith(XML_TAGS.XML_ATTRIBUTE_COMMAND_TITLE) && attrName.length() > XML_TAGS.XML_ATTRIBUTE_COMMAND_TITLE.length()) {
                    getCommandTitles().put(attrName.substring(XML_TAGS.XML_ATTRIBUTE_COMMAND_TITLE.length() + 1), attrValue);
                }
            }
        }
        parser.next();

        int eventType = parser.getEventType();
        do {
            if (eventType == XmlPullParser.START_TAG) {
                final String name = parser.getName();
                if (XML_TAGS.XML_TAG_USSD.equalsIgnoreCase(name)) {
                    baseCommands.add(new UssdBaseCommand(parser));
                } else if (XML_TAGS.XML_TAG_SMS.equalsIgnoreCase(name)) {
                    baseCommands.add(new SmsBaseCommand(parser));
                } else if (XML_TAGS.XML_TAG_LINK.equalsIgnoreCase(name)) {
                    baseCommands.add(new LinkBaseCommand(parser));
                }else if (XML_TAGS.XML_TAG_IVR.equalsIgnoreCase(name)){
                    baseCommands.add(new IvrBaseCommand(parser));
                }
            }
            eventType = parser.next();
        }
        while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_COMMAND_BLOCK.equals(parser.getName()))
                && (eventType != XmlPullParser.END_DOCUMENT));

    }

}
