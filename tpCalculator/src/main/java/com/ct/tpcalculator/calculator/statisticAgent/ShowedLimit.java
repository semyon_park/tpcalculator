package com.ct.tpcalculator.calculator.statisticAgent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Semyon on 19.08.2015.
 * Класс служит для сохранения отображенных лимитов
 */
public class ShowedLimit {

    private static final String ID_KEY  = "id";
    private static final String DATE_KEY = "date";
    private static final  String VIEW_TYPE = "view_type";


    public static final int VIEW_TYPE_MINIMUM_STEP = 0; // при достижении минимального порога preferences_minimum_limit_step
    public static final int VIEW_TYPE_LIMIT_IS_UP = 1; // при исчерпании лимита
    public static final int VIEW_TYPE_LIMIT_IS_NOT_SHOWED = 2; // лимит не отображался

    private String id;
    private Date date;
    private int viewType; //view_type - при каких условиях был отображен notification: 0-при достижении минимального порога preferences_minimum_limit_step, 1 - при исчерпании лимита.

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

   public JSONObject getJsonObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ID_KEY, id);
        jsonObject.put(DATE_KEY, date.getTime());
        jsonObject.put(VIEW_TYPE, viewType);
        return jsonObject;
    }

    public ShowedLimit(JSONObject jsonObject) throws JSONException {
        id = jsonObject.getString(ID_KEY);
        date = new Date(jsonObject.getLong(DATE_KEY));
        viewType = jsonObject.getInt(VIEW_TYPE);
    }

    public ShowedLimit(String id, Date date, int viewType){
        this.id = id;
        this.date = date;
        this.viewType = viewType;
    }

}
