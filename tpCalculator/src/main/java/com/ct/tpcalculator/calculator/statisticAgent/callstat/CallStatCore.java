package com.ct.tpcalculator.calculator.statisticAgent.callstat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.ct.tpcalculator.calculator.data.DataDirectionTypes;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticCall;
import com.ct.tpcalculator.calculator.statisticAgent.Consts;
import com.ct.tpcalculator.calculator.statisticAgent.LimitsChecker;
import com.ct.tpcalculator.calculator.statisticAgent.base.SQLHelper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Andrey on 07.07.2015.
 */
public class CallStatCore {


    private CallStatCore() {
    }

    public static void check(Context context) {
        e("check(): call");
        ArrayList<StatisticCall> calls = getCallDetails(context);

        boolean someNewRowsAdded = false;
        for (StatisticCall call : calls) {
            long result = insertToBase(context, call);
            if (result > 0) {
                someNewRowsAdded = true;
            }
        }

        if (someNewRowsAdded) {
            LimitsChecker.checkCallsLimits(context);
        }
    }

    private static long insertToBase(Context context, StatisticCall call) {
        //------------------------------------------------------------------------------------------

//            SimpleDateFormat newFormat = new SimpleDateFormat("dd.MM HH:mm:ss");
//            String timeS = newFormat.format(call.getDate());
//
//            String type = null;
//            switch (call.getType()) {
//                case CallModel.TYPE_INCOMING:
//                    type = "In";
//                    break;
//                case CallModel.TYPE_OUTGOING:
//                    type = "Out";
//                    break;
//                case CallModel.TYPE_MISSED:
//                    type = "Mis";
//                    break;
//            }
//            String message = "insertToBase(): " + timeS + "; Type: " + type + "; Name: " + call.getName() + "; N: " + call.getNumber() + "; Duration: " + call.getDuration();
//            e(message);

        SQLiteDatabase db = SQLHelper.getInstance(context).getWritableDatabase();
        return db.insertWithOnConflict(StatisticCall.KEY_CLASS_TYPE, null, call.toContentValues(), SQLiteDatabase.CONFLICT_IGNORE);

        //------------------------------------------------------------------------------------------
    }

    private static ArrayList<StatisticCall> getCallDetails(Context context) {
        ArrayList<StatisticCall> arr = new ArrayList<>();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            return arr;
        }
        Cursor cursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null /*CallLog.Calls.DATE + " DESC"*/);
        if (cursor == null) {
            return arr;
        }
        int name = cursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        int number = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = cursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = cursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = cursor.getColumnIndex(CallLog.Calls.DURATION);
        int contactId = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            contactId = cursor.getColumnIndex(CallLog.Calls.PHONE_ACCOUNT_ID);
        }

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String nameValue = cursor.getString(name);
                String numberValue = cursor.getString(number);
                int numberType;
                int typeInt = cursor.getInt(type);
                if (typeInt == 1) {
                    numberType = DataDirectionTypes.INCOMING;
                } else if (typeInt == 2) {
                    numberType = DataDirectionTypes.OUTGOING;
                } else {
                    continue;
                }
                int durationValue = cursor.getInt(duration);
                Date dateValue = new Date(cursor.getLong(date));

                if (durationValue > 0) {
                    StatisticCall call = new StatisticCall(contactId, nameValue, numberValue, numberType, dateValue, durationValue);
                    arr.add(call);
                }
            }
        }
        cursor.close();
        return arr;
    }

    private static long dateRFCToLong(String dateString) {
        long dateLong = 0L;
        SimpleDateFormat format = new SimpleDateFormat("EEE dd MMM yyyy HH:mm:ss Z", Locale.US);
        Date date = null;
        try {
            date = format.parse(dateString);
            dateLong = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateLong;
    }

    //------------------------------------------------------------------------------------------
    private static boolean debug = Consts.DEBUG && true;
    private static String className = "CallStatCore.";

    private static void e(String message) {
        if (debug) {
            Log.e(Consts.APP_TAG, className + message);
            lf(message);
        }
    }

    private static void base(Context context) {
        SimpleDateFormat newFormat = new SimpleDateFormat("dd.MM HH:mm:ss");
        String timeS = newFormat.format(System.currentTimeMillis());
        e("#########################/ Call /############################");
        e("LogData: " + timeS);
        SQLiteDatabase db = SQLHelper.getInstance(context).getWritableDatabase();
        String rawQuery = "SELECT * FROM " + StatisticCall.KEY_CLASS_TYPE;
        Cursor cursor = db.rawQuery(rawQuery, null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Date: " + newFormat.format(cursor.getLong((cursor.getColumnIndex(StatisticCall.KEY_DATE)))));
                sb.append("; Type: " + cursor.getInt(cursor.getColumnIndex(StatisticCall.KEY_TYPE)));
                sb.append("; Name: " + cursor.getString(cursor.getColumnIndex(StatisticCall.KEY_NAME)));
                sb.append("; Num: " + cursor.getString(cursor.getColumnIndex(StatisticCall.KEY_NUMBER)));
                sb.append("; Duration: " + cursor.getInt(cursor.getColumnIndex(StatisticCall.KEY_DURATION)));

                e("insert: " + sb.toString());
            }
        }

        e("Cursor count: " + cursor.getCount());
        e("#############################################################");
        e("");

        cursor.close();
    }

    public static ArrayList<StatisticCall> GetStatisticCalls(Context context, Date startDate, Date endDate) {
        SQLiteDatabase db = SQLHelper.getInstance(context).getWritableDatabase();
        String rawQuery = "SELECT * FROM " + StatisticCall.KEY_CLASS_TYPE +
                " WHERE " + StatisticCall.KEY_DATE + " >= " + startDate.getTime() +
                " AND " + StatisticCall.KEY_DATE + " <= " + endDate.getTime() +
                " ORDER BY " + StatisticCall.KEY_DATE;

        Cursor cursor = db.rawQuery(rawQuery, null);
        ArrayList<StatisticCall> calls = new ArrayList<>();

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Date date = new Date(cursor.getLong(cursor.getColumnIndex(StatisticCall.KEY_DATE)));
                int type = cursor.getInt(cursor.getColumnIndex(StatisticCall.KEY_TYPE));
                String name = cursor.getString(cursor.getColumnIndex(StatisticCall.KEY_NAME));
                String number = cursor.getString(cursor.getColumnIndex(StatisticCall.KEY_NUMBER));
                int duration = cursor.getInt(cursor.getColumnIndex(StatisticCall.KEY_DURATION));
                int contactId = cursor.getInt(cursor.getColumnIndex(StatisticCall.KEY_CONTACT_ID));
                StatisticCall call = new StatisticCall(contactId, name, number, type, date, duration);
                calls.add(call);
            }
        }

        cursor.close();

        return calls;
    }

    public static ArrayList<StatisticCall> GetStatisticCalls(Context context, Date startDate, Date endDate, int dataDirectionType) {
        SQLiteDatabase db = SQLHelper.getInstance(context).getWritableDatabase();
        String rawQuery;
        if (dataDirectionType == -1) {
            rawQuery = "SELECT * FROM " + StatisticCall.KEY_CLASS_TYPE +
                    " WHERE " + StatisticCall.KEY_DATE + " >= " + startDate.getTime() +
                    " AND " + StatisticCall.KEY_DATE + " <= " + endDate.getTime() +
                    " ORDER BY " + StatisticCall.KEY_DATE;
        } else {
            rawQuery = "SELECT * FROM " + StatisticCall.KEY_CLASS_TYPE +
                    " WHERE " + StatisticCall.KEY_DATE + " >= " + startDate.getTime() +
                    " AND " + StatisticCall.KEY_DATE + " <= " + endDate.getTime() +
                    " AND " + StatisticCall.KEY_TYPE + " = " + Integer.toString(dataDirectionType) +
                    " ORDER BY " + StatisticCall.KEY_DATE;
        }

        Cursor cursor = db.rawQuery(rawQuery, null);
        ArrayList<StatisticCall> calls = new ArrayList<>();

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Date date = new Date(cursor.getLong(cursor.getColumnIndex(StatisticCall.KEY_DATE)));
                int type = cursor.getInt(cursor.getColumnIndex(StatisticCall.KEY_TYPE));
                String name = cursor.getString(cursor.getColumnIndex(StatisticCall.KEY_NAME));
                String number = cursor.getString(cursor.getColumnIndex(StatisticCall.KEY_NUMBER));
                int duration = cursor.getInt(cursor.getColumnIndex(StatisticCall.KEY_DURATION));
                int contactId = cursor.getInt(cursor.getColumnIndex(StatisticCall.KEY_CONTACT_ID));
                StatisticCall call = new StatisticCall(contactId, name, number, type, date, duration);
                calls.add(call);
            }
        }

        cursor.close();

        return calls;
    }

    private static void d(String message) {
        if (debug) Log.d(Consts.APP_TAG, className + message);
    }

    private static void lf(String message) {
        if (debug) {
            try {
                File logFile = new File(Environment.getExternalStorageDirectory().toString() + "/CallLogFile.txt");

                SimpleDateFormat newFormat = new SimpleDateFormat("dd.MM HH:mm:ss");
                String timeS = newFormat.format(System.currentTimeMillis());

                FileWriter writer = new FileWriter(logFile.getAbsoluteFile(), true);
                BufferedWriter bW = new BufferedWriter(writer);
                bW.write("\nTime: " + timeS + "| " + message);
                bW.close();
            } catch (IOException ex) {

            }
        }
    }
    //------------------------------------------------------------------------------------------
}
