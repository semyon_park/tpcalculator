package com.ct.tpcalculator.calculator.statisticAgent.msgstat;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.ct.tpcalculator.calculator.statisticAgent.Consts;

public class MsgStatService extends IntentService {

    public MsgStatService() {
        super("MsgStatService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        e("onHandleIntent: call");
        AlarmMsgReceiver.setAlarm(getApplicationContext());

        MsgCore.check(this.getApplicationContext());

        AlarmMsgReceiver.completeWakefulIntent(intent);
    }

    //------------------------------------------------------------------------------------------
    private boolean debug = Consts.DEBUG && true;
    private String className = "MsgStatService.";

    private void e(String message) {
        if (debug) Log.e(Consts.APP_TAG, className + message);
    }

    private void d(String message) {
        if (debug) Log.d(Consts.APP_TAG, className + message);
    }
    //------------------------------------------------------------------------------------------
}
