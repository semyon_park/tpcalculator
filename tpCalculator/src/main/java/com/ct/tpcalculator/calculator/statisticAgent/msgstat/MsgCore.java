package com.ct.tpcalculator.calculator.statisticAgent.msgstat;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.util.Log;
import com.ct.tpcalculator.calculator.data.DataDirectionTypes;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticMessage;
import com.ct.tpcalculator.calculator.statisticAgent.Consts;
import com.ct.tpcalculator.calculator.statisticAgent.LimitsChecker;
import com.ct.tpcalculator.calculator.statisticAgent.base.SQLHelper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Andrey on 08.07.2015.
 */
public class MsgCore {

    private MsgCore() {
    }

    public static void check(Context context) {
        e("check(): call");
        ArrayList<StatisticMessage> SMSs = getMessagesDetails(context, "content://sms");
        boolean isSmsRowsAdded = false;
        for (StatisticMessage sms : SMSs)
        {
            if (insertToBase(context, sms) > 0){
                isSmsRowsAdded = true;
            }
        }

//        ArrayList<StatisticMessage> MMSs = getMessagesDetails(context, "content://mms");
//        boolean isMmsRowsAdded = false;
//        for (StatisticMessage mms : MMSs) {
//            if (insertToBase(context, mms) > 0) {
//                isMmsRowsAdded = true;
//            }
//        }

        if (isSmsRowsAdded)
        {
            LimitsChecker.checkSmsLimits(context);
        }

//        if (isMmsRowsAdded)
//        {
//            LimitsChecker.checkSmsLimits(context, false);
//        }

//        base(context);
    }

    private static long insertToBase(Context context, StatisticMessage sms) {
//        //------------------------------------------------------------------------------------------
//        String type = "unknown";
//        switch (sms.getType()) {
//            case SmsModel.TYPE_RECEIVED:
//                type = "In";
//                break;
//            case SmsModel.TYPE_SENDED:
//                type = "Out";
//                break;
//        }
//        String message = "insertToBase(): Type: " + type + "; Name: " + sms.getName() + "; N: " + sms.getNumber();
//        e(message);

        SQLiteDatabase db = SQLHelper.getInstance(context).getWritableDatabase();
        return db.insertWithOnConflict(StatisticMessage.KEY_CLASS_TYPE, null, sms.toContentValues(), SQLiteDatabase.CONFLICT_IGNORE);
    }

    private static ArrayList<StatisticMessage> getMessagesDetails(Context context, String dataPath) {

        //-------------------------/ Load contacts to Map /----------------------------
        HashMap<String, String> contacts = new HashMap<String, String>();
        Cursor cursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cursor.getString(
                        cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = context.getContentResolver().query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contacts.put(phoneNo, name);
                    }
                    pCur.close();
                }
            }
        }
        cursor.close();
        //--------------------------------------------------------------------------------

        //-------------------------/ Load sms to base /----------------------------
        ArrayList<StatisticMessage> arr = new ArrayList<>();

        Uri msg_content = Uri.parse(dataPath);
        cursor = context.getContentResolver().query(msg_content, null, null, null, null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                long date = cursor.getLong(cursor.getColumnIndex("date"));
                int type = cursor.getInt(cursor.getColumnIndex("type"));
                String number = cursor.getString(cursor.getColumnIndex("address"));
                String nameC = contacts.get(number);

                int msgType;
                if (type == 1) {
                    msgType = DataDirectionTypes.INCOMING;
                } else if (type == 2) {
                    msgType = DataDirectionTypes.OUTGOING;
                } else {
                    continue;
                }
                StatisticMessage msg = new StatisticMessage(nameC, number, msgType, new Date(date),
                        dataPath.equalsIgnoreCase("content://sms")? StatisticMessage.TYPE_MSG_SMS: StatisticMessage.TYPE_MSG_MMS);

                arr.add(msg);
            }
        }
        cursor.close();

        return arr;
    }

    //------------------------------------------------------------------------------------------
    private static boolean debug = Consts.DEBUG && true;
    private static String className = "MsgstatCore.";

    private static void e(String message) {
        if (debug) {
            Log.e(Consts.APP_TAG, className + message);
            lf(message);
        }
    }

    private static void d(String message) {
        if (debug) Log.d(Consts.APP_TAG, className + message);
    }

    private static void base(Context context) {
        SimpleDateFormat newFormat = new SimpleDateFormat("dd.MM HH:mm:ss");
        String timeS = newFormat.format(System.currentTimeMillis());
        e("#########################/ SMS /############################");
        e("LogData: " + timeS);
        SQLiteDatabase db = SQLHelper.getInstance(context).getWritableDatabase();
        String rawQuery = "SELECT * FROM " + StatisticMessage.KEY_CLASS_TYPE;
        Cursor cursor = db.rawQuery(rawQuery, null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Date: " + newFormat.format(cursor.getLong((cursor.getColumnIndex(StatisticMessage.KEY_DATE)))));
                sb.append("; Type: " + cursor.getInt(cursor.getColumnIndex(StatisticMessage.KEY_TYPE)));
                sb.append("; Name: " + cursor.getString(cursor.getColumnIndex(StatisticMessage.KEY_NAME)));
                sb.append("; Num: " + cursor.getString(cursor.getColumnIndex(StatisticMessage.KEY_NUMBER)));
                sb.append("; MsgType: " + cursor.getInt(cursor.getColumnIndex(StatisticMessage.KEY_MSG_TYPE)));
                e("insert: " + sb.toString());
            }
        }
        e("Cursor count: " + cursor.getCount());
        e("#############################################################");
        e("");

        cursor.close();
    }

    public static  ArrayList<StatisticMessage> GetStatisticMessageStatistic(Context context, Date startDate, Date endDate, int msgType){
        SQLiteDatabase db = SQLHelper.getInstance(context).getWritableDatabase();
        String rawQuery = "SELECT * FROM " + StatisticMessage.KEY_CLASS_TYPE +
                " WHERE " +StatisticMessage.KEY_DATE + " >= " + startDate.getTime() +
                " AND " + StatisticMessage.KEY_DATE + " <= " + endDate.getTime() +
                " AND " + StatisticMessage.KEY_MSG_TYPE + " = " + msgType;

        Cursor cursor = db.rawQuery(rawQuery, null);

        ArrayList<StatisticMessage> messages = new ArrayList<>();
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Date date = new Date(cursor.getLong((cursor.getColumnIndex(StatisticMessage.KEY_DATE))));
                int msgDirectionType = cursor.getInt(cursor.getColumnIndex(StatisticMessage.KEY_TYPE));
                String name = cursor.getString(cursor.getColumnIndex(StatisticMessage.KEY_NAME));
                String number = cursor.getString(cursor.getColumnIndex(StatisticMessage.KEY_NUMBER));
                StatisticMessage message = new StatisticMessage(name, number, msgDirectionType, date, msgType);
                messages.add(message);
            }
        }

        cursor.close();

        return messages;
    }

    private static void lf(String message) {
        if (debug) {
            try {
                File logFile = new File(Environment.getExternalStorageDirectory().toString() + "/MsgLogFile.txt");

                SimpleDateFormat newFormat = new SimpleDateFormat("dd.MM HH:mm:ss");
                String timeS = newFormat.format(System.currentTimeMillis());

                FileWriter writer = new FileWriter(logFile.getAbsoluteFile(), true);
                BufferedWriter bW = new BufferedWriter(writer);
                bW.write("\nTime: " + timeS + "| " + message);
                bW.close();
            } catch (IOException ex) {

            }
        }
    }
    //------------------------------------------------------------------------------------------
}
