package com.ct.tpcalculator.calculator.data.calculationData;

import com.ct.tpcalculator.calculator.data.statisticData.StatisticMessage;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by USER on 10.07.2015.
 */
public class StatisticMessageForCalculation implements Serializable {

    private StatisticMessage message;

    private double price;

    private boolean isCalculated;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isCalculated() {
        return isCalculated;
    }

    public void setCalculated(boolean isCalculated) {
        this.isCalculated = isCalculated;
    }

    public StatisticMessage getMessage(){
        return message;
    }

    public StatisticMessageForCalculation(StatisticMessage message){
        this.message = message;
    }

    public Date getMessageDate(){
        return message.getMessageDate();
    }

    public int getDirectionType(){
        return message.getDirectionType();
    }

    public String getPhoneNumber(){
        return message.getPhoneNumber();
    }

    @Override
    protected StatisticMessageForCalculation clone() {
        return new StatisticMessageForCalculation(message);
    }
}
