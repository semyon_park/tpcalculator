package com.ct.tpcalculator.calculator.statisticAgent;

/**
 * Created by Andrey on 02.07.2015.
 */
public class Consts {

    public static final String APP_TAG = "TpCalculator";

    public static final boolean DEBUG = false;

    public static final String APP_PACKAGE = "com.ct.tpcalculator";
}
