package com.ct.tpcalculator.calculator.data.statisticData;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by USER on 18.06.2015.
 */
public class StatisticCalls  implements Serializable {

    private ArrayList<StatisticCall> mInRegionCalls;
    private ArrayList<StatisticCall> mInterRegionCalls;
    private ArrayList<StatisticCall> mInternationalCalls;

    public ArrayList<StatisticCall> getInRegionCalls() {
        return mInRegionCalls;
    }

    public ArrayList<StatisticCall> getInterRegionCalls() {
        return mInterRegionCalls;
    }

    public ArrayList<StatisticCall> getInternationalCalls() {
        return mInternationalCalls;
    }

    public StatisticCalls(ArrayList<StatisticCall> inRegionCalls, ArrayList<StatisticCall> interRegionCalls, ArrayList<StatisticCall> internationalCalls) {
        mInRegionCalls = inRegionCalls;
        mInterRegionCalls = interRegionCalls;
        mInternationalCalls = internationalCalls;
    }

//
//    private void sortCalls(ArrayList<StatisticCall> calls) {
//        inRegionCalls = new ArrayList<>();
//        interRegionCalls = new ArrayList<>();
//        internationalCalls = new ArrayList<>();
//
//        for (StatisticCall call : calls) {
//            if (call.getPhoneNumber().length() < getMinDigits()) {
//                continue;
//            }
//            if (call.getPhoneNumber().startsWith("*")) {
//                continue;
//            }
//            // ���� ���-�� ���� � ������ �������� ������ ���� �����, �� ���� ����� �������� ����� �������, ��� ������ �������.
//            if (call.getPhoneNumber().length() >= getMinDigits() && call.getPhoneNumber().length() <= getMaxDigits()) {
//                getInRegionCalls().add(call);
//            } else if (!isPhoneNumberAtHomeCountry(call.getPhoneNumber())) { // ���� ����� �������� �� ���������, ��� ������ ������, �� ����� ������� ��� �������������
//                getInternationalCalls().add(call);
//            } else if (!isPhoneNumberAtHomeRegion(call.getPhoneNumber())) {// ���� ����� �������� �� ���������, ��� ������ �������, �� ����� ������� ��� ���������������
//                getInterRegionCalls().add(call);
//            } else {
//                getInRegionCalls().add(call);
//            }
//        }
//    }

}
