package com.ct.tpcalculator.calculator.statisticAgent.netstat;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.ct.tpcalculator.calculator.statisticAgent.Consts;

/**
 * Created by Andrey on 08.07.2015.
 */
public class AlarmNetReceiver extends WakefulBroadcastReceiver {

    public  static final long ALARM_INTERVAL = 60 * 1000;

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, NetStatService.class);
        // Start the service, keeping the device awake while it is launching.
        e("Starting service @ " + SystemClock.elapsedRealtime());
        startWakefulService(context, service);
    }

    public static boolean alarmExists(Context context) {
        Intent intent = new Intent(context, AlarmNetReceiver.class);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_NO_CREATE) != null;
    }

    public static void setAlarm(Context context) {
        setAlarm(context, 0);
    }

    public static void setAlarm(Context context, long delay) {
        // Check alarm exist, exit if true
        if (alarmExists(context)) return;

        AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        if (am == null) {
            e("Couldn't retrieve alarm manager!");
            return;
        }

        Intent intent = new Intent(context, AlarmNetReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + delay, ALARM_INTERVAL, pi);
    }

    //------------------------------------------------------------------------------------------
    private static boolean debug = Consts.DEBUG;
    private static String className = "AlarmNetReceiver.";

    private static void e(String message) {
        if (debug) {
            Log.e(Consts.APP_TAG, className + message);
        }
    }

    private static void i(String message) {
        if (debug) {
            Log.i(Consts.APP_TAG, className + message);
        }
    }
    //------------------------------------------------------------------------------------------
}
