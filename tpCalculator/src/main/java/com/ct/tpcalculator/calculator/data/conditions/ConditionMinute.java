package com.ct.tpcalculator.calculator.data.conditions;

import com.ct.tpcalculator.calculator.data.XML_TAGS;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticCallForCalculation;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by USER on 16.06.2015.
 */
public class ConditionMinute extends Condition  implements Serializable {

    public final int TYPE_CALL = 0;
    public final int TYPE_DAY = 1;
    public final int TYPE_WEEK = 2;

    private int type;
    private ArrayList<Minute> minutes;

    public int getType() {
        return type;
    }

    public ArrayList<Minute> getMinutes() {
        return minutes;
    }

    public ConditionMinute(XmlPullParser parser) throws XmlPullParserException, IOException {
        super(parser);
        minutes = new ArrayList<>();
        final String tag = parser.getName();
        if (XML_TAGS.XML_TAG_CONDITION_MINUTE.equalsIgnoreCase(tag)) {
            for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
                final String attrName = parser.getAttributeName(i);
                final String attrValue = parser.getAttributeValue(i);
                if (XML_TAGS.XML_ATTRIBUTE_TYPE.equalsIgnoreCase(attrName)) {
                    type = getType(attrValue);
                }
            }
        }
        parser.next();

        int eventType = parser.getEventType();
        do {
            if (eventType == XmlPullParser.START_TAG) {
                final String name = parser.getName();
                if (XML_TAGS.XML_TAG_MINUTE.equalsIgnoreCase(name)) {
                    getMinutes().add(new Minute(parser));
                }
            }
            eventType = parser.next();
        }
        while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_CONDITION_MINUTE.equals(parser.getName()))
                && (eventType != XmlPullParser.END_DOCUMENT));
    }

    private int getType(String value) {
        if (value.equalsIgnoreCase("day")) {
            return TYPE_DAY;
        } else if (value.equalsIgnoreCase("week")) {
            return TYPE_WEEK;
        } else {
            return TYPE_CALL;
        }
    }

    @Override
    public void calculatePrice(StatisticCallForCalculation call) {
        int minutes = call.getNotCalculatedMinutes();
        double price = 0;
        for (int i = call.getCalculatedMinutes() + 1; i <= minutes; i++) {
            price += getMinutePrice(i);
        }
        call.setPrice(price);
        call.setCalculated(true);
    }

    private double getMinutePrice(int minuteIndex) {
        for (Minute minute : getMinutes()) {
            if ((minuteIndex >= minute.getStart()) && (minuteIndex <= minute.getEnd())) {
                return minute.getPrice();
            }
        }
        return getPrice();
    }

    public class Minute  implements Serializable {

        private String name;
        private double price;
        private int start;
        private int end;

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }

        public Minute(XmlPullParser parser) throws XmlPullParserException, IOException {
            final String tag = parser.getName();
            if (XML_TAGS.XML_TAG_MINUTE.equalsIgnoreCase(tag)) {
                for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
                    final String attrName = parser.getAttributeName(i);
                    final String attrValue = parser.getAttributeValue(i);
                    if (XML_TAGS.XML_ATTRIBUTE_NAME.equalsIgnoreCase(attrName)) {
                        name = attrValue;
                    } else if (XML_TAGS.XML_ATTRIBUTE_PRICE.equalsIgnoreCase(attrName)) {
                        price = Double.parseDouble(attrValue);
                    } else if (XML_TAGS.XML_ATTRIBUTE_START.equalsIgnoreCase(attrName)) {
                        start = Integer.parseInt(attrValue);
                    } else if (XML_TAGS.XML_ATTRIBUTE_END.equalsIgnoreCase(attrName)) {
                        end = Integer.parseInt(attrValue);
                    }
                }
            }
            parser.next();
        }

    }
}
