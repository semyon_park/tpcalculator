package com.ct.tpcalculator.calculator.data;

import com.ct.tpcalculator.calculator.data.conditions.CallsCondition;
import com.ct.tpcalculator.calculator.data.conditions.InternetCondition;
import com.ct.tpcalculator.calculator.data.conditions.MmsCondition;
import com.ct.tpcalculator.calculator.data.conditions.SmsCondition;

import java.io.Serializable;

/**
 * Created by USER on 26.06.2015.
 */
public abstract class Package  implements Serializable {
    protected String id;
    protected LocalizedStrings titles;
    protected LocalizedStrings descriptions;
    protected SubscriptionPriceType priceType;
    protected double price;
    protected double changePrice;
    protected CallsCondition callsCondition;
    protected SmsCondition smsCondition;
    protected MmsCondition mmsCondition;
    protected InternetCondition internetCondition;
    protected Commands commands;

    public String getId() {
        return id;
    }

    public LocalizedStrings getTitles() {
        return titles;
    }

    public LocalizedStrings getDescriptions(){
        return descriptions;
    }

    public SubscriptionPriceType getPriceType() {
        return priceType;
    }

    public double getPrice() {
        return price;
    }

    public double getChangePrice() {
        return changePrice;
    }

    public CallsCondition getCallsCondition() {
        return callsCondition;
    }

    public SmsCondition getSmsCondition() {
        return smsCondition;
    }

    public MmsCondition getMmsCondition() {
        return mmsCondition;
    }

    public InternetCondition getInternetCondition() {
        return internetCondition;
    }

    public Commands getCommands() {
        return commands;
    }

}
