package com.ct.tpcalculator.calculator.statisticAgent.netstat;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticInternetTraffic;
import com.ct.tpcalculator.calculator.statisticAgent.Consts;
import com.ct.tpcalculator.calculator.statisticAgent.LimitsChecker;
import com.ct.tpcalculator.calculator.statisticAgent.base.SQLHelper;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Andrey on 02.07.2015.
 */
public class NetStatCore {

    public static boolean WIFI_ENABLED = false;
    public static boolean MOBILE_ENABLED = false;

    private static final String KEY_LAST_TOTAL_TX = "KEY_LAST_TOTAL_TX";
    private static final String KEY_LAST_TOTAL_RX = "KEY_LAST_TOTAL_RX";
    private static final String KEY_LAST_MOB_TX = "KEY_LAST_MOB_TX";
    private static final String KEY_LAST_MOB_RX = "KEY_LAST_MOB_RX";

    private static final String KEY_LAST_SNAP_TIME = "KEY_LAST_SNAP_TIME";
    private static final String KEY_COUNT_WIFI_TX_TRAFFIC = "KEY_COUNT_WIFI_TX_TRAFFIC";
    private static final String KEY_COUNT_MOB_TX_TRAFFIC = "KEY_COUNT_MOB_TX_TRAFFIC";
    private static final String KEY_COUNT_WIFI_RX_TRAFFIC = "KEY_COUNT_WIFI_RX_TRAFFIC";
    private static final String KEY_COUNT_MOB_RX_TRAFFIC = "KEY_COUNT_MOB_RX_TRAFFIC";

    private static void insertToBase(Context context, StatisticInternetTraffic snap) {
        SQLiteDatabase db = SQLHelper.getInstance(context).getWritableDatabase();
        db.insertWithOnConflict(StatisticInternetTraffic.KEY_CLASS_TYPE, null, snap.toContentValues(), SQLiteDatabase.CONFLICT_REPLACE);

        if (Consts.DEBUG)
            db.insertWithOnConflict(StatisticInternetTraffic.KEY_CLASS_TYPE + "_TEST", null, snap.toContentValues(), SQLiteDatabase.CONFLICT_REPLACE);

    }

    private static void updateConnectionStatus(Context context) {
        WIFI_ENABLED = isConnectedWifi(context);
        MOBILE_ENABLED = isConnectedMobile(context);
    }

    public static boolean isConnectedWifi(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    public static boolean isConnectedMobile(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    public static void check(Context context) {
        e("check(): call");

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(context);
        long lastTotalTxBytes = mSettings.getLong(KEY_LAST_TOTAL_TX, 0);
        long lastTotalRxBytes = mSettings.getLong(KEY_LAST_TOTAL_RX, 0);
        long lastMobileTxBytes = mSettings.getLong(KEY_LAST_MOB_TX, 0);
        long lastMobileRxBytes = mSettings.getLong(KEY_LAST_MOB_RX, 0);

        long lastSnapTime = mSettings.getLong(KEY_LAST_SNAP_TIME, 0L);

        long countWifiRxTraffic = mSettings.getLong(KEY_COUNT_WIFI_RX_TRAFFIC, 0);
        long countMobRxTraffic = mSettings.getLong(KEY_COUNT_MOB_RX_TRAFFIC, 0);
        long countWifiTxTraffic = mSettings.getLong(KEY_COUNT_WIFI_TX_TRAFFIC, 0);
        long countMobTxTraffic = mSettings.getLong(KEY_COUNT_MOB_TX_TRAFFIC, 0);

        long currTime = System.currentTimeMillis();

        GregorianCalendar currDate = new GregorianCalendar();
        currDate.setTimeInMillis(currTime);

//        currDate.set(Calendar.MINUTE, 0);
        currDate.set(Calendar.SECOND, 0);

        currTime = currDate.getTimeInMillis();

//        currTime = currTime - ((currDate.get(Calendar.MINUTE) + 1)*60 - currDate.get(Calendar.SECOND) + 1)*1000;

        if (lastSnapTime == 0) lastSnapTime = currTime;

//        SimpleDateFormat newFormat = new SimpleDateFormat("dd.MM HH:mm:ss");
//        String timeS = newFormat.format(lastSnapTime);
//        e("lastSnapTime: " + timeS);

        GregorianCalendar snapDate = new GregorianCalendar();
        snapDate.setTimeInMillis(lastSnapTime);

        updateConnectionStatus(context);

        long tempTxTraffic = 0;
        long tempRxTraffic = 0;

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        if (debug) {
            e(".......................................................................");
            e("check(): TimeMillis: Last: " + dateFormat.format(new Date(lastSnapTime)) + "; Curr: " + dateFormat.format(new Date(currTime)));
        }

        if (currTime - 2 * 60 * 1000 >= lastSnapTime) { // for testing
//        if (currTime - 60 * 60 * 1000 >= lastSnapTime) {

//            snapDate.add(Calendar.HOUR_OF_DAY, 1);
            snapDate.add(Calendar.MINUTE, 1);   // for testing

            StatisticInternetTraffic t = new StatisticInternetTraffic(StatisticInternetTraffic.TYPE_WIFI);
            t.setDate(snapDate.getTime());
            t.setReceivedBytes(countWifiRxTraffic);
            t.setTransmittedBytes(countWifiTxTraffic);
//            e("check(): TimeMillis: Wifi   PreInsert: " + t.toLog());
            if (countWifiRxTraffic > 0 | countWifiTxTraffic > 0) {
                insertToBase(context, t);
            }

            StatisticInternetTraffic m = new StatisticInternetTraffic(StatisticInternetTraffic.TYPE_MOBILE);
            m.setDate(snapDate.getTime());
            m.setReceivedBytes(countMobRxTraffic);
            m.setTransmittedBytes(countMobTxTraffic);
//            e("check(): TimeMillis: Mobile PreInsert: " + m.toLog());
            if (countMobRxTraffic > 0 | countMobTxTraffic > 0) {
//                e("check(): TimeMillis: InsertToBase: call");
                insertToBase(context, m);

                LimitsChecker.checkInternetTrafficLimits(context);
            }

            lastSnapTime = currTime;
            countWifiRxTraffic = 0;
            countWifiTxTraffic = 0;
            countMobRxTraffic = 0;
            countMobTxTraffic = 0;

            save(mSettings, KEY_COUNT_WIFI_RX_TRAFFIC, countWifiRxTraffic);
            save(mSettings, KEY_COUNT_WIFI_TX_TRAFFIC, countWifiTxTraffic);
            save(mSettings, KEY_COUNT_MOB_RX_TRAFFIC, countMobRxTraffic);
            save(mSettings, KEY_COUNT_MOB_TX_TRAFFIC, countMobTxTraffic);

        }

        long currTotalTxBytes = TrafficStats.getTotalTxBytes(); // 0  // ��� ������������ ���������� (after restart)
        long currTotalRxBytes = TrafficStats.getTotalRxBytes(); // 0
        long currMobileTxBytes = TrafficStats.getMobileTxBytes(); // 0
        long currMobileRxBytes = TrafficStats.getMobileRxBytes(); // 0


        boolean checked = false;
        // ----------------------------------------------------------------------------------------
        //         10                200       ||||||||       0                 80
        if ((currTotalTxBytes < lastTotalTxBytes) || (currTotalRxBytes < lastTotalRxBytes)) { // ������������� (phone restarted)
            e("check(): device restarted, check values...");
            countMobRxTraffic = countMobRxTraffic + currMobileRxBytes;
            countMobTxTraffic = countMobTxTraffic + currMobileTxBytes;
            save(mSettings, KEY_COUNT_MOB_RX_TRAFFIC, countMobRxTraffic);
            save(mSettings, KEY_COUNT_MOB_TX_TRAFFIC, countMobTxTraffic);
            lastMobileRxBytes = currMobileRxBytes;
            lastMobileTxBytes = currMobileTxBytes;

            countWifiRxTraffic = countWifiRxTraffic + (currTotalRxBytes - currMobileRxBytes);
            countWifiTxTraffic = countWifiTxTraffic + (currTotalTxBytes - currMobileTxBytes);
            save(mSettings, KEY_COUNT_WIFI_RX_TRAFFIC, countWifiRxTraffic);
            save(mSettings, KEY_COUNT_WIFI_TX_TRAFFIC, countWifiTxTraffic);
            lastTotalRxBytes = currTotalRxBytes;
            lastTotalTxBytes = currTotalTxBytes;

            save(mSettings, KEY_LAST_TOTAL_RX, lastTotalRxBytes);
            save(mSettings, KEY_LAST_TOTAL_TX, lastTotalTxBytes);
            save(mSettings, KEY_LAST_MOB_RX, lastMobileRxBytes);
            save(mSettings, KEY_LAST_MOB_TX, lastMobileTxBytes);

            checked = true;
        }

        if ((lastTotalRxBytes == 0)                               // ��� ��������� ��� ������ ��������� (install or reset app data)
                || (lastTotalTxBytes == 0)) {
//            e("check(): device restarted, current total = 0...");
            lastTotalRxBytes = currTotalRxBytes;                    // => 0
            lastTotalTxBytes = currTotalTxBytes;                    // => 0
        }

        if ((lastMobileRxBytes == 0L)
                || (lastMobileTxBytes == 0L)) {
//            e("check(): device restarted, check mobile = 0...");
            lastMobileTxBytes = currMobileTxBytes;
            lastMobileRxBytes = currMobileRxBytes;
        }

        if (!checked) { // normal mode
//            e("check(): device restarted, check normal mode...");
            // Calc Wifi /--------------------------------------------------------------------------------------------------------
            tempRxTraffic = (currTotalRxBytes - currMobileRxBytes)                         // current Wifi traffic downloaded
                    - (lastTotalRxBytes - lastMobileRxBytes);                            // last Wifi traffic downloaded
            tempTxTraffic = (currTotalTxBytes - currMobileTxBytes)                         // current Wifi traffic uploaded
                    - (lastTotalTxBytes - lastMobileTxBytes);                            // last Wifi traffic uploaded

            countWifiRxTraffic = countWifiRxTraffic + tempRxTraffic;
            countWifiTxTraffic = countWifiTxTraffic + tempTxTraffic;

            save(mSettings, KEY_COUNT_WIFI_RX_TRAFFIC, countWifiRxTraffic);
            save(mSettings, KEY_COUNT_WIFI_TX_TRAFFIC, countWifiTxTraffic);

            lastTotalRxBytes = currTotalRxBytes;
            lastTotalTxBytes = currTotalTxBytes;

            save(mSettings, KEY_LAST_TOTAL_RX, lastTotalRxBytes);
            save(mSettings, KEY_LAST_TOTAL_TX, lastTotalTxBytes);
            // Calc Mobile /-------------------------------------------------------------------------------------------------------
            tempRxTraffic = currMobileRxBytes                         // current Mobile traffic downloaded
                    - lastMobileRxBytes;                            // last Mobile traffic downloaded
            tempTxTraffic = currMobileTxBytes                         // current Mobile traffic uploaded
                    - lastMobileTxBytes;                            // last Mobile traffic uploaded

            countMobRxTraffic = countMobRxTraffic + tempRxTraffic;
            countMobTxTraffic = countMobTxTraffic + tempTxTraffic;
            save(mSettings, KEY_COUNT_MOB_RX_TRAFFIC, countMobRxTraffic);
            save(mSettings, KEY_COUNT_MOB_TX_TRAFFIC, countMobTxTraffic);

            lastMobileRxBytes = currMobileRxBytes;
            lastMobileTxBytes = currMobileTxBytes;

            save(mSettings, KEY_LAST_MOB_RX, lastMobileRxBytes);
            save(mSettings, KEY_LAST_MOB_TX, lastMobileTxBytes);

//            e("check(): Trafff:  Dw: " + Long.valueOf(countWifiRxTraffic/1024) + " KB; Uw: " + Long.valueOf(countWifiTxTraffic/1024) + " KB"
//                    + "; Dm: " + Long.valueOf(countMobRxTraffic/1024) + " KB; Um: " + Long.valueOf(countMobTxTraffic/1024) + " KB");
        }
        // ----------------------------------------------------------------------------------------


//        save(mSettings, KEY_LAST_TOTAL_RX, lastTotalRxBytes);
//        save(mSettings, KEY_LAST_TOTAL_TX, lastTotalTxBytes);
//        save(mSettings, KEY_LAST_MOB_RX, lastMobileRxBytes);
//        save(mSettings, KEY_LAST_MOB_TX, lastMobileTxBytes);
        save(mSettings, KEY_LAST_SNAP_TIME, lastSnapTime);
//        base(context);
    }

    private static void save(SharedPreferences mSettings, String key, long value) {
        SharedPreferences.Editor e = mSettings.edit();
        e.putLong(key, value);
        e.apply();
    }


    //------------------------------------------------------------------------------------------
    private static boolean debug = Consts.DEBUG && true;
    private static String className = "NetStatCore.";

    private static void e(String message) {
        if (debug) {
            Log.e(Consts.APP_TAG, className + message);
//            lf(message);
        }
    }

    public static void base(Context context) {
                //------------------------------------------------------------------------------------------
        SimpleDateFormat newFormat = new SimpleDateFormat("dd.MM HH:mm:ss");
        String timeS = newFormat.format(System.currentTimeMillis());

        e("BaseInet:....................................................................");
        e("BaseInet: LogData: " + timeS);
        SQLiteDatabase db = SQLHelper.getInstance(context).getWritableDatabase();
        String rawQuery = "SELECT * FROM " + StatisticInternetTraffic.KEY_CLASS_TYPE;
        Cursor cursor = db.rawQuery(rawQuery, null);

        StatisticInternetTraffic traff;
        long d = 0;
        long dm = 0;
        long dw = 0;
        long u = 0;
        long um = 0;
        long uw = 0;

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {

                traff = new StatisticInternetTraffic(cursor.getInt(cursor.getColumnIndex(StatisticInternetTraffic.KEY_TYPE)));
                traff.setDate(new Date(cursor.getLong(cursor.getColumnIndex(StatisticInternetTraffic.KEY_DATE))));
                traff.setReceivedBytes(cursor.getLong(cursor.getColumnIndex(StatisticInternetTraffic.KEY_DOWNLOAD)));
                traff.setTransmittedBytes(cursor.getLong(cursor.getColumnIndex(StatisticInternetTraffic.KEY_UPLOAD)));

                e("BaseInet: #" + cursor.getPosition() + " " + traff.toLog());

                d += traff.getReceivedBytes();
                u += traff.getTransmittedBytes();

                if (traff.getType() == StatisticInternetTraffic.TYPE_MOBILE) {
                    dm += traff.getReceivedBytes();
                    um += traff.getTransmittedBytes();
                } else {
                    dw += traff.getReceivedBytes();
                    uw += traff.getTransmittedBytes();
                }
            }
        }
        if (cursor != null) cursor.close();
//        if (db != null && db.isOpen()) db.close();
        e("BaseInet: All: download: " + d + "; upload: " + u + "; total: " + (d+u));
        e("BaseInet: Mobile: download: " + dm + "; upload: " + um + "; total: " + (dm+um));
        e("BaseInet: Wifi: download: " + dw + "; upload: " + uw + "; total: " + (dw+uw));
        e("BaseInet:....................................................................");
    }

    public static ArrayList<StatisticInternetTraffic> GetInternetStatisticData(Context context, Date startDate, Date endDate, int internetTrafficType) {
        SQLiteDatabase db = SQLHelper.getInstance(context).getWritableDatabase();
        String rawQuery;
        if (internetTrafficType == StatisticInternetTraffic.TYPE_WIFI_MOBILE) {
            rawQuery = "SELECT * FROM " + StatisticInternetTraffic.KEY_CLASS_TYPE +
                    " WHERE " + StatisticInternetTraffic.KEY_DATE + " >= " + startDate.getTime() +
                    " AND " + StatisticInternetTraffic.KEY_DATE + " <= " + endDate.getTime();
        } else {
            rawQuery = "SELECT * FROM " + StatisticInternetTraffic.KEY_CLASS_TYPE +
                    " WHERE " + StatisticInternetTraffic.KEY_DATE + " >= " + startDate.getTime() +
                    " AND " + StatisticInternetTraffic.KEY_DATE + " <= " + endDate.getTime() +
                    " AND " + StatisticInternetTraffic.KEY_TYPE + " == " + internetTrafficType;
        }

        Cursor cursor = db.rawQuery(rawQuery, null);

        ArrayList<StatisticInternetTraffic> internetTraffic = new ArrayList<>();

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {

                Date date = new Date(cursor.getLong(cursor.getColumnIndex(StatisticInternetTraffic.KEY_DATE)));
                int down = cursor.getInt(cursor.getColumnIndex(StatisticInternetTraffic.KEY_DOWNLOAD));
                int up = cursor.getInt(cursor.getColumnIndex(StatisticInternetTraffic.KEY_UPLOAD));
                int type = cursor.getInt(cursor.getColumnIndex(StatisticInternetTraffic.KEY_TYPE));
                StatisticInternetTraffic traffic = new StatisticInternetTraffic(type);
                traffic.setDate(date);
                traffic.setTransmittedBytes(up);
                traffic.setReceivedBytes(down);
                internetTraffic.add(traffic);
            }
        }

        cursor.close();

        return internetTraffic;
    }

    private static void d(String message) {
        if (debug) Log.d(Consts.APP_TAG, className + message);
    }

    private static void lf(String message) {
        if (debug) {
            //        String folderToSave = Environment.getExternalStorageDirectory();

            try {
                File logFile = new File(Environment.getExternalStorageDirectory().toString() + "/NetLogFile.txt");

                SimpleDateFormat newFormat = new SimpleDateFormat("dd.MM HH:mm:ss");
                String timeS = newFormat.format(System.currentTimeMillis());


                FileWriter writer = new FileWriter(logFile.getAbsoluteFile(), true);
                BufferedWriter bW = new BufferedWriter(writer);
                bW.write("\nTime: " + timeS + "| " + message);
                bW.close();
//                writer.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void baseCopy(Context context) {
        e("baseCopy: call");

        SimpleDateFormat all = new SimpleDateFormat("yyyy.MM.dd.HH.mm");
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(System.currentTimeMillis());
        String date = all.format(calendar.getTime());

        String baseName = "/TC_base_" + date + ".db";
        File base = new File(Environment.getExternalStorageDirectory().toString() + baseName);
        File from = new File(context.getApplicationInfo().dataDir + "/databases", "TpCalculator.db");
        if (base.exists()) {
            e("baseCopy: base.exists");
            base.delete();
        }

        BufferedInputStream in = null;
        FileOutputStream out = null;
        try {
            base.createNewFile();
            in = new BufferedInputStream(new FileInputStream(from));
            out = new FileOutputStream(base);

            int i = 0;
            byte[] bytesIn = new byte[1024];
            while ((i = in.read(bytesIn)) >= 0) {
                out.write(bytesIn, 0, i);
            }
            e("baseCopy: copy finished!");
        } catch (FileNotFoundException e) {
            e("baseCopy: FileNotFoundException: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            e("baseCopy: IOException: " + e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (out != null) out.close();
                if (in != null) in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //------------------------------------------------------------------------------------------
//  ����� ����� ���, ����� ����� ������������
//
//	public static void getPakagesInfoUsingHashMap(Context context) {
//		e("#################  getPakagesInfoUsingHashMap  #################");
//		long Dmobile = 0;
//		long Umobile = 0;
//
//		final PackageManager pm = context.getPackageManager();
//		// get a list of installed apps.
//		List<ApplicationInfo> packages = pm.getInstalledApplications(0);
//
//		// loop through the list of installed packages and see if the selected
//		// app is in the list
//		for (ApplicationInfo packageInfo : packages) {
//			// get the UID for the selected app
//			int UID = packageInfo.uid;
//			String package_name = packageInfo.packageName;
//			ApplicationInfo app = null;
//			try {
//				app = pm.getApplicationInfo(package_name, 0);
//			} catch (PackageManager.NameNotFoundException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			String name = (String) pm.getApplicationLabel(app);
//			// internet usage for particular app(sent and received)
//			long Dcurr = TrafficStats.getUidRxBytes(UID);
//            long Ucurr = TrafficStats.getUidTxBytes(UID);
//
//			Dmobile += Dcurr;
//			Umobile += Ucurr;
//
////			e("App: " + name + ": Dcurr: " + Dcurr + "; Ucurr: " + Ucurr);
////			e(name + ": " + String.format( "%.2f", total )+" MB");
//		}
//		e("TotalUIDs: Dmobile: " + Dmobile + "; Umobile: " + Umobile);
//		e("TotalAll: Dmobile: " + TrafficStats.getTotalRxBytes() + "; Umobile: " + TrafficStats.getTotalTxBytes());
//		e("TotalMobile: Dmobile: " + TrafficStats.getMobileRxBytes() + "; Umobile: " + TrafficStats.getMobileTxBytes());
//
//		e("####################################################################");
//	}


}
