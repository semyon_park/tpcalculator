package com.ct.tpcalculator.calculator.statisticAgent.exception;


import android.content.Context;
import android.util.Log;

import com.ct.tpcalculator.calculator.statisticAgent.callstat.AlarmCallReceiver;
import com.ct.tpcalculator.calculator.statisticAgent.msgstat.AlarmMsgReceiver;
import com.ct.tpcalculator.calculator.statisticAgent.netstat.AlarmNetReceiver;

public class CrashHandler implements Thread.UncaughtExceptionHandler {

    private Context mContext;
    private static final String TAG = "CrashHandler";
    private Thread.UncaughtExceptionHandler defaultUEH;

    public CrashHandler(Context context) {
        mContext = context;
        defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        Log.e(TAG, "uncaughtException: call");
        Log.e(TAG, Log.getStackTraceString(ex));

        try {
            //------------------------------/ Set alarms /-------------------------------
            AlarmCallReceiver.setAlarm(mContext);
            AlarmMsgReceiver.setAlarm(mContext);
            AlarmNetReceiver.setAlarm(mContext);

            System.exit(2);
        } catch (Exception exception) {
            Log.e(TAG, "Crash handler failed!");
            Log.e(TAG, exception.toString());
            defaultUEH.uncaughtException(thread, ex);
        }
    }
}
