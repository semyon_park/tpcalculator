package com.ct.tpcalculator.calculator.data;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class Languages extends ArrayList<Language> implements Serializable {

	public Languages(XmlPullParser parser) throws XmlPullParserException, IOException {

		int eventType = parser.getEventType();
		do {
			if (eventType == XmlPullParser.START_TAG) {
				final String name = parser.getName();
				if (XML_TAGS.XML_TAG_LANGUAGE.equalsIgnoreCase(name)) {
					add(new Language(parser));
				}
			}
			eventType = parser.next();
		} while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_LANGUAGES.equals(parser.getName()))
						&& (eventType != XmlPullParser.END_DOCUMENT));
	}

    public Languages(){
        super();
    }

	public Language findLanguage(String locale) {
		for (Language l : this) {
			if (l.getLocale().equalsIgnoreCase(locale)) return l;
		}
		return null;
	}

    public Languages getAvailableLanguages(String languages){
        String[] languagesList = languages.split(";");
        Languages availableLanguages = new Languages();
        for (String languageCode : languagesList){
            Language language = findLanguage(languageCode);
            if (language != null){
                availableLanguages.add(language);
            }
        }
        return availableLanguages;
    }
}
