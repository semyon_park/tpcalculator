package com.ct.tpcalculator.calculator.data;

/**
 * Created by USER on 16.06.2015.
 * All XML TAGS AND ATTRIBUTES names
 */
public class XML_TAGS {

    public final static String XML_TAG_COUNTRY = "country";
    public final static String XML_TAG_ERROR = "error";
    public final static String XML_TAG_LANGUAGE = "language";
    public final static String XML_TAG_LANGUAGES = "languages";
    public final static String XML_ATTRIBUTE_TITLE = "title";
    public final static String XML_ATTRIBUTE_DESCRIPTION = "description";
    public final static String XML_ATTRIBUTE_OFFER = "offer";
    public final static String XML_ATTRIBUTE_NUMBER_PREFIX = "numberPrefix";
    public final static String XML_TAG_AVAILABLE_LANGUAGES = "availableLanguages";
    public final static String XML_TAG_REGION = "region";
    public final static String XML_ATTRIBUTE_ID = "id";
    public final static String XML_TAG_OPERATOR = "operator";
    public final static String XML_TAG_TARIFF_PLAN = "tp";
    public final static String XML_TAG_SERVICE = "service";
    public final static String XML_ATTRIBUTE_PRICE_TYPE = "priceType";
    public final static String XML_ATTRIBUTE_PRICE = "price";
    public final static String XML_ATTRIBUTE_CHANGE_PRICE = "changePrice";
    public final static String XML_ATTRIBUTE_PERIOD = "period";
    public final static String XML_ATTRIBUTE_AVAILABLE_SERVICES = "availableServices";
    public final static String XML_ATTRIBUTE_ENABLED_SERVICES = "enabledServices";
    public final static String XML_ATTRIBUTE_IS_ARCHIVE = "isArchive";
    public final static String XML_ATTRIBUTE_FILE_VERSION = "fileVersion";
    public final static String XML_ATTRIBUTE_LIMIT = "limit";
    public final static String XML_TAG_INTERCOUNTRIES = "intercountries";
    public final static String XML_TAG_INREGION = "inregion";
    public final static String XML_TAG_INTERREGIONS = "interregions";
    public final static String XML_TAG_INCOMING = "incoming";
    public final static String XML_TAG_OUTGOING = "outgoing";
    public final static String XML_TAG_CONDITION_PREFIX = "conditionPrefix";
    public final static String XML_TAG_CONDITION_DEFAULT = "conditionDefault";
    public final static String XML_TAG_CONDITION_TIME = "conditionTime";
    public final static String XML_TAG_CONDITION_MINUTE = "conditionMinute";
    public final static String XML_TAG_CONDITION_DAY = "conditionDay";
    public final static String XML_TAG_TIME = "time";
    public final static String XML_TAG_MINUTE = "minute";
    public final static String XML_TAG_DAY = "day";
    public final static String XML_ATTRIBUTE_NAME = "name";
    public final static String XML_ATTRIBUTE_START = "start";
    public final static String XML_ATTRIBUTE_END = "end";
    public final static String XML_ATTRIBUTE_TYPE = "type";
    public final static String XML_TAG_CALLS = "calls";
    public final static String XML_TAG_SMS = "sms";
    public final static String XML_TAG_MMS = "mms";
    public final static String XML_TAG_INTERNET = "internet";
    public final static String XML_ATTRIBUTE_WEEKDAY = "weekday";
    public final static String XML_ATTRIBUTE_VALUE = "value";
    public final static String XML_TAG_LIMITS = "limits";
    public final static String XML_TAG_LIMIT = "limit";
    public final static String XML_TAG_COMMANDS = "commands";
    public final static String XML_TAG_COMMAND = "command";
    public final static String XML_TAG_COMMAND_BLOCK = "command_block";
    public final static String XML_TAG_USSD = "ussd";
    public final static String XML_TAG_LINK = "link";
    public final static String XML_TAG_IVR = "ivr";
    public final static String XML_ATTRIBUTE_COMMAND ="command";
    public final static String XML_ATTRIBUTE_COMMAND_TITLE ="command_title";
    public final static String XML_ATTRIBUTE_SMS_NUMBER="smsNumber";
    public final static String XML_ATTRIBUTE_SMS_TEXT="smsText";
    public final static String XML_ATTRIBUTE_LINK="link";
    public final static String XML_ATTRIBUTE_MIN_DIGITS_COUNT="minDigits";
    public final static String XML_ATTRIBUTE_MAX_DIGITS_COUNT="maxDigits";
    public final static String XML_ATTRIBUTE_SERVICE_TYPE="serviceType";
    public final static String XML_ATTRIBUTE_FILE_NAME = "fileName";
    public final static String XML_ATTRIBUTE_ICON = "icon";
    public final static String XML_ATTRIBUTE_CURRENCY = "currency";
    public final static String XML_ATTRIBUTE_PHONE_NUMBER ="phoneNumber";
}
