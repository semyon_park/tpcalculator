package com.ct.tpcalculator.calculator.data;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * Created by USER on 16.06.2015.
 * Saves localized Strings.  key - language code(ru, uz, en ...); value - title by language
 */
public class LocalizedStrings extends Hashtable<String, String> implements Serializable {

    public String getStringByLanguage(String languageCode){
        String localizedString = get(languageCode);
        if (localizedString == null){
            return "";
        }else{
            return localizedString;
        }
    }
}
