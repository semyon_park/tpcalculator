package com.ct.tpcalculator.calculator.data.calculationData;

import com.ct.tpcalculator.calculator.data.Service;
import com.ct.tpcalculator.calculator.data.SubscriptionPriceType;
import com.ct.tpcalculator.helpers.Utils;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by USER on 06.07.2015.
 */
public class ServiceToOffer  implements Serializable {

    private final Service service;
    private final StatisticLogsForCalculation logs;
    private final double servicePrice;
    private final double priceDifferenceWithTp;
    private final Date dateOfActivation;

    public ServiceToOffer(Service service, StatisticLogsForCalculation logs, double servicePrice, double priceDifferenceWithTp, Date dateOfActivation) {
        this.service = service;
        this.logs = logs;
        this.servicePrice = servicePrice;
        this.priceDifferenceWithTp = priceDifferenceWithTp;
        this.dateOfActivation = dateOfActivation;
    }

    public Date getDateOfActivation() {
        return dateOfActivation;
    }

    public double getServicePrice() {
        return servicePrice;
    }

    public Service getService() {
        return service;
    }

    public StatisticLogsForCalculation getLogs() {
        return logs;
    }

    public double getPriceDifferenceWithTp() {
        return priceDifferenceWithTp;
    }

    public Date getDateOfDeactivation() {
        Calendar endDateCalendar = Calendar.getInstance();
        endDateCalendar.setTime(dateOfActivation);

        switch (service.getPriceType().getPriceType()) {
            case SubscriptionPriceType.MONTH:
                endDateCalendar.add(Calendar.MONTH, service.getPriceType().getPeriodCount());
                break;
            case SubscriptionPriceType.WEEK:
                endDateCalendar.add(Calendar.WEEK_OF_YEAR, service.getPriceType().getPeriodCount());
                break;
            case SubscriptionPriceType.DAY:
            default:
                endDateCalendar.add(Calendar.DAY_OF_MONTH, service.getPriceType().getPeriodCount());
                break;
        }
        Date endDate = endDateCalendar.getTime();
        endDate = Utils.getEndOfDay(endDate);

        return endDate;
    }
}
