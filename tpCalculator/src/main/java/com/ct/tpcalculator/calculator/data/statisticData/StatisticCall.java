package com.ct.tpcalculator.calculator.data.statisticData;

import android.content.ContentValues;
import com.ct.tpcalculator.helpers.Utils;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by USER on 18.06.2015.
 */
public class StatisticCall implements Serializable {

    public static final String KEY_CLASS_TYPE = "callModel";
    public static final String KEY_NAME = "callName";
    public static final String KEY_NUMBER = "callNumber";
    public static final String KEY_DATE = "callDate";
    public static final String KEY_TYPE = "callType";
    public static final String KEY_DURATION = "callDuration";
    public static final String KEY_CONTACT_ID = "contactId";

    private final String name;
    private final String phoneNumber;
    private final int callType;
    private final Date callDate;
    private final int callDuration; // In Minutes
    private final int contactId;

    public int getContactId() {
        return contactId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getCallType() {
        return callType;
    }

    public Date getCallDate() {
        return callDate;
    }

    public int getCallDuration() { // ����� ���-�� �����
        return callDuration;
    }

    public int getCallDurationInMinutes() {
        return Utils.getMinutes(callDuration);// In Minutes
    }

    public String getName() {
        return name;
    }

    public StatisticCall(int contactId, String name, String phoneNumber, int callType, Date callDate, int callDuration) {
        this.contactId = contactId;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.callType = callType;
        this.callDate = callDate;
        this.callDuration = callDuration;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();

        values.put(KEY_NAME, this.getName());
        values.put(KEY_NUMBER, this.getPhoneNumber());
        values.put(KEY_DATE, this.getCallDate().getTime());
        values.put(KEY_TYPE, this.getCallType());
        values.put(KEY_DURATION, this.getCallDuration());
        values.put(KEY_CONTACT_ID, this.getContactId());

        return values;
    }
}
