package com.ct.tpcalculator.calculator.statisticAgent;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.util.Pair;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.activities.MainActivity;
import com.ct.tpcalculator.application.fragments.ChartBaseFragment;
import com.ct.tpcalculator.calculator.PackageCalculator;
import com.ct.tpcalculator.calculator.data.Country;
import com.ct.tpcalculator.calculator.data.DataDirectionTypes;
import com.ct.tpcalculator.calculator.data.Limit;
import com.ct.tpcalculator.calculator.data.MainData;
import com.ct.tpcalculator.calculator.data.MobileOperator;
import com.ct.tpcalculator.calculator.data.Region;
import com.ct.tpcalculator.calculator.data.TariffPlan;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticCallForCalculation;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticCallsForCalculation;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticInternetTrafficForCalculation;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticInternetTrafficsForCalculation;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticMessageForCalculation;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticMessagesForCalculation;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticInternetTraffics;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticMessage;
import com.ct.tpcalculator.helpers.FileCache;
import com.ct.tpcalculator.helpers.L;
import com.ct.tpcalculator.helpers.StatisticData;
import com.ct.tpcalculator.helpers.Utils;
import com.ct.tpcalculator.testapplication.activities.TestActivity;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;

/**
 * Created by USER on 21.08.2015.
 */
public class LimitsChecker {

    private static final int calls_incoming_limit_is_up_notificationId = 0; // лимимт на ВХОДЯЩИЕ звонки
    private static final int calls_outgoing_limit_is_up_notificationId = 1; // лимимт на ИСХОДЯЩИЕ звонки
    private static final int calls_incoming_minimum_notificationId = 2; // лимит для ВХОДЯЩИХ звонков подходит к концу
    private static final int calls_outgoing_minimum_notificationId = 3; // лимит для ИСХОДЯЩИХ звонков подходит к концу
    private static final int calls_limit_is_up_notificationId = 4; // лимимт на ВХОДЯЩИЕ и ИСХОДЯЩИЕ звонки
    private static final int calls_minimum_is_up_notificationId = 5; // лимимт для ВХОДЯЩИХ и ИСХОДЯЩИХ звонков подходит к концу

    private static final int sms_incoming_limit_is_up_notificationId = 6; // лимимт на ВХОДЯЩИЕ смс
    private static final int sms_outgoing_limit_is_up_notificationId = 7; // лимимт на ИСХОДЯЩИЕ смс
    private static final int sms_incoming_minimum_notificationId = 8; // лимит для ВХОДЯЩИХ смс подходит к концу
    private static final int sms_outgoing_minimum_notificationId = 9; // лимит для ИСХОДЯЩИХ смс подходит к концу
    private static final int sms_limit_is_up_notificationId = 10; // лимимт на ВХОДЯЩИЕ и ИСХОДЯЩИЕ смс
    private static final int sms_minimum_is_up_notificationId = 11; // лимимт для ВХОДЯЩИХ и ИСХОДЯЩИХ смс подходит к концу

    private static final int mms_incoming_limit_is_up_notificationId = 12; // лимимт на ВХОДЯЩИЕ ммс
    private static final int mms_outgoing_limit_is_up_notificationId = 13; // лимимт на ИСХОДЯЩИЕ ммс
    private static final int mms_incoming_minimum_notificationId = 14; // лимит для ВХОДЯЩИХ ммс подходит к концу
    private static final int mms_outgoing_minimum_notificationId = 15; // лимит для ИСХОДЯЩИХ ммс подходит к концу
    private static final int mms_limit_is_up_notificationId = 16; // лимимт на ВХОДЯЩИЕ и ИСХОДЯЩИЕ ммс
    private static final int mms_minimum_is_up_notificationId = 17; // лимимт для ВХОДЯЩИХ и ИСХОДЯЩИХ ммс подходит к концу

    private static final int internet_limit_is_up_notificationId = 18; // лимит на интернет трафик
    private static final int internet_outgoing_limit_is_up_notificationId = 19;
    private static final int internet_minimum_notificationId = 20; // лимит для интернет трафика подходит к концу
    private static final int internet_outgoing_minimum_notificationId = 21;

    private static final int minimum_limit_step = 10;

    public static void checkCallsLimits(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean showNotifications = sharedPreferences.getBoolean(context.getString(R.string.preferences_show_notifications), false);
        if (!showNotifications) { // Если показывать нотификации не надо, то и подсчитывать лимиты не стоит
            return;
        }

        Calendar c = Calendar.getInstance();
        Date startDate = c.getTime();

        L.e("checkCallsLimits START - " + new SimpleDateFormat("HH:mm:ss.SSSZ").format(startDate));

        MainData mainData = FileCache.getMainData();
        if (mainData == null)
            return;

        String mobileOperatorId = sharedPreferences.getString(context.getString(R.string.preferences_selected_mobile_operator_id), "");
        if (mainData.getCountries() == null || mainData.getCountries().size() == 0 ||
                mainData.getCountries().get(0).getRegions() == null || mainData.getCountries().get(0).getRegions().size() == 0) {
            return;
        }
        MobileOperator mobileOperator = mainData.getCountries().get(0).getRegions().get(0).findMobileOperator(mobileOperatorId);
        if (mobileOperator == null)
            return;

        String tpId = sharedPreferences.getString(context.getString(R.string.preferences_selected_tp_id), "");
        TariffPlan tariffPlan = mobileOperator.findTariffPlan(tpId);
        if (tariffPlan == null) {
            return;
        }

        Country country = mainData.getCountries().get(0);
        Region region = country.getRegions().get(0);

        String showedLimitsData = sharedPreferences.getString(context.getString(R.string.preferences_showed_calls_limits), "");

        ShowedLimits showedLimits = null;
        try {
            showedLimits = new ShowedLimits(showedLimitsData);
        } catch (JSONException e) {
            L.e(e.getMessage());
        }
        if (showedLimits == null) {
            showedLimits = new ShowedLimits();
        }

        checkCallsOfTariffPlanLimits(context, showedLimits, country, region, tariffPlan, startDate);
        checkCallsUserLimits(context, showedLimits, country, region, startDate);

        showedLimitsData = showedLimits.GetJSon();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getString(R.string.preferences_showed_calls_limits), showedLimitsData);
        editor.apply();
        L.e("checkCallsLimits END - " + new SimpleDateFormat("HH:mm:ss.SSSZ").format(Calendar.getInstance().getTime()));
    }

    private static void checkCallsOfTariffPlanLimits(Context context, ShowedLimits showedLimits, Country country, Region region, TariffPlan tariffPlan, Date startDate) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int limitMinimumStep = sharedPreferences.getInt(context.getString(R.string.preferences_minimum_limit_step), 0);
        if (limitMinimumStep == 0) {
            return;
        }

        Pair<Date, Date> intervalPeriod = tariffPlan.getIntervalPeriodByDate(startDate);
        startDate = intervalPeriod.first;
        Date endDate = intervalPeriod.second;

        ArrayList<String> countryPrefixes = new ArrayList<>(Arrays.asList(country.getNumberPrefix().split(";")));
        ArrayList<String> regionPrefixes = new ArrayList<>(Arrays.asList(region.getNumberPrefix().split(";")));

        StatisticCallsForCalculation callsForCalculation = new StatisticCallsForCalculation(
                StatisticData.getInstance().getStatisticCalls(countryPrefixes, regionPrefixes,
                        region.getMinDigitsCount(), region.getMaxDigitsCount(), startDate, endDate, context));

        PackageCalculator packageCalculator = new PackageCalculator(tariffPlan, startDate);

        if (tariffPlan.getCallsCondition().getInRegionConditions() != null &&
                tariffPlan.getCallsCondition().getInRegionConditions().getLimits() != null) {
            packageCalculator.CalculateCallsLimits(callsForCalculation.getInRegionCallsForCalculation(),
                    tariffPlan.getCallsCondition().getInRegionConditions().getLimits(), startDate, endDate);
        }
        if (tariffPlan.getCallsCondition().getInterRegionsConditions() != null &&
                tariffPlan.getCallsCondition().getInterRegionsConditions().getLimits() != null) {
            packageCalculator.CalculateCallsLimits(callsForCalculation.getInterRegionCallsForCalculation(),
                    tariffPlan.getCallsCondition().getInterRegionsConditions().getLimits(), startDate, endDate);
        }
        if (tariffPlan.getCallsCondition().getInterCountriesConditions() != null &&
                tariffPlan.getCallsCondition().getInterCountriesConditions().getLimits() != null) {
            packageCalculator.CalculateCallsLimits(callsForCalculation.getInternationalCallsForCalculation(),
                    tariffPlan.getCallsCondition().getInterCountriesConditions().getLimits(), startDate, endDate);
        }

        // C настроек берем лимиты которые мы уже показали


        // удалить из списка сохраненных лимитов, которые не входят в промежуток startDate - endDate
        for (int i = showedLimits.size() - 1; i >= 0; i--) {
            ShowedLimit showedLimit = showedLimits.get(i);
            if (showedLimit.getId().equalsIgnoreCase(context.getString(R.string.preferences_user_calls_mix_limits))) {
                continue;
            }
            if (showedLimit.getDate().before(startDate) || showedLimit.getDate().after(endDate)) {
                showedLimits.remove(i);
            }
        }

        Enumeration<Limit> e = packageCalculator.getUsedLimits().keys();
        while (e.hasMoreElements()) {
            Limit limit = e.nextElement();
            boolean isLimitIsUp = packageCalculator.isLimitIsUp(limit); // Лимит исчерпан
            if (isLimitIsUp) { // Отображение сообщений, что лимит полностью исчерпан
                String limitUpNotificationText = String.format(context.getString(R.string.limit_is_up_notification_text),
                        tariffPlan.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()),
                        limit.getTitles() == null ? "" : limit.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode())); // текст notification

                boolean showNotification = true;
                boolean notificationNeverShowed = true; // notification никогда не отображался
                for (ShowedLimit showedLimit : showedLimits) { // пробегаемся по отображеным лимитам
                    if (showedLimit.getId().equalsIgnoreCase(limit.getId())) { // проверяем по id
                        notificationNeverShowed = false; // notification отображался
                        showNotification = showedLimit.getViewType() != ShowedLimit.VIEW_TYPE_LIMIT_IS_UP; // Отображаем сообщение который не был отображен как "исчерпан"
                        if (showNotification) {
                            showedLimit.setDate(Calendar.getInstance().getTime()); // дата отображения
                            showedLimit.setViewType(ShowedLimit.VIEW_TYPE_LIMIT_IS_UP); // был отобажен когда лимит исчерпан

                            Intent in = new Intent(context, TestActivity.class);

                            Utils.showNotification(context, in, Integer.parseInt(limit.getId()),
                                    context.getString(R.string.limit_is_up_notification_title), context.getString(R.string.limit_is_up_notification_title),
                                    limitUpNotificationText, R.drawable.ic_launcher);  // отобразить notification

                            showNotification = false;
                            break; // выходим из цикла
                        }
                    }
                }
                if (showNotification) { // если notification не был отображен
                    Intent in = new Intent(context, TestActivity.class);
                    Utils.showNotification(context, in, Integer.parseInt(limit.getId()),
                            context.getString(R.string.limit_is_up_notification_title), context.getString(R.string.limit_is_up_notification_title),
                            limitUpNotificationText, R.drawable.ic_launcher);  // отобразить notification
                }
                if (notificationNeverShowed) { // Если notification никогда не отображался то тогда добавляем его в список отображеных)
                    showedLimits.add(new ShowedLimit(limit.getId(), Calendar.getInstance().getTime(), ShowedLimit.VIEW_TYPE_LIMIT_IS_UP));
                }
            } else { // Отображение сообщений, что лимит подходит к концу
                int usedLimit = packageCalculator.getUsedLimits().get(limit); // кол-во использованного лимита
                double limitMinimumStepValue = limit.getValue() * limitMinimumStep / 100; // значение минимального порога лимита

                boolean isLimitIsMinimumStep = (limit.getValue() - usedLimit) <= limitMinimumStepValue; // перешел ли за миним порог лимита

                if (isLimitIsMinimumStep) {
                    boolean notificationShowed = false;
                    for (ShowedLimit showedLimit : showedLimits) {
                        if (showedLimit.getId().equalsIgnoreCase(limit.getId())) {
                            notificationShowed = true;
                            break;
                        }
                    }
                    if (!notificationShowed) {
                        String minimumNotificationText = String.format(context.getString(R.string.limit_minimum_notification_text),
                                tariffPlan.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()),
                                limit.getTitles() == null ? "" : limit.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()), limit.getValue() - usedLimit);

                        Intent in = new Intent(context, MainActivity.class);
                        in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_CALLS);
                        Utils.showNotification(context, in, Integer.parseInt(limit.getId()),
                                context.getString(R.string.limit_minimum_notification_title), context.getString(R.string.limit_minimum_notification_title),
                                minimumNotificationText, R.drawable.ic_launcher);

                        showedLimits.add(new ShowedLimit(limit.getId(), Calendar.getInstance().getTime(), ShowedLimit.VIEW_TYPE_MINIMUM_STEP));
                    }
                }
            }
        }
    }

    /*
    * Метод проверяет кол-во использованного лимита, установленного пользователем в виде константы (Например 45 минут - входящих и исходящих звонков)
    * */
    private static void checkCallsUserLimits(Context context, ShowedLimits showedLimits, Country country, Region region, Date startDate) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        // Высчитываем общий лимит для исходящих звонков, так как тарифицируются только исходящие звонки
        int mixCallsLimit = sharedPreferences.getInt(context.getString(R.string.preferences_user_calls_mix_limits), 0);
        if (mixCallsLimit == 0) {
            return;
        }

        Pair<Date, Date> intervalPeriod = Utils.getMonthOfTheDate(startDate);
        startDate = intervalPeriod.first;
        Date endDate = intervalPeriod.second;

        ArrayList<String> countryPrefixes = new ArrayList<>(Arrays.asList(country.getNumberPrefix().split(";")));
        ArrayList<String> regionPrefixes = new ArrayList<>(Arrays.asList(region.getNumberPrefix().split(";")));

        StatisticCallsForCalculation callsForCalculation = new StatisticCallsForCalculation(
                StatisticData.getInstance().getStatisticCalls(countryPrefixes, regionPrefixes,
                        region.getMinDigitsCount(), region.getMaxDigitsCount(), startDate, endDate, context));

        ShowedLimit mixShowedLimit = showedLimits.findShowedLimitById(context.getString(R.string.preferences_user_calls_mix_limits));
        if (mixShowedLimit == null) {
            mixShowedLimit = new ShowedLimit(context.getString(R.string.preferences_user_calls_mix_limits), startDate, ShowedLimit.VIEW_TYPE_LIMIT_IS_NOT_SHOWED);
            showedLimits.add(mixShowedLimit);
        } else {
            if (mixShowedLimit.getDate().before(startDate) || mixShowedLimit.getDate().after(endDate)) {
                showedLimits.remove(mixShowedLimit);
                mixShowedLimit = new ShowedLimit(context.getString(R.string.preferences_user_calls_mix_limits), startDate, ShowedLimit.VIEW_TYPE_LIMIT_IS_NOT_SHOWED);
                showedLimits.add(mixShowedLimit);
            }
        }

        int incomingCalls = 0;
        int outgoingCalls = 0;

        // Подсчитываем общее кол-во входящих и исходящих минут
        for (StatisticCallForCalculation call : callsForCalculation.getAllCallsForCalculation()) {
            int minutes = call.getNotCalculatedMinutes();
            if (minutes == 0) {
                continue;
            }
            if (call.getCallType() == DataDirectionTypes.INCOMING) {
                incomingCalls += minutes;
            } else if (call.getCallType() == DataDirectionTypes.OUTGOING) {
                outgoingCalls += minutes;
            }
        }

        if (mixCallsLimit <= outgoingCalls) {
            if (mixShowedLimit.getViewType() != ShowedLimit.VIEW_TYPE_LIMIT_IS_UP) {

                mixShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_LIMIT_IS_UP);
                mixShowedLimit.setDate(startDate);

                String limitIsUpNotificationText = String.format(context.getString(R.string.user_limit_is_up_notification_text), context.getString(R.string.limit_mix_calls_type));

                Intent in = new Intent(context, MainActivity.class);
                in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_CALLS);
                Utils.showNotification(context, in, calls_limit_is_up_notificationId,
                        context.getString(R.string.limit_is_up_notification_title),
                        context.getString(R.string.limit_is_up_notification_title),
                        limitIsUpNotificationText, R.drawable.ic_launcher);
            }
        } else {
            double limitMinimumStepValue = mixCallsLimit * minimum_limit_step / 100; // значение минимального порога лимита
            boolean isLimitIsMinimumStep = (mixCallsLimit - outgoingCalls) <= limitMinimumStepValue; // перешел ли за миним порог лимита
            if (isLimitIsMinimumStep) {
                if (mixShowedLimit.getViewType() == ShowedLimit.VIEW_TYPE_LIMIT_IS_NOT_SHOWED) {
                    mixShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_MINIMUM_STEP);
                    mixShowedLimit.setDate(startDate);
                    String minimumNotificationText = String.format(context.getString(R.string.user_limit_minimum_notification_text),
                            context.getString(R.string.limit_mix_calls_type), mixCallsLimit - outgoingCalls, context.getString(R.string.minutes));

                    Intent in = new Intent(context, MainActivity.class);
                    in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_CALLS);
                    Utils.showNotification(context, in, calls_minimum_is_up_notificationId,
                            context.getString(R.string.limit_minimum_notification_title),
                            context.getString(R.string.limit_minimum_notification_title),
                            minimumNotificationText, R.drawable.ic_launcher);
                }
            }
        }
//        // Высчитываем лимит для входящих звонков
//        int incomingCallsLimit = sharedPreferences.getInt(context.getString(R.string.preferences_user_calls_incoming_limits), 0);
//        if (incomingCallsLimit > 0) {
//            if (incomingCallsLimit <= incomingCalls) {
//                boolean showNotification = false;
//                if (incomingShowedLimit.getViewType() != ShowedLimit.VIEW_TYPE_LIMIT_IS_UP) {
//                    showNotification = true;
//                    incomingShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_LIMIT_IS_UP);
//                    incomingShowedLimit.setDate(c.getTime());
//                }
//                if (showNotification) {
//                    String limitIsUpNotificationText = String.format(context.getString(R.string.user_limit_is_up_notification_text),
//                            context.getString(R.string.limit_incoming_calls_type));
//
//                    Intent in = new Intent(context, MainActivity.class);
//                    in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_CALLS);
//                    Utils.showNotification(context, in, calls_incoming_limit_is_up_notificationId,
//                            context.getString(R.string.limit_is_up_notification_title),
//                            context.getString(R.string.limit_is_up_notification_title),
//                            limitIsUpNotificationText, R.drawable.ic_launcher);
//                }
//            } else {
//                double limitMinimumStepValue = incomingCallsLimit * minimumLimitStep / 100; // значение минимального порога лимита
//                boolean isLimitIsMinimumStep = (incomingCallsLimit - incomingCalls) <= limitMinimumStepValue; // перешел ли за миним порог лимита
//                if (isLimitIsMinimumStep) {
//                    if (incomingShowedLimit.getViewType() == ShowedLimit.VIEW_TYPE_LIMIT_IS_NOT_SHOWED) {
//                        incomingShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_MINIMUM_STEP);
//                        incomingShowedLimit.setDate(c.getTime());
//                        String minimumNotificationText = String.format(context.getString(R.string.user_limit_minimum_notification_text),
//                                context.getString(R.string.limit_incoming_calls_type), incomingCallsLimit - incomingCalls, context.getString(R.string.minutes));
//
//                        Intent in = new Intent(context, MainActivity.class);
//                        in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_CALLS);
//                        Utils.showNotification(context, in, calls_incoming_minimum_notificationId,
//                                context.getString(R.string.limit_minimum_notification_title),
//                                context.getString(R.string.limit_minimum_notification_title),
//                                minimumNotificationText, R.drawable.ic_launcher);
//                    }
//                }
//            }
//        }
//
//        // Высчитываем лимит для исходящих звонков
//        int outgoingCallsLimit = sharedPreferences.getInt(context.getString(R.string.preferences_user_calls_outgoing_limits), 0);
//        if (outgoingCallsLimit > 0) {
//            if (outgoingCallsLimit <= outgoingCalls) {
//                boolean showNotification = false;
//                if (outgoingShowedLimit.getViewType() != ShowedLimit.VIEW_TYPE_LIMIT_IS_UP) {
//                    showNotification = true;
//                    outgoingShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_LIMIT_IS_UP);
//                    outgoingShowedLimit.setDate(c.getTime());
//                }
//                if (showNotification) {
//                    String limitIsUpNotificationText = String.format(context.getString(R.string.user_limit_is_up_notification_text),
//                            context.getString(R.string.limit_outgoing_calls_type));
//
//                    Intent in = new Intent(context, MainActivity.class);
//                    in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_CALLS);
//                    Utils.showNotification(context, in, calls_outgoing_limit_is_up_notificationId,
//                            context.getString(R.string.limit_is_up_notification_title),
//                            context.getString(R.string.limit_is_up_notification_title),
//                            limitIsUpNotificationText, R.drawable.ic_launcher);
//                }
//            } else {
//                double limitMinimumStepValue = outgoingCallsLimit * minimumLimitStep / 100; // значение минимального порога лимита
//                boolean isLimitIsMinimumStep = (outgoingCallsLimit - outgoingCalls) <= limitMinimumStepValue; // перешел ли за миним порог лимита
//                if (isLimitIsMinimumStep) {
//                    if (outgoingShowedLimit.getViewType() == ShowedLimit.VIEW_TYPE_LIMIT_IS_NOT_SHOWED) {
//                        outgoingShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_MINIMUM_STEP);
//                        outgoingShowedLimit.setDate(c.getTime());
//
//                        String minimumNotificationText = String.format(context.getString(R.string.user_limit_minimum_notification_text),
//                                context.getString(R.string.limit_outgoing_calls_type), outgoingCallsLimit - outgoingCalls, context.getString(R.string.minutes));
//
//                        Intent in = new Intent(context, MainActivity.class);
//                        in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_CALLS);
//                        Utils.showNotification(context, in, calls_outgoing_minimum_notificationId,
//                                context.getString(R.string.limit_minimum_notification_title),
//                                context.getString(R.string.limit_minimum_notification_title),
//                                minimumNotificationText, R.drawable.ic_launcher);
//                    }
//                }
//            }
//        }
    }

    public static void checkSmsLimits(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean showNotifications = sharedPreferences.getBoolean(context.getString(R.string.preferences_show_notifications), false);
        if (!showNotifications) { // Если показывать нотификации не надо, то и подсчитывать лимиты не стоит
            return;
        }

        Calendar c = Calendar.getInstance();
        Date startDate = c.getTime();

        L.e("checkSmsLimits START - " + new SimpleDateFormat("HH:mm:ss.SSSZ").format(startDate));
        MainData mainData = FileCache.getMainData();
        if (mainData == null)
            return;

        String mobileOperatorId = sharedPreferences.getString(context.getString(R.string.preferences_selected_mobile_operator_id), "");
        if (mainData.getCountries() == null || mainData.getCountries().size() == 0 ||
                mainData.getCountries().get(0).getRegions() == null || mainData.getCountries().get(0).getRegions().size() == 0) {
            return;
        }
        MobileOperator mobileOperator = mainData.getCountries().get(0).getRegions().get(0).findMobileOperator(mobileOperatorId);
        if (mobileOperator == null)
            return;

        String tpId = sharedPreferences.getString(context.getString(R.string.preferences_selected_tp_id), "");
        TariffPlan tariffPlan = mobileOperator.findTariffPlan(tpId);
        if (tariffPlan == null) {
            return;
        }

        Country country = mainData.getCountries().get(0);
        Region region = country.getRegions().get(0);

        String showedLimitsData = sharedPreferences.getString(context.getString(R.string.preferences_showed_msg_limits), "");
        ShowedLimits showedLimits = null;
        try {
            showedLimits = new ShowedLimits(showedLimitsData);
        } catch (JSONException e) {
            L.e(e.getMessage());
        }
        if (showedLimits == null) {
            showedLimits = new ShowedLimits();
        }


        checkSmsOfTariffPlanLimits(context, showedLimits, country, region, tariffPlan, startDate);

        checkSmsUserLimits(context, showedLimits, country, region, startDate);

        showedLimitsData = showedLimits.GetJSon();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getString(R.string.preferences_showed_msg_limits), showedLimitsData);
        editor.apply();


        L.e("checkSmsLimits END - " + new SimpleDateFormat("HH:mm:ss.SSSZ").format(Calendar.getInstance().getTime()));
    }

    private static void checkSmsOfTariffPlanLimits(Context context, ShowedLimits showedLimits, Country country, Region region, TariffPlan tariffPlan, Date startDate) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int limitMinimumStep = sharedPreferences.getInt(context.getString(R.string.preferences_minimum_limit_step), 0);

        if (limitMinimumStep == 0) {
            return;
        }

        Pair<Date, Date> intervalPeriod = tariffPlan.getIntervalPeriodByDate(startDate);

        startDate = intervalPeriod.first;
        Date endDate = intervalPeriod.second;

        ArrayList<String> countryPrefixes = new ArrayList<>(Arrays.asList(country.getNumberPrefix().split(";")));
        ArrayList<String> regionPrefixes = new ArrayList<>(Arrays.asList(region.getNumberPrefix().split(";")));

        StatisticMessagesForCalculation messagesForCalculation = new StatisticMessagesForCalculation(
                StatisticData.getInstance().getStatisticMessages(countryPrefixes, regionPrefixes,
                        region.getMinDigitsCount(), region.getMaxDigitsCount(), StatisticMessage.TYPE_MSG_SMS,
                        startDate, endDate, context));

        PackageCalculator packageCalculator = new PackageCalculator(tariffPlan, startDate);

        if (tariffPlan.getSmsCondition().getInRegionConditions() != null &&
                tariffPlan.getSmsCondition().getInRegionConditions().getLimits() != null) {
            packageCalculator.CalculateMessagesLimits(messagesForCalculation.getInRegionMessagesForCalculation(),
                    tariffPlan.getSmsCondition().getInRegionConditions().getLimits(), startDate, endDate);
        }
        if (tariffPlan.getSmsCondition().getInterRegionsConditions() != null &&
                tariffPlan.getSmsCondition().getInterRegionsConditions().getLimits() != null) {
            packageCalculator.CalculateMessagesLimits(messagesForCalculation.getInterRegionMessagesForCalculation(),
                    tariffPlan.getSmsCondition().getInterRegionsConditions().getLimits(), startDate, endDate);
        }
        if (tariffPlan.getSmsCondition().getInterCountriesConditions() != null &&
                tariffPlan.getSmsCondition().getInterCountriesConditions().getLimits() != null) {
            packageCalculator.CalculateMessagesLimits(messagesForCalculation.getInternationalMessagesForCalculation(),
                    tariffPlan.getSmsCondition().getInterCountriesConditions().getLimits(), startDate, endDate);
        }

        // удалить из списка сохраненных лимитов, которые не входят в промежуток startDate - endDate
        for (int i = showedLimits.size() - 1; i >= 0; i--) {
            ShowedLimit showedLimit = showedLimits.get(i);
            if (showedLimit.getId().equalsIgnoreCase(context.getString(R.string.preferences_user_sms_mix_limits))) {
                continue;
            }
            if (showedLimit.getDate().before(startDate) || showedLimit.getDate().after(endDate)) {
                showedLimits.remove(i);
            }
        }

        Enumeration<Limit> e = packageCalculator.getUsedLimits().keys();
        while (e.hasMoreElements()) {
            Limit limit = e.nextElement();
            boolean isLimitIsUp = packageCalculator.isLimitIsUp(limit); // Лимит исчерпан
            if (isLimitIsUp) {
                String limitUpNotificationText = String.format(context.getString(R.string.limit_is_up_notification_text),
                        tariffPlan.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()),
                        limit.getTitles() == null ? "" : limit.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode())); // текст notification

                boolean showNotification = true;
                boolean notificationNeverShowed = true; // notification никогда не отображался
                for (ShowedLimit showedLimit : showedLimits) { // пробегаемся по отображеным лимитам
                    if (showedLimit.getId().equalsIgnoreCase(limit.getId())) { // проверяем по id
                        notificationNeverShowed = false; // notification отображался
                        showNotification = showedLimit.getViewType() != ShowedLimit.VIEW_TYPE_LIMIT_IS_UP;
                        if (!showNotification) {
                            showedLimit.setDate(Calendar.getInstance().getTime()); // дата отображения
                            showedLimit.setViewType(ShowedLimit.VIEW_TYPE_LIMIT_IS_UP); // был отобажен когда лимит исчерпан

                            Intent in = new Intent(context, MainActivity.class);
                            in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_MESSAGES);
                            Utils.showNotification(context, in, Integer.parseInt(limit.getId()),
                                    context.getString(R.string.limit_is_up_notification_title), context.getString(R.string.limit_is_up_notification_title),
                                    limitUpNotificationText, R.drawable.ic_launcher);  // отобразить notification

                            showNotification = false; // ставим флаг что notification отображен
                            break; // выходим из цикла
                        }
                    }
                }
                if (showNotification) {
                    Intent in = new Intent(context, MainActivity.class);
                    in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_MESSAGES);
                    Utils.showNotification(context, in, Integer.parseInt(limit.getId()),
                            context.getString(R.string.limit_is_up_notification_title), context.getString(R.string.limit_is_up_notification_title),
                            limitUpNotificationText, R.drawable.ic_launcher);  // отобразить notification
                }
                if (notificationNeverShowed) { // Если notification никогда не отображался то тогда добавляем его в список отображеных)
                    showedLimits.add(new ShowedLimit(limit.getId(), Calendar.getInstance().getTime(), ShowedLimit.VIEW_TYPE_LIMIT_IS_UP));
                }
            } else {
                int usedLimit = packageCalculator.getUsedLimits().get(limit); // кол-во использованного лимита
                double limitMinimumStepValue = limit.getValue() * limitMinimumStep / 100; // значение минимального порога лимита

                boolean isLimitIsMinimumStep = (limit.getValue() - usedLimit) <= limitMinimumStepValue; // перешел ли за миним порог лимита

                if (isLimitIsMinimumStep) {
                    boolean notificationShowed = false;
                    for (ShowedLimit showedLimit : showedLimits) {
                        if (showedLimit.getId().equalsIgnoreCase(limit.getId())) {
                            notificationShowed = true;
                            break;
                        }
                    }
                    if (!notificationShowed) {
                        String minimumNotificationText = String.format(context.getString(R.string.limit_minimum_notification_text),
                                tariffPlan.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()),
                                limit.getTitles() == null ? "" : limit.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()), limit.getValue() - usedLimit);

                        Intent in = new Intent(context, MainActivity.class);
                        in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_MESSAGES);
                        Utils.showNotification(context, in, Integer.parseInt(limit.getId()),
                                context.getString(R.string.limit_minimum_notification_title), context.getString(R.string.limit_minimum_notification_title),
                                minimumNotificationText, R.drawable.ic_launcher);

                        showedLimits.add(new ShowedLimit(limit.getId(), Calendar.getInstance().getTime(), ShowedLimit.VIEW_TYPE_MINIMUM_STEP));
                    }
                }
            }
        }
    }

    private static void checkSmsUserLimits(Context context, ShowedLimits showedLimits, Country country, Region region, Date startDate) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int messagesLimit = sharedPreferences.getInt(context.getString(R.string.preferences_user_sms_mix_limits), 0);

        if (messagesLimit == 0) {
            return;
        }

        Pair<Date, Date> intervalPeriod = Utils.getMonthOfTheDate(startDate);
        startDate = intervalPeriod.first;
        Date endDate = intervalPeriod.second;

        ShowedLimit mixShowedLimit = showedLimits.findShowedLimitById(context.getString(R.string.preferences_user_sms_mix_limits));
        if (mixShowedLimit == null) {
            mixShowedLimit = new ShowedLimit(context.getString(R.string.preferences_user_sms_mix_limits), startDate, ShowedLimit.VIEW_TYPE_LIMIT_IS_NOT_SHOWED);
            showedLimits.add(mixShowedLimit);
        } else {
            if (mixShowedLimit.getDate().before(startDate) || mixShowedLimit.getDate().after(endDate)) {
                showedLimits.remove(mixShowedLimit);
                mixShowedLimit = new ShowedLimit(context.getString(R.string.preferences_user_sms_mix_limits), startDate, ShowedLimit.VIEW_TYPE_LIMIT_IS_NOT_SHOWED);
                showedLimits.add(mixShowedLimit);
            }
        }

        int incomingMessages = 0;
        int outgoingMessages = 0;

        ArrayList<String> countryPrefixes = new ArrayList<>(Arrays.asList(country.getNumberPrefix().split(";")));
        ArrayList<String> regionPrefixes = new ArrayList<>(Arrays.asList(region.getNumberPrefix().split(";")));

        StatisticMessagesForCalculation messagesForCalculation = new StatisticMessagesForCalculation(
                StatisticData.getInstance().getStatisticMessages(countryPrefixes, regionPrefixes,
                        region.getMinDigitsCount(), region.getMaxDigitsCount(), StatisticMessage.TYPE_MSG_SMS, startDate, endDate, context));

        for (StatisticMessageForCalculation message : messagesForCalculation.getAllMessages()) {
            if (message.getDirectionType() == DataDirectionTypes.INCOMING) {
                incomingMessages += 1;
            } else if (message.getDirectionType() == DataDirectionTypes.OUTGOING) {
                outgoingMessages += 1;
            }
        }


        // Высчитываем общий лимит для исходящих CMC MMC, так как тарифицируются только исходящие
        if (messagesLimit <= outgoingMessages) {
            if (mixShowedLimit.getViewType() != ShowedLimit.VIEW_TYPE_LIMIT_IS_UP) {

                mixShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_LIMIT_IS_UP);
                mixShowedLimit.setDate(startDate);

                String limitIsUpNotificationText = String.format(context.getString(R.string.user_limit_is_up_notification_text),
                        context.getString(R.string.limit_mix_sms_type));

                Intent in = new Intent(context, MainActivity.class);
                in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_MESSAGES);
                Utils.showNotification(context, in, sms_limit_is_up_notificationId,
                        context.getString(R.string.limit_is_up_notification_title),
                        context.getString(R.string.limit_is_up_notification_title),
                        limitIsUpNotificationText, R.drawable.ic_launcher);
            }
        } else {
            double limitMinimumStepValue = messagesLimit * minimum_limit_step / 100; // значение минимального порога лимита
            boolean isLimitIsMinimumStep = (messagesLimit - outgoingMessages) <= limitMinimumStepValue; // перешел ли за миним порог лимита
            if (isLimitIsMinimumStep) {
                if (mixShowedLimit.getViewType() == ShowedLimit.VIEW_TYPE_LIMIT_IS_NOT_SHOWED) {
                    mixShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_MINIMUM_STEP);
                    mixShowedLimit.setDate(startDate);
                    String minimumNotificationText = String.format(context.getString(R.string.user_limit_minimum_notification_text),
                            context.getString(R.string.limit_mix_sms_type),
                            messagesLimit - outgoingMessages, context.getString(R.string.items));

                    Intent in = new Intent(context, MainActivity.class);
                    in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_MESSAGES);
                    Utils.showNotification(context, in, sms_minimum_is_up_notificationId, context.getString(R.string.limit_minimum_notification_title),
                            context.getString(R.string.limit_minimum_notification_title), minimumNotificationText, R.drawable.ic_launcher);
                }
            }
        }

// Расчет отдельно для входящих и исходящих смс сообщений
//        int incomingMessagesLimit;
//        int outgoingMessagesLimit;
//
//        if (isSms) {
//            incomingMessagesLimit = sharedPreferences.getInt(context.getString(R.string.preferences_user_sms_incoming_limits), 0);
//            outgoingMessagesLimit = sharedPreferences.getInt(context.getString(R.string.preferences_user_sms_outgoing_limits), 0);
//        } else {
//            incomingMessagesLimit = sharedPreferences.getInt(context.getString(R.string.preferences_user_mms_incoming_limits), 0);
//            outgoingMessagesLimit = sharedPreferences.getInt(context.getString(R.string.preferences_user_mms_outgoing_limits), 0);
//        }
//
//
//        // Высчитываем лимит для входящих CMC MMC
//        if (incomingMessagesLimit > 0) {
//            if (incomingMessagesLimit <= incomingMessages) {
//                boolean showNotification = false;
//                if (incomingShowedLimit.getViewType() != ShowedLimit.VIEW_TYPE_LIMIT_IS_UP) {
//                    showNotification = true;
//                    incomingShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_LIMIT_IS_UP);
//                    incomingShowedLimit.setDate(c.getTime());
//                }
//                if (showNotification) {
//                    String limitIsUpNotificationText = String.format(context.getString(R.string.user_limit_is_up_notification_text),
//                            context.getString(isSms ? R.string.limit_incoming_sms_type : R.string.limit_incoming_mms_type));
//
//                    Intent in = new Intent(context, MainActivity.class);
//                    in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_MESSAGES);
//                    Utils.showNotification(context, in, isSms ? sms_incoming_limit_is_up_notificationId : mms_incoming_limit_is_up_notificationId,
//                            context.getString(R.string.limit_is_up_notification_title),
//                            context.getString(R.string.limit_is_up_notification_title),
//                            limitIsUpNotificationText, R.drawable.ic_launcher);
//                }
//            } else {
//                double limitMinimumStepValue = incomingMessagesLimit * minimumLimitStep / 100; // значение минимального порога лимита
//                boolean isLimitIsMinimumStep = (incomingMessagesLimit - incomingMessages) <= limitMinimumStepValue; // перешел ли за миним порог лимита
//                if (isLimitIsMinimumStep) {
//                    if (incomingShowedLimit.getViewType() == ShowedLimit.VIEW_TYPE_LIMIT_IS_NOT_SHOWED) {
//                        incomingShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_MINIMUM_STEP);
//                        incomingShowedLimit.setDate(c.getTime());
//                        String minimumNotificationText = String.format(context.getString(R.string.user_limit_minimum_notification_text),
//                                context.getString(isSms ? R.string.limit_incoming_sms_type : R.string.limit_incoming_mms_type),
//                                incomingMessagesLimit - incomingMessages, context.getString(R.string.items));
//
//                        Intent in = new Intent(context, MainActivity.class);
//                        in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_MESSAGES);
//                        Utils.showNotification(context, in, isSms ? sms_incoming_minimum_notificationId : mms_incoming_minimum_notificationId,
//                                context.getString(R.string.limit_minimum_notification_title),
//                                context.getString(R.string.limit_minimum_notification_title),
//                                minimumNotificationText, R.drawable.ic_launcher);
//                    }
//                }
//            }
//        }
//
//        // Высчитываем лимит для исходящих CMC MMC
//        if (outgoingMessagesLimit > 0) {
//            if (outgoingMessagesLimit <= outgoingMessages) {
//                boolean showNotification = false;
//                if (outgoingShowedLimit.getViewType() != ShowedLimit.VIEW_TYPE_LIMIT_IS_UP) {
//                    showNotification = true;
//                    outgoingShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_LIMIT_IS_UP);
//                    outgoingShowedLimit.setDate(c.getTime());
//                }
//                if (showNotification) {
//                    String limitIsUpNotificationText = String.format(context.getString(R.string.user_limit_is_up_notification_text),
//                            context.getString(isSms ? R.string.limit_incoming_sms_type : R.string.limit_incoming_mms_type));
//
//                    Intent in = new Intent(context, MainActivity.class);
//                    in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_MESSAGES);
//                    Utils.showNotification(context, in, isSms ? sms_outgoing_limit_is_up_notificationId : mms_outgoing_limit_is_up_notificationId,
//                            context.getString(R.string.limit_is_up_notification_title),
//                            context.getString(R.string.limit_is_up_notification_title),
//                            limitIsUpNotificationText, R.drawable.ic_launcher);
//                }
//            } else {
//                double limitMinimumStepValue = outgoingMessagesLimit * minimumLimitStep / 100; // значение минимального порога лимита
//                boolean isLimitIsMinimumStep = (outgoingMessagesLimit - outgoingMessages) <= limitMinimumStepValue; // перешел ли за миним порог лимита
//                if (isLimitIsMinimumStep) {
//                    if (outgoingShowedLimit.getViewType() == ShowedLimit.VIEW_TYPE_LIMIT_IS_NOT_SHOWED) {
//                        outgoingShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_MINIMUM_STEP);
//                        outgoingShowedLimit.setDate(c.getTime());
//
//                        String minimumNotificationText = String.format(context.getString(R.string.user_limit_minimum_notification_text),
//                                context.getString(isSms ? R.string.limit_incoming_sms_type : R.string.limit_incoming_mms_type),
//                                outgoingMessagesLimit - outgoingMessages, context.getString(R.string.items));
//
//                        Intent in = new Intent(context, MainActivity.class);
//                        in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_MESSAGES);
//                        Utils.showNotification(context, in, isSms ? sms_outgoing_minimum_notificationId : mms_outgoing_minimum_notificationId,
//                                context.getString(R.string.limit_minimum_notification_title),
//                                context.getString(R.string.limit_minimum_notification_title),
//                                minimumNotificationText, R.drawable.ic_launcher);
//                    }
//                }
//            }
//        }
    }

    public static void checkInternetTrafficLimits(Context context) {

        Date startDate = Calendar.getInstance().getTime();

        L.e("checkInternetTrafficLimits START - " + new SimpleDateFormat("HH:mm:ss.SSSZ").format(startDate));

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean showNotifications = sharedPreferences.getBoolean(context.getString(R.string.preferences_show_notifications), false);
        if (!showNotifications) { // Если показывать нотификации не надо, то и подсчитывать лимиты не стоит
            return;
        }

        String showedLimitsData = sharedPreferences.getString(context.getString(R.string.preferences_showed_internet_limits), "");
        ShowedLimits showedLimits = null;
        try {
            showedLimits = new ShowedLimits(showedLimitsData);
        } catch (JSONException e) {
            L.e(e.getMessage());
        }
        if (showedLimits == null) {
            showedLimits = new ShowedLimits();
        }

        checkInternetTrafficOfTariffPlanLimits(context, showedLimits, startDate);

        checkInternetTrafficUserLimits(context, showedLimits, startDate);

        showedLimitsData = showedLimits.GetJSon();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getString(R.string.preferences_showed_internet_limits), showedLimitsData);
        editor.apply();
        L.e("checkInternetTrafficLimits END - " + new SimpleDateFormat("HH:mm:ss.SSSZ").format(Calendar.getInstance().getTime()));
    }

    private static void checkInternetTrafficOfTariffPlanLimits(Context context, ShowedLimits showedLimits, Date startDate) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int limitMinimumStep = sharedPreferences.getInt(context.getString(R.string.preferences_minimum_limit_step), 0);

        if (limitMinimumStep == 0) {
            return;
        }

        MainData mainData = FileCache.getMainData();
        if (mainData == null)
            return;

        String mobileOperatorId = sharedPreferences.getString(context.getString(R.string.preferences_selected_mobile_operator_id), "");
        if (mainData.getCountries() == null || mainData.getCountries().size() == 0 ||
                mainData.getCountries().get(0).getRegions() == null || mainData.getCountries().get(0).getRegions().size() == 0) {
            return;
        }
        MobileOperator mobileOperator = mainData.getCountries().get(0).getRegions().get(0).findMobileOperator(mobileOperatorId);
        if (mobileOperator == null)
            return;

        String tpId = sharedPreferences.getString(context.getString(R.string.preferences_selected_tp_id), "");
        TariffPlan tariffPlan = mobileOperator.findTariffPlan(tpId);
        if (tariffPlan == null) {
            return;
        }

        Pair<Date, Date> intervalPeriod = tariffPlan.getIntervalPeriodByDate(startDate);

        startDate = intervalPeriod.first;
        Date endDate = intervalPeriod.second;

        // удалить из списка сохраненных лимитов, которые не входят в промежуток startDate - endDate
        for (int i = showedLimits.size() - 1; i >= 0; i--) {
            ShowedLimit showedLimit = showedLimits.get(i);
            if (showedLimit.getId().equalsIgnoreCase(context.getString(R.string.preferences_user_internet_limits))) { // Для пользовательских лимитов, лимиты месячные
                continue;
            } else {
                if (showedLimit.getDate().before(startDate) || showedLimit.getDate().after(endDate)) {
                    showedLimits.remove(i);
                }
            }

        }

        StatisticInternetTraffics internetTraffics = StatisticData.getInstance().getStatisticInternetTraffic(context, startDate, endDate);
        internetTraffics.setMobileInternetTrafficInBytes(startDate, internetTraffics.getMobileInternetTrafficValueInBytes());

        StatisticInternetTrafficsForCalculation internetTrafficsForCalculation = new StatisticInternetTrafficsForCalculation(internetTraffics);

        PackageCalculator packageCalculator = new PackageCalculator(tariffPlan, startDate);

        if (tariffPlan.getInternetCondition() != null && tariffPlan.getInternetCondition().getInRegionConditions() != null && tariffPlan.getInternetCondition().getInRegionConditions().getLimits() != null) {

            packageCalculator.CalculateInternetTrafficLimits(internetTrafficsForCalculation.getMobileInternetTraffic(), tariffPlan.getInternetCondition().getInRegionConditions().getLimits(), startDate, endDate);
        }


        Enumeration<Limit> e = packageCalculator.getUsedLimits().keys();
        while (e.hasMoreElements()) {
            Limit limit = e.nextElement();
            boolean isLimitIsUp = packageCalculator.isLimitIsUp(limit); // Лимит исчерпан
            if (isLimitIsUp) {
                String limitUpNotificationText = String.format(context.getString(R.string.limit_is_up_notification_text),
                        tariffPlan.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()),
                        limit.getTitles() == null ? "" : limit.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode())); // текст notification

                boolean notificationShowed = false;
                boolean notificationNeverShowed = true; // notification никогда не отображался
                for (ShowedLimit showedLimit : showedLimits) { // пробегаемся по отображеным лимитам
                    if (showedLimit.getId().equalsIgnoreCase(limit.getId())) { // проверяем по id
                        notificationNeverShowed = false; // notification отображался
                        notificationShowed = showedLimit.getViewType() == ShowedLimit.VIEW_TYPE_LIMIT_IS_UP;
                        if (!notificationShowed) {
                            showedLimit.setDate(Calendar.getInstance().getTime()); // дата отображения
                            showedLimit.setViewType(ShowedLimit.VIEW_TYPE_LIMIT_IS_UP); // был отобажен когда лимит исчерпан

                            Intent in = new Intent(context, MainActivity.class);
                            in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_INTERNET);

                            Utils.showNotification(context, in, Integer.parseInt(limit.getId()),
                                    context.getString(R.string.limit_is_up_notification_title), context.getString(R.string.limit_is_up_notification_title),
                                    limitUpNotificationText, R.drawable.ic_launcher);  // отобразить notification

                            notificationShowed = true; // ставим флаг что notification отображен
                            break; // выходим из цикла
                        }
                    }
                }
                if (!notificationShowed) { // если notification не был отображен
                    Intent in = new Intent(context, MainActivity.class);
                    in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_INTERNET);
                    Utils.showNotification(context, in, Integer.parseInt(limit.getId()),
                            context.getString(R.string.limit_is_up_notification_title), context.getString(R.string.limit_is_up_notification_title),
                            limitUpNotificationText, R.drawable.ic_launcher);  // отобразить notification
                }
                if (notificationNeverShowed) { // Если notification никогда не отображался то тогда добавляем его в список отображеных)
                    showedLimits.add(new ShowedLimit(limit.getId(), Calendar.getInstance().getTime(), ShowedLimit.VIEW_TYPE_LIMIT_IS_UP));
                }
            } else {
                int usedLimit = packageCalculator.getUsedLimits().get(limit); // кол-во использованного лимита
                double limitMinimumStepValue = limit.getValue() * limitMinimumStep / 100; // значение минимального порога лимита

                boolean isLimitIsMinimumStep = (limit.getValue() - usedLimit) <= limitMinimumStepValue; // перешел ли за миним порог лимита

                if (isLimitIsMinimumStep) {
                    boolean notificationShowed = false;
                    for (ShowedLimit showedLimit : showedLimits) {
                        if (showedLimit.getId().equalsIgnoreCase(limit.getId())) {
                            notificationShowed = true;
                            break;
                        }
                    }
                    if (!notificationShowed) {
                        String minimumNotificationText = String.format(context.getString(R.string.limit_minimum_notification_text),
                                tariffPlan.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()),
                                limit.getTitles() == null ? "" : limit.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()), limit.getValue() - usedLimit);

                        Intent in = new Intent(context, MainActivity.class);
                        in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_INTERNET);
                        Utils.showNotification(context, in, Integer.parseInt(limit.getId()),
                                context.getString(R.string.limit_minimum_notification_title), context.getString(R.string.limit_minimum_notification_title),
                                minimumNotificationText, R.drawable.ic_launcher);

                        showedLimits.add(new ShowedLimit(limit.getId(), Calendar.getInstance().getTime(), ShowedLimit.VIEW_TYPE_MINIMUM_STEP));
                    }
                }
            }
        }
    }

    private static void checkInternetTrafficUserLimits(Context context, ShowedLimits showedLimits, Date startDate) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int internetTrafficLimit = sharedPreferences.getInt(context.getString(R.string.preferences_user_internet_limits), 0);
        if (internetTrafficLimit == 0) {
            return;
        }

        ShowedLimit userInternetShowedLimit = showedLimits.findShowedLimitById(context.getString(R.string.preferences_user_internet_limits));

        Pair<Date, Date> intervalPeriod = Utils.getMonthOfTheDate(startDate);
        startDate = intervalPeriod.first;
        Date endDate = intervalPeriod.second;
        if (userInternetShowedLimit == null) {
            userInternetShowedLimit = new ShowedLimit(context.getString(R.string.preferences_user_internet_limits), startDate, ShowedLimit.VIEW_TYPE_LIMIT_IS_NOT_SHOWED);
            showedLimits.add(userInternetShowedLimit);
        } else {
            if (userInternetShowedLimit.getDate().before(startDate) || userInternetShowedLimit.getDate().after(endDate)) {
                showedLimits.remove(userInternetShowedLimit);
                userInternetShowedLimit = new ShowedLimit(context.getString(R.string.preferences_user_internet_limits), startDate, ShowedLimit.VIEW_TYPE_LIMIT_IS_NOT_SHOWED);
                showedLimits.add(userInternetShowedLimit);
            }
        }

        StatisticInternetTraffics internetTraffics = StatisticData.getInstance().getStatisticInternetTraffic(context, startDate, endDate);
        internetTraffics.setMobileInternetTrafficInBytes(startDate, internetTraffics.getMobileInternetTrafficValueInBytes());
        StatisticInternetTrafficsForCalculation internetTrafficsForCalculation = new StatisticInternetTrafficsForCalculation(internetTraffics);

        int totalTraffic = 0;

        for (StatisticInternetTrafficForCalculation internetTraffic : internetTrafficsForCalculation.getMobileInternetTraffic()) {
            int receivedMegaBytes = internetTraffic.getMobileReceivedNotCalculatedMegaBytes();
            if (receivedMegaBytes != 0) {
                totalTraffic += receivedMegaBytes;
            }

            int transmittedMegaBytes = internetTraffic.getMobileTransmittedMegaBytes();
            if (transmittedMegaBytes != 0) {
                totalTraffic += transmittedMegaBytes;
            }

        }

        if (internetTrafficLimit <= totalTraffic) {
            if (userInternetShowedLimit.getViewType() != ShowedLimit.VIEW_TYPE_LIMIT_IS_UP) {

                userInternetShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_LIMIT_IS_UP);
                userInternetShowedLimit.setDate(startDate);

                String limitIsUpNotificationText = String.format(context.getString(R.string.user_limit_is_up_notification_text),
                        context.getString(R.string.limit_internet_type));

                Intent in = new Intent(context, MainActivity.class);
                in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_INTERNET);
                Utils.showNotification(context, in, internet_limit_is_up_notificationId,
                        context.getString(R.string.limit_is_up_notification_title),
                        context.getString(R.string.limit_is_up_notification_title),
                        limitIsUpNotificationText, R.drawable.ic_launcher);
            }
        } else {
            double limitMinimumStepValue = internetTrafficLimit * minimum_limit_step / 100; // значение минимального порога лимита
            boolean isLimitIsMinimumStep = (internetTrafficLimit - totalTraffic) <= limitMinimumStepValue; // перешел ли за миним порог лимита
            if (isLimitIsMinimumStep) {
                if (userInternetShowedLimit.getViewType() == ShowedLimit.VIEW_TYPE_LIMIT_IS_NOT_SHOWED) {
                    userInternetShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_MINIMUM_STEP);
                    userInternetShowedLimit.setDate(startDate);
                    String minimumNotificationText = String.format(context.getString(R.string.user_limit_minimum_notification_text),
                            context.getString(R.string.limit_internet_type), internetTrafficLimit - totalTraffic, context.getString(R.string.mb));

                    Intent in = new Intent(context, MainActivity.class);
                    in.putExtra(MainActivity.ARG_CHART_FRAGMENT_TYPE, ChartBaseFragment.CHART_FRAGMENT_INTERNET);
                    Utils.showNotification(context, in, internet_minimum_notificationId,
                            context.getString(R.string.limit_minimum_notification_title),
                            context.getString(R.string.limit_minimum_notification_title),
                            minimumNotificationText, R.drawable.ic_launcher);
                }
            }
        }

        // <editor-fold desc="Раздельный учет интернет трафика (входящего и исходящего)">
//        int receivedTrafficLimit = sharedPreferences.getInt(context.getString(R.string.preferences_user_internet_incoming_limits), 0);
//        int transmttedTrafficLimit = sharedPreferences.getInt(context.getString(R.string.preferences_user_internet_outgoing_limits), 0);
//
//        if (receivedTrafficLimit > 0) {
//            if (receivedTrafficLimit <= receivedTraffic) {
//                boolean showNotification = false;
//                if (incomingShowedLimit.getViewType() != ShowedLimit.VIEW_TYPE_LIMIT_IS_UP) {
//                    showNotification = true;
//                    incomingShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_LIMIT_IS_UP);
//                    incomingShowedLimit.setDate(c.getTime());
//                }
//                if (showNotification) {
//                    String limitIsUpNotificationText = String.format(context.getString(R.string.user_limit_is_up_notification_text),
//                            context.getString(R.string.limit_internet_type));
//
//                    Intent in = new Intent(context, TestActivity.class);
//                    Utils.showNotification(context, in, internet_incoming_limit_is_up_notificationId,
//                            context.getString(R.string.limit_is_up_notification_title),
//                            context.getString(R.string.limit_is_up_notification_title),
//                            limitIsUpNotificationText, R.drawable.ic_launcher);
//                }
//            } else {
//                double limitMinimumStepValue = receivedTrafficLimit * minimumLimitStep / 100; // значение минимального порога лимита
//                boolean isLimitIsMinimumStep = (receivedTrafficLimit - receivedTraffic) <= limitMinimumStepValue; // перешел ли за миним порог лимита
//                if (isLimitIsMinimumStep) {
//                    if (incomingShowedLimit.getViewType() == ShowedLimit.VIEW_TYPE_LIMIT_IS_NOT_SHOWED) {
//                        incomingShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_MINIMUM_STEP);
//                        incomingShowedLimit.setDate(c.getTime());
//                        String minimumNotificationText = String.format(context.getString(R.string.user_limit_minimum_notification_text),
//                                context.getString(R.string.limit_internet_type), receivedTrafficLimit - receivedTraffic);
//
//                        Intent in = new Intent(context, TestActivity.class);
//                        Utils.showNotification(context, in, internet_incoming_minimum_notificationId,
//                                context.getString(R.string.limit_minimum_notification_title),
//                                context.getString(R.string.limit_minimum_notification_title),
//                                minimumNotificationText, R.drawable.ic_launcher);
//                    }
//                }
//            }
//        }
//
//        if (transmttedTrafficLimit > 0) {
//            if (transmttedTrafficLimit <= transmittedTraffic) {
//                boolean showNotification = false;
//                if (outgoingShowedLimit.getViewType() != ShowedLimit.VIEW_TYPE_LIMIT_IS_UP) {
//                    showNotification = true;
//                    outgoingShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_LIMIT_IS_UP);
//                    outgoingShowedLimit.setDate(c.getTime());
//                }
//                if (showNotification) {
//                    String limitIsUpNotificationText = String.format(context.getString(R.string.user_limit_is_up_notification_text),
//                            context.getString(R.string.limit_internet_type));
//
//                    Intent in = new Intent(context, TestActivity.class);
//                    Utils.showNotification(context, in, internet_outgoing_limit_is_up_notificationId,
//                            context.getString(R.string.limit_is_up_notification_title),
//                            context.getString(R.string.limit_is_up_notification_title),
//                            limitIsUpNotificationText, R.drawable.ic_launcher);
//                }
//            } else {
//                double limitMinimumStepValue = transmttedTrafficLimit * minimumLimitStep / 100; // значение минимального порога лимита
//                boolean isLimitIsMinimumStep = (transmttedTrafficLimit - transmittedTraffic) <= limitMinimumStepValue; // перешел ли за миним порог лимита
//                if (isLimitIsMinimumStep) {
//                    if (outgoingShowedLimit.getViewType() == ShowedLimit.VIEW_TYPE_LIMIT_IS_NOT_SHOWED) {
//                        outgoingShowedLimit.setViewType(ShowedLimit.VIEW_TYPE_MINIMUM_STEP);
//                        outgoingShowedLimit.setDate(c.getTime());
//
//                        String minimumNotificationText = String.format(context.getString(R.string.user_limit_minimum_notification_text),
//                                context.getString(R.string.limit_internet_type), transmttedTrafficLimit - transmittedTraffic);
//
//                        Intent in = new Intent(context, TestActivity.class);
//                        Utils.showNotification(context, in, internet_outgoing_minimum_notificationId,
//                                context.getString(R.string.limit_minimum_notification_title),
//                                context.getString(R.string.limit_minimum_notification_title),
//                                minimumNotificationText, R.drawable.ic_launcher);
//                    }
//                }
//            }
//        }
        // </editor-fold>

    }

}
