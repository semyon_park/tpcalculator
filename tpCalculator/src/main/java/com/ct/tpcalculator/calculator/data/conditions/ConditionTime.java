package com.ct.tpcalculator.calculator.data.conditions;

import com.ct.tpcalculator.calculator.data.XML_TAGS;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticCallForCalculation;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticMessageForCalculation;
import com.ct.tpcalculator.helpers.Utils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by USER on 16.06.2015.
 */
public class ConditionTime extends Condition  implements Serializable {

    private ArrayList<Time> times;

    public ArrayList<Time> getTimes() {
        return times;
    }

    public ConditionTime(XmlPullParser parser) throws XmlPullParserException, IOException {
        super(parser);
        times = new ArrayList<>();
        int eventType = parser.getEventType();
        do {
            if (eventType == XmlPullParser.START_TAG) {
                final String name = parser.getName();
                if (XML_TAGS.XML_TAG_TIME.equalsIgnoreCase(name)) {
                    getTimes().add(new Time(parser));
                }
            }
            eventType = parser.next();
        }
        while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_CONDITION_TIME.equals(parser.getName()))
                && (eventType != XmlPullParser.END_DOCUMENT));
    }

    public void calculatePrice(StatisticCallForCalculation call) {
        int minutes = call.getNotCalculatedMinutes();
        double pricePerMinute = getPriceByTime(call.getCallDate());
        double price = pricePerMinute * minutes;
        call.setPrice(price);
        call.setCalculated(true);
    }

    public void calculatePrice(StatisticMessageForCalculation message) {
        double price = getPriceByTime(message.getMessageDate());
        message.setPrice(price);
        message.setCalculated(true);
    }

    private double getPriceByTime(Date time) {
        for (Time timePeriod : getTimes()) {
            if (Utils.IsTimeAIsAfterOrEqualsTimeB(time, timePeriod.start) && Utils.IsTimeAIsAfterOrEqualsTimeB(timePeriod.end, time)){
                return timePeriod.getPrice();
            }
        }
        return getPrice();
    }

    public class Time  implements Serializable{

        private String name;
        private double price;
        private Date start;
        private Date end;

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }

        public Date getStart() {
            return start;
        }

        public Date getEnd() {
            return end;
        }

        public Time(XmlPullParser parser) throws XmlPullParserException, IOException {
            final String tag = parser.getName();
            if (XML_TAGS.XML_TAG_TIME.equalsIgnoreCase(tag)) {
                for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
                    final String attrName = parser.getAttributeName(i);
                    final String attrValue = parser.getAttributeValue(i);
                    if (XML_TAGS.XML_ATTRIBUTE_NAME.equalsIgnoreCase(attrName)) {
                        name = attrValue;
                    } else if (XML_TAGS.XML_ATTRIBUTE_PRICE.equalsIgnoreCase(attrName)) {
                        price = Double.parseDouble(attrValue);
                    } else if (XML_TAGS.XML_ATTRIBUTE_END.equalsIgnoreCase(attrName)) {
                        SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                        try {
                            end = sdf.parse(attrValue);
                        } catch (ParseException ex) {
                            end = new Date(0);
                        }
                    } else if (XML_TAGS.XML_ATTRIBUTE_START.equalsIgnoreCase(attrName)) {
                        SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                        try {
                            start = sdf.parse(attrValue);
                        } catch (ParseException e) {
                            start = new Date(0);
                        }
                    }
                }
            }
            parser.next();
        }

    }
}
