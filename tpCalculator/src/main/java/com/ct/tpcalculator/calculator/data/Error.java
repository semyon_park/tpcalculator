package com.ct.tpcalculator.calculator.data;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;

public class Error implements Serializable {

	public final static int NO_ERROR = 0;
	public final static int INVALID_REGISTRATION_DATA = 1;
	public final static int INVALID_APP_ID = 2;
	public final static int INVALID_LANGUAGE = 3;
	public final static int EMPTY_DATA = 4;
	public final static int INVALID_PLATFORM = 7;

	public final static int UNKNOWN_ERROR = 99;

	private int code = NO_ERROR;

    private CharSequence errorDescription = "";

    public CharSequence getErrorDescription(){
        return errorDescription;
    }

	public Error(XmlPullParser parser) throws XmlPullParserException, IOException {

		int eventType = parser.getEventType();
		do {
			if (eventType == XmlPullParser.START_TAG) {
				for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
					final String attrName = parser.getAttributeName(i);
					final String attrValue = parser.getAttributeValue(i);
					if ("code".equalsIgnoreCase(attrName)) {
						try {
							code = Integer.parseInt(attrValue);
						} catch (NumberFormatException e) {
							code = UNKNOWN_ERROR;
						}
					}else if ("text".equalsIgnoreCase(attrName)){
                        errorDescription = attrValue;
                    }
				}
			}
			eventType = parser.next();
		} while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_ERROR.equalsIgnoreCase(parser.getName()))
						&& (eventType != XmlPullParser.END_DOCUMENT));
		
	}

	public int getCode() {
		return code;
	}
}
