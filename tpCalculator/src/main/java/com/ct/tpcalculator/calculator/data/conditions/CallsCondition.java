package com.ct.tpcalculator.calculator.data.conditions;

import com.ct.tpcalculator.calculator.data.XML_TAGS;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by USER on 16.06.2015.
 */
public class CallsCondition implements Serializable {

    private Conditions inRegionConditions;

    private Conditions interRegionsConditions;

    private Conditions interCountriesConditions;

    public Conditions getInRegionConditions() {
        return inRegionConditions;
    }

    public Conditions getInterRegionsConditions() {
        return interRegionsConditions;
    }

    public Conditions getInterCountriesConditions() {
        return interCountriesConditions;
    }

    public CallsCondition(XmlPullParser parser) throws XmlPullParserException, IOException {
        int eventType = parser.getEventType();
        do {
            if (eventType == XmlPullParser.START_TAG) {
                final String name = parser.getName();
                if (XML_TAGS.XML_TAG_INREGION.equalsIgnoreCase(name)) {
                    inRegionConditions = new Conditions(parser, name);
                } else if (XML_TAGS.XML_TAG_INTERCOUNTRIES.equalsIgnoreCase(name)) {
                    interCountriesConditions = new Conditions(parser, name);
                } else if (XML_TAGS.XML_TAG_INTERREGIONS.equalsIgnoreCase(name)) {
                    interRegionsConditions = new Conditions(parser, name);
                }
            }
            eventType = parser.next();
        }
        while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_CALLS.equals(parser.getName()))
                && (eventType != XmlPullParser.END_DOCUMENT));
    }

}
