package com.ct.tpcalculator.calculator.data.statisticData;

import android.content.ContentValues;
import com.ct.tpcalculator.helpers.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by USER on 24.06.2015.
 */
public class StatisticInternetTraffic {

    public static final int TYPE_WIFI = 1;
    public static final int TYPE_MOBILE = 2;
    public static final int TYPE_WIFI_MOBILE = 3;

    public static final String KEY_CLASS_TYPE = "netModel";
    public static final String KEY_DOWNLOAD = "netDownload";
    public static final String KEY_UPLOAD = "netUpload";
    public static final String KEY_DATE = "netDate";
    public static final String KEY_TYPE = "netType";

    private final int type;
    private long receivedBytes;
    private long transmittedBytes;
    private Date date;

    public int getType() {
        return type;
    }

    public void setReceivedBytes(long receivedBytes) {
        this.receivedBytes = receivedBytes;
    }

    public void setTransmittedBytes(long transmittedBytes) {
        this.transmittedBytes = transmittedBytes;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public long getReceivedBytes() {
        return receivedBytes;
    }

    public int getReceivedMegaBytes(){
        return Utils.getMegaBytes(receivedBytes);
    }

    public long getTransmittedBytes() {
        return transmittedBytes;
    }

    public int getTransmittedMegaBytes(){
        return Utils.getMegaBytes(transmittedBytes);
    }

    public StatisticInternetTraffic(int type) {
        this.type = type;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();

        values.put(KEY_DOWNLOAD, this.getReceivedBytes());
        values.put(KEY_UPLOAD, this.getTransmittedBytes());
        values.put(KEY_DATE, this.getDate().getTime());
        values.put(KEY_TYPE, this.getType());

        return values;
    }

    public String toLog() {
        SimpleDateFormat newFormat = new SimpleDateFormat("dd.MM HH:mm:ss");
        String byteFormat = "%1$s B | %2$s KB | %3$s MB";
        long total = receivedBytes + transmittedBytes;

        StringBuilder sb = new StringBuilder("| StatisticInternetTraffic |");
        sb.append(" Date: " + newFormat.format(date) + "(" + date.getTime() + ")");
        sb.append("; Type: " + getType(type));
        sb.append("; Download: " + String.format(byteFormat, receivedBytes, getKBytes(receivedBytes), getMBytes(receivedBytes)));
        sb.append("; Upload: " + String.format(byteFormat, transmittedBytes, getKBytes(transmittedBytes), getMBytes(transmittedBytes)));
        sb.append("; Total: " + String.format(byteFormat, total, getKBytes(total), getMBytes(total)));
        return sb.toString();
    }

    private String getType(int type) {
        if (type == TYPE_WIFI)
            return "Wi-Fi ";
        else
            return "Mobile";
    }

    private String getKBytes(long bytes) {
        return bytesFormat(bytes / 1024);
    }

    private String getMBytes(long bytes) {
        return bytesFormat(bytes / (1024 * 1024));
    }

    private String bytesFormat(double d) {
        return String.format("%1$.1f", d);
    }

}
