package com.ct.tpcalculator.calculator.statisticAgent.netstat;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.ct.tpcalculator.calculator.statisticAgent.Consts;

public class NetStatService extends IntentService {
    public NetStatService(String name) {
        super(name);
    }

    public NetStatService() {
        this("NetStatService");
    }

    @Override
    public IBinder onBind(Intent intent) {
        e("onBind: call");
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        e("onHandleIntent: call");

        AlarmNetReceiver.setAlarm(getApplicationContext());

        NetStatCore.check(this.getApplicationContext());

        AlarmNetReceiver.completeWakefulIntent(intent);
    }

    //------------------------------------------------------------------------------------------
    private boolean debug = Consts.DEBUG && false;
    private String className = "NetStatService.";

    private void e(String message) {
        if (debug) Log.e(Consts.APP_TAG, className + message);
    }

    private void d(String message) {
        if (debug) Log.d(Consts.APP_TAG, className + message);
    }
    //------------------------------------------------------------------------------------------
}
