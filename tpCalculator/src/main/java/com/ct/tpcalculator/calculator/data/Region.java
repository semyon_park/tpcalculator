package com.ct.tpcalculator.calculator.data;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class Region implements Serializable {

    private final LocalizedStrings titles;
    private String id;
    private String numberPrefix;
    private String availableLanguages;
    private ArrayList<MobileOperator> mobileOperators;
    private int minDigitsCount;
    private int maxDigitsCount;
    private String fileName;
    private final LocalizedStrings offers;

    public LocalizedStrings getTitles() {
        return titles;
    }

    public LocalizedStrings getOffers(){
        return offers;
    }

    public String getId() {
        return id;
    }

    public String getNumberPrefix() {
        return numberPrefix;
    }

    public String getAvailableLanguages() {
        return availableLanguages;
    }

    public ArrayList<MobileOperator> getMobileOperators() {
        return mobileOperators;
    }

    public String getFileName() {
        return fileName;
    }

    public Region(XmlPullParser parser) throws XmlPullParserException, IOException {
        titles = new LocalizedStrings();
        offers = new LocalizedStrings();

        mobileOperators = new ArrayList<>();

        final String tag = parser.getName();
        if (XML_TAGS.XML_TAG_REGION.equalsIgnoreCase(tag)) {
            for (int i = 0, N = parser.getAttributeCount(); i < N; i++) {
                final String attrName = parser.getAttributeName(i);
                final String attrValue = parser.getAttributeValue(i);
                if (XML_TAGS.XML_ATTRIBUTE_ID.equalsIgnoreCase(attrName)) {
                    id = attrValue;
                } else if (attrName.startsWith(XML_TAGS.XML_ATTRIBUTE_TITLE) && attrName.length() > XML_TAGS.XML_ATTRIBUTE_TITLE.length()) {
                    titles.put(attrName.substring(XML_TAGS.XML_ATTRIBUTE_TITLE.length() + 1), attrValue);
                } else if (XML_TAGS.XML_ATTRIBUTE_NUMBER_PREFIX.equalsIgnoreCase(attrName)) {
                    numberPrefix = attrValue;
                } else if (XML_TAGS.XML_TAG_AVAILABLE_LANGUAGES.equalsIgnoreCase(attrName)) {
                    availableLanguages = attrValue;
                } else if (XML_TAGS.XML_ATTRIBUTE_MIN_DIGITS_COUNT.equalsIgnoreCase(attrName)) {
                    minDigitsCount = Integer.parseInt(attrValue);
                } else if (XML_TAGS.XML_ATTRIBUTE_MAX_DIGITS_COUNT.equalsIgnoreCase(attrName)) {
                    maxDigitsCount = Integer.parseInt(attrValue);
                } else if (XML_TAGS.XML_ATTRIBUTE_FILE_NAME.equalsIgnoreCase(attrName)) {
                    fileName = attrValue;
                } else if (attrName.startsWith(XML_TAGS.XML_ATTRIBUTE_OFFER) && attrName.length() > XML_TAGS.XML_ATTRIBUTE_OFFER.length()) {
                    offers.put(attrName.substring(XML_TAGS.XML_ATTRIBUTE_OFFER.length() + 1), attrValue);
                }
            }
        }
        int eventType = parser.getEventType();
        do {
            if (eventType == XmlPullParser.START_TAG) {
                final String name = parser.getName();
                if (XML_TAGS.XML_TAG_OPERATOR.equalsIgnoreCase(name)) {
                    getMobileOperators().add(new MobileOperator(parser));
                }
            }
            eventType = parser.next();
        }
        while (!(eventType == XmlPullParser.END_TAG && XML_TAGS.XML_TAG_REGION.equals(parser.getName()))
                && (eventType != XmlPullParser.END_DOCUMENT));

    }

    public int getMinDigitsCount() {
        return minDigitsCount;
    }

    public int getMaxDigitsCount() {
        return maxDigitsCount;
    }

    public MobileOperator findMobileOperator(String mobileOperatorId) {
        for (MobileOperator mobileOperator : mobileOperators) {
            if (mobileOperator.getId().equalsIgnoreCase(mobileOperatorId))
                return mobileOperator;
        }
        return null;
    }

}
