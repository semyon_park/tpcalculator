package com.ct.tpcalculator.application.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.helpers.InputFilterMinMax;

public class NotificationSettingsActivity extends BaseActivity {

    private static final String ARG_ACTIVITY_STATE = "ARG_ACTIVITY_STATE";

    private static final int ACTIVITY_STATE_USER_LIMIT_VALUES = 0;
    private static final int ACTIVITY_STATE_LIMIT_BY_TP = 1;
    private static final int ACTIVITY_STATE_DISABLE_LIMITS = 2;

    private int mActivityState;

    private TextView mTitle;
    private LinearLayout mLimitValueRadio;
    private LinearLayout mLimitNotificationContainer;
    private EditText mCallsEditText;
    private EditText mSmsEditText;
    private EditText mInternetEditText;
    private LinearLayout mTpValueRadio;
    private LinearLayout mTpValueContainer;
    private EditText mMinLimitValue;
    private LinearLayout mDisableNotificationRadio;
    private ImageView mLimitValueRadioImage;
    private ImageView mTpValueRadioImage;
    private ImageView mDisableNotificationRadioImage;

    private int mCalls;
    private int mSms;
    private int mInternet;
    private int mPercentLimit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            mCalls = sharedPreferences.getInt(getString(R.string.preferences_user_calls_mix_limits), 0);
            mSms = sharedPreferences.getInt(getString(R.string.preferences_user_sms_mix_limits), 0);
            mInternet = sharedPreferences.getInt(getString(R.string.preferences_user_internet_limits), 0);
            mPercentLimit = sharedPreferences.getInt(getString(R.string.preferences_minimum_limit_step), 10);

            boolean showNotifications = sharedPreferences.getBoolean(getString(R.string.preferences_show_notifications), true);

            if (showNotifications) {
                if (mPercentLimit == 0) {
                    mActivityState = ACTIVITY_STATE_USER_LIMIT_VALUES;
                } else {
                    mActivityState = ACTIVITY_STATE_LIMIT_BY_TP;
                }
            } else {
                mActivityState = ACTIVITY_STATE_DISABLE_LIMITS;
            }
        }else{
             mActivityState = savedInstanceState.getInt(ARG_ACTIVITY_STATE);
        }

        setContentView(R.layout.activity_notification_settings);        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        mTitle = (TextView) findViewById(R.id.title_text);
        mTitle.setTypeface(Application.lightTypeface);

        ImageView backCommand = (ImageView) findViewById(R.id.back);
        backCommand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveSettings()) {
                    finish();
                }
            }
        });

        mLimitValueRadio = (LinearLayout) findViewById(R.id.limit_value_radio);
        mLimitValueRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivityState = ACTIVITY_STATE_USER_LIMIT_VALUES;
                setActivityState();
            }
        });

        TextView limitValueRadioTitle = (TextView) findViewById(R.id.limit_value_radio_title);
        limitValueRadioTitle.setTypeface(Application.lightTypeface);

        mLimitValueRadioImage = (ImageView) findViewById(R.id.limit_value_radio_image);

        mLimitNotificationContainer = (LinearLayout) findViewById(R.id.limit_notification_container);

        TextView callsLabel = (TextView) findViewById(R.id.calls_label);
        callsLabel.setTypeface(Application.lightTypeface);

        mCallsEditText = (EditText) findViewById(R.id.calls);
        mCallsEditText.setTypeface(Application.lightTypeface);
        mCallsEditText.setFilters(new InputFilter[]{new InputFilterMinMax(0, 999999)}); // устанавливаем min и max значения
        mCallsEditText.setText(Integer.toString(mCalls));

        TextView smsLabel = (TextView) findViewById(R.id.sms_label);
        smsLabel.setTypeface(Application.lightTypeface);

        mSmsEditText = (EditText) findViewById(R.id.sms);
        mSmsEditText.setTypeface(Application.lightTypeface);
        mSmsEditText.setFilters(new InputFilter[]{new InputFilterMinMax(0, 999999)}); // устанавливаем min и max значения
        mSmsEditText.setText(Integer.toString(mSms));

        TextView internetLabel = (TextView) findViewById(R.id.internet_label);
        internetLabel.setTypeface(Application.lightTypeface);

        mInternetEditText = (EditText) findViewById(R.id.internet);
        mInternetEditText.setTypeface(Application.lightTypeface);
        mInternetEditText.setFilters(new InputFilter[]{new InputFilterMinMax(0, 999999)}); // устанавливаем min и max значения
        mInternetEditText.setText(Integer.toString(mInternet));

        mTpValueRadio = (LinearLayout) findViewById(R.id.tp_value_radio);
        mTpValueRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivityState = ACTIVITY_STATE_LIMIT_BY_TP;
                setActivityState();
            }
        });

        TextView tpValueRadioTitle = (TextView) findViewById(R.id.tp_value_radio_title);
        tpValueRadioTitle.setTypeface(Application.lightTypeface);

        mTpValueRadioImage = (ImageView) findViewById(R.id.tp_value_radio_image);

        mTpValueContainer = (LinearLayout) findViewById(R.id.tp_notification_container);

        TextView minTpLimitValue = (TextView) findViewById(R.id.min_limit_title);
        minTpLimitValue.setTypeface(Application.lightTypeface);

        mMinLimitValue = (EditText) findViewById(R.id.min_limit_value);
        mMinLimitValue.setTypeface(Application.lightTypeface);
        mMinLimitValue.setFilters(new InputFilter[]{new InputFilterMinMax(1, 100)}); // устанавливаем min и max значения
        mMinLimitValue.setText(Integer.toString(mPercentLimit));

        mDisableNotificationRadio = (LinearLayout) findViewById(R.id.disable_notifications_radio);
        mDisableNotificationRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivityState = ACTIVITY_STATE_DISABLE_LIMITS;
                setActivityState();
            }
        });

        TextView disableNotificationTitle = (TextView) findViewById(R.id.disable_notifications_radio_title);
        disableNotificationTitle.setTypeface(Application.lightTypeface);

        mDisableNotificationRadioImage = (ImageView) findViewById(R.id.disable_notifications_radio_image);

        setActivityState();
    }

    private void setActivityState() {
        switch (mActivityState) {
            case ACTIVITY_STATE_USER_LIMIT_VALUES:
                mLimitValueRadioImage.setImageResource(R.drawable.radio_on);
                mTpValueRadioImage.setImageResource(R.drawable.radio_off);
                mDisableNotificationRadioImage.setImageResource(R.drawable.radio_off);

                mLimitNotificationContainer.setVisibility(View.VISIBLE);
                mTpValueContainer.setVisibility(View.GONE);

                break;
            case ACTIVITY_STATE_LIMIT_BY_TP:
                mLimitValueRadioImage.setImageResource(R.drawable.radio_off);
                mTpValueRadioImage.setImageResource(R.drawable.radio_on);
                mDisableNotificationRadioImage.setImageResource(R.drawable.radio_off);

                mLimitNotificationContainer.setVisibility(View.GONE);
                mTpValueContainer.setVisibility(View.VISIBLE);

                break;
            case ACTIVITY_STATE_DISABLE_LIMITS:
                mLimitValueRadioImage.setImageResource(R.drawable.radio_off);
                mTpValueRadioImage.setImageResource(R.drawable.radio_off);
                mDisableNotificationRadioImage.setImageResource(R.drawable.radio_on);

                mLimitNotificationContainer.setVisibility(View.GONE);
                mTpValueContainer.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (saveSettings()) {
            super.onBackPressed();
        }
    }

    private boolean saveSettings() {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();

        if (mActivityState == ACTIVITY_STATE_DISABLE_LIMITS) {
            editor.putBoolean(getString(R.string.preferences_show_notifications), false);
        } else {
            if (mActivityState == ACTIVITY_STATE_USER_LIMIT_VALUES) {
                if (TextUtils.isEmpty(mCallsEditText.getText())) {
                    Toast.makeText(NotificationSettingsActivity.this, getString(R.string.value_cannot_be_null), Toast.LENGTH_SHORT).show();
                    mCallsEditText.requestFocus();
                    return false;
                }
                int calls = Integer.parseInt(mCallsEditText.getText().toString());

                if (TextUtils.isEmpty(mSmsEditText.getText())) {
                    Toast.makeText(NotificationSettingsActivity.this, getString(R.string.value_cannot_be_null), Toast.LENGTH_SHORT).show();
                    mSmsEditText.requestFocus();
                    return false;
                }
                int sms = Integer.parseInt(mSmsEditText.getText().toString());

                if (TextUtils.isEmpty(mInternetEditText.getText())) {
                    Toast.makeText(NotificationSettingsActivity.this, getString(R.string.value_cannot_be_null), Toast.LENGTH_SHORT).show();
                    mInternetEditText.requestFocus();
                    return false;
                }
                int internet = Integer.parseInt(mInternetEditText.getText().toString());

                editor.putInt(getString(R.string.preferences_minimum_limit_step), 0);

                editor.putInt(getString(R.string.preferences_user_calls_mix_limits), calls);
                editor.putInt(getString(R.string.preferences_user_sms_mix_limits), sms);
                editor.putInt(getString(R.string.preferences_user_internet_limits), internet);

            } else {
                if (TextUtils.isEmpty(mMinLimitValue.getText())) {
                    Toast.makeText(NotificationSettingsActivity.this, getString(R.string.value_cannot_be_null), Toast.LENGTH_SHORT).show();
                    mMinLimitValue.requestFocus();
                    return false;
                }
                int minValueLimit = Integer.parseInt(mMinLimitValue.getText().toString());

                editor.putInt(getString(R.string.preferences_minimum_limit_step), minValueLimit);

                editor.putInt(getString(R.string.preferences_user_calls_mix_limits), 0);
                editor.putInt(getString(R.string.preferences_user_sms_mix_limits), 0);
                editor.putInt(getString(R.string.preferences_user_internet_limits), 0);
            }

            editor.putBoolean(getString(R.string.preferences_show_notifications), true);
        }
        editor.apply();
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(ARG_ACTIVITY_STATE, mActivityState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mActivityState = savedInstanceState.getInt(ARG_ACTIVITY_STATE);
    }
}