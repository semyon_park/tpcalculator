package com.ct.tpcalculator.application.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.ct.tpcalculator.helpers.GoogleAnalyticsHelper;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.*;
import com.github.mikephil.charting.utils.ValueFormatter;
import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.views.*;
import com.ct.tpcalculator.application.views.Switch;
import com.ct.tpcalculator.calculator.data.*;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticCall;
import com.ct.tpcalculator.calculator.statisticAgent.callstat.CallStatCore;
import com.ct.tpcalculator.helpers.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.text.DecimalFormat;
import java.util.*;

public class ChartCallsFragment extends ChartBaseFragment {

    public static final String TAG = ChartCallsFragment.class.getName();
    private static final String GA_PAGE_NAME = "ChartCallsFragment";

    private LineChart mLineChart;

    private PieChart mPieChart;

    private LinearLayout mLineChartArea;

    private View mPieChartArea;

    private LinearLayout mListArea;

    private Spinner mLineChartNumberFilterSpinner;

    private Spinner mLineChartCallDirectionSpinner;

    private ListView mLineChartLegendListView;

    private SelectPeriod mLineChartSelectPeriod;

    private TextView mPieChartIncomingValue;

    private TextView mPieChartOutgoingValue;

    protected TextView mPieChartTotalCallsValue;

    protected Switch mPieChartUnitsSwitcher;

    private SelectPeriod mPieChartSelectPeriod;

    protected ListView mListView;

    protected ThreeStateSwitch mListViewThreeStateSwitch;

    protected TextView mListViewSelectPeriodLabel;

    protected TextView mListViewTotalCallsCount;

    private String[] mCallTypes;

    protected String[] mCallDirectionTypes;

    private String[] mCallDirectionTypesForPie;

    private int mDataViewType = DATA_VIEW_TYPE_LINE_CHART;

    protected int mPieChartUnitsFilter = INCOMING_OUTGOING_CALLS;

    protected int mListAreaDirectionFilter = 1;

    private Date mStartDate;
    private Date mEndDate;

    protected final static int INCOMING_OUTGOING_CALLS = 0;
    protected final static int OUTGOING_CALLS = 1;
    protected final static int INCOMING_CALLS = 2;
    private final static int INCOMING_OUTGOING_MINUTES = 3;
    private final static int OUTGOING_MINUTES = 4;
    private final static int INCOMING_MINUTES = 5;

    protected final static int CALL_FILTER_TYPE_BY_OPERATOR = 0;
    protected final static int CALL_FILTER_TYPE_BY_REGION = 1;
    protected final static int CALL_FILTER_TYPE_BY_CONTACTS = 2;

    public static final String ARG_START_DATE = "ARG_START_DATE";
    public static final String ARG_END_DATE = "ARG_END_DATE";
    public static final String ARG_DATA_VIEW_TYPE = "ARG_DATA_VIEW_TYPE";
    public static final String ARG_PIE_CHART_UNITS_FILTER = "ARG_PIE_CHART_UNITS_FILTER";
    public static final String ARG_LIST_VIEW_DIRECTION_FILTER = "ARG_LIST_VIEW_DIRECTION_FILTER";

    private int[] mLegendColors;

    public ChartCallsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallTypes = new String[]{getString(R.string.by_operator), getString(R.string.by_region), getString(R.string.by_contacts)};
        mCallDirectionTypes = new String[]{getString(R.string.incoming_outgoing_calls), getString(R.string.outgoing_calls), getString(R.string.incoming_calls),
                getString(R.string.incoming_outgoing_minutes), getString(R.string.outgoing_minutes), getString(R.string.incoming_minutes)};
        mCallDirectionTypesForPie = new String[]{getString(R.string.incoming_outgoing_calls), getString(R.string.incoming_outgoing_minutes)};
        mLegendColors = new int[]{R.color.circle_red, R.color.circle_orange, R.color.circle_yellow,
                R.color.circle_light_yellow, R.color.circle_light_green, R.color.circle_green,
                R.color.circle_light_blue, R.color.circle_blue, R.color.circle_purple, R.color.circle_pink};

        GoogleAnalyticsHelper.sendLog(GA_PAGE_NAME, null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            long startDate = savedInstanceState.getLong(ARG_START_DATE);
            if (startDate != 0L) {
                mStartDate = new Date(startDate);
            }
            long endDate = savedInstanceState.getLong(ARG_END_DATE);
            if (endDate != 0L) {
                mEndDate = new Date(endDate);
            }
            mDataViewType = savedInstanceState.getInt(ARG_DATA_VIEW_TYPE);
            mPieChartUnitsFilter = savedInstanceState.getInt(ARG_PIE_CHART_UNITS_FILTER);
            mListAreaDirectionFilter = savedInstanceState.getInt(ARG_LIST_VIEW_DIRECTION_FILTER);
        }

        if (mStartDate == null || mEndDate == null) {
            Calendar calendar = Calendar.getInstance();
            Date startDate = calendar.getTime();
            Pair<Date, Date> period = Utils.getMonthOfTheDate(startDate);

            mStartDate = period.first;
            mEndDate = period.second;
        }

        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_chart_calls, container, false);

        // <editor-fold desc="Line Chart Area Elements">
        mLineChartSelectPeriod = (SelectPeriod) root.findViewById(R.id.line_chart_area_select_period);
        mLineChartSelectPeriod.setSelectPeriodListener(new SelectPeriod.SelectPeriodListener() {
            @Override
            public void datesChanged(Date startDate, Date endDate) {
                datesPeriodChanged(startDate, endDate);
            }
        });

        mLineChartLegendListView = (ListView) root.findViewById(R.id.line_chart_area_legend);

        mLineChartArea = (LinearLayout) root.findViewById(R.id.line_chart_area);

        mLineChartNumberFilterSpinner = (Spinner) root.findViewById(R.id.line_chart_area_number_filter);
        mLineChartNumberFilterSpinner.setAdapter(new FilterArrayAdapter(getActivity(), mCallTypes));
        mLineChartNumberFilterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                redrawData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mLineChartCallDirectionSpinner = (Spinner) root.findViewById(R.id.line_chart_area_direction_filter);
        mLineChartCallDirectionSpinner.setAdapter(new FilterArrayAdapter(getActivity(), mCallDirectionTypes));
        mLineChartCallDirectionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                redrawData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mLineChart = (LineChart) root.findViewById(R.id.line_chart);
        mLineChart.setDrawGridBackground(false);
        // no description text
        mLineChart.setDescription("");
        mLineChart.setNoDataText(getString(R.string.no_data));
        mLineChart.setNoDataTextDescription(getString(R.string.no_data_description));
        // enable value highlighting
        mLineChart.setHighlightEnabled(false);
        // enable touch gestures
        mLineChart.setTouchEnabled(true);
        // enable scaling and dragging
        mLineChart.setDragEnabled(true);
        mLineChart.setScaleEnabled(true);
        mLineChart.getAxisRight().setEnabled(false);
        mLineChart.getAxisLeft().setTypeface(Application.lightTypeface);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mLineChart.getAxisLeft().setTextColor(getActivity().getResources().getColor(android.R.color.white, getActivity().getTheme()));
        } else {
            mLineChart.getAxisLeft().setTextColor(getActivity().getResources().getColor(android.R.color.white));
        }
        mLineChart.getAxisLeft().enableGridDashedLine(1f, 1f, 0f);
        mLineChart.getXAxis().setTypeface(Application.lightTypeface);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mLineChart.getXAxis().setTextColor(getActivity().getResources().getColor(R.color.text_color_white, getActivity().getTheme()));
        } else {
            mLineChart.getXAxis().setTextColor(getActivity().getResources().getColor(R.color.text_color_white));
        }
        mLineChart.setPinchZoom(true);
        mLineChart.getAxisLeft().setValueFormatter(new CustomValueFormatter());
        mLineChart.getLegend().setEnabled(false);

        // </editor-fold>

        // <editor-fold desc="Pie Chart Area Elements">

        mPieChartSelectPeriod = (SelectPeriod) root.findViewById(R.id.pie_chart_area_select_period);
        mPieChartSelectPeriod.setSelectPeriodListener(new SelectPeriod.SelectPeriodListener() {
            @Override
            public void datesChanged(Date startDate, Date endDate) {
                datesPeriodChanged(startDate, endDate);
            }
        });

        mPieChartTotalCallsValue = (TextView) root.findViewById(R.id.pie_chart_area_total_calls_value);
        mPieChartTotalCallsValue.setTypeface(Application.condensedTypeface);

        mPieChartUnitsSwitcher = (Switch) root.findViewById(R.id.pie_chart_area_units_filter);
        mPieChartUnitsSwitcher.setThumbResource(R.drawable.short_switch_thumb_2_selector);
        mPieChartUnitsSwitcher.setThumbResource2(R.drawable.short_switch_thumb_1_selector);
        mPieChartUnitsSwitcher.setTrackResource(R.drawable.short_switcher_track);
        mPieChartUnitsSwitcher.setBackgroundOnResource(R.color.dark_bg);

        if (mPieChartUnitsFilter == INCOMING_OUTGOING_MINUTES) {
            mPieChartUnitsSwitcher.toggle();
        }
        mPieChartUnitsSwitcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int newValue = isChecked ? INCOMING_OUTGOING_MINUTES : INCOMING_OUTGOING_CALLS;

                if (mPieChartUnitsFilter != newValue) {
                    mPieChartUnitsFilter = newValue;
                    if (mDataViewType == DATA_VIEW_TYPE_PIE_CHART) {
                        pieChartClicked();
                    }
                }
            }
        });

        mPieChartIncomingValue = (TextView) root.findViewById(R.id.pie_chart_area_incoming_value);
        mPieChartIncomingValue.setTypeface(Application.regularTypeface);

        mPieChartOutgoingValue = (TextView) root.findViewById(R.id.pie_chart_area_outgoing_value);
        mPieChartOutgoingValue.setTypeface(Application.regularTypeface);

        mPieChartArea = root.findViewById(R.id.pie_chart_area);

        mPieChart = (PieChart) root.findViewById(R.id.pie_chart);
        mPieChart.setDescription("");
        mPieChart.setNoDataText(getString(R.string.no_data));
        mPieChart.setNoDataTextDescription(getString(R.string.no_data_description));
        // enable value highlighting
        mPieChart.setHighlightEnabled(false);
        // enable touch gestures
        mPieChart.setTouchEnabled(true);
        mPieChart.setDragDecelerationFrictionCoef(0.95f);
        mPieChart.setDrawHoleEnabled(true);
        mPieChart.setHoleColorTransparent(true);
        mPieChart.setTransparentCircleColor(Color.WHITE);
        mPieChart.setTransparentCircleAlpha(110);
        mPieChart.setHoleRadius(80f);
        mPieChart.setTransparentCircleRadius(80f);
//        mPieChart.setDrawCenterText(true);
        mPieChart.setRotationAngle(-110);
        // enable rotation of the chart by touch
        mPieChart.setRotationEnabled(false);

        mPieChart.getLegend().setEnabled(false);

        // </editor-fold>

        // <editor-fold desc="List View Area Elements">

        mListViewTotalCallsCount = (TextView) root.findViewById(R.id.list_area_total_calls_count);
        mListViewTotalCallsCount.setTypeface(Application.condensedTypeface);

        mListViewSelectPeriodLabel = (TextView) root.findViewById(R.id.date_label);
        mListViewSelectPeriodLabel.setTypeface(Application.condensedTypeface);

        LinearLayout listViewSelectPeriod = (LinearLayout) root.findViewById(R.id.list_area_period_selector);
        listViewSelectPeriod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity(), R.style.Theme_SelectDate);
                dialog.setContentView(R.layout.select_date_range);
                TextView selectPeriodLabel = (TextView) dialog.findViewById(R.id.select_period);
                selectPeriodLabel.setTypeface(Application.lightTypeface);

                final DatePicker startDatePicker = (DatePicker) dialog.findViewById(R.id.dpStartDate);
                final DatePicker endDatePicker = (DatePicker) dialog.findViewById(R.id.dpEndDate);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(mStartDate);
                startDatePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                calendar.setTime(mEndDate);
                endDatePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                Button ok_button = (Button) dialog.findViewById(R.id.ok_button);
                ok_button.setTypeface(Application.lightTypeface);
                ok_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(startDatePicker.getYear(), startDatePicker.getMonth(), startDatePicker.getDayOfMonth(), 0, 0, 0);
                        Date startDate = calendar.getTime();
                        calendar.set(endDatePicker.getYear(), endDatePicker.getMonth(), endDatePicker.getDayOfMonth(), 23, 59, 59);
                        Date endDate = calendar.getTime();

                        if (startDate.getTime() > endDate.getTime()) {
                            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setMessage(R.string.min_max_date_exception);
                            alertDialog.setTitle(R.string.exception);
                            alertDialog.show();
                            alertDialog.setNegativeButton(R.string.cancel, null);
                        } else {
                            datesPeriodChanged(startDate, endDate);
                        }
                        dialog.dismiss();
                    }
                });

                Button cancel_button = (Button) dialog.findViewById(R.id.cancel_button);
                cancel_button.setTypeface(Application.lightTypeface);
                cancel_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        mListArea = (LinearLayout) root.findViewById(R.id.list_area);

        mListView = (ListView) root.findViewById(R.id.list);

        mListViewThreeStateSwitch = (ThreeStateSwitch) root.findViewById(R.id.list_area_direction_filter);
        mListViewThreeStateSwitch.setThumbResource1(R.drawable.long_switch_thumb_1_selector);
        mListViewThreeStateSwitch.setThumbResource2(R.drawable.long_switch_thumb_2_selector);
        mListViewThreeStateSwitch.setThumbResource3(R.drawable.long_switch_thumb_3_selector);
        mListViewThreeStateSwitch.setTrackResource(R.drawable.long_switcher_track);
        mListViewThreeStateSwitch.setState(mListAreaDirectionFilter);
        mListViewThreeStateSwitch.setOnStateChangeListener(new ThreeStateSwitch.OnStateChangeListener() {
            @Override
            public void stateChanged(int state) {
                redrawData();
            }
        });

//        threeStateSwitch.setmThumbFirstStateDrawable(getResources().getDrawable(R.drawable.first_switch_thumb));
//        threeStateSwitch.setmThumbSecondStateDrawable(getResources().getDrawable(R.drawable.second_switch_thumb));
//        threeStateSwitch.setmThumbThirdStateDrawable(getResources().getDrawable(R.drawable.third_switch_thumb));
//        mListViewThreeStateSwitch.setGravity(Gravity.TOP);

        //</editor-fold>

        switch (mDataViewType) {
            case DATA_VIEW_TYPE_LINE_CHART:
                lineChartClicked();
                break;
            case DATA_VIEW_TYPE_PIE_CHART:
                pieChartClicked();
                break;
            case DATA_VIEW_TYPE_LIST:
                listViewClicked();
                break;
        }

        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mStartDate != null) {
            outState.putLong(ARG_START_DATE, mStartDate.getTime());
        }
        if (mEndDate != null) {
            outState.putLong(ARG_END_DATE, mEndDate.getTime());
        }
        outState.putInt(ARG_DATA_VIEW_TYPE, mDataViewType);
        outState.putInt(ARG_PIE_CHART_UNITS_FILTER, mPieChartUnitsFilter);
        outState.putInt(ARG_LIST_VIEW_DIRECTION_FILTER, mListAreaDirectionFilter);
    }

    @Override
    public void showChart(int chart) {
        mDataViewType = chart;
        switch (chart) {
            case DATA_VIEW_TYPE_LINE_CHART:
                lineChartClicked();
                break;
            case DATA_VIEW_TYPE_PIE_CHART:
                pieChartClicked();
                break;
            case DATA_VIEW_TYPE_LIST:
                listViewClicked();
                break;
        }
    }

    @Override
    public int getSelectedChartType() {
        return mDataViewType;
    }

    private void datesPeriodChanged(Date startDate, Date endDate) {
        mStartDate = startDate;
        mEndDate = endDate;
        redrawData();
    }

    private void lineChartClicked() {
        mDataViewType = DATA_VIEW_TYPE_LINE_CHART;
        mLineChartArea.setVisibility(View.VISIBLE);
        mPieChartArea.setVisibility(View.GONE);
        mListArea.setVisibility(View.GONE);

//        mLineChartCallDirectionSpinner.setAdapter(new FilterArrayAdapter(getActivity(), mCallDirectionTypes));
        redrawData();
    }

    private void pieChartClicked() {
        mDataViewType = DATA_VIEW_TYPE_PIE_CHART;
        mLineChartArea.setVisibility(View.GONE);
        mPieChartArea.setVisibility(View.VISIBLE);
        mListArea.setVisibility(View.GONE);

//        mLineChartCallDirectionSpinner.setAdapter(new FilterArrayAdapter(getActivity(), mCallDirectionTypesForPie));
        redrawData();
    }

    private void listViewClicked() {
        mDataViewType = DATA_VIEW_TYPE_LIST;
        mLineChartArea.setVisibility(View.GONE);
        mPieChartArea.setVisibility(View.GONE);
        mListArea.setVisibility(View.VISIBLE);

        redrawData();
    }

    // Перерисовать графики
    private void redrawData() {
        int viewType = mDataViewType;

        if (viewType == DATA_VIEW_TYPE_LINE_CHART) {
            int callType = mLineChartNumberFilterSpinner.getSelectedItemPosition();
            if (callType == CALL_FILTER_TYPE_BY_OPERATOR) {
                setLineChartDataByMobileOperators(mStartDate, mEndDate);
            } else if (callType == CALL_FILTER_TYPE_BY_REGION) {
                setLineChartDataByRegion(mStartDate, mEndDate);
            } else if (callType == CALL_FILTER_TYPE_BY_CONTACTS) {
                setLineChartDataByContacts(mStartDate, mEndDate);
            }
        } else if (viewType == DATA_VIEW_TYPE_PIE_CHART) {
            setPieChart(mStartDate, mEndDate);
        } else if (viewType == DATA_VIEW_TYPE_LIST) {
            fillCallsList(mStartDate, mEndDate);
        }
    }

    // Строит линейный график, где фильтр равен = по операторам
    private void setLineChartDataByMobileOperators(Date startDate, Date endDate) {
        mLineChartSelectPeriod.setStartDate(startDate);
        mLineChartSelectPeriod.setEndDate(endDate);

        Region region = Application.getInstance().getRegion();
        if (region == null)
            return;

        ArrayList<ChartData> chartDataArrayList = new ArrayList<>();

        // Создаем датасетов столько, сколько у нас мобильных операторов в регионе
        for (MobileOperator mobileOperator : region.getMobileOperators()) {
            LineDataSet lineDataSet = new LineDataSet(new ArrayList<Entry>(), Utils.getLocalizedString(mobileOperator.getTitles()));
            chartDataArrayList.add(new ChartData(lineDataSet));
        }
        // Создаем датасет для номеров телефонов, у которых мы не сможем определить оператора
        LineDataSet otherDataSet = new LineDataSet(new ArrayList<Entry>(), getString(R.string.other));
        chartDataArrayList.add(new ChartData(otherDataSet));

        ArrayList<String> xVals = new ArrayList<>();

        fillChartWithEmptyData(startDate, endDate, xVals, chartDataArrayList);

        int callDirectionType = mLineChartCallDirectionSpinner.getSelectedItemPosition();

        fillYValue(startDate, endDate, callDirectionType, chartDataArrayList, CALL_FILTER_TYPE_BY_OPERATOR);

        Collections.sort(chartDataArrayList, new ChartDataComparator());

        int yMax = 0;

        // Меняем лейбл на датасете - добавляем общее кол-во звонков или минут
        ArrayList<LineDataSet> lineDataSets = new ArrayList<>();
        for (ChartData chartData : chartDataArrayList) {
            if (chartData.getAccumulator() != 0) {
                chartData.getLineDataSet().setLabel(String.format("%1$s (%2$s)", chartData.getLineDataSet().getLabel(), Integer.toString(chartData.getAccumulator())));
                lineDataSets.add(chartData.getLineDataSet());
                if (chartData.getMaxValue() > yMax) {
                    yMax = chartData.getMaxValue();
                }
            }
        }
        int colorMultiplier = 1;
        if (lineDataSets.size() <= 5) {
            colorMultiplier = 2;
        }
        // Присваиваем датасетам свойства по умолчанию
        for (int i = 0; i < lineDataSets.size(); i++) {
            setDefaultDataSetProperties(lineDataSets.get(i), i * colorMultiplier);
        }

        LineData data = new LineData(xVals, lineDataSets);

        mLineChart.getAxisLeft().setAxisMinValue(0f);
        mLineChart.getAxisLeft().setAxisMaxValue(yMax + 1000);
        mLineChart.getAxisLeft().setLabelCount(((yMax + 1000) / 1000), false);

        mLineChartLegendListView.setAdapter(new LegendArrayAdapter(getActivity(), lineDataSets.toArray(new DataSet[0])));

        // set data
        mLineChart.setData(data);

        mLineChart.animateX(1500, Easing.EasingOption.EaseInOutQuart);
    }

    // Строит линейный график, где фильтр равен = по регионам
    private void setLineChartDataByRegion(Date startDate, Date endDate) {
        mLineChartSelectPeriod.setStartDate(startDate);
        mLineChartSelectPeriod.setEndDate(endDate);

        Country country = Application.getInstance().getCountry();
        if (country == null)
            return;

        Region region = Application.getInstance().getRegion();
        if (region == null)
            return;

        ArrayList<ChartData> chartDataArrayList = new ArrayList<>();

        // Создаем 3 датасета: внутри региона, межрегиональные, международные
        LineDataSet lineDataSetInregion = new LineDataSet(new ArrayList<Entry>(), getString(R.string.inregion));
        chartDataArrayList.add(new ChartData(lineDataSetInregion));
        LineDataSet lineDataSetInterRegion = new LineDataSet(new ArrayList<Entry>(), getString(R.string.interregion));
        chartDataArrayList.add(new ChartData(lineDataSetInterRegion));
        LineDataSet lineDataSetInternational = new LineDataSet(new ArrayList<Entry>(), getString(R.string.international));
        chartDataArrayList.add(new ChartData(lineDataSetInternational));
        LineDataSet lineDataSetOthers = new LineDataSet(new ArrayList<Entry>(), getString(R.string.other));
        chartDataArrayList.add(new ChartData(lineDataSetOthers));

        ArrayList<String> xVals = new ArrayList<>();

        fillChartWithEmptyData(startDate, endDate, xVals, chartDataArrayList);

        int callDirectionType = mLineChartCallDirectionSpinner.getSelectedItemPosition();

        fillYValue(startDate, endDate, callDirectionType, chartDataArrayList, CALL_FILTER_TYPE_BY_REGION);

        // Сортируем dataset по общему кол-ву звонков или минут
        Collections.sort(chartDataArrayList, new ChartDataComparator());

        int yMax = 0;

        // Меняем лейбл на датасете - добавляем общее кол-во звонков или минут
        ArrayList<LineDataSet> lineDataSets = new ArrayList<>();
        for (ChartData chartData : chartDataArrayList) {
            if (chartData.getAccumulator() != 0) {
                chartData.getLineDataSet().setLabel(String.format("%1$s (%2$s)", chartData.getLineDataSet().getLabel(), Integer.toString(chartData.getAccumulator())));
                lineDataSets.add(chartData.getLineDataSet());
                if (chartData.getMaxValue() > yMax) {
                    yMax = chartData.getMaxValue();
                }
            }
        }

        int colorMultiplier = 1;
        if (lineDataSets.size() <= 5) {
            colorMultiplier = 2;
        }

        // Присваиваем датасетам свойства по умолчанию
        for (int i = 0; i < lineDataSets.size(); i++) {
            setDefaultDataSetProperties(lineDataSets.get(i), i * colorMultiplier);
        }

        LineData data = new LineData(xVals, lineDataSets);

        mLineChart.getAxisLeft().setAxisMinValue(0f);
        mLineChart.getAxisLeft().setAxisMaxValue(yMax + 1000);
        mLineChart.getAxisLeft().setLabelCount(((yMax + 1000) / 1000), false);

        mLineChartLegendListView.setAdapter(new LegendArrayAdapter(getActivity(), lineDataSets.toArray(new DataSet[0])));

        // set data
        mLineChart.setData(data);

        mLineChart.animateX(1500, Easing.EasingOption.EaseInOutQuart);
    }

    // Строит линейный график, где фильтр равен = по операторам
    private void setLineChartDataByContacts(Date startDate, Date endDate) {
        mLineChartSelectPeriod.setStartDate(startDate);
        mLineChartSelectPeriod.setEndDate(endDate);

        int callDirectionType = mLineChartCallDirectionSpinner.getSelectedItemPosition();

        ArrayList<ChartData> chartDataArrayList = getChartDataByContacts(startDate, endDate, callDirectionType);

        ArrayList<String> xVals = new ArrayList<>();

        fillChartWithEmptyData(startDate, endDate, xVals, chartDataArrayList);

        fillYValue(startDate, endDate, callDirectionType, chartDataArrayList, CALL_FILTER_TYPE_BY_CONTACTS);

        // Сортируем dataset по общему кол-ву звонков или минут
        Collections.sort(chartDataArrayList, new ChartDataComparator());

        int yMax = 0;

        // Меняем лейбл на датасете - добавляем общее кол-во звонков или минут
        ArrayList<LineDataSet> lineDataSets = new ArrayList<>();
        for (int i = 0; i < chartDataArrayList.size(); i++) {
            ChartData chartData = chartDataArrayList.get(i);
            if (chartData.getAccumulator() != 0) {
                chartData.getLineDataSet().setLabel(String.format("%1$s (%2$s)", chartData.getLineDataSet().getLabel(), Integer.toString(chartData.getAccumulator())));
                lineDataSets.add(chartData.getLineDataSet());
                if (chartData.getMaxValue() > yMax) {
                    yMax = chartData.getMaxValue();
                }
            }
        }

        // Присваиваем датасетам свойства по умолчанию
        for (int i = 0; i < lineDataSets.size(); i++) {
            setDefaultDataSetProperties(lineDataSets.get(i), i);
        }

        LineData data = new LineData(xVals, lineDataSets);

        mLineChart.getAxisLeft().setAxisMinValue(0f);
        mLineChart.getAxisLeft().setAxisMaxValue(yMax + 1000);
        mLineChart.getAxisLeft().setLabelCount(((yMax + 1000) / 1000), false);

        mLineChartLegendListView.setAdapter(new LegendArrayAdapter(getActivity(), lineDataSets.toArray(new DataSet[0])));

        // set data
        mLineChart.setData(data);

        mLineChart.animateX(1500, Easing.EasingOption.EaseInOutQuart);
    }

    // Строит график в виде пирога
    private void setPieChart(Date startDate, Date endDate) {

        mPieChartSelectPeriod.setStartDate(startDate);
        mPieChartSelectPeriod.setEndDate(endDate);

//        int incomingCalls = 0;
//        int outgoingCalls = 0;
//
//        ArrayList<StatisticCall> calls = CallStatCore.GetStatisticCalls(getActivity(), startDate, endDate);
//        for (StatisticCall call : calls) {
//            if (call.getCallType() == DataDirectionTypes.INCOMING) {
//                if (mPieChartUnitsFilter == INCOMING_OUTGOING_CALLS) {
//                    incomingCalls += 1;
//                } else {
//                    incomingCalls += call.getCallDurationInMinutes();
//                }
//            } else {
//                if (mPieChartUnitsFilter == INCOMING_OUTGOING_CALLS) {
//                    outgoingCalls += 1;
//                } else {
//                    outgoingCalls += call.getCallDurationInMinutes();
//                }
//            }
//        }

        ArrayList<Entry> yVals1 = getChartDataByRegion(startDate, endDate);

        int incomingCalls = (int) yVals1.get(0).getVal();
        int outgoingCalls = (int) yVals1.get(1).getVal();

//        yVals1.add(new Entry(incomingCalls, 0));
//        yVals1.add(new Entry(outgoingCalls, 1));

        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add(""); // incomingCalls
        xVals.add(""); // outgoingCalls


        PieDataSet dataSet = new PieDataSet(yVals1, "Election Results");
        dataSet.setSliceSpace(0f);
        dataSet.setSelectionShift(0f);
        dataSet.setDrawValues(false);

        // add colors
        ArrayList<Integer> colors = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            colors.add(getResources().getColor(R.color.green_color, getActivity().getTheme()));
            colors.add(getResources().getColor(R.color.yellow_color, getActivity().getTheme()));
        } else {
            colors.add(getResources().getColor(R.color.green_color));
            colors.add(getResources().getColor(R.color.yellow_color));
        }

        dataSet.setColors(colors);
        PieData data = new PieData(xVals, dataSet);
        data.setDrawValues(false);
        mPieChart.setData(data);
        mPieChart.invalidate();

        String incomingCallsValueFormatted;
        String outgoingCallsValueFormatted;
        String totalCallsValueFormatted;

        DecimalFormat formatter = new DecimalFormat("##");
        if (mPieChartUnitsFilter == INCOMING_OUTGOING_MINUTES) {
            incomingCallsValueFormatted = Integer.toString(incomingCalls);
            outgoingCallsValueFormatted = Integer.toString(outgoingCalls);
            totalCallsValueFormatted = String.format("%1$s %2$s", formatter.format(incomingCalls + outgoingCalls), getString(R.string.minutes));
        } else {
            incomingCallsValueFormatted = Integer.toString(incomingCalls);
            outgoingCallsValueFormatted = Integer.toString(outgoingCalls);
            int callsSum = incomingCalls + outgoingCalls;
            String callsText = getString(R.string.calls_units);
            if (callsSum == 1){
                callsText = getString(R.string.call_units);
            }else if (callsSum == 2 || callsSum == 3 || callsSum ==4){
                callsText = getString(R.string.calls_units_2);
            }
            totalCallsValueFormatted = String.format("%1$s %2$s", Integer.toString(callsSum), callsText);
        }
        mPieChartIncomingValue.setText(incomingCallsValueFormatted);
        mPieChartOutgoingValue.setText(outgoingCallsValueFormatted);
        mPieChartTotalCallsValue.setText(totalCallsValueFormatted);
    }

    protected ArrayList<ChartData> getChartDataByContacts(Date startDate, Date endDate, int direction) {
        ArrayList<ChartData> chartDataArrayList = new ArrayList<>();

        ArrayList<StatisticCall> calls = CallStatCore.GetStatisticCalls(getActivity(), startDate, endDate);

        for (StatisticCall call : calls) {
            if ((direction == INCOMING_CALLS || direction == INCOMING_MINUTES) && (call.getCallType() == DataDirectionTypes.OUTGOING)) {
                continue;
            } else if ((direction == OUTGOING_CALLS || direction == OUTGOING_MINUTES) && (call.getCallType() == DataDirectionTypes.INCOMING)) {
                continue;
            }

            boolean exist = false;
            for (ChartData chartData : chartDataArrayList) {
                if (chartData.getLineDataSet().getLabel().equalsIgnoreCase(call.getPhoneNumber())) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                LineDataSet lineDataSet = new LineDataSet(new ArrayList<Entry>(), call.getPhoneNumber());
                chartDataArrayList.add(new ChartData(lineDataSet));
            }
        }

        return chartDataArrayList;
    }

    protected ArrayList<Entry> getChartDataByRegion(Date startDate, Date endDate) {
        int incomingCalls = 0;
        int outgoingCalls = 0;

        ArrayList<StatisticCall> calls = CallStatCore.GetStatisticCalls(getActivity(), startDate, endDate);
        for (StatisticCall call : calls) {
            if (call.getCallType() == DataDirectionTypes.INCOMING) {
                if (mPieChartUnitsFilter == INCOMING_OUTGOING_CALLS) {
                    incomingCalls += 1;
                } else {
                    incomingCalls += call.getCallDurationInMinutes();
                }
            } else {
                if (mPieChartUnitsFilter == INCOMING_OUTGOING_CALLS) {
                    outgoingCalls += 1;
                } else {
                    outgoingCalls += call.getCallDurationInMinutes();
                }
            }
        }

        ArrayList<Entry> yVals = new ArrayList<>();

        yVals.add(new Entry(incomingCalls, 0));
        yVals.add(new Entry(outgoingCalls, 1));

        return yVals;
    }

    // Заполняем Y значениями
    protected void fillYValue(Date startDate, Date endDate, int callDirectionType, ArrayList<ChartData> chartDataArrayList, int callFilterType) {
        Date date = startDate;
        int index = 0;
        ArrayList<StatisticCall> calls = CallStatCore.GetStatisticCalls(getActivity(), startDate, endDate);
        // Теперь находим соответ dataset и присваиваем знаечение в зависимости от дня
        for (StatisticCall call : calls) {
            if ((callDirectionType == INCOMING_CALLS || callDirectionType == INCOMING_MINUTES) && (call.getCallType() == DataDirectionTypes.OUTGOING)) {
                continue;
            } else if ((callDirectionType == OUTGOING_CALLS || callDirectionType == OUTGOING_MINUTES) && (call.getCallType() == DataDirectionTypes.INCOMING)) {
                continue;
            }

            ChartData chartData = null;
            if (callFilterType == CALL_FILTER_TYPE_BY_OPERATOR) {
                chartData = getChartDataFilterMobileOperator(call.getPhoneNumber(), chartDataArrayList);
            } else if (callFilterType == CALL_FILTER_TYPE_BY_REGION) {
                chartData = getChartDataFilterRegion(call.getPhoneNumber(), chartDataArrayList);
            } else if (callFilterType == CALL_FILTER_TYPE_BY_CONTACTS) {
                chartData = getChartDataFilterContacts(call.getPhoneNumber(), chartDataArrayList);
            } else {
                return;
            }

            if (Utils.getIsCalendarDateAEqualsDateB(date, call.getCallDate()) && date != startDate) {
                Entry entry = chartData.getLineDataSet().getYVals().get(index - 1);
                float currentValue = entry.getVal();
                if (callDirectionType == INCOMING_MINUTES || callDirectionType == OUTGOING_MINUTES || callDirectionType == INCOMING_OUTGOING_MINUTES) {
                    entry.setVal(currentValue + call.getCallDurationInMinutes() * 1000);// Специально умножаем на 1000 чтобы градуировка на Y оси была из целых чисел
                    chartData.setAccumulator(chartData.getAccumulator() + call.getCallDurationInMinutes());
                    chartData.setMaxValue((int) (currentValue + call.getCallDurationInMinutes() * 1000));
                } else {
                    entry.setVal(currentValue + 1000);              // Специально умножаем на 1000 чтобы градуировка на Y оси была из целых чисел
                    chartData.setAccumulator(chartData.getAccumulator() + 1);
                    chartData.setMaxValue((int) currentValue + 1000);
                }
            } else {
                if (!Utils.getIsCalendarDateAEqualsDateB(date, call.getCallDate())) {
                    // перескакиваем дни когда никто не звонил
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                    date = calendar.getTime();
                    while (!Utils.getIsCalendarDateAEqualsDateB(date, call.getCallDate())) {
                        calendar.setTime(date);
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                        date = calendar.getTime();
                        index++;
                    }
                }
                Entry entry = chartData.getLineDataSet().getYVals().get(index);
                if (callDirectionType == INCOMING_MINUTES || callDirectionType == OUTGOING_MINUTES || callDirectionType == INCOMING_OUTGOING_MINUTES) {
                    entry.setVal(call.getCallDurationInMinutes() * 1000); // Специально умножаем на 1000 чтобы градуировка на Y оси была из целых чисел
                } else {
                    entry.setVal(1000);
                }
                chartData.setMaxValue((int) entry.getVal());
                chartData.setAccumulator(chartData.getAccumulator() + (int) entry.getVal() / 1000);

                index++;

                date = call.getCallDate();
            }
        }

    }

    // Возращает заполненый ChartData по Y когда фильтр стоит по мобильному оператору
    protected ChartData getChartDataFilterMobileOperator(String phoneNumber, ArrayList<ChartData> chartDataArrayList) {
        Region region = Application.getInstance().getRegion();
        int mobileOperatorIndex = -1;
        for (int i = 0; i < region.getMobileOperators().size(); i++) {
            MobileOperator mobileOperator = region.getMobileOperators().get(i);
            boolean phoneNumberStartsWithPrefix = false;
            for (String prefix : mobileOperator.getNumberPrefix()) {
                phoneNumberStartsWithPrefix = Utils.isPhoneNumberStartWith(phoneNumber, prefix);
                if (phoneNumberStartsWithPrefix) {
                    break;
                }
            }
            if (phoneNumberStartsWithPrefix) {
                mobileOperatorIndex = i;

                break;
            }
        }

        if (mobileOperatorIndex == -1) {
            mobileOperatorIndex = chartDataArrayList.size() - 1;
        }

        return chartDataArrayList.get(mobileOperatorIndex);
    }

    // Возращает заполненый ChartData по Y когда фильтр стоит по региону
    protected ChartData getChartDataFilterRegion(String phoneNumber, ArrayList<ChartData> chartDataArrayList) {
        Country country = Application.getInstance().getCountry();
        Region region = Application.getInstance().getRegion();

        ArrayList<String> countryPrefixes = new ArrayList<>(Arrays.asList(country.getNumberPrefix().split(";")));
        ArrayList<String> regionPrefixes = new ArrayList<>(Arrays.asList(region.getNumberPrefix().split(";")));

        int regionType = Utils.GetPhoneNumberRegionType(phoneNumber, countryPrefixes, regionPrefixes, region.getMinDigitsCount(), region.getMaxDigitsCount());

        ChartData chartData;
        if (regionType == DataRegionTypes.InRegion) {
            chartData = chartDataArrayList.get(0);
        } else if (regionType == DataRegionTypes.InterRegion) {
            chartData = chartDataArrayList.get(1);
        } else if (regionType == DataRegionTypes.InterCountries) {
            chartData = chartDataArrayList.get(2);
        } else {
            chartData = chartDataArrayList.get(3);
        }
        return chartData;
    }

    // Возращает заполненый ChartData по Y когда фильтр стоит по контактам
    protected ChartData getChartDataFilterContacts(String phoneNumber, ArrayList<ChartData> chartDataArrayList) {
        int dataSetIndex = -1;
        for (int i = 0; i < chartDataArrayList.size(); i++) {
            ChartData chartData = chartDataArrayList.get(i);
            if (chartData.getLineDataSet().getLabel().equalsIgnoreCase(phoneNumber)) {
                dataSetIndex = i;
                break;
            }
        }

        if (dataSetIndex == -1) {
            return null;
        }

        return chartDataArrayList.get(dataSetIndex);
    }

    // Заполняет Chart data начальными значениями: x - по каждому дню, y - пустыми значениями
    private void fillChartWithEmptyData(Date startDate, Date endDate, ArrayList<String> xVals, ArrayList<ChartData> yVals) {
        int j = 0;
        Date dateA = startDate;
        // Вначале заполняем все датасеты и X пустыми значениями
        while (!Utils.getIsCalendarDateAEqualsDateB(dateA, endDate)) {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateA);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            if (day == 1) {
                xVals.add(j, String.format("%1$s/%2$s", calendar.get(Calendar.MONTH) + 1, Integer.toString(day)));
            } else {
                xVals.add(j, Integer.toString(day));
            }
            for (ChartData chartData : yVals) {
                chartData.getLineDataSet().addEntry(new Entry(0, j));
            }
            calendar.add(Calendar.DAY_OF_YEAR, 1);

            dateA = calendar.getTime();
            j++;
        }
        // устанавливаем частоту отображения значений на X оси
        if (j <= 31) {
            mLineChart.getXAxis().setLabelsToSkip(1);
        } else if (j <= 61) {
            mLineChart.getXAxis().setLabelsToSkip(2);
        } else {
            mLineChart.getXAxis().resetLabelsToSkip();
        }
        // Добавляем последний день
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        xVals.add(j, Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));

        // По Y пока присваиваем значения = 0
        for (ChartData chartData : yVals) {
            chartData.getLineDataSet().addEntry(new Entry(0, j));
        }
    }

    // Присваивает датасету свойства по умолчанию.
    private void setDefaultDataSetProperties(LineDataSet lineDataSet, int colorIndex) {
        int colorCode = getColorCodeByIndex(colorIndex);

        lineDataSet.setColor(colorCode);
        lineDataSet.setLineWidth(2f);
        lineDataSet.setDrawCircles(false);
        lineDataSet.setDrawFilled(true);
        lineDataSet.setDrawValues(false);
        lineDataSet.setFillAlpha(90);
        lineDataSet.setFillColor(colorCode);
    }

    private int getColorCodeByIndex(int i) {

        if (mLegendColors.length > i) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return getResources().getColor(mLegendColors[i], getActivity().getTheme());
            } else {
                return getResources().getColor(mLegendColors[i]);
            }
        } else {
            Random rand = new Random();
            int r = rand.nextInt(255);
            int g = rand.nextInt(255);
            int b = rand.nextInt(255);
            return Color.rgb(r, g, b);
        }
    }

    // Заполнить список
    protected void fillCallsList(Date startDate, Date endDate) {

        mListViewSelectPeriodLabel.setText(String.format("%1$s - %2$s",
                Utils.getFormattedDate_DayMonth(startDate), Utils.getFormattedDate_DayMonth(endDate)));

        mListAreaDirectionFilter = mListViewThreeStateSwitch.getState();
        int state = mListAreaDirectionFilter;

        if (state != 0) {
            state = state == 1 ? -1 : DataDirectionTypes.OUTGOING;
        }
        ArrayList<StatisticCall> calls = CallStatCore.GetStatisticCalls(getActivity(), startDate, endDate, state);
        ListData listData = new ListData();
        int totalCallsCount = 0;
        for (StatisticCall call : calls) {
            if (call.getCallType() == DataDirectionTypes.INCOMING) {
                listData.add(new ListItemData(call.getContactId(), call.getName(), call.getPhoneNumber(), 1, 0, call.getCallDuration(), 0));
            } else {
                listData.add(new ListItemData(call.getContactId(), call.getName(), call.getPhoneNumber(), 0, 1, 0, call.getCallDuration()));
            }
            totalCallsCount += 1;
        }

        Collections.sort(listData, new ListDataComparator(state));

        mListViewTotalCallsCount.setText(Integer.toString(totalCallsCount));

        mListView.setAdapter(new CallsListArrayAdapter(getActivity(), listData));
    }

    // Для присвоения текста для лейблов на Y оси
    public class CustomValueFormatter implements ValueFormatter {

        private DecimalFormat mFormat;

        public CustomValueFormatter() {
            mFormat = new DecimalFormat("##");
        }

        @Override
        public String getFormattedValue(float value) {
            int callDirectionType = mLineChartCallDirectionSpinner.getSelectedItemPosition();
            if (callDirectionType == INCOMING_OUTGOING_MINUTES || callDirectionType == INCOMING_MINUTES || callDirectionType == OUTGOING_MINUTES) {
                if (Math.floor(value / 1000) == value / 1000) {
                    return String.format("%1$s %2$s", mFormat.format(value / 1000), getString(R.string.minutes));     // Делим на 1000 так как мы до этого значение умножали на 1000 чтобы градуировка на Y оси была из целых чисел
                } else {
                    return "";
                }
            } else {
                if (Math.floor(value / 1000) == value / 1000) {
                    return mFormat.format(value / 1000);
                } else {
                    return "";
                }
            }

        }
    }

    // Для списка фильтров
    public class FilterArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final String[] values;

        public FilterArrayAdapter(Context context, String[] values) {
            super(context, R.layout.spinner_item, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.spinner_item, parent, false);
                TextView textView = (TextView) convertView.findViewById(R.id.label);
                textView.setTypeface(Application.condensedTypeface);
                textView.setText(values[position]);
            }
            return convertView;
        }
    }

    // Для списка легенд
    public class LegendArrayAdapter extends ArrayAdapter<String> {
        private final Context mContext;
        private final DataSet[] mDataSets;

        public LegendArrayAdapter(Context context, DataSet[] dataSets) {
            super(context, R.layout.legend_list_item);
            this.mContext = context;
            this.mDataSets = dataSets;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.legend_list_item, parent, false);
            }
            final DataSet dataSet = mDataSets[position];

            final CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);
            checkBox.setTypeface(Application.lightTypeface);
            checkBox.setChecked(dataSet.isVisible());
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dataSet.setVisible(checkBox.isChecked());
                    mLineChart.invalidate();
                }
            });

            TextView phoneNumber = (TextView) convertView.findViewById(R.id.phoneNumber);
            phoneNumber.setText(dataSet.getLabel());
            phoneNumber.setTypeface(Application.lightTypeface);
            phoneNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkBox.setChecked(!checkBox.isChecked());
                    dataSet.setVisible(checkBox.isChecked());
                    mLineChart.invalidate();
                }
            });

            FrameLayout colorBox = (FrameLayout) convertView.findViewById(R.id.color_box);
            if (Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                colorBox.setBackgroundDrawable(Utils.getColoredCircle(dataSet.getColor()));
            } else {
                colorBox.setBackground(Utils.getColoredCircle(dataSet.getColor()));
            }

            if (position % 2 == 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    convertView.setBackgroundColor(getResources().getColor(R.color.dark_bg, getActivity().getTheme()));
                } else {
                    convertView.setBackgroundColor(getResources().getColor(R.color.dark_bg));
                }
                if (position == mDataSets.length - 1) {
                    phoneNumber.setBackgroundResource(R.drawable.dark_list_item_top_bottom_border);
                } else {
                    phoneNumber.setBackgroundResource(R.drawable.dark_list_item_top_border);
                }

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    convertView.setBackgroundColor(getResources().getColor(R.color.light_bg, getActivity().getTheme()));
                } else {
                    convertView.setBackgroundColor(getResources().getColor(R.color.light_bg));
                }

                if (position == mDataSets.length - 1) {
                    phoneNumber.setBackgroundResource(R.drawable.light_list_item_top_bottom_border);
                } else {
                    phoneNumber.setBackgroundResource(R.drawable.light_list_item_top_border);
                }
            }

            convertView.setTag(dataSet);
            return convertView;
        }

        @Override
        public int getCount() {
            return mDataSets.length;
        }

    }

    // Здесь хранятся датасет для чарта и высчитывается общая сумма значения Y
    public class ChartData {

        int mMaxValue;

        public int getMaxValue() {
            return mMaxValue;
        }

        public void setMaxValue(int maxValue) {
            if (mMaxValue < maxValue) {
                this.mMaxValue = maxValue;
            }
        }

        private LineDataSet mLineDataSet;

        public LineDataSet getLineDataSet() {
            return mLineDataSet;
        }

        public int getAccumulator() {
            return mAccumulator;
        }

        public void setAccumulator(int accumulator) {
            this.mAccumulator = accumulator;
        }

        private int mAccumulator;

        public ChartData(LineDataSet lineDataSet) {
            lineDataSet.setDrawCubic(true);
            mLineDataSet = lineDataSet;
        }
    }

    protected class CallsListArrayAdapter extends ArrayAdapter<ListItemData> {
        private final Context mContext;
        private final ListData mListData;

        public CallsListArrayAdapter(Context context, ListData listData) {
            super(context, R.layout.statistic_list_item);
            this.mContext = context;
            this.mListData = listData;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.statistic_list_item, parent, false);
            }

            final ListItemData listItemData = mListData.get(position);

            TextView contactName = (TextView) convertView.findViewById(R.id.phoneNumber);
            contactName.setTypeface(Application.lightTypeface);
            contactName.setText(String.format("%1$s  (%2$s)", TextUtils.isEmpty(listItemData.getTitle()) ? listItemData.getPhoneNumber() : listItemData.getTitle(),
                    Integer.toString(listItemData.getIncomingCallsCount() + listItemData.getOutgoingCallsCount())));

            LinearLayout outgoingLayout = (LinearLayout) convertView.findViewById(R.id.outgoing_content);
            LinearLayout incomingLayout = (LinearLayout) convertView.findViewById(R.id.incoming_content);

            TextView incomingCallsCount = (TextView) convertView.findViewById(R.id.incoiming_count);
            incomingCallsCount.setTypeface(Application.lightTypeface);
            incomingCallsCount.setText(Integer.toString(listItemData.getIncomingCallsCount()));
            if (listItemData.getIncomingCallsCount() == 0) {
                incomingLayout.setVisibility(View.GONE);
            } else {
                incomingLayout.setVisibility(View.VISIBLE);
            }

            TextView outgoingCallsCount = (TextView) convertView.findViewById(R.id.outgoing_count);
            outgoingCallsCount.setTypeface(Application.lightTypeface);
            outgoingCallsCount.setText(Integer.toString(listItemData.getOutgoingCallsCount()));
            if (listItemData.getOutgoingCallsCount() == 0) {
                outgoingLayout.setVisibility(View.GONE);
            } else {
                outgoingLayout.setVisibility(View.VISIBLE);
            }

            TextView incomingDuration = (TextView) convertView.findViewById(R.id.incoiming_duration);
            incomingDuration.setTypeface(Application.lightTypeface);
            incomingDuration.setText(String.format("(%1$s)", Utils.getDuration(getActivity(), listItemData.getIncomingCallsInSeconds())));

            TextView outgoingDuration = (TextView) convertView.findViewById(R.id.outgoing_duration);
            outgoingDuration.setTypeface(Application.lightTypeface);
            outgoingDuration.setText(String.format("(%1$s)", Utils.getDuration(getActivity(), listItemData.getOutgoingCallsInSeconds())));

            final OvalImageView imageContact = (OvalImageView) convertView.findViewById(R.id.contact_image);
            imageContact.setImageResource(R.drawable.avatar);
            imageContact.setTag(listItemData.getPhoneNumber());
            final AsyncTask<String, Void, Bitmap> task = new AsyncTask<String, Void, Bitmap>() {
                @Override
                protected Bitmap doInBackground(String... params) {
                    return Utils.getPhotoUri(getContext(), params[0]);
                }

                @Override
                protected void onPostExecute(Bitmap photo) {
                    if (photo != null) {
                        try {
                            if (imageContact.getTag().equals(listItemData.getPhoneNumber())) {
                                imageContact.setImageBitmap(photo);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            };
            task.execute(listItemData.getPhoneNumber());

            LinearLayout contentLayout = (LinearLayout) convertView.findViewById(R.id.content);

            if (position % 2 == 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    convertView.setBackgroundColor(getResources().getColor(R.color.dark_bg, getActivity().getTheme()));
                } else {
                    convertView.setBackgroundColor(getResources().getColor(R.color.dark_bg));
                }
                if (position == mListData.size() - 1) {
                    contentLayout.setBackgroundResource(R.drawable.dark_list_item_top_bottom_border);
                } else {
                    contentLayout.setBackgroundResource(R.drawable.dark_list_item_top_border);
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    convertView.setBackgroundColor(getResources().getColor(R.color.light_bg, getActivity().getTheme()));
                } else {
                    convertView.setBackgroundColor(getResources().getColor(R.color.light_bg));
                }
                if (position == mListData.size() - 1) {
                    contentLayout.setBackgroundResource(R.drawable.light_list_item_top_bottom_border);
                } else {
                    contentLayout.setBackgroundResource(R.drawable.light_list_item_top_border);
                }
            }

            return convertView;
        }

        @Override
        public int getCount() {
            return mListData.size();
        }

    }

    // Данные для отображения в виде списка
    public class ListData extends ArrayList<ListItemData> {

        @Override
        public boolean add(ListItemData item) {
            ListItemData foundedItem = getFoundedItemIndex(item.getTitle(), item.getPhoneNumber());
            if (foundedItem == null) {
                super.add(item);
            } else {
                foundedItem.setIncomingCallsCount(foundedItem.getIncomingCallsCount() + item.getIncomingCallsCount());
                foundedItem.setOutgoingCallsCount(foundedItem.getOutgoingCallsCount() + item.getOutgoingCallsCount());

                foundedItem.setIncomingCallsInSeconds(foundedItem.getIncomingCallsInSeconds() + item.getIncomingCallsInSeconds());
                foundedItem.setOutgoingCallsInSeconds(foundedItem.getOutgoingCallsInSeconds() + item.getOutgoingCallsInSeconds());
            }
            return true;
        }

        private ListItemData getFoundedItemIndex(String title, String phoneNumber) {
            for (ListItemData listItemData : this) {
                if (listItemData.getTitle() != null && !TextUtils.isEmpty(title)){
                    if (listItemData.getTitle().equalsIgnoreCase(title)) {
                        return listItemData;
                    }
                }else if (listItemData.getPhoneNumber() != null && !TextUtils.isEmpty(phoneNumber)){
                    if (listItemData.getPhoneNumber().equalsIgnoreCase(phoneNumber)) {
                        return listItemData;
                    }
                }

            }
            return null;
        }

    }

    // Элемент списка
    protected class ListItemData {

        public ListItemData(int contactId, String title, String phoneNumber, int incomingCallsCount, int outgoingCallsCount, int incomingCallsDuration, int outgoingCallsDuration) {
            mContactId = contactId;
            mTitle = title;
            mPhoneNumber = phoneNumber;
            mIncomingCallsCount = incomingCallsCount;
            mOutgoingCallsCount = outgoingCallsCount;
            mIncomingCallsInSeconds = incomingCallsDuration;
            mOutgoingCallsInSeconds = outgoingCallsDuration;
            mSumIncomingAndOutgoing = mOutgoingCallsInSeconds + mIncomingCallsInSeconds;
        }

        public int getContactId() {
            return mContactId;
        }

        public String getPhoneNumber() {
            return mPhoneNumber;
        }

        public String getTitle() {
            return mTitle;
        }

        public int getIncomingCallsCount() {
            return mIncomingCallsCount;
        }

        public void setIncomingCallsCount(int incomingCallsCount) {
            this.mIncomingCallsCount = incomingCallsCount;
        }

        public int getOutgoingCallsCount() {
            return mOutgoingCallsCount;
        }

        public void setOutgoingCallsCount(int outgoingCallsCount) {
            this.mOutgoingCallsCount = outgoingCallsCount;
        }

        public int getIncomingCallsInSeconds() {
            return mIncomingCallsInSeconds;
        }

        public void setIncomingCallsInSeconds(int incomingCallsInSeconds) {
            this.mIncomingCallsInSeconds = incomingCallsInSeconds;
            mSumIncomingAndOutgoing = mOutgoingCallsInSeconds + mIncomingCallsInSeconds;
        }

        public int getOutgoingCallsInSeconds() {
            return mOutgoingCallsInSeconds;
        }

        public void setOutgoingCallsInSeconds(int outgoingCallsInSeconds) {
            this.mOutgoingCallsInSeconds = outgoingCallsInSeconds;
            mSumIncomingAndOutgoing = mOutgoingCallsInSeconds + mIncomingCallsInSeconds;
        }

        public int getSumIncomingAndOutgoing() {
            return mSumIncomingAndOutgoing;
        }

        private final String mTitle;
        private final String mPhoneNumber;
        private int mIncomingCallsCount;
        private int mOutgoingCallsCount;
        private int mIncomingCallsInSeconds;
        private int mOutgoingCallsInSeconds;
        private int mSumIncomingAndOutgoing;
        private final int mContactId;
    }

    protected class ListDataComparator implements Comparator<ListItemData> {
        private int mDirectionType;

        public ListDataComparator(int directionType) {
            mDirectionType = directionType;
        }

        public int compare(ListItemData object1, ListItemData object2) {
            if (mDirectionType == DataDirectionTypes.INCOMING) {
                return (object1.getIncomingCallsInSeconds() > object2.getIncomingCallsInSeconds() ?
                        -1 : (object1.getIncomingCallsInSeconds() == object2.getIncomingCallsInSeconds() ? 0 : 1));
            } else if (mDirectionType == DataDirectionTypes.OUTGOING) {
                return (object1.getOutgoingCallsInSeconds() > object2.getOutgoingCallsInSeconds() ?
                        -1 : (object1.getOutgoingCallsInSeconds() == object2.getOutgoingCallsInSeconds() ? 0 : 1));
            } else {
                return (object1.getSumIncomingAndOutgoing() > object2.getSumIncomingAndOutgoing() ?
                        -1 : (object1.getSumIncomingAndOutgoing() == object2.getSumIncomingAndOutgoing() ? 0 : 1));
            }
        }
    }

    // Сравнитель, для сравнения и сотрировки по значению Акумулятора в ChartData
    public class ChartDataComparator implements Comparator<ChartData> {
        public int compare(ChartData object1, ChartData object2) {
            return (object1.getAccumulator() > object2.getAccumulator() ? -1 : (object1.getAccumulator() == object2.getAccumulator() ? 0 : 1));
        }
    }

}
