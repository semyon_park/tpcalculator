package com.ct.tpcalculator.application.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ct.tpcalculator.helpers.GoogleAnalyticsHelper;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.views.OvalImageView;
import com.ct.tpcalculator.calculator.data.DataDirectionTypes;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticMessage;
import com.ct.tpcalculator.calculator.statisticAgent.msgstat.MsgCore;
import com.ct.tpcalculator.helpers.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class ChartMessagesFragment extends ChartCallsFragment {

    public static final String TAG = ChartMessagesFragment.class.getName();
    private static final String GA_PAGE_NAME = "ChartMessagesFragment";

    public ChartMessagesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallDirectionTypes = new String[]{getString(R.string.incoming_outgoing_messages), getString(R.string.outgoing_messages), getString(R.string.incoming_messages)};

        GoogleAnalyticsHelper.sendLog(GA_PAGE_NAME, null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) super.onCreateView(inflater, container, savedInstanceState);
        mPieChartUnitsSwitcher.setVisibility(View.GONE);
        mPieChartTotalCallsValue.setVisibility(View.GONE);
        return root;
    }

    // Заполняем Y значениями
    protected void fillYValue(Date startDate, Date endDate, int msgDirectionType, ArrayList<ChartData> chartDataArrayList, int callFilterType) {
        Date date = startDate;
        int index = 0;
        ArrayList<StatisticMessage> messages = MsgCore.GetStatisticMessageStatistic(getActivity(), startDate, endDate, StatisticMessage.TYPE_MSG_SMS);
        // Теперь находим соответ dataset и присваиваем знаечение в зависимости от дня
        for (StatisticMessage message : messages) {
            if ((msgDirectionType == INCOMING_CALLS) && (message.getDirectionType() == DataDirectionTypes.OUTGOING)) {
                continue;
            } else if ((msgDirectionType == OUTGOING_CALLS) && (message.getDirectionType() == DataDirectionTypes.INCOMING)) {
                continue;
            }

            ChartData chartData;
            if (callFilterType == CALL_FILTER_TYPE_BY_OPERATOR) {
                chartData = getChartDataFilterMobileOperator(message.getPhoneNumber(), chartDataArrayList);
            } else if (callFilterType == CALL_FILTER_TYPE_BY_REGION) {
                chartData = getChartDataFilterRegion(message.getPhoneNumber(), chartDataArrayList);
            } else if (callFilterType == CALL_FILTER_TYPE_BY_CONTACTS) {
                chartData = getChartDataFilterContacts(message.getPhoneNumber(), chartDataArrayList);
            } else {
                return;
            }


            if (Utils.getIsCalendarDateAEqualsDateB(date, message.getMessageDate()) && date != startDate) {
                Entry entry = chartData.getLineDataSet().getYVals().get(index - 1);
                float currentValue = entry.getVal();
                entry.setVal(currentValue + 1000);              // Специально умножаем на 1000 чтобы градуировка на Y оси была из целых чисел
                chartData.setAccumulator(chartData.getAccumulator() + 1);
                chartData.setMaxValue((int) currentValue + 1000);
            } else {
                if (!Utils.getIsCalendarDateAEqualsDateB(date, message.getMessageDate())) {
                    // перескакиваем дни когда никто не звонил
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                    date = calendar.getTime();
                    while (!Utils.getIsCalendarDateAEqualsDateB(date, message.getMessageDate())) {
                        calendar.setTime(date);
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                        date = calendar.getTime();
                        index++;
                    }
                }
                Entry entry = chartData.getLineDataSet().getYVals().get(index);
                entry.setVal(1000);
                chartData.setMaxValue((int) entry.getVal());
                chartData.setAccumulator(chartData.getAccumulator() + (int) entry.getVal() / 1000);

                index++;

                date = message.getMessageDate();
            }
        }

    }

    protected ArrayList<ChartData> getChartDataByContacts(Date startDate, Date endDate, int direction) {
        ArrayList<ChartData> chartDataArrayList = new ArrayList<>();

        ArrayList<StatisticMessage> messages = MsgCore.GetStatisticMessageStatistic(getActivity(), startDate, endDate, StatisticMessage.TYPE_MSG_SMS);

        for (StatisticMessage message : messages) {
            if (direction == INCOMING_CALLS && message.getDirectionType() == DataDirectionTypes.OUTGOING) {
                continue;
            } else if (direction == OUTGOING_CALLS && message.getDirectionType() == DataDirectionTypes.INCOMING) {
                continue;
            }

            boolean exist = false;
            for (ChartData chartData : chartDataArrayList) {
                if (chartData.getLineDataSet().getLabel().equalsIgnoreCase(message.getPhoneNumber())) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                LineDataSet lineDataSet = new LineDataSet(new ArrayList<Entry>(), message.getPhoneNumber());
                chartDataArrayList.add(new ChartData(lineDataSet));
            }
        }

        return chartDataArrayList;
    }

    protected ArrayList<Entry> getChartDataByRegion(Date startDate, Date endDate) {
        int incomingCalls = 0;
        int outgoingCalls = 0;

        ArrayList<StatisticMessage> messages = MsgCore.GetStatisticMessageStatistic(getActivity(), startDate, endDate, StatisticMessage.TYPE_MSG_SMS);
//        CallStatCore.GetStatisticCalls(getActivity(), startDate, endDate);
        for (StatisticMessage message : messages) {
            if (message.getDirectionType() == DataDirectionTypes.INCOMING) {
//                if (mPieChartUnitsFilter == INCOMING_OUTGOING_CALLS) {
                incomingCalls += 1;
//                }
            } else {
//                if (mPieChartUnitsFilter == INCOMING_OUTGOING_CALLS) {
                outgoingCalls += 1;
//                } else {
//                    outgoingCalls += message.getCallDurationInMinutes();
//                }
            }
        }

        ArrayList<Entry> yVals = new ArrayList<>();

        yVals.add(new Entry(incomingCalls, 0));
        yVals.add(new Entry(outgoingCalls, 1));

        return yVals;
    }

    // Заполнить список
    protected void fillCallsList(Date startDate, Date endDate) {

        mListViewSelectPeriodLabel.setText(String.format("%1$s - %2$s",
                Utils.getFormattedDate_DayMonth(startDate), Utils.getFormattedDate_DayMonth(endDate)));

        mListAreaDirectionFilter = mListViewThreeStateSwitch.getState();
        int state = mListAreaDirectionFilter;

        if (state != 0) {
            state = state == 1 ? -1 : DataDirectionTypes.OUTGOING;
        }

        ArrayList<ListItemData> messagesArrayList = new ArrayList<>();

        ArrayList<StatisticMessage> messages = MsgCore.GetStatisticMessageStatistic(getActivity(), startDate, endDate, StatisticMessage.TYPE_MSG_SMS);

        int totalCallsCount = 0;
        for (StatisticMessage message : messages) {
            if (state == DataDirectionTypes.INCOMING && message.getDirectionType() == DataDirectionTypes.OUTGOING) {
                continue;
            } else if (state == DataDirectionTypes.OUTGOING && message.getDirectionType() == DataDirectionTypes.INCOMING) {
                continue;
            }

            totalCallsCount += 1;

            boolean exist = false;
            for (ListItemData listItemData : messagesArrayList) {
                if ((listItemData.getTitle() != null && listItemData.getTitle().equalsIgnoreCase(message.getName())) ||
                        (listItemData.getPhoneNumber() != null && listItemData.getPhoneNumber().equalsIgnoreCase(message.getPhoneNumber()))) {
                    if (message.getDirectionType() == DataDirectionTypes.OUTGOING) {
                        listItemData.setOutgoingCallsCount(listItemData.getOutgoingCallsCount() + 1);
                    } else {
                        listItemData.setIncomingCallsCount(listItemData.getIncomingCallsCount() + 1);
                    }
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                int incomingCount = 0;
                int outgoingCount = 0;
                if (message.getDirectionType() == DataDirectionTypes.OUTGOING) {
                    outgoingCount = 1;
                } else {
                    incomingCount = 1;
                }
                messagesArrayList.add(new ListItemData(message.getName(), message.getPhoneNumber(), incomingCount, outgoingCount));
            }
        }

        Collections.sort(messagesArrayList, new ListDataComparator(state));

        mListViewTotalCallsCount.setText(Integer.toString(totalCallsCount));

        mListView.setAdapter(new MessagesListArrayAdapter(getActivity(), messagesArrayList));
    }


    private class MessagesListArrayAdapter extends ArrayAdapter<ListItemData> {
        private final Context mContext;
        private final ArrayList<ListItemData> mListData;

        public MessagesListArrayAdapter(Context context, ArrayList<ListItemData> listData) {
            super(context, R.layout.statistic_list_item);
            this.mContext = context;
            this.mListData = listData;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.statistic_list_item, parent, false);
            }

            final ListItemData listItemData = mListData.get(position);

            TextView contactName = (TextView) convertView.findViewById(R.id.phoneNumber);
            contactName.setTypeface(Application.lightTypeface);
            contactName.setText(TextUtils.isEmpty(listItemData.getTitle()) ? listItemData.getPhoneNumber() : listItemData.getTitle());

            LinearLayout outgoingLayout = (LinearLayout) convertView.findViewById(R.id.outgoing_content);
            LinearLayout incomingLayout = (LinearLayout) convertView.findViewById(R.id.incoming_content);

            TextView incomingCallsCount = (TextView) convertView.findViewById(R.id.incoiming_count);
            incomingCallsCount.setTypeface(Application.lightTypeface);
            incomingCallsCount.setText(Integer.toString(listItemData.getIncomingCallsCount()));
            if (listItemData.getIncomingCallsCount() == 0) {
                incomingLayout.setVisibility(View.GONE);
            } else {
                incomingLayout.setVisibility(View.VISIBLE);
            }

            TextView outgoingCallsCount = (TextView) convertView.findViewById(R.id.outgoing_count);
            outgoingCallsCount.setTypeface(Application.lightTypeface);
            outgoingCallsCount.setText(Integer.toString(listItemData.getOutgoingCallsCount()));
            if (listItemData.getOutgoingCallsCount() == 0) {
                outgoingLayout.setVisibility(View.GONE);
            } else {
                outgoingLayout.setVisibility(View.VISIBLE);
            }
//
//            TextView incomingDuration = (TextView) rowView.findViewById(R.id.incoiming_duration);
//            incomingDuration.setTypeface(Application.lightTypeface);
//            incomingDuration.setText(String.format("(%1$s)", Utils.getDuration(getActivity(), listItemData.getIncomingCallsInSeconds())));
//
//            TextView outgoingDuration = (TextView) rowView.findViewById(R.id.outgoing_duration);
//            outgoingDuration.setTypeface(Application.lightTypeface);
//            outgoingDuration.setText(String.format("(%1$s)", Utils.getDuration(getActivity(), listItemData.getOutgoingCallsInSeconds())));

            final OvalImageView imageContact = (OvalImageView) convertView.findViewById(R.id.contact_image);
            imageContact.setImageResource(R.drawable.avatar);
            imageContact.setTag(listItemData.getPhoneNumber());
            final AsyncTask<String, Void, Bitmap> task = new AsyncTask<String, Void, Bitmap>() {
                @Override
                protected Bitmap doInBackground(String... params) {
                    return Utils.getPhotoUri(getContext(), params[0]);
                }

                @Override
                protected void onPostExecute(Bitmap photo) {
                    if (photo != null) {
                        try {
                            if (imageContact.getTag().equals(listItemData.getPhoneNumber())) {
                                imageContact.setImageBitmap(photo);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            };
            task.execute(listItemData.getPhoneNumber());

            LinearLayout contentLayout = (LinearLayout) convertView.findViewById(R.id.content);

            if (position % 2 == 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    convertView.setBackgroundColor(getResources().getColor(R.color.dark_bg, getActivity().getTheme()));
                } else {
                    convertView.setBackgroundColor(getResources().getColor(R.color.dark_bg));
                }
                if (position == mListData.size() - 1) {
                    contentLayout.setBackgroundResource(R.drawable.dark_list_item_top_bottom_border);
                } else {
                    contentLayout.setBackgroundResource(R.drawable.dark_list_item_top_border);
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    convertView.setBackgroundColor(getResources().getColor(R.color.light_bg, getActivity().getTheme()));
                } else {
                    convertView.setBackgroundColor(getResources().getColor(R.color.light_bg));
                }
                if (position == mListData.size() - 1) {
                    contentLayout.setBackgroundResource(R.drawable.light_list_item_top_bottom_border);
                } else {
                    contentLayout.setBackgroundResource(R.drawable.light_list_item_top_border);
                }
            }

            return convertView;
        }

        @Override
        public int getCount() {
            return mListData.size();
        }

    }

    protected class ListItemData {

        public ListItemData(String title, String phoneNumber, int incomingCallsCount, int outgoingCallsCount) {
            mTitle = title;
            mPhoneNumber = phoneNumber;
            mIncomingCallsCount = incomingCallsCount;
            mOutgoingCallsCount = outgoingCallsCount;
            mSumIncomingAndOutgoing = mIncomingCallsCount + mOutgoingCallsCount;
        }

        public String getPhoneNumber() {
            return mPhoneNumber;
        }

        public String getTitle() {
            return mTitle;
        }

        public int getIncomingCallsCount() {
            return mIncomingCallsCount;
        }

        public void setIncomingCallsCount(int incomingCallsCount) {
            this.mIncomingCallsCount = incomingCallsCount;
            mSumIncomingAndOutgoing = mIncomingCallsCount + mOutgoingCallsCount;
        }

        public int getOutgoingCallsCount() {
            return mOutgoingCallsCount;
        }

        public void setOutgoingCallsCount(int outgoingCallsCount) {
            this.mOutgoingCallsCount = outgoingCallsCount;
            mSumIncomingAndOutgoing = mIncomingCallsCount + mOutgoingCallsCount;
        }

        public int getSumIncomingAndOutgoing() {
            return mSumIncomingAndOutgoing;
        }

        private final String mTitle;
        private final String mPhoneNumber;
        private int mIncomingCallsCount;
        private int mOutgoingCallsCount;
        private int mSumIncomingAndOutgoing;
    }

    protected class ListDataComparator implements Comparator<ListItemData> {
        private int mDirectionType;

        public ListDataComparator(int directionType) {
            mDirectionType = directionType;
        }

        public int compare(ListItemData object1, ListItemData object2) {
            if (mDirectionType == DataDirectionTypes.INCOMING) {
                return (object1.getIncomingCallsCount() > object2.getIncomingCallsCount() ?
                        -1 : (object1.getIncomingCallsCount() == object2.getIncomingCallsCount() ? 0 : 1));
            } else if (mDirectionType == DataDirectionTypes.OUTGOING) {
                return (object1.getOutgoingCallsCount() > object2.getOutgoingCallsCount() ?
                        -1 : (object1.getOutgoingCallsCount() == object2.getOutgoingCallsCount() ? 0 : 1));
            } else {
                return (object1.getSumIncomingAndOutgoing() > object2.getSumIncomingAndOutgoing() ?
                        -1 : (object1.getSumIncomingAndOutgoing() == object2.getSumIncomingAndOutgoing() ? 0 : 1));
            }
        }
    }
}
