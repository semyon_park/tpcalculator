package com.ct.tpcalculator.application.views;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by marat on 10/2/2015.
 */
public class PieChart extends com.github.mikephil.charting.charts.PieChart {

    public PieChart(Context context) {
        super(context);
    }

    public PieChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PieChart(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//
//        int width = MeasureSpec.getSize(widthMeasureSpec);
//        setMeasuredDimension(width, width);
//    }
}
