package com.ct.tpcalculator.application.fragments;

import android.support.v4.app.Fragment;

/**
 * Created by marat on 9/30/2015.
 */
public abstract class ChartBaseFragment extends Fragment {


    public static final int DATA_VIEW_TYPE_LINE_CHART = 0;
    public static final int DATA_VIEW_TYPE_PIE_CHART = 1;
    public static final int DATA_VIEW_TYPE_LIST = 2;

    public static final int CHART_FRAGMENT_CALLS = 0;
    public static final int CHART_FRAGMENT_MESSAGES = 1;
    public static final int CHART_FRAGMENT_INTERNET = 2;

    public abstract void showChart(int chart);

    public abstract int getSelectedChartType();

}
