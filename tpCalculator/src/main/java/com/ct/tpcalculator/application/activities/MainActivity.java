package com.ct.tpcalculator.application.activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.fragments.ChartBaseFragment;
import com.ct.tpcalculator.application.fragments.MainFragment;
import com.ct.tpcalculator.application.fragments.NavigationDrawerFragment;
import com.ct.tpcalculator.application.listeners.OnFragmentChangedListener;
import com.ct.tpcalculator.application.navigationControllers.MainNavigationController;
import com.ct.tpcalculator.helpers.CommandExecutionUtil;
import com.ct.tpcalculator.helpers.billing.IabBroadcastReceiver;
import com.ct.tpcalculator.helpers.billing.IabHelper;
import com.ct.tpcalculator.helpers.billing.IabResult;
import com.ct.tpcalculator.helpers.billing.Inventory;
import com.ct.tpcalculator.helpers.billing.Purchase;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, OnFragmentChangedListener, IabBroadcastReceiver.IabBroadcastListener {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private ImageButton mNavigationButton;
    private final MainNavigationController mMainNavigationController = new MainNavigationController(this);

    private ImageButton mLineChart;
    private ImageButton mPieChart;
    private ImageButton mListView;

    private TextView mTitle;

    // Provides purchase notification while this app is running
    private IabBroadcastReceiver mBroadcastReceiver;
    // SKUs for our products: the premium upgrade
//    public static final String SKU_PROFESSIONAL = "pro";
//    public static final String SKU_STANDARD = "standard";
//    public static final String SKU_LITE = "lite";

    public static final String SKU_GENERAL_YEAR_SUBSCRIPTION = "general_year_subscription";

    public static final String ARG_CHART_FRAGMENT_TYPE = "ARG_CHART_FRAGMENT_TYPE";

    @Override
    protected void onStart() {
        super.onStart();
        if (Application.getInstance().getSelectedMobileOperator() == null || !Application.getInstance().isAccountSetup()) {
            Application.getInstance().addAccount();
            Intent setupActivityIntent = new Intent(this, SetupActivity.class);
            setupActivityIntent.putExtra(SetupActivity.SHOW_ABOUT_PAGE, true);
            startActivityForResult(setupActivityIntent, SetupActivity.ACTIVITY_SETUP);
        } else {
            CommandExecutionUtil.requestAllPermissions(this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initBilling();

        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        mTitle = (TextView) findViewById(R.id.title_text);
        mTitle.setTypeface(Application.lightTypeface);

        mNavigationButton = (ImageButton) findViewById(R.id.navigation);
        mNavigationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNavigationDrawerFragment.openCloseDrawer();
            }
        });
        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

        mLineChart = (ImageButton) findViewById(R.id.line_chart);
        mLineChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainFragment mainFragment = getMainFragment();
                if (mainFragment == null) {
                    return;
                }
                mainFragment.redrawChart(ChartBaseFragment.DATA_VIEW_TYPE_LINE_CHART);
                onChartChanged(mainFragment.getSelectedChartFragmentType(), ChartBaseFragment.DATA_VIEW_TYPE_LINE_CHART);
            }
        });

        mPieChart = (ImageButton) findViewById(R.id.pie_chart);
        mPieChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainFragment mainFragment = getMainFragment();
                if (mainFragment == null) {
                    return;
                }

                mainFragment.redrawChart(ChartBaseFragment.DATA_VIEW_TYPE_PIE_CHART);
                onChartChanged(mainFragment.getSelectedChartFragmentType(), ChartBaseFragment.DATA_VIEW_TYPE_PIE_CHART);
            }
        });

        mListView = (ImageButton) findViewById(R.id.list);
        mListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainFragment mainFragment = getMainFragment();
                if (mainFragment == null) {
                    return;
                }

                mainFragment.redrawChart(ChartBaseFragment.DATA_VIEW_TYPE_LIST);
                onChartChanged(mainFragment.getSelectedChartFragmentType(), ChartBaseFragment.DATA_VIEW_TYPE_LIST);
            }
        });


    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        if (getIntent() != null) {
            MainFragment mainFragment = getMainFragment();
            if (mainFragment != null) {
                int fragmentType = getIntent().getIntExtra(ARG_CHART_FRAGMENT_TYPE, -1);

                if (fragmentType != -1) {
                    mainFragment.setSelectedChartFragmentType(fragmentType);
                }
            }
        }
        return super.onCreateView(name, context, attrs);
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMainNavigationController);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mMainNavigationController, mMainNavigationController.getIntentFilter());
    }

    @Override
    public void onNavigationDrawerItemSelected(String action) {
//        Intent intent = new Intent(action);
//        if (action.equalsIgnoreCase(MainNavigationController.ACTION_SHOW_BUY_PRO)){
//            intent.putExtra(MainNavigationController.EXTRA_FRAGMENT, getMainFragment());
//        }
        mMainNavigationController.onReceive(this, new Intent(action));
    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen()) {
            mNavigationDrawerFragment.openCloseDrawer();
            return;
        }

        super.onBackPressed();

        if (getSupportFragmentManager().getBackStackEntryCount() == 0)
            finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SetupActivity.ACTIVITY_SETUP && resultCode != RESULT_OK) {
            finish();
        }if (requestCode == SetupActivity.ACTIVITY_SETTINGS && resultCode == RESULT_OK){
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(mBroadcastReceiver);

        IabHelper iabHelper = Application.getInstance().getIabHelper();
        if (iabHelper != null) iabHelper.dispose();
        Application.getInstance().setIabHelper(null);
    }

    private void initBilling() {

        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAh5wLgNfhUmSXkxNbhN768ml6NpIQ/PCoAHyEcac/JWG8G9CebMiPU79wboKEEcFAl4nOM0jidk26G6";
        base64EncodedPublicKey += getKeySecondPart();
        base64EncodedPublicKey += getKeyThirdPart();

        // compute your public key and store it in base64EncodedPublicKey
        Application.getInstance().setIabHelper(new IabHelper(this, base64EncodedPublicKey));

        final IabHelper iabHelper = Application.getInstance().getIabHelper();
        // TODO: enable debug logging (for a production application, you should set this to false).
        iabHelper.enableDebugLogging(true);
        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        iabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Toast.makeText(getApplication(), "Problem setting up in-app billing: " + result, Toast.LENGTH_LONG).show();
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (iabHelper == null) return;

                // Important: Dynamically register for broadcast messages about updated purchases.
                // We register the receiver here instead of as a <receiver> in the Manifest
                // because we always call getPurchases() at startup, so therefore we can ignore
                // any broadcasts sent while the app isn't running.
                // Note: registering this listener in an Activity is a bad idea, but is done here
                // because this is a SAMPLE. Regardless, the receiver must be registered after
                // IabHelper is setup, but before first call to getPurchases().
                mBroadcastReceiver = new IabBroadcastReceiver(MainActivity.this);
                IntentFilter broadcastFilter = new IntentFilter(IabBroadcastReceiver.ACTION);
                registerReceiver(mBroadcastReceiver, broadcastFilter);

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                queryInventory();
            }
        });
    }

    private String getKeyThirdPart() {
        return "Wt/+FHMB9UTAPejbti39BFQWUijTbcILHob9ODhV7nZbIC1Z29oTKAcj2lHN6QflNbN6Y6MsXPn9wJlFITX6S4WprQngWl1TwlhyKHaleE0gDfIru8pkLXe+OcOQTOHIf8V5J9CE5YFMGQIDAQAB";
    }

    private String getKeySecondPart() {
        return getString(R.string.key_second_part);
    }

    @Override
    public void receivedBroadcast() {
        // Received a broadcast notification that the inventory of items has changed
        queryInventory();
    }

    private void queryInventory() {
        List<String> additionalSkuList = new ArrayList<>();
//        additionalSkuList.add(SKU_PROFESSIONAL);
//        additionalSkuList.add(SKU_STANDARD);
//        additionalSkuList.add(SKU_LITE);
        additionalSkuList.add(SKU_GENERAL_YEAR_SUBSCRIPTION);

        Application.getInstance().getIabHelper().queryInventoryAsync(true, additionalSkuList, mGotInventoryListener);
    }

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            // Have we been disposed of in the meantime? If so, quit.
            if (Application.getInstance().getIabHelper() == null) return;

            // Is it a failure?
            if (result.isFailure()) {
//                complain("Failed to query inventory: " + result);
                return;
            }

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

            Application.getInstance().setSkuMap(inventory.getSkuMap());

            // Do we have the subscriptions?
//            Purchase proPurchase = inventory.getPurchase(SKU_PROFESSIONAL);
//            if (proPurchase != null && verifyDeveloperPayload(proPurchase)){
//                Application.getInstance().setIsPremiumAccount(true);
//                return;
//            }
//
//            Purchase standardPurchase = inventory.getPurchase(SKU_STANDARD);
//            if(standardPurchase != null && verifyDeveloperPayload(standardPurchase)){
//                Application.getInstance().setIsPremiumAccount(true);
//                return;
//            }
//
//            Purchase litePurchase = inventory.getPurchase(SKU_LITE);
//            if (litePurchase != null && verifyDeveloperPayload(litePurchase)){
//                Application.getInstance().setIsPremiumAccount(true);
//                return;
//            }

            Purchase generalOnYearSubscription = inventory.getPurchase(SKU_GENERAL_YEAR_SUBSCRIPTION);
            if (generalOnYearSubscription != null){
                Application.getInstance().setIsPremiumAccount(true);
                return;
            }

            Application.getInstance().setIsPremiumAccount(false);
        }
    };

    /**
     * Verifies the developer payload of a purchase.
     */
    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        /*
        * Пока у нас нет лишнего сервера куда бы мы записывали все id покупок(payload), поэтому пока обойдемся без этой проверки.
        * */
        return true;
    }


    private MainFragment getMainFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(MainFragment.TAG);
        if (fragment != null && fragment instanceof MainFragment)
            return (MainFragment) fragment;
        return null;
    }

    public void onChartChanged(int chartFragment, int chartType) {
        if (chartFragment == ChartBaseFragment.CHART_FRAGMENT_INTERNET) {
            mListView.setVisibility(View.GONE);
        } else {
            mListView.setVisibility(View.VISIBLE);
        }

        if (chartType == ChartBaseFragment.DATA_VIEW_TYPE_LINE_CHART) {
            mLineChart.setImageResource(R.drawable.ic_ab_graph_white);
            mPieChart.setImageResource(R.drawable.ic_ab_pie);
            mListView.setImageResource(R.drawable.ic_ab_list);
        } else if (chartType == ChartBaseFragment.DATA_VIEW_TYPE_PIE_CHART) {
            mLineChart.setImageResource(R.drawable.ic_ab_graph);
            mPieChart.setImageResource(R.drawable.ic_ab_pie_white);
            mListView.setImageResource(R.drawable.ic_ab_list);
        } else if (chartType == ChartBaseFragment.DATA_VIEW_TYPE_LIST) {
            mLineChart.setImageResource(R.drawable.ic_ab_graph);
            mPieChart.setImageResource(R.drawable.ic_ab_pie);
            mListView.setImageResource(R.drawable.ic_ab_list_white);
        }
    }

    @Override
    public void onFragmentChanged(String fragmentTag, String fragmentTitle) {
        if (fragmentTag.equalsIgnoreCase(MainFragment.TAG)) {
            mLineChart.setVisibility(View.VISIBLE);
            mPieChart.setVisibility(View.VISIBLE);
            mListView.setVisibility(View.VISIBLE);

            MainFragment mainFragment = getMainFragment();
            if (mainFragment != null) {
                int chartFragment = mainFragment.getSelectedChartFragmentType();
                int chartType = mainFragment.getSelectedChartType();
                onChartChanged(chartFragment, chartType);
            }
        } else {
            mLineChart.setVisibility(View.GONE);
            mPieChart.setVisibility(View.GONE);
            mListView.setVisibility(View.GONE);
        }

        mTitle.setText(fragmentTitle);
    }

}
