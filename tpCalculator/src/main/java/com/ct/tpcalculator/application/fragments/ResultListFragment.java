package com.ct.tpcalculator.application.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.activities.MainActivity;
import com.ct.tpcalculator.application.navigationControllers.ResultNavigationController;
import com.ct.tpcalculator.application.views.AsyncImageView;
import com.ct.tpcalculator.calculator.data.MobileOperator;
import com.ct.tpcalculator.calculator.data.calculationData.CalculationResult;
import com.ct.tpcalculator.calculator.data.calculationData.PackageToOffer;
import com.ct.tpcalculator.calculator.data.calculationData.PackagesToOffer;
import com.ct.tpcalculator.helpers.GoogleAnalyticsHelper;
import com.ct.tpcalculator.helpers.billing.SkuDetails;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Semyon on 11/19/2015.
 */
public class ResultListFragment extends BaseMainFragment {

    public static final String TAG = ResultListFragment.class.getName();
    private static final String GA_PAGE_NAME = "ResultListFragment";

    public static final String ARG_PACKAGES_TO_OFFER = "ARG_PACKAGES_TO_OFFER";

    private CalculationResult mCalculationResult;

//    private String mSku = "";

    public ResultListFragment() {

    }

    public static ResultListFragment newInstance(CalculationResult calculationResult) {
        ResultListFragment fragment = new ResultListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PACKAGES_TO_OFFER, calculationResult);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GoogleAnalyticsHelper.sendLog(GA_PAGE_NAME, null);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_result_list, container, false);

        Bundle extras = getArguments();
        if (extras != null) {
            mCalculationResult = (CalculationResult) extras.getSerializable(ARG_PACKAGES_TO_OFFER);
        }

        AsyncImageView imageView = (AsyncImageView) root.findViewById(R.id.mobile_operator_image);
        String iconPath = mCalculationResult.getMobileOperator().getIconPath();
        if (!TextUtils.isEmpty(iconPath)) {
            imageView.loadImage(iconPath);
        }

        TextView mobileOperator = (TextView) root.findViewById(R.id.mobile_operator_label);
        mobileOperator.setTypeface(Application.lightTypeface);
        mobileOperator.setText(mCalculationResult.getMobileOperator().getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));

        TextView tp = (TextView) root.findViewById(R.id.tp_label);
        tp.setTypeface(Application.lightTypeface);
        tp.setText(Html.fromHtml(String.format(getResources().getString(R.string.current_tp),
                mCalculationResult.getTariffPlan().getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()))));

        TextView currentCost = (TextView) root.findViewById(R.id.current_cost);
        currentCost.setTypeface(Application.lightTypeface);
        currentCost.setText(Html.fromHtml(String.format(getResources().getString(R.string.current_cost_text),
                mCalculationResult.getFormattedPrice(), Application.getInstance().getCountry().getCurrency(), mCalculationResult.getFormattedPriceInYear())));

//        mSku = "";
        if (mCalculationResult != null) {
            ArrayList<ListItemData> listData = new ArrayList<>();
            for (PackagesToOffer packagesToOffer : mCalculationResult.getPackagesToOffer()) {
                for (PackageToOffer packageToOffer : packagesToOffer.getPackagesToOffer()) {
                    listData.add(new ListItemData(packagesToOffer.getMobileOperator(), packageToOffer));
                }
            }

            Collections.sort(listData, new ListDataComparator());

            ListView listView = (ListView) root.findViewById(R.id.list);
            listView.setAdapter(new ResultListArrayAdapter(getActivity(), listData));

//            if (!listData.isEmpty()) {
//                if (listData.get(0).getPackageToOffer().getDiffOfCost() * 12f <= 30f) {
//                    mSku = MainActivity.SKU_LITE;
//                } else if (listData.get(0).getPackageToOffer().getDiffOfCost() * 12f <= 100f) {
//                    mSku = MainActivity.SKU_STANDARD;
//                } else {
//                    mSku = MainActivity.SKU_PROFESSIONAL;
//                }
//            }
        }

        View buyProContainer = root.findViewById(R.id.buyProContainer);
        buyProContainer.setVisibility(View.GONE);

//        if (!Application.getInstance().getIsPremiumAccount() && !TextUtils.isEmpty(mSku)) {
        if (!Application.getInstance().getIsPremiumAccount()) {
            showBuyProVersion();
        }

        return root;
    }

    private void showBuyProVersion() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent, getActivity().getTheme())));
        } else {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        }

        dialog.setContentView(R.layout.dialog_buy_pro);
        TextView titleTextView = (TextView) dialog.findViewById(R.id.title);
        titleTextView.setTypeface(Application.lightTypeface);

        TextView infoTextView = (TextView) dialog.findViewById(R.id.info);
        infoTextView.setTypeface(Application.lightTypeface);

//        SkuDetails liteSkuDetails = Application.getInstance().getSkuById(MainActivity.SKU_LITE);
//        SkuDetails standardDetails = Application.getInstance().getSkuById(MainActivity.SKU_STANDARD);
//        SkuDetails proDetails = Application.getInstance().getSkuById(MainActivity.SKU_PROFESSIONAL);

        SkuDetails generalOnYearSubscription = Application.getInstance().getSkuById(MainActivity.SKU_GENERAL_YEAR_SUBSCRIPTION);

        TextView priceTextView = (TextView) dialog.findViewById(R.id.price);
        priceTextView.setTypeface(Application.lightTypeface);


        Button buyProButton = (Button) dialog.findViewById(R.id.btnBuyPro);
        buyProButton.setTypeface(Application.lightTypeface);
        buyProButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mSku = "test";
//                Application.getInstance().buyProVersion(getActivity(), ResultListFragment.this, mSku);
                Application.getInstance().buyProVersion(getActivity(), ResultListFragment.this, MainActivity.SKU_GENERAL_YEAR_SUBSCRIPTION);
                dialog.dismiss();
            }
        });

//        if (liteSkuDetails != null && standardDetails != null && proDetails != null) {
        if (generalOnYearSubscription != null) {
//            priceTextView.setText(Html.fromHtml(String.format(getString(R.string.price_list), liteSkuDetails.getPrice(), standardDetails.getPrice(), proDetails.getPrice())));
            priceTextView.setText(Html.fromHtml(String.format(getString(R.string.price_list), generalOnYearSubscription.getPrice())));
        } else {
            priceTextView.setText(R.string.price_list_notavailable);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                priceTextView.setTextColor(getResources().getColor(R.color.red_color, getActivity().getTheme()));
            } else {
                priceTextView.setTextColor(getResources().getColor(R.color.red_color));
            }
            buyProButton.setVisibility(View.GONE);
        }

        ImageView cancelButton = (ImageView) dialog.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener == null) {
            return;
        }
        mListener.onFragmentChanged(TAG, getString(R.string.recommended_tp));
    }

    // Элементы списка
    public class ResultListArrayAdapter extends ArrayAdapter<ListItemData> {
        private final Context mContext;
        private final ArrayList<ListItemData> mListData;

        public ResultListArrayAdapter(Context context, ArrayList<ListItemData> listData) {
            super(context, R.layout.statistic_list_item);
            this.mContext = context;
            this.mListData = listData;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.result_list_item, parent, false);
            }

            final ListItemData listItemData = mListData.get(position);

            AsyncImageView imageView = (AsyncImageView) convertView.findViewById(R.id.mobile_operator_image);
            String iconPath = listItemData.getMobileOperator().getIconPath();
            if (Application.getInstance().getIsPremiumAccount() && !TextUtils.isEmpty(iconPath)) {
                imageView.loadImage(iconPath);
            } else {
                imageView.setImageResource(R.drawable.ic_launcher);
            }

            TextView operatorLabel = (TextView) convertView.findViewById(R.id.mobile_operator_label);
            operatorLabel.setTypeface(Application.lightTypeface);
            if (Application.getInstance().getIsPremiumAccount()) {
                operatorLabel.setText(listItemData.getMobileOperator().getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));
            } else {
                operatorLabel.setText(getString(R.string.mobile_operator1));
            }

            TextView tpLabel = (TextView) convertView.findViewById(R.id.tp_label);
            tpLabel.setTypeface(Application.lightTypeface);
            String tpName;
            if (Application.getInstance().getIsPremiumAccount()) {
                tpName = listItemData.getPackageToOffer().getTariffPlan().getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode());
            } else {
                tpName = getString(R.string.tariff_plan_without_doublepoint);
            }
            tpLabel.setText(tpName);

            TextView savingValue = (TextView) convertView.findViewById(R.id.saving_value);
            savingValue.setTypeface(Application.lightTypeface);
            savingValue.setText(Html.fromHtml(String.format(getResources().getString(R.string.save_value_text),
                    listItemData.getPackageToOffer().getFormattedDiffOfCost(), Application.getInstance().getCountry().getCurrency(),
                    listItemData.getPackageToOffer().getFormattedDiffOfCostInYear())));

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent showResultTp = new Intent(ResultNavigationController.ACTION_SHOW_RESULT_TP);
                    showResultTp.putExtra(ResultTpFragment.ARG_MOBILE_OPERATOR, listItemData.getMobileOperator());
                    showResultTp.putExtra(ResultTpFragment.ARG_PACKAGE_TO_OFFER, listItemData.getPackageToOffer());
//                    showResultTp.putExtra(ResultTpFragment.ARG_SELECTED_SKU, mSku);
                    showResultTp.putExtra(ResultTpFragment.ARG_SELECTED_SKU, MainActivity.SKU_GENERAL_YEAR_SUBSCRIPTION);
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(showResultTp);
                }
            });

            return convertView;
        }

        @Override
        public int getCount() {
            return mListData.size();
        }

    }

    public class ListItemData {

        public boolean isExpanded() {
            return mExpanded;
        }

        public void setExpanded(boolean expanded) {
            this.mExpanded = expanded;
        }

        private boolean mExpanded = false;

        public MobileOperator getMobileOperator() {
            return mMobileOperator;
        }

        public PackageToOffer getPackageToOffer() {
            return mPackageToOffer;
        }

        private final MobileOperator mMobileOperator;
        private final PackageToOffer mPackageToOffer;

        public ListItemData(MobileOperator mobileOperator, PackageToOffer packageToOffer) {
            mMobileOperator = mobileOperator;
            mPackageToOffer = packageToOffer;
        }
    }


    public class ListDataComparator implements Comparator<ListItemData> {
        public int compare(ListItemData object1, ListItemData object2) {
            return (object1.getPackageToOffer().getDiffOfCost() > object2.getPackageToOffer().getDiffOfCost() ?
                    -1 : (object1.getPackageToOffer().getDiffOfCost() == object2.getPackageToOffer().getDiffOfCost() ? 0 : 1));
        }
    }

}
