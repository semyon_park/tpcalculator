package com.ct.tpcalculator.application.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.ct.tpcalculator.helpers.GoogleAnalyticsHelper;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.*;
import com.github.mikephil.charting.utils.ValueFormatter;
import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.views.PieChart;
import com.ct.tpcalculator.application.views.SelectPeriod;
import com.ct.tpcalculator.calculator.data.DataDirectionTypes;
import com.ct.tpcalculator.calculator.data.Region;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticInternetTraffic;
import com.ct.tpcalculator.calculator.statisticAgent.netstat.NetStatCore;
import com.ct.tpcalculator.helpers.Utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ChartInternetFragment extends ChartBaseFragment {

    public static final String TAG = ChartInternetFragment.class.getName();

    private static final String GA_PAGE_NAME = "ChartInternetFragment";

    private LineChart mLineChart;

    private PieChart mPieChart;

    private PieChart mPieChartMobileData;

    private LinearLayout mLineChartArea;

    private View mPieChartArea;

    private ListView mLineChartLegendListView;

    private LinearLayout mLineChartSelectPeriod;

    private TextView mLineChartSelectPeriodLabel;

    private TextView mPieChartTotalWifiValue;

    private TextView mPieChartTotalMobileValue;

    private TextView mPieChartIncomingWifi;
    private TextView mPieChartOutgoingWifi;
    private TextView mPieChart3GIncoming;
    private TextView mPieChart3GOutgoing;

    private SelectPeriod mPieChartSelectPeriod;

    private int mDataViewType = DATA_VIEW_TYPE_LINE_CHART;

    private Date mStartDate;
    private Date mEndDate;

    public static final String ARG_START_DATE = "ARG_START_DATE";
    public static final String ARG_END_DATE = "ARG_END_DATE";
    public static final String ARG_DATA_VIEW_TYPE = "ARG_DATA_VIEW_TYPE";


    public ChartInternetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GoogleAnalyticsHelper.sendLog(GA_PAGE_NAME, null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (savedInstanceState == null) {
            if (mStartDate == null && mEndDate == null) {
                Calendar calendar = Calendar.getInstance();
                Date startDate = calendar.getTime();
                Pair<Date, Date> period = Utils.getMonthOfTheDate(startDate);

                mStartDate = period.first;
                mEndDate = period.second;
            }
        } else {
            mStartDate = new Date(savedInstanceState.getLong(ARG_START_DATE));
            mEndDate = new Date(savedInstanceState.getLong(ARG_END_DATE));
            mDataViewType = savedInstanceState.getInt(ARG_DATA_VIEW_TYPE);
        }

        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_chart_internet, container, false);
        // <editor-fold desc="Line Chart Area Elements">

        mLineChartSelectPeriodLabel = (TextView) root.findViewById(R.id.date_label);
        mLineChartSelectPeriodLabel.setTypeface(Application.condensedTypeface);

        mLineChartSelectPeriod = (LinearLayout) root.findViewById(R.id.line_chart_area_select_period);
        mLineChartSelectPeriod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity(), R.style.Theme_SelectDate);
                dialog.setContentView(R.layout.select_date_range);
                TextView selectPeriodLabel = (TextView) dialog.findViewById(R.id.select_period);
                selectPeriodLabel.setTypeface(Application.lightTypeface);

                final DatePicker startDatePicker = (DatePicker) dialog.findViewById(R.id.dpStartDate);
                final DatePicker endDatePicker = (DatePicker) dialog.findViewById(R.id.dpEndDate);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(mStartDate);
                startDatePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                calendar.setTime(mEndDate);
                endDatePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                Button ok_button = (Button) dialog.findViewById(R.id.ok_button);
                ok_button.setTypeface(Application.lightTypeface);
                ok_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(startDatePicker.getYear(), startDatePicker.getMonth(), startDatePicker.getDayOfMonth(), 0, 0, 0);
                        Date startDate = calendar.getTime();
                        calendar.set(endDatePicker.getYear(), endDatePicker.getMonth(), endDatePicker.getDayOfMonth(), 23, 59, 59);
                        Date endDate = calendar.getTime();

                        if (startDate.getTime() > endDate.getTime()) {
                            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setMessage(R.string.min_max_date_exception);
                            alertDialog.setTitle(R.string.exception);
                            alertDialog.show();
                            alertDialog.setNegativeButton(R.string.cancel, null);
                        } else {
                            datesPeriodChanged(startDate, endDate);
                        }
                        dialog.dismiss();
                    }
                });

                Button cancel_button = (Button) dialog.findViewById(R.id.cancel_button);
                cancel_button.setTypeface(Application.lightTypeface);
                cancel_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        mLineChartLegendListView = (ListView) root.findViewById(R.id.line_chart_area_legend);

        mLineChartArea = (LinearLayout) root.findViewById(R.id.line_chart_area);

        mLineChart = (LineChart) root.findViewById(R.id.line_chart);
        mLineChart.setDrawGridBackground(false);
        // no description text
        mLineChart.setDescription("");
        mLineChart.setNoDataText(getString(R.string.no_data));
        mLineChart.setNoDataTextDescription(getString(R.string.no_data_description));
        // enable value highlighting
        mLineChart.setHighlightEnabled(false);
        // enable touch gestures
        mLineChart.setTouchEnabled(true);
        // enable scaling and dragging
        mLineChart.setDragEnabled(true);
        mLineChart.setScaleEnabled(true);
        mLineChart.getAxisRight().setEnabled(false);
        mLineChart.getAxisLeft().setTypeface(Application.lightTypeface);
        mLineChart.getAxisLeft().enableGridDashedLine(1f, 1f, 0f);
        mLineChart.getXAxis().setTypeface(Application.lightTypeface);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mLineChart.getAxisLeft().setTextColor(getResources().getColor(R.color.text_color_white, getActivity().getTheme()));
            mLineChart.getXAxis().setTextColor(getResources().getColor(R.color.text_color_white, getActivity().getTheme()));
        } else {
            mLineChart.getAxisLeft().setTextColor(getResources().getColor(R.color.text_color_white));
            mLineChart.getXAxis().setTextColor(getResources().getColor(R.color.text_color_white));
        }

        mLineChart.setPinchZoom(true);
        mLineChart.getAxisLeft().setValueFormatter(new CustomValueFormatter());
        mLineChart.getLegend().setEnabled(false);

        // </editor-fold>

        // <editor-fold desc="Pie Chart Area Elements">

        mPieChartSelectPeriod = (SelectPeriod) root.findViewById(R.id.pie_chart_area_select_period);
        mPieChartSelectPeriod.setSelectPeriodListener(new SelectPeriod.SelectPeriodListener() {
            @Override
            public void datesChanged(Date startDate, Date endDate) {
                datesPeriodChanged(startDate, endDate);
            }
        });

        mPieChartTotalWifiValue = (TextView) root.findViewById(R.id.pie_chart_area_total_wifi_internet_value);
        mPieChartTotalWifiValue.setTypeface(Application.condensedTypeface);

        mPieChartTotalMobileValue = (TextView) root.findViewById(R.id.pie_chart_area_total_mobile_internet_value);
        mPieChartTotalMobileValue.setTypeface(Application.condensedTypeface);

        mPieChartIncomingWifi = (TextView) root.findViewById(R.id.pie_chart_area_wifi_incoming_value);
        mPieChartIncomingWifi.setTypeface(Application.regularTypeface);

        mPieChartOutgoingWifi = (TextView) root.findViewById(R.id.pie_chart_area_wifi_outgoing_value);
        mPieChartOutgoingWifi.setTypeface(Application.regularTypeface);

        mPieChart3GIncoming = (TextView) root.findViewById(R.id.pie_chart_area_mobile_incoming_value);
        mPieChart3GIncoming.setTypeface(Application.regularTypeface);

        mPieChart3GOutgoing = (TextView) root.findViewById(R.id.pie_chart_area_mobile_outgoing_value);
        mPieChart3GOutgoing.setTypeface(Application.regularTypeface);

        mPieChartArea = root.findViewById(R.id.pie_chart_area);

        mPieChart = (PieChart) root.findViewById(R.id.pie_chart);
        mPieChart.setDescription("");
        mPieChart.setNoDataText(getString(R.string.no_data));
        mPieChart.setNoDataTextDescription(getString(R.string.no_data_description));
        // enable value highlighting
        mPieChart.setHighlightEnabled(false);
        // enable touch gestures
        mPieChart.setTouchEnabled(true);
        mPieChart.setDragDecelerationFrictionCoef(0.95f);
        mPieChart.setDrawHoleEnabled(true);
        mPieChart.setHoleColorTransparent(true);
        mPieChart.setTransparentCircleColor(Color.WHITE);
        mPieChart.setTransparentCircleAlpha(110);
        mPieChart.setHoleRadius(83f);
        mPieChart.setTransparentCircleRadius(83f);

//        mPieChart.setDrawCenterText(true);
        mPieChart.setRotationAngle(-110);
        // enable rotation of the chart by touch
        mPieChart.setRotationEnabled(false);

        mPieChart.getLegend().setEnabled(false);

        mPieChartMobileData = (PieChart) root.findViewById(R.id.pie_chart_mobile_data);
        mPieChartMobileData.setDescription("");
        mPieChartMobileData.setNoDataTextDescription("");
        // enable value highlighting
        mPieChartMobileData.setHighlightEnabled(false);
        // enable touch gestures
        mPieChartMobileData.setTouchEnabled(true);
        mPieChartMobileData.setDragDecelerationFrictionCoef(0.95f);
        mPieChartMobileData.setDrawHoleEnabled(true);
        mPieChartMobileData.setHoleColorTransparent(true);
        mPieChartMobileData.setTransparentCircleColor(Color.WHITE);
        mPieChartMobileData.setTransparentCircleAlpha(110);
        mPieChartMobileData.setHoleRadius(83f);
        mPieChartMobileData.setTransparentCircleRadius(83f);
//        mPieChart.setDrawCenterText(true);
        mPieChartMobileData.setRotationAngle(-110);
        // enable rotation of the chart by touch
        mPieChartMobileData.setRotationEnabled(false);

        mPieChartMobileData.getLegend().setEnabled(false);

        // </editor-fold>

        switch (mDataViewType) {
            case DATA_VIEW_TYPE_LINE_CHART:
                lineChartClicked();
                break;
            case DATA_VIEW_TYPE_PIE_CHART:
                pieChartClicked();
                break;
        }

        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(ARG_START_DATE, mStartDate.getTime());
        outState.putLong(ARG_END_DATE, mEndDate.getTime());
        outState.putInt(ARG_DATA_VIEW_TYPE, mDataViewType);
    }

    @Override
    public void showChart(int chart) {
        mDataViewType = chart;
        switch (chart) {
            case DATA_VIEW_TYPE_LINE_CHART:
                lineChartClicked();
                break;
            case DATA_VIEW_TYPE_PIE_CHART:
                pieChartClicked();
                break;
        }
    }

    @Override
    public int getSelectedChartType() {
        return mDataViewType;
    }

    private void datesPeriodChanged(Date startDate, Date endDate) {
        mStartDate = startDate;
        mEndDate = endDate;
        redrawData();
    }

    private void lineChartClicked() {
        mDataViewType = DATA_VIEW_TYPE_LINE_CHART;
        mLineChartArea.setVisibility(View.VISIBLE);
        mPieChartArea.setVisibility(View.GONE);

        redrawData();
    }

    private void pieChartClicked() {
        mDataViewType = DATA_VIEW_TYPE_PIE_CHART;
        mLineChartArea.setVisibility(View.GONE);
        mPieChartArea.setVisibility(View.VISIBLE);

        redrawData();
    }

    // Перерисовать графики
    private void redrawData() {
        int viewType = mDataViewType;

        if (viewType == DATA_VIEW_TYPE_LINE_CHART) {
            setLineChartData(mStartDate, mEndDate);
        } else if (viewType == DATA_VIEW_TYPE_PIE_CHART) {
            setPieChart(mStartDate, mEndDate);
        }
    }

    // Строит линейный график, где фильтр равен = по операторам
    private void setLineChartData(Date startDate, Date endDate) {
        mLineChartSelectPeriodLabel.setText(String.format("%1$s - %2$s",
                Utils.getFormattedDate_DayMonth(startDate), Utils.getFormattedDate_DayMonth(endDate)));

        Region region = Application.getInstance().getRegion();
        if (region == null)
            return;

        ArrayList<ChartData> chartDataArrayList = new ArrayList<>();

        LineDataSet wifiIncomingDataSet = new LineDataSet(new ArrayList<Entry>(), getString(R.string.wifi_incoming));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            wifiIncomingDataSet.setColor(getResources().getColor(R.color.dark_green_color, getActivity().getTheme()));
            wifiIncomingDataSet.setFillColor(getResources().getColor(R.color.dark_green_color, getActivity().getTheme()));
        } else {
            wifiIncomingDataSet.setColor(getResources().getColor(R.color.dark_green_color));
            wifiIncomingDataSet.setFillColor(getResources().getColor(R.color.dark_green_color));
        }
        chartDataArrayList.add(new ChartData(wifiIncomingDataSet));

        LineDataSet wifiOutgoingDataSet = new LineDataSet(new ArrayList<Entry>(), getString(R.string.wifi_outgoing));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            wifiOutgoingDataSet.setColor(getResources().getColor(R.color.circle_light_green, getActivity().getTheme()));
            wifiOutgoingDataSet.setFillColor(getResources().getColor(R.color.circle_light_green, getActivity().getTheme()));
        } else {
            wifiOutgoingDataSet.setColor(getResources().getColor(R.color.circle_light_green));
            wifiOutgoingDataSet.setFillColor(getResources().getColor(R.color.circle_light_green));
        }
        chartDataArrayList.add(new ChartData(wifiOutgoingDataSet));

        LineDataSet mobileIncomingDataSet = new LineDataSet(new ArrayList<Entry>(), getString(R.string.mobile_incoming));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mobileIncomingDataSet.setColor(getResources().getColor(R.color.dark_yellow_color, getActivity().getTheme()));
            mobileIncomingDataSet.setFillColor(getResources().getColor(R.color.dark_yellow_color, getActivity().getTheme()));
        } else {
            mobileIncomingDataSet.setColor(getResources().getColor(R.color.dark_yellow_color));
            mobileIncomingDataSet.setFillColor(getResources().getColor(R.color.dark_yellow_color));
        }
        chartDataArrayList.add(new ChartData(mobileIncomingDataSet));

        LineDataSet mobileOutgoingDataSet = new LineDataSet(new ArrayList<Entry>(), getString(R.string.mobile_outgoing));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mobileOutgoingDataSet.setColor(getResources().getColor(R.color.yellow_color, getActivity().getTheme()));
            mobileOutgoingDataSet.setFillColor(getResources().getColor(R.color.yellow_color, getActivity().getTheme()));
        } else {
            mobileOutgoingDataSet.setColor(getResources().getColor(R.color.yellow_color));
            mobileOutgoingDataSet.setFillColor(getResources().getColor(R.color.yellow_color));
        }
        chartDataArrayList.add(new ChartData(mobileOutgoingDataSet));

        ArrayList<String> xVals = new ArrayList<>();

        fillChartWithEmptyData(startDate, endDate, xVals, chartDataArrayList);

        fillYValue(startDate, endDate, chartDataArrayList);

        // Меняем лейбл на датасете - добавляем общее кол-во истраченных МБ

        DecimalFormat decimalFormat = new DecimalFormat("#####.##");

        ArrayList<LineDataSet> lineDataSets = new ArrayList<>();
        for (ChartData chartData : chartDataArrayList) {
            if (chartData.getAccumulator() != 0) {
                chartData.getLineDataSet().setLabel(String.format("%1$s - %2$s %3$s",
                        chartData.getLineDataSet().getLabel(), decimalFormat.format(chartData.getAccumulator() / 1024f / 1024f), getString(R.string.mb)));
                lineDataSets.add(chartData.getLineDataSet());
            }
        }

        // Присваиваем датасетам свойства по умолчанию
        for (int i = 0; i < lineDataSets.size(); i++) {
            setDefaultDataSetProperties(lineDataSets.get(i));
        }

        LineData data = new LineData(xVals, lineDataSets);

        mLineChart.getAxisLeft().setAxisMinValue(0f);
        mLineChartLegendListView.setAdapter(new LegendArrayAdapter(getActivity(), lineDataSets.toArray(new DataSet[0])));

        // set data
        mLineChart.setData(data);

        mLineChart.animateX(1500, Easing.EasingOption.EaseInOutQuart);
    }

    // Строит график в виде пирога
    private void setPieChart(Date startDate, Date endDate) {

        mPieChartSelectPeriod.setStartDate(startDate);
        mPieChartSelectPeriod.setEndDate(endDate);

        long wifiIncomingInternet = 0;
        long wifiOutgoingInternet = 0;
        long mobileIncomingInternet = 0;
        long mobileOutgoingInternet = 0;

        ArrayList<StatisticInternetTraffic> internetTraffics = NetStatCore.GetInternetStatisticData(getActivity(), startDate, endDate, StatisticInternetTraffic.TYPE_WIFI_MOBILE);
        for (StatisticInternetTraffic internetTraffic : internetTraffics) {
            if (internetTraffic.getType() == StatisticInternetTraffic.TYPE_WIFI) {
                wifiIncomingInternet += internetTraffic.getReceivedBytes();
                wifiOutgoingInternet += internetTraffic.getTransmittedBytes();
            } else if (internetTraffic.getType() == StatisticInternetTraffic.TYPE_MOBILE) {
                mobileIncomingInternet += internetTraffic.getReceivedBytes();
                mobileOutgoingInternet += internetTraffic.getTransmittedBytes();
            }
        }

//        ArrayList<String> xWifiVals = new ArrayList<>();
//        xWifiVals.add(""); // wifIncomingInternet
//        xWifiVals.add(""); // wifiOutgoingInternet

        ArrayList<Entry> yWifiVals = new ArrayList<>();

        yWifiVals.add(new Entry(wifiIncomingInternet / 1024f / 1024f, 0));
        yWifiVals.add(new Entry(wifiOutgoingInternet / 1024f / 1024f, 1));

        PieDataSet wifiDataSet = new PieDataSet(yWifiVals, "Election Results");
        wifiDataSet.setSliceSpace(0f);
        wifiDataSet.setSelectionShift(0f);
        wifiDataSet.setDrawValues(false);

        // add colors
        ArrayList<Integer> colors = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            colors.add(getResources().getColor(R.color.dark_green_color, getActivity().getTheme()));
            colors.add(getResources().getColor(R.color.green_color, getActivity().getTheme()));
        } else {
            colors.add(getResources().getColor(R.color.dark_green_color));
            colors.add(getResources().getColor(R.color.green_color));
        }
        wifiDataSet.setColors(colors);

        ArrayList<String> xMobileVals = new ArrayList<>();
        xMobileVals.add(""); // wifIncomingInternet
        xMobileVals.add(""); // wifiOutgoingInternet

        ArrayList<Entry> yMobileVals = new ArrayList<>();

        yMobileVals.add(new Entry(mobileIncomingInternet / 1024f / 1024f, 0));
        yMobileVals.add(new Entry(mobileOutgoingInternet / 1024f / 1024f, 1));

        PieDataSet mobileDataSet = new PieDataSet(yMobileVals, "Election Results");
        mobileDataSet.setSliceSpace(0f);
        mobileDataSet.setSelectionShift(0f);
        mobileDataSet.setDrawValues(false);

        ArrayList<Integer> colors1 = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            colors1.add(getResources().getColor(R.color.dark_yellow_color, getActivity().getTheme()));
            colors1.add(getResources().getColor(R.color.yellow_color, getActivity().getTheme()));
        } else {
            colors1.add(getResources().getColor(R.color.dark_yellow_color));
            colors1.add(getResources().getColor(R.color.yellow_color));
        }
        mobileDataSet.setColors(colors1);

        PieData data = new PieData(xMobileVals, wifiDataSet);
//        data.addDataSet(mobileDataSet);
        data.setDrawValues(false);

        mPieChart.setData(data);
        mPieChart.invalidate();

        PieData mobileData = new PieData(xMobileVals, mobileDataSet);
        mobileData.setDrawValues(false);

        mPieChartMobileData.setData(mobileData);
        mPieChartMobileData.invalidate();

        DecimalFormat decimalFormat = new DecimalFormat("#####.##");

        mPieChartTotalWifiValue.setText(String.format("%1$s %2$s", decimalFormat.format(wifiIncomingInternet / 1024f / 1024f + wifiOutgoingInternet / 1024f / 1024f), getString(R.string.mb)));
        mPieChartTotalMobileValue.setText(String.format("%1$s %2$s", decimalFormat.format(mobileIncomingInternet / 1024f / 1024f + mobileOutgoingInternet / 1024f / 1024f), getString(R.string.mb)));

        mPieChartIncomingWifi.setText(String.format("%1$s %2$s", decimalFormat.format(wifiIncomingInternet / 1024f / 1024f), getString(R.string.mb)));
        mPieChartOutgoingWifi.setText(String.format("%1$s %2$s", decimalFormat.format(wifiOutgoingInternet / 1024f / 1024f), getString(R.string.mb)));

        mPieChart3GIncoming.setText(String.format("%1$s %2$s", decimalFormat.format(mobileIncomingInternet / 1024f / 1024f), getString(R.string.mb)));
        mPieChart3GOutgoing.setText(String.format("%1$s %2$s", decimalFormat.format(mobileOutgoingInternet / 1024f / 1024f), getString(R.string.mb)));
    }

    // Заполняем Y значениями
    private void fillYValue(Date startDate, Date endDate, ArrayList<ChartData> chartDataArrayList) {
        Date date = startDate;
        int index = 0;
        ArrayList<StatisticInternetTraffic> internetTraffic = NetStatCore.GetInternetStatisticData(getActivity(), startDate, endDate, StatisticInternetTraffic.TYPE_WIFI_MOBILE);
        // Теперь находим соответ dataset и присваиваем знаечение в зависимости от дня
        for (StatisticInternetTraffic internetTrafficItem : internetTraffic) {

            ChartData incomingChartData = getChartDataFilterNetworkTypeAndDirection(internetTrafficItem.getType(), DataDirectionTypes.INCOMING, chartDataArrayList);
            ChartData outgoingChartData = getChartDataFilterNetworkTypeAndDirection(internetTrafficItem.getType(), DataDirectionTypes.OUTGOING, chartDataArrayList);

            if (incomingChartData == null || outgoingChartData == null){
                return;
            }
            if (Utils.getIsCalendarDateAEqualsDateB(date, internetTrafficItem.getDate()) && date != startDate) {
                Entry incomingEntry = incomingChartData.getLineDataSet().getYVals().get(index - 1);
                float incomingCurrentValue = incomingEntry.getVal();
                incomingEntry.setVal(incomingCurrentValue + internetTrafficItem.getReceivedBytes());
                incomingChartData.setAccumulator(incomingChartData.getAccumulator() + internetTrafficItem.getReceivedBytes());
                incomingChartData.setMaxValue((long) (incomingCurrentValue + internetTrafficItem.getReceivedBytes()));

                Entry outgoingEntry = outgoingChartData.getLineDataSet().getYVals().get(index - 1);
                float outgoingCurrentValue = outgoingEntry.getVal();
                outgoingEntry.setVal(outgoingCurrentValue + internetTrafficItem.getTransmittedBytes());
                outgoingChartData.setAccumulator(outgoingChartData.getAccumulator() + internetTrafficItem.getTransmittedBytes());
                outgoingChartData.setMaxValue((long) (outgoingCurrentValue + internetTrafficItem.getTransmittedBytes()));
            } else {
                if (!Utils.getIsCalendarDateAEqualsDateB(date, internetTrafficItem.getDate())) {
                    // перескакиваем интернетом не пользовались
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                    date = calendar.getTime();
                    if (!Utils.getIsCalendarDateAEqualsDateB(date, internetTrafficItem.getDate())) {
                        index++;
                    }
                    while (!Utils.getIsCalendarDateAEqualsDateB(date, internetTrafficItem.getDate())) {
                        calendar.setTime(date);
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                        date = calendar.getTime();
                        index++;
                    }
                }
                Entry incomingEntry = incomingChartData.getLineDataSet().getYVals().get(index);
                incomingEntry.setVal(internetTrafficItem.getReceivedBytes());
                incomingChartData.setMaxValue((long) incomingEntry.getVal());
                incomingChartData.setAccumulator(incomingChartData.getAccumulator() + (int) incomingEntry.getVal());

                Entry outgoingEntry = outgoingChartData.getLineDataSet().getYVals().get(index);
                outgoingEntry.setVal(internetTrafficItem.getTransmittedBytes());
                outgoingChartData.setMaxValue((long) outgoingEntry.getVal());
                outgoingChartData.setAccumulator(outgoingChartData.getAccumulator() + (int) outgoingEntry.getVal());

                index++;

                date = internetTrafficItem.getDate();
            }
        }
    }

    private ChartData getChartDataFilterNetworkTypeAndDirection(int networkType, int incoming_outgoing, ArrayList<ChartData> chartDataArrayList) {
        if (networkType == StatisticInternetTraffic.TYPE_WIFI && incoming_outgoing == DataDirectionTypes.INCOMING) {
            return chartDataArrayList.get(0);
        } else if (networkType == StatisticInternetTraffic.TYPE_WIFI && incoming_outgoing == DataDirectionTypes.OUTGOING) {
            return chartDataArrayList.get(1);
        } else if (networkType == StatisticInternetTraffic.TYPE_MOBILE && incoming_outgoing == DataDirectionTypes.INCOMING) {
            return chartDataArrayList.get(2);
        } else if (networkType == StatisticInternetTraffic.TYPE_MOBILE && incoming_outgoing == DataDirectionTypes.OUTGOING) {
            return chartDataArrayList.get(3);
        }
        return null;
    }

    // Заполняет Chart data начальными значениями: x - по каждому дню, y - пустыми значениями
    private void fillChartWithEmptyData(Date startDate, Date endDate, ArrayList<String> xVals, ArrayList<ChartData> yVals) {
        int j = 0;
        Date dateA = startDate;
        // Вначале заполняем все датасеты и X пустыми значениями
        while (!Utils.getIsCalendarDateAEqualsDateB(dateA, endDate)) {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateA);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            if (day == 1) {
                xVals.add(j, String.format("%1$s/%2$s", calendar.get(Calendar.MONTH) + 1, Integer.toString(day)));
            } else {
                xVals.add(j, Integer.toString(day));
            }
            for (ChartData chartData : yVals) {
                chartData.getLineDataSet().addEntry(new Entry(0, j));
            }
            calendar.add(Calendar.DAY_OF_YEAR, 1);

            dateA = calendar.getTime();
            j++;
        }
        // устанавливаем частоту отображения значений на X оси
        if (j <= 31) {
            mLineChart.getXAxis().setLabelsToSkip(1);
        } else if (j <= 61) {
            mLineChart.getXAxis().setLabelsToSkip(2);
        } else {
            mLineChart.getXAxis().resetLabelsToSkip();
        }
        // Добавляем последний день
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endDate);
        xVals.add(j, Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)));

        // По Y пока присваиваем значения = 0
        for (ChartData chartData : yVals) {
            chartData.getLineDataSet().addEntry(new Entry(0, j));
        }
    }

    // Присваивает датасету свойства по умолчанию.
    private void setDefaultDataSetProperties(LineDataSet lineDataSet) {
        lineDataSet.setLineWidth(2f);
        lineDataSet.setDrawCircles(false);
        lineDataSet.setDrawFilled(true);
        lineDataSet.setDrawValues(false);
        lineDataSet.setFillAlpha(90);
    }

    // Для присвоения текста для лейблов на Y оси
    public class CustomValueFormatter implements ValueFormatter {

        private DecimalFormat mFormat;

        public CustomValueFormatter() {
            mFormat = new DecimalFormat("##");
        }

        @Override
        public String getFormattedValue(float value) {
//            int callDirectionType = mLineChartCallDirectionSpinner.getSelectedItemPosition();
//            if (callDirectionType == INCOMING_OUTGOING_MINUTES || callDirectionType == INCOMING_MINUTES || callDirectionType == OUTGOING_MINUTES) {
//                if (Math.floor(value / 1000) == value / 1000) {
//                    return String.format("%1$s %2$s", mFormat.format(value / 1000), getString(R.string.minutes));     // Делим на 1000 так как мы до этого значение умножали на 1000 чтобы градуировка на Y оси была из целых чисел
//                } else {
//                    return "";
//                }
//            } else {
//                if (Math.floor(value / 1000) == value / 1000) {
//                    return mFormat.format(value / 1000);
//                } else {
//                    return "";
//                }
//            }
            DecimalFormat decimalFormat = new DecimalFormat("#####.##");
            return decimalFormat.format(value / 1024f / 1024f);

//            return Float.toString(value);
        }
    }

    // Для списка легенд
    public class LegendArrayAdapter extends ArrayAdapter<String> {
        private final Context mContext;
        private final DataSet[] mDataSets;

        public LegendArrayAdapter(Context context, DataSet[] dataSets) {
            super(context, R.layout.spinner_item);
            this.mContext = context;
            this.mDataSets = dataSets;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.legend_list_item, parent, false);
            }
            final DataSet dataSet = mDataSets[position];

            final CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);
            checkBox.setTypeface(Application.lightTypeface);
            checkBox.setChecked(mLineChart.getData().contains((LineDataSet) dataSet));
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLineChart.getAxisLeft().resetAxisMaxValue();
                    if (checkBox.isChecked()) {
                        mLineChart.getData().addDataSet((LineDataSet) dataSet);
                    } else {
                        mLineChart.getData().removeDataSet((LineDataSet) dataSet);
                    }
                    mLineChart.notifyDataSetChanged();
                    mLineChart.invalidate();
                }
            });

            TextView phoneNumber = (TextView) convertView.findViewById(R.id.phoneNumber);
            phoneNumber.setText(dataSet.getLabel());
            phoneNumber.setTypeface(Application.lightTypeface);
            phoneNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkBox.setChecked(!checkBox.isChecked());
                    if (checkBox.isChecked()) {
                        mLineChart.getData().addDataSet((LineDataSet) dataSet);
                    } else {
                        mLineChart.getData().removeDataSet((LineDataSet) dataSet);
                    }
                    mLineChart.notifyDataSetChanged();
                    mLineChart.invalidate();
                }
            });

            FrameLayout colorBox = (FrameLayout) convertView.findViewById(R.id.color_box);
            if(Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                colorBox.setBackgroundDrawable(Utils.getColoredCircle(dataSet.getColor()));
            } else {
                colorBox.setBackground(Utils.getColoredCircle(dataSet.getColor()));
            }

            if (position % 2 == 0) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    convertView.setBackgroundColor(getResources().getColor(R.color.dark_bg, getActivity().getTheme()));
                } else {
                    convertView.setBackgroundColor(getResources().getColor(R.color.dark_bg));
                }
                if (position == mDataSets.length - 1) {
                    phoneNumber.setBackgroundResource(R.drawable.dark_list_item_top_bottom_border);
                } else {
                    phoneNumber.setBackgroundResource(R.drawable.dark_list_item_top_border);
                }

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    convertView.setBackgroundColor(getResources().getColor(R.color.light_bg, getActivity().getTheme()));
                } else {
                    convertView.setBackgroundColor(getResources().getColor(R.color.light_bg));
                }
                if (position == mDataSets.length - 1) {
                    phoneNumber.setBackgroundResource(R.drawable.light_list_item_top_bottom_border);
                } else {
                    phoneNumber.setBackgroundResource(R.drawable.light_list_item_top_border);
                }
            }


            convertView.setTag(dataSet);
            return convertView;
        }

        @Override
        public int getCount() {
            return mDataSets.length;
        }

    }

    // Здесь хранятся датасет для чарта и высчитывается общая сумма значения Y
    public class ChartData {

        long mMaxValue;

        public long getMaxValue() {
            return mMaxValue;
        }

        public void setMaxValue(long maxValue) {
            if (mMaxValue < maxValue) {
                this.mMaxValue = maxValue;
            }
        }

        private LineDataSet mLineDataSet;

        public LineDataSet getLineDataSet() {
            return mLineDataSet;
        }

        public long getAccumulator() {
            return mAccumulator;
        }

        public void setAccumulator(long accumulator) {
            this.mAccumulator = accumulator;
        }

        private long mAccumulator;

        public ChartData(LineDataSet lineDataSet) {
            mLineDataSet = lineDataSet;
        }
    }

}
