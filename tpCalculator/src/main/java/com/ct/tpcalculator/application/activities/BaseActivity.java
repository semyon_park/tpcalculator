package com.ct.tpcalculator.application.activities;

import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.calculator.statisticAgent.callstat.CallStatCore;
import com.ct.tpcalculator.calculator.statisticAgent.msgstat.MsgCore;
import com.ct.tpcalculator.calculator.statisticAgent.netstat.NetStatCore;
import com.ct.tpcalculator.helpers.CommandExecutionUtil;

/**
 * Created by marat on 11/27/2015.
 */
public class BaseActivity extends AppCompatActivity {

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        for (int grantResult:grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        youMustAcceptAllPermissions();
                    }
                });
                return;
            }
        }
        CallStatCore.check(this);
        MsgCore.check(this);
        NetStatCore.check(this);
    }

    private void youMustAcceptAllPermissions(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        dialog.setContentView(R.layout.dialog_question_yes_no);

        TextView titleTextView = (TextView) dialog.findViewById(R.id.title);
        titleTextView.setTypeface(Application.lightTypeface);
        titleTextView.setText(getString(R.string.app_name));

        TextView infoTextView = (TextView) dialog.findViewById(R.id.info);
        infoTextView.setTypeface(Application.lightTypeface);
        infoTextView.setText(getString(R.string.applyPermissions));

        Button yesButton = (Button) dialog.findViewById(R.id.btnYes);
        yesButton.setTypeface(Application.lightTypeface);
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                CommandExecutionUtil. requestAllPermissions(BaseActivity.this);
            }
        });

        Button noButton = (Button) dialog.findViewById(R.id.btnNo);
        noButton.setTypeface(Application.lightTypeface);
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
