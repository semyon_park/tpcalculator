package com.ct.tpcalculator.application.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.util.Pair;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.navigationControllers.ResultNavigationController;
import com.ct.tpcalculator.application.views.ProgressIndicatorControl;
import com.ct.tpcalculator.application.views.SelectTpControl;
import com.ct.tpcalculator.calculator.ExpenseController;
import com.ct.tpcalculator.calculator.data.Country;
import com.ct.tpcalculator.calculator.data.MobileOperator;
import com.ct.tpcalculator.calculator.data.Region;
import com.ct.tpcalculator.calculator.data.Service;
import com.ct.tpcalculator.calculator.data.TariffPlan;
import com.ct.tpcalculator.calculator.data.calculationData.CalculationResult;
import com.ct.tpcalculator.calculator.data.calculationData.PackageToOffer;
import com.ct.tpcalculator.calculator.data.calculationData.PackagesToOffer;
import com.ct.tpcalculator.calculator.data.calculationData.StatisticLogsForCalculation;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticInternetTraffics;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticLogs;
import com.ct.tpcalculator.helpers.GoogleAnalyticsHelper;
import com.ct.tpcalculator.helpers.StatisticData;
import com.ct.tpcalculator.helpers.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ResultSettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ResultSettingsFragment extends BaseMainFragment {


    public static final String TAG = ResultSettingsFragment.class.getName();
    private static final String GA_PAGE_NAME = "ResultSettingsFragment";

    private static final String STATE_SELECTED_MOBILE_OPERATOR = "STATE_SELECTED_MOBILE_OPERATOR";
    private static final String STATE_SELECTED_TP = "STATE_SELECTED_TP";
    private static final String STATE_SELECTED_SERVICES = "STATE_SELECTED_SERVICES";
    private static final String STATE_INTERNET_TRAFFIC = "STATE_INTERNET_TRAFFIC";

    private SelectTpControl mSelectTpControl;
    private ViewGroup mData;
    private ProgressIndicatorControl mProgressControl;
    private TextView mInternetTrafficValue;

    private String mSaveInstanceStateOperatorId;
    private String mSaveInstanceStateTpId;
    private ArrayList<String> mSaveInstanceStateServices;
    private int mSaveInstanceStateInternetTraffic;
    private int mOriginalInternetTrafficData;

    public static ResultSettingsFragment newInstance() {
        return new ResultSettingsFragment();
    }

    public ResultSettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retain this fragment across configuration changes.
        setRetainInstance(true);
        GoogleAnalyticsHelper.sendLog(GA_PAGE_NAME, null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_result_settings, container, false);

        restoreData(savedInstanceState);

        mData = (ViewGroup) root.findViewById(R.id.data);
        mProgressControl = (ProgressIndicatorControl) root.findViewById(R.id.progress);
        mProgressControl.setProgressText(getString(R.string.tp_is_calculating));
        mSelectTpControl = (SelectTpControl) root.findViewById(R.id.content);

        TextView resultPeriodLabel = (TextView) root.findViewById(R.id.result_period_label);
        resultPeriodLabel.setTypeface(Application.lightTypeface);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
        Date date = c.getTime();
        Pair<Date, Date> startEndDate = Utils.getMonthOfTheDate(date);
        resultPeriodLabel.setText(String.format(getString(R.string.resultPeriodDescription), Utils.getFormattedDate_DayMonthYear(startEndDate.first), Utils.getFormattedDate_DayMonthYear(startEndDate.second)));

        LinearLayout internetTrafficContent = (LinearLayout) root.findViewById(R.id.internet_traffic_content);
        internetTrafficContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setInternetTrafficValue();
            }
        });
        TextView internetTrafficLabel = (TextView) root.findViewById(R.id.internet_traffic_label);
        internetTrafficLabel.setTypeface(Application.lightTypeface);

        mInternetTrafficValue = (TextView) root.findViewById(R.id.internet_traffic_value);
        mInternetTrafficValue.setTypeface(Application.lightTypeface);
        mInternetTrafficValue.setText(Integer.toString(mSaveInstanceStateInternetTraffic));

        mSelectTpControl.setSelectedMobileOperatorId(mSaveInstanceStateOperatorId);
        mSelectTpControl.setSelectedTpId(mSaveInstanceStateTpId);
        mSelectTpControl.setSelectedServices(mSaveInstanceStateServices);
        mSelectTpControl.mobileOperatorChanged(true);
        mSelectTpControl.fillSelectedServicesListView();

        ImageView internetTrafficInfo = (ImageView) root.findViewById(R.id.internet_traffic_info);
        internetTrafficInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Application.getInstance().showInfoDialog(getContext(), getContext().getString(R.string.internet_traffic_description));
            }
        });

        Button btnNext = (Button) root.findViewById(R.id.btnNext);
        btnNext.setTypeface(Application.lightTypeface);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSaveInstanceStateOperatorId = mSelectTpControl.getSelectedMobileOperatorId();
                mSaveInstanceStateTpId = mSelectTpControl.getSelectedTpId();
                mSaveInstanceStateServices = mSelectTpControl.getSelectedServices();
                if (TextUtils.isEmpty(mInternetTrafficValue.getText())) {
                    mSaveInstanceStateInternetTraffic = 0;
                } else {
                    mSaveInstanceStateInternetTraffic = Integer.parseInt(mInternetTrafficValue.getText().toString());
                }

                calculateTp();
            }
        });

        return root;
    }

    private void calculateTp() {
        new AsyncTask<Void, Void, CalculationResult>() {

            @Override
            protected void onPreExecute() {
                showProgress();
            }

            @Override
            protected CalculationResult doInBackground(Void... params) {

                Region region = Application.getInstance().getRegion();
                if (region == null) {
                    return null;
                }

                MobileOperator selectedMobileOperator = region.findMobileOperator(mSaveInstanceStateOperatorId);
                if (selectedMobileOperator == null) {
                    return null;
                }

                TariffPlan selectedTariffPlan = selectedMobileOperator.findTariffPlan(mSaveInstanceStateTpId);
                if (selectedTariffPlan == null) {
                    return null;
                }

                Calendar c = Calendar.getInstance();
                c.add(Calendar.MONTH, -1);
                Date date = c.getTime();
                Pair<Date, Date> startEndDate = Utils.getMonthOfTheDate(date);

//                final ArrayList<Service> availableServices = selectedMobileOperator.getAvailableServices(selectedTariffPlan.getAvailableServices());

//                ArrayList<Pair<Service, Date>> selectedServices = new ArrayList<>();
//                for (String serviceId : mSaveInstanceStateServices) {
//                    Service service = Utils.findServiceById(availableServices, serviceId);
//                    if (service != null) {
//                        selectedServices.add(new Pair<>(service, startEndDate.first));
//                    }
//                }

                Country country = Application.getInstance().getCountry();
                ArrayList<String> countryPrefixes = new ArrayList<>(Arrays.asList(country.getNumberPrefix().split(";")));

                ArrayList<String> regionPrefixes = new ArrayList<>(Arrays.asList(region.getNumberPrefix().split(";")));

                StatisticLogs statisticLogs = StatisticData.getInstance().getStatisticData(countryPrefixes, regionPrefixes, region.getMinDigitsCount(),
                        region.getMaxDigitsCount(), Utils.getStartOfDay(startEndDate.first),
                        Utils.getEndOfDay(startEndDate.second), getActivity());

                statisticLogs.getInternetTraffics().setMobileInternetTrafficInBytes(startEndDate.first, (long) mSaveInstanceStateInternetTraffic * 1024L * 1024L);

                PackageToOffer packageToOffer = new PackageToOffer(0);
                packageToOffer.setTariffPlan(selectedTariffPlan);

                ArrayList<Service> availableServices = selectedMobileOperator.getAvailableServices(selectedTariffPlan.getAvailableServices());
//                ArrayList<Service> enabledServices = selectedMobileOperator.getAvailableServices(selectedTariffPlan.getMustHaveServices());

                for (String serviceId : mSaveInstanceStateServices) {
                    Service service = Utils.findServiceById(availableServices, serviceId);
                    if (service != null) {
                        packageToOffer.addService(service, Utils.getStartOfDay(startEndDate.first), Utils.getEndOfDay(startEndDate.second));
                    }
                }

//                for (Service service : enabledServices) {
//                    packageToOffer.addEnabledService(service, Utils.getStartOfDay(startEndDate.first),
//                            Utils.getEndOfDay(startEndDate.second));
//                }
                double price = ExpenseController.getInstance().calculateExpense(Utils.getStartOfDay(startEndDate.first),
                        Utils.getEndOfDay(startEndDate.second), new StatisticLogsForCalculation(statisticLogs), selectedTariffPlan, packageToOffer.getServices(), packageToOffer.getEnabledServices());

//                double price = ExpenseController.getInstance().calculateExpense(Utils.getStartOfDay(startEndDate.first),
//                        Utils.getEndOfDay(startEndDate.second), selectedTariffPlan, selectedServices, statisticLogs);

                ArrayList<PackagesToOffer> result = ExpenseController.getInstance().calculatePackagesToOffer(Utils.getStartOfDay(startEndDate.first),
                        Utils.getEndOfDay(startEndDate.second), price, statisticLogs);

                return new CalculationResult(result, selectedMobileOperator, selectedTariffPlan, price);
            }

            @Override
            protected void onPostExecute(CalculationResult calculationResult) {
                hideProgress();
                if (calculationResult != null) {
                    Intent showOfferIntent = new Intent(ResultNavigationController.ACTION_SHOW_RESULT_LIST);
                    showOfferIntent.putExtra(ResultListFragment.ARG_PACKAGES_TO_OFFER, calculationResult);
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(showOfferIntent);
                }
            }

        }.execute();
    }

    private void setInternetTrafficValue() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent, getActivity().getTheme())));
        } else {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        }
        dialog.setContentView(R.layout.dialog_select_internet_traffic);

        TextView infoTextView = (TextView)dialog.findViewById(R.id.info);
        infoTextView.setTypeface(Application.lightTypeface);

        TextView titleTextView = (TextView) dialog.findViewById(R.id.title);
        titleTextView.setTypeface(Application.lightTypeface);

        final EditText trafficInput = (EditText) dialog.findViewById(R.id.internet_traffic_value);
        trafficInput.setTypeface(Application.lightTypeface);
        trafficInput.setText(mInternetTrafficValue.getText().toString().equalsIgnoreCase("0") ? "" : mInternetTrafficValue.getText());
        trafficInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
            }
        });
        trafficInput.requestFocusFromTouch();

        Button okButton = (Button) dialog.findViewById(R.id.btnOk);
        okButton.setTypeface(Application.lightTypeface);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInternetTrafficValue.setText(trafficInput.getText());
                dialog.dismiss();
            }
        });

        Button cancelButton = (Button) dialog.findViewById(R.id.btnCancel);
        cancelButton.setTypeface(Application.lightTypeface);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    protected void showProgress() {
        if (mData != null) {
            mData.setVisibility(View.GONE);
        }
        if (mProgressControl != null) {
            mProgressControl.showProgressIndicator();
        }
    }

    protected void hideProgress() {
        mData.setVisibility(View.VISIBLE);
        mProgressControl.hideProgressIndicator();
    }

    private void restoreData(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mSaveInstanceStateOperatorId = savedInstanceState.getString(STATE_SELECTED_MOBILE_OPERATOR);
            mSaveInstanceStateTpId = savedInstanceState.getString(STATE_SELECTED_TP);
            mSaveInstanceStateServices = (ArrayList<String>) savedInstanceState.getSerializable(STATE_SELECTED_SERVICES);
            mSaveInstanceStateInternetTraffic = savedInstanceState.getInt(STATE_INTERNET_TRAFFIC);
        }
        if (TextUtils.isEmpty(mSaveInstanceStateOperatorId)) {
            mSaveInstanceStateOperatorId = Application.getInstance().getSelectedMobileOperator().getId();
            mSaveInstanceStateTpId = Application.getInstance().getSelectedTariffPlan().getId();
            mSaveInstanceStateServices = new ArrayList<>();
            for (Service service : Application.getInstance().getSelectedServices()) {
                mSaveInstanceStateServices.add(service.getId());
            }
            Calendar c = Calendar.getInstance();

            c.add(Calendar.MONTH, -1);
            Date date = c.getTime();

            Pair<Date, Date> startEndDate = Utils.getMonthOfTheDate(date);

            StatisticInternetTraffics internetTraffics = StatisticData.getInstance().getStatisticInternetTraffic(getActivity(), startEndDate.first, startEndDate.second);

            mSaveInstanceStateInternetTraffic = Utils.getMegaBytes(internetTraffics.getMobileInternetTrafficValueInBytes());
            mOriginalInternetTrafficData = mSaveInstanceStateInternetTraffic;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener == null) {
            return;
        }
        mListener.onFragmentChanged(TAG, getString(R.string.calculate_tp_settings));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mSelectTpControl != null) {
            mSaveInstanceStateOperatorId = mSelectTpControl.getSelectedMobileOperatorId();
            mSaveInstanceStateTpId = mSelectTpControl.getSelectedTpId();
            mSaveInstanceStateServices = mSelectTpControl.getSelectedServices();
            int internetTrafffic = 0;
            if (!TextUtils.isEmpty(mInternetTrafficValue.getText())) {
                try {
                    internetTrafffic = Integer.parseInt(mInternetTrafficValue.getText().toString());
                } catch (NumberFormatException ex) {
                }
            }
            mSaveInstanceStateInternetTraffic = internetTrafffic;
        }
        outState.putString(STATE_SELECTED_MOBILE_OPERATOR, mSaveInstanceStateOperatorId);
        outState.putString(STATE_SELECTED_TP, mSaveInstanceStateTpId);
        outState.putSerializable(STATE_SELECTED_SERVICES, mSaveInstanceStateServices);
        outState.putInt(STATE_INTERNET_TRAFFIC, mSaveInstanceStateInternetTraffic);
    }


}
