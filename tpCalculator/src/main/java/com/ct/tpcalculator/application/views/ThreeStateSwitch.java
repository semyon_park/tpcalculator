package com.ct.tpcalculator.application.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.widget.CompoundButton;
import com.ct.tpcalculator.R;

public class ThreeStateSwitch extends CompoundButton {
    private final static String TAG = ThreeStateSwitch.class.getName();

    public static final int STATE_FIRST = 0;
    public static final int STATE_SECOND = 1;
    public static final int STATE_THIRD = 2;

    private static final int TOUCH_MODE_IDLE = 0;
    private static final int TOUCH_MODE_DOWN = 1;
    private static final int TOUCH_MODE_DRAGGING = 2;
    private static final int TOUCH_MODE_ANIMATE = 3;

    private Drawable mThumbFirstStateDrawable;
    private Drawable mThumbSecondStateDrawable;
    private Drawable mThumbThirdStateDrawable;
    private Drawable mThumbDrawable1;
    private Drawable mThumbDrawable2;
    private Drawable mThumbDrawable3;
    private Drawable mTrackDrawable;
    private Drawable mBackground;

    private int mSwitchMinWidth;
    private int mSwitchPadding;

    private int mTouchMode;
    private float mTouchX;
    private float mTouchY;
    private VelocityTracker mVelocityTracker = VelocityTracker.obtain();

    private OnStateChangeListener mStateChangeListener;

    public void setOnStateChangeListener(OnStateChangeListener stateChangeListener) {
        mStateChangeListener = stateChangeListener;
    }

//    private int mMinFlingVelocity;
//    private int mTouchSlop;

    private int mThumbWidth;
    private int mInternalTrackWidth;
    private int mSwitchWidth;
    private int mSwitchHeight;
    private float mThumbPosition;
    private float mFirstStateThumbPosition;
    private float mSecondStateThumbPosition;
    private float mThirdStateThumbPosition;
    private float mTargetPosition;
    private int mTargetState;

    private int mSwitchLeft;
    private int mSwitchTop;
    private int mSwitchRight;
    private int mSwitchBottom;

    @SuppressWarnings("hiding")
    private final Rect mTempRect = new Rect();

    private static final int[] CHECKED_STATE_SET = {android.R.attr.state_checked};

    public ThreeStateSwitch(Context context) {
        this(context, null);
    }

    public ThreeStateSwitch(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.switchStyle);
    }

    public ThreeStateSwitch(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.threeStateSwitch, defStyle, 0);

        mThumbDrawable1 = a.getDrawable(R.styleable.threeStateSwitch_tsThumb);
        mThumbDrawable2 = a.getDrawable(R.styleable.threeStateSwitch_tsThumb);
        mThumbDrawable3 = a.getDrawable(R.styleable.threeStateSwitch_tsThumb);
        mTrackDrawable = a.getDrawable(R.styleable.threeStateSwitch_tsTrack);
        mBackground = a.getDrawable(R.styleable.threeStateSwitch_tsBackground);

        mSwitchMinWidth = a.getDimensionPixelSize(R.styleable.threeStateSwitch_tsSwitchMinWidth, 0);
        mSwitchPadding = a.getDimensionPixelSize(R.styleable.threeStateSwitch_tsSwitchPadding, 0);

        a.recycle();

        ViewConfiguration config = ViewConfiguration.get(context);
//        mTouchSlop = config.getScaledTouchSlop();
//        mMinFlingVelocity = config.getScaledMinimumFlingVelocity();

        // Refresh display with current params
        refreshDrawableState();
        setState(STATE_FIRST);
    }

    public void setSwitchPadding(int pixels) {
        mSwitchPadding = pixels;
        requestLayout();
    }

    public int getSwitchPadding() {
        return mSwitchPadding;
    }

    public void setSwitchMinWidth(int pixels) {
        mSwitchMinWidth = pixels;
        requestLayout();
    }

    public int getSwitchMinWidth() {
        return mSwitchMinWidth;
    }

    public void setTrackDrawable(Drawable track) {
        mTrackDrawable = track;
        requestLayout();
    }

    public void setTrackResource(int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setTrackDrawable(getContext().getResources().getDrawable(resId, getContext().getTheme()));
        } else {
            //noinspection deprecation
            setTrackDrawable(getContext().getResources().getDrawable(resId));
        }
    }

    public Drawable getTrackDrawable() {
        return mTrackDrawable;
    }

    public void setThumbDrawable1(Drawable thumb) {
        mThumbDrawable1 = thumb;
        requestLayout();
    }

    public void setThumbResource1(int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setThumbDrawable1(getContext().getResources().getDrawable(resId, getContext().getTheme()));
        } else {
            //noinspection deprecation
            setThumbDrawable1(getContext().getResources().getDrawable(resId));
        }
    }

    public Drawable getThumbDrawable1() {
        return mThumbDrawable1;
    }


    public void setThumbDrawable2(Drawable thumb) {
        mThumbDrawable2 = thumb;
        requestLayout();
    }

    public void setThumbResource2(int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setThumbDrawable2(getContext().getResources().getDrawable(resId, getContext().getTheme()));
        } else {
            //noinspection deprecation
            setThumbDrawable2(getContext().getResources().getDrawable(resId));
        }
    }

    public Drawable getThumbDrawable2() {
        return mThumbDrawable2;
    }

    public void setThumbDrawable3(Drawable thumb) {
        mThumbDrawable3 = thumb;
        requestLayout();
    }

    public void setThumbResource3(int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setThumbDrawable3(getContext().getResources().getDrawable(resId, getContext().getTheme()));
        } else {
            //noinspection deprecation
            setThumbDrawable3(getContext().getResources().getDrawable(resId));
        }
    }

    public Drawable getThumbDrawable3() {
        return mThumbDrawable3;
    }

    public void setThreeStateSwitchBackgroundDrawable(Drawable background) {
        mBackground = background;
        requestLayout();
    }

    public void setThreeStateSwitchBackgroundResource(int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setThreeStateSwitchBackgroundDrawable(getContext().getResources().getDrawable(resId, getContext().getTheme()));
        } else {
            //noinspection deprecation
            setThreeStateSwitchBackgroundDrawable(getContext().getResources().getDrawable(resId));
        }
    }

    public Drawable getThreeStateSwitchBackgroundDrawable() {
        return mBackground;
    }

    public Drawable getmThumbFirstStateDrawable() {
        return mThumbFirstStateDrawable;
    }

    public void setmThumbFirstStateDrawable(Drawable thumbFirstStateDrawable) {
        this.mThumbFirstStateDrawable = thumbFirstStateDrawable;
    }

    public Drawable getmThumbSecondStateDrawable() {
        return mThumbSecondStateDrawable;
    }

    public void setmThumbSecondStateDrawable(Drawable thumbSecondStateDrawable) {
        this.mThumbSecondStateDrawable = thumbSecondStateDrawable;
    }

    public Drawable getmThumbThirdStateDrawable() {
        return mThumbThirdStateDrawable;
    }

    public void setmThumbThirdStateDrawable(Drawable thumbThirdStateDrawable) {
        this.mThumbThirdStateDrawable = thumbThirdStateDrawable;
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        mThumbWidth = 0;
        int thumbHeight = 0;
        int maxThumbWidth = 0;

        if (mTargetState == 0) {
            if (mThumbDrawable1 != null) {
                mThumbDrawable1.getPadding(mTempRect);
                mThumbWidth = mThumbDrawable1.getIntrinsicWidth() - mTempRect.left - mTempRect.right;

                maxThumbWidth = Math.max(mThumbWidth + mTempRect.left, mThumbWidth + mTempRect.right);
                thumbHeight = mThumbDrawable1.getIntrinsicHeight() - (mTempRect.top + mTempRect.bottom);
            }
        } else if (mTargetState == 1) {
            if (mThumbDrawable2 != null) {
                mThumbDrawable2.getPadding(mTempRect);
                mThumbWidth = mThumbDrawable2.getIntrinsicWidth() - mTempRect.left - mTempRect.right;

                maxThumbWidth = Math.max(mThumbWidth + mTempRect.left, mThumbWidth + mTempRect.right);
                thumbHeight = mThumbDrawable2.getIntrinsicHeight() - (mTempRect.top + mTempRect.bottom);
            }
        } else if (mTargetState == 2) {
            if (mThumbDrawable3 != null) {
                mThumbDrawable3.getPadding(mTempRect);
                mThumbWidth = mThumbDrawable3.getIntrinsicWidth() - mTempRect.left - mTempRect.right;

                maxThumbWidth = Math.max(mThumbWidth + mTempRect.left, mThumbWidth + mTempRect.right);
                thumbHeight = mThumbDrawable3.getIntrinsicHeight() - (mTempRect.top + mTempRect.bottom);
            }
        }

        int switchHeight = 0;
        mInternalTrackWidth = 0;
        mSwitchWidth = 0;
        mSwitchHeight = 0;

        if (mTrackDrawable != null) {
            mTrackDrawable.getPadding(mTempRect);
            mInternalTrackWidth = Math.max(maxThumbWidth, mTrackDrawable.getIntrinsicWidth() - (mTempRect.left + mTempRect.right));
            final int switchWidth = Math.max(mSwitchMinWidth, mTempRect.left + mInternalTrackWidth + mTempRect.right);
            int internalTrackHeight = Math.max(mTrackDrawable.getIntrinsicHeight() - (mTempRect.top + mTempRect.bottom), thumbHeight);
            switchHeight = Math.max(mTrackDrawable.getIntrinsicHeight(), internalTrackHeight + mTempRect.top + mTempRect.bottom);

            mSwitchWidth = switchWidth;
            mSwitchHeight = switchHeight;

            mInternalTrackWidth = mSwitchWidth - (mTempRect.left + mTempRect.right);
        }

        mFirstStateThumbPosition = 0;
        mSecondStateThumbPosition = (mInternalTrackWidth / 2) - (mThumbWidth / 2);
        mThirdStateThumbPosition = getThumbScrollRange();

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        final int measuredHeight = getMeasuredHeight();
        if (measuredHeight < switchHeight) {
            setMeasuredDimension(MeasureSpec.makeMeasureSpec(getMeasuredWidth(), widthMeasureSpec), switchHeight);
        }
    }

    private void animateThumbToCheckedState(int state) {
        switch (state) {
            case STATE_FIRST:
                mTargetPosition = mFirstStateThumbPosition;
                break;
            case STATE_SECOND:
                mTargetPosition = mSecondStateThumbPosition;
                break;
            case STATE_THIRD:
                mTargetPosition = mThirdStateThumbPosition;
                break;
        }
        mThumbPosition = mTargetPosition;
        if (mTargetState != state) {
            mTargetState = state;
            if (mStateChangeListener != null) {
                mStateChangeListener.stateChanged(state);
            }
        }
        mTouchMode = TOUCH_MODE_ANIMATE;
        invalidate();
    }

    private void animateNextPosition() {
        Log.d(TAG, "Animate next position");
        float firstDistance = Math.abs(mTargetPosition - mFirstStateThumbPosition);
        float secondDistance = Math.abs(mTargetPosition - mSecondStateThumbPosition);
        float thirdDistance = Math.abs(mTargetPosition - mThirdStateThumbPosition);

        int newState;
        if (firstDistance <= secondDistance && firstDistance <= thirdDistance) {
            mThumbPosition = mFirstStateThumbPosition;
            newState = STATE_FIRST;
        } else if (secondDistance <= firstDistance && secondDistance <= thirdDistance) {
            mThumbPosition = mSecondStateThumbPosition;
            newState = STATE_SECOND;
        } else {
            mThumbPosition = mThirdStateThumbPosition;
            newState = STATE_THIRD;
        }

        setState(newState);

//        if (Math.abs(mTargetPosition - mThumbPosition) > mTouchSlop) {
//            mThumbPosition += (mThumbPosition > mTargetPosition) ? -mTouchSlop : mTouchSlop;
//            invalidate();
//        } else {
//            stopAnimation();
//        }
    }

    private void stopAnimation() {
        mThumbPosition = mTargetPosition;
        mTouchMode = TOUCH_MODE_IDLE;
        setState(mTargetState);
    }

    @Override
    public void toggle() {
        try {
            throw new Exception("Not supported exception");
        } catch (Exception e) {
            e.printStackTrace();
        }
        animateThumbToCheckedState(mTargetState);
    }

    private int getTargetCheckedState() {
        if (mThumbPosition == 0)
            return STATE_FIRST;
        else if (mThumbPosition == (getThumbScrollRange() / 2) - (mSwitchWidth / 2))
            return STATE_SECOND;
        else
            return STATE_THIRD;
    }

    private void setThumbPosition(int state) {
        switch (state) {
            case STATE_FIRST:
                mThumbPosition = mFirstStateThumbPosition;
                break;
            case STATE_SECOND:
                mThumbPosition = mSecondStateThumbPosition;
                break;
            case STATE_THIRD:
                mThumbPosition = mThirdStateThumbPosition;
                break;
        }
    }

    public void setState(int state) {
        setThumbPosition(state);
        mTargetState = state;

        Log.d(TAG, "Current state = " + state);

        switch (state) {
            case STATE_FIRST:
                if (mThumbFirstStateDrawable != null) {
                    mThumbDrawable1 = mThumbFirstStateDrawable;
                }
                break;
            case STATE_SECOND:
                if (mThumbSecondStateDrawable != null) {
                    mThumbDrawable1 = mThumbSecondStateDrawable;
                }
                break;
            case STATE_THIRD:
                if (mThumbThirdStateDrawable != null) {
                    mThumbDrawable1 = mThumbThirdStateDrawable;
                }
                break;
        }
        invalidate();
    }

    public int getState() {
        return mTargetState;
    }

    private int getThumbScrollRange() {
        if (mTrackDrawable == null) {
            return 0;
        }
        return mInternalTrackWidth - mThumbWidth;
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isChecked()) {
            mergeDrawableStates(drawableState, CHECKED_STATE_SET);
        }
        return drawableState;
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();

        int[] myDrawableState = getDrawableState();
        // Set the state of the Drawable
        // Drawable may be null when checked state is set from XML, from super constructor
        if (mThumbDrawable1 != null) mThumbDrawable1.setState(myDrawableState);
        if (mThumbDrawable2 != null) mThumbDrawable2.setState(myDrawableState);
        if (mThumbDrawable3 != null) mThumbDrawable3.setState(myDrawableState);
        if (mTrackDrawable != null) mTrackDrawable.setState(myDrawableState);
        if (mBackground != null) mBackground.setState(myDrawableState);

        invalidate();
    }

    @Override
    protected boolean verifyDrawable(Drawable who) {
        return super.verifyDrawable(who) || who == mThumbDrawable1 || who == mThumbDrawable2 || who == mThumbDrawable3 || who == mTrackDrawable;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        setThumbPosition(mTargetState);

        int switchRight;
        int switchLeft;

        switchRight = getWidth() - getPaddingRight();
        switchLeft = switchRight - mSwitchWidth;

        int switchTop = 0;
        int switchBottom = 0;
        switch (getGravity() & Gravity.VERTICAL_GRAVITY_MASK) {
            default:
            case Gravity.TOP:
                switchTop = getPaddingTop();
                switchBottom = switchTop + mSwitchHeight;
                break;

            case Gravity.CENTER_VERTICAL:
                switchTop = (getPaddingTop() + getHeight() - getPaddingBottom()) / 2 - mSwitchHeight / 2;
                switchBottom = switchTop + mSwitchHeight;
                break;

            case Gravity.BOTTOM:
                switchBottom = getHeight() - getPaddingBottom();
                switchTop = switchBottom - mSwitchHeight;
                break;
        }

        mSwitchLeft = switchLeft;
        mSwitchTop = switchTop;
        mSwitchBottom = switchBottom;
        mSwitchRight = switchRight;
    }

    @Override
    public int getCompoundPaddingRight() {
        int padding = super.getCompoundPaddingRight() + mSwitchWidth;
        if (!TextUtils.isEmpty(getText())) {
            padding += mSwitchPadding;
        }
        return padding;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int switchLeft = mSwitchLeft;
        int switchTop = mSwitchTop;
        int switchRight = mSwitchRight;
        int switchBottom = mSwitchBottom;

        int alpha = (int) (mThumbPosition / getThumbScrollRange() * (float) 0xff);

        if (mBackground != null) {
            mBackground.setBounds(switchLeft, switchTop, switchRight, switchBottom);
            mBackground.draw(canvas);
        }

        if (mTrackDrawable != null) {
            mTrackDrawable.setBounds(switchLeft, switchTop, switchRight, switchBottom);
            mTrackDrawable.draw(canvas);
            canvas.save();

            mTrackDrawable.getPadding(mTempRect);
            int switchInnerLeft = switchLeft + mTempRect.left;
            int switchInnerTop = switchTop + mTempRect.top;
            int switchInnerRight = switchRight - mTempRect.right;
            int switchInnerBottom = switchBottom - mTempRect.bottom;
            canvas.clipRect(switchInnerLeft, switchTop, switchInnerRight, switchBottom);

            if (mTargetState == 0) {
                if (mThumbDrawable1 != null) {
                    Log.d(TAG, "Thumb position = " + mThumbPosition);
                    mThumbDrawable1.getPadding(mTempRect);
                    final int thumbPos = (int) (mThumbPosition + 0.5f);
                    int thumbLeft = switchInnerLeft - mTempRect.left + thumbPos;
                    int thumbRight = switchInnerLeft + thumbPos + mThumbWidth + mTempRect.right;
                    int thumbTop = switchInnerTop - mTempRect.top;
                    int thumbBottom = switchInnerBottom + mTempRect.bottom;

                    mThumbDrawable1.setBounds(thumbLeft, thumbTop, thumbRight, thumbBottom);
                    mThumbDrawable1.draw(canvas);
                }
            } else if (mTargetState == 1) {
                if (mThumbDrawable2 != null) {
                    Log.d(TAG, "Thumb position = " + mThumbPosition);
                    mThumbDrawable2.getPadding(mTempRect);
                    final int thumbPos = (int) (mThumbPosition + 0.5f);
                    int thumbLeft = switchInnerLeft - mTempRect.left + thumbPos;
                    int thumbRight = switchInnerLeft + thumbPos + mThumbWidth + mTempRect.right;
                    int thumbTop = switchInnerTop - mTempRect.top;
                    int thumbBottom = switchInnerBottom + mTempRect.bottom;

                    mThumbDrawable2.setBounds(thumbLeft, thumbTop, thumbRight, thumbBottom);
                    mThumbDrawable2.draw(canvas);
                }
            } else if (mTargetState == 2) {
                if (mThumbDrawable3 != null) {
                    Log.d(TAG, "Thumb position = " + mThumbPosition);
                    mThumbDrawable3.getPadding(mTempRect);
                    final int thumbPos = (int) (mThumbPosition + 0.5f);
                    int thumbLeft = switchInnerLeft - mTempRect.left + thumbPos;
                    int thumbRight = switchInnerLeft + thumbPos + mThumbWidth + mTempRect.right;
                    int thumbTop = switchInnerTop - mTempRect.top;
                    int thumbBottom = switchInnerBottom + mTempRect.bottom;

                    mThumbDrawable3.setBounds(thumbLeft, thumbTop, thumbRight, thumbBottom);
                    mThumbDrawable3.draw(canvas);
                }
            }
            canvas.restore();
        }

        if (mTouchMode == TOUCH_MODE_ANIMATE) {
            animateNextPosition();
            mTouchMode = TOUCH_MODE_IDLE;
        }
    }

    /**
     * @return true if (x, y) is within the target area of the switch thumb
     */
    private boolean hitThumb(float x, float y) {
        if (mTargetState == 0) {
            if (mThumbDrawable1 != null) {
                mThumbDrawable1.getPadding(mTempRect);
                final int thumbTop = mSwitchTop;
                final int thumbLeft = mSwitchLeft + (int) (mThumbPosition + 0.5f);
                final int thumbRight = thumbLeft + mThumbWidth + mTempRect.left + mTempRect.right;
                final int thumbBottom = mSwitchBottom;
                return x > thumbLeft && x < thumbRight && y > thumbTop && y < thumbBottom;
            }
        } else if (mTargetState == 1) {
            if (mThumbDrawable2 != null) {
                mThumbDrawable2.getPadding(mTempRect);
                final int thumbTop = mSwitchTop;
                final int thumbLeft = mSwitchLeft + (int) (mThumbPosition + 0.5f);
                final int thumbRight = thumbLeft + mThumbWidth + mTempRect.left + mTempRect.right;
                final int thumbBottom = mSwitchBottom;
                return x > thumbLeft && x < thumbRight && y > thumbTop && y < thumbBottom;
            }
        } else if (mTargetState == 2) {
            if (mThumbDrawable3 != null) {
                mThumbDrawable3.getPadding(mTempRect);
                final int thumbTop = mSwitchTop;
                final int thumbLeft = mSwitchLeft + (int) (mThumbPosition + 0.5f);
                final int thumbRight = thumbLeft + mThumbWidth + mTempRect.left + mTempRect.right;
                final int thumbBottom = mSwitchBottom;
                return x > thumbLeft && x < thumbRight && y > thumbTop && y < thumbBottom;
            }
        }

        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        mVelocityTracker.addMovement(ev);
        final int action = ev.getActionMasked();
        switch (action) {
            case MotionEvent.ACTION_DOWN: {
                final float x = ev.getX();
                final float y = ev.getY();
                if (isEnabled() && hitThumb(x, y)) {
                    mTouchMode = TOUCH_MODE_DOWN;
                    mTouchX = x;
                    mTouchY = y;
                }
                break;
            }

            case MotionEvent.ACTION_MOVE: {
                switch (mTouchMode) {
                    case TOUCH_MODE_IDLE:
                        // Didn't target the thumb, treat normally.
                        break;

                    case TOUCH_MODE_DOWN: {
                        final float x = ev.getX();
                        final float y = ev.getY();
                        mTouchMode = TOUCH_MODE_DRAGGING;
                        getParent().requestDisallowInterceptTouchEvent(true);
                        mTouchX = x;
                        mTouchY = y;
                        return true;
                    }

                    case TOUCH_MODE_DRAGGING: {
                        final float x = ev.getX();
                        final float dx = x - mTouchX;
                        float newPos = Math.max(0, Math.min(mThumbPosition + dx, getThumbScrollRange()));
                        if (newPos != mThumbPosition) {
                            mThumbPosition = newPos;
                            mTouchX = x;
                            invalidate();
                        }
                        return true;
                    }
                }
                break;
            }

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                if (mTouchMode == TOUCH_MODE_DRAGGING) {
                    stopDrag(ev);
                    return true;
                }
                mTouchMode = TOUCH_MODE_IDLE;
                mVelocityTracker.clear();
                break;
            }
        }

        return super.onTouchEvent(ev);
    }

    private void stopDrag(MotionEvent ev) {
        mTouchMode = TOUCH_MODE_IDLE;
        // Up and not canceled, also checks the switch has not been disabled during the drag
        boolean commitChange = ev.getAction() == MotionEvent.ACTION_UP && isEnabled();

        cancelSuperTouch(ev);

        mVelocityTracker.computeCurrentVelocity(1000);
        float xvel = ev.getX() - mSwitchLeft;

        float firstDistance = Math.abs(mThumbPosition - mFirstStateThumbPosition);
        float secondDistance = Math.abs(mThumbPosition - mSecondStateThumbPosition);
        float thirdDistance = Math.abs(mThumbPosition - mThirdStateThumbPosition);

        int newState;
        if (firstDistance <= secondDistance && firstDistance <= thirdDistance) {
            newState = STATE_FIRST;
        } else if (secondDistance <= firstDistance && secondDistance <= thirdDistance) {
            newState = STATE_SECOND;
        } else {
            newState = STATE_THIRD;
        }

        if (commitChange) {
            mVelocityTracker.computeCurrentVelocity(1000);
        }
        animateThumbToCheckedState(newState);
    }

    private void cancelSuperTouch(MotionEvent ev) {
        MotionEvent cancel = MotionEvent.obtain(ev);
        cancel.setAction(MotionEvent.ACTION_CANCEL);
        super.onTouchEvent(cancel);
        cancel.recycle();
    }

    public interface OnStateChangeListener {
        void stateChanged(int state);
    }
}
