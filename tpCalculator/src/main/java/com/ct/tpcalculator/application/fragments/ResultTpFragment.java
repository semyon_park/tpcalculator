package com.ct.tpcalculator.application.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.activities.BaseActivity;
import com.ct.tpcalculator.application.activities.MainActivity;
import com.ct.tpcalculator.application.views.AsyncImageView;
import com.ct.tpcalculator.application.views.ProgressIndicatorControl;
import com.ct.tpcalculator.calculator.data.Command;
import com.ct.tpcalculator.calculator.data.MobileOperator;
import com.ct.tpcalculator.calculator.data.calculationData.PackageToOffer;
import com.ct.tpcalculator.calculator.data.calculationData.ServiceToOffer;
import com.ct.tpcalculator.helpers.CommandExecutionUtil;
import com.ct.tpcalculator.helpers.GoogleAnalyticsHelper;
import com.ct.tpcalculator.helpers.Utils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class ResultTpFragment extends BaseMainFragment {

    public static final String TAG = ResultTpFragment.class.getName();
    private static final String GA_PAGE_NAME = "ResultTpFragment";

    public static final String ARG_MOBILE_OPERATOR = "ARG_MOBILE_OPERATOR";
    public static final String ARG_PACKAGE_TO_OFFER = "ARG_PACKAGE_TO_OFFER";
    public static final String ARG_SELECTED_SKU = "ARG_SELECTED_SKU";

    private MobileOperator mMobileOperator;
    private PackageToOffer mPackageToOffer;
//    private String mSku;
    private ViewGroup mData;
    private ProgressIndicatorControl mProgressControl;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param mobileOperator MobileOperator.
     * @param packageToOffer PackageToOffer.
     * @return A new instance of fragment ResultTpFragment.
     */
    public static ResultTpFragment newInstance(MobileOperator mobileOperator, PackageToOffer packageToOffer, String sku) {
        ResultTpFragment fragment = new ResultTpFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_MOBILE_OPERATOR, mobileOperator);
        args.putSerializable(ARG_PACKAGE_TO_OFFER, packageToOffer);
        args.putString(ARG_SELECTED_SKU, sku);
        fragment.setArguments(args);
        return fragment;
    }

    public ResultTpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMobileOperator = (MobileOperator) getArguments().getSerializable(ARG_MOBILE_OPERATOR);
            mPackageToOffer = (PackageToOffer) getArguments().getSerializable(ARG_PACKAGE_TO_OFFER);
//            mSku = getArguments().getString(ARG_SELECTED_SKU);
        }
        GoogleAnalyticsHelper.sendLog(GA_PAGE_NAME, null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_result_tp, container, false);
        mData = (ViewGroup) root.findViewById(R.id.data);
        mProgressControl = (ProgressIndicatorControl) root.findViewById(R.id.progress);
        mProgressControl.setProgressText(getString(R.string.tp_is_calculating));

        AsyncImageView imageView = (AsyncImageView) root.findViewById(R.id.mobile_operator_image);
        String iconPath = mMobileOperator.getIconPath();
        if (Application.getInstance().getIsPremiumAccount() && !TextUtils.isEmpty(iconPath)) {
            imageView.loadImage(iconPath);
        }

        TextView mobileOperator = (TextView) root.findViewById(R.id.mobile_operator_label);
        mobileOperator.setTypeface(Application.lightTypeface);
        if (Application.getInstance().getIsPremiumAccount()) {
            mobileOperator.setText(mMobileOperator.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));
        }else{
            mobileOperator.setText(getString(R.string.mobile_operator1));
        }
        TextView currentCost = (TextView) root.findViewById(R.id.current_cost);
        currentCost.setTypeface(Application.lightTypeface);
        currentCost.setText(Html.fromHtml(String.format(getResources().getString(R.string.current_cost_text),
                mPackageToOffer.getFormattedPreviousCost(), Application.getInstance().getCountry().getCurrency(), mPackageToOffer.getFormattedPreviousCostInYear())));

        TextView savingValue = (TextView) root.findViewById(R.id.saving_value);
        savingValue.setTypeface(Application.lightTypeface);
        savingValue.setText(Html.fromHtml(String.format(getResources().getString(R.string.save_value_text),
                mPackageToOffer.getFormattedDiffOfCost(), Application.getInstance().getCountry().getCurrency(),
                mPackageToOffer.getFormattedDiffOfCostInYear())));


        TextView generalInfoTitle = (TextView) root.findViewById(R.id.general_info_label);
        generalInfoTitle.setTypeface(Application.lightTypeface);

        TextView generalInfoDescription = (TextView) root.findViewById(R.id.general_info_description);
        generalInfoDescription.setTypeface(Application.lightTypeface);
        String description = Application.getInstance().getIsPremiumAccount() ? mPackageToOffer.getTariffPlan().getDescriptions().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()) : getString(R.string.show_tp_buy_pro);
        generalInfoDescription.setText(description);

        setupCollapsableBlock(generalInfoTitle, generalInfoDescription);

        LinearLayout generalInfoHeader = (LinearLayout) root.findViewById(R.id.general_info_header);
        generalInfoHeader.setOnClickListener(new collapsableViewClickListener(generalInfoTitle, generalInfoDescription));

        if (Application.getInstance().getIsPremiumAccount()) {
            final Button enableTp = (Button) root.findViewById(R.id.btnEnableTp);
            enableTp.setTypeface(Application.lightTypeface);
            enableTp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    enableTpButtonClicked();
                }
            });

            if (!mMobileOperator.getId().equalsIgnoreCase(Application.getInstance().getSelectedMobileOperator().getId()) || mPackageToOffer.getTariffPlan().getCommands() == null ||
                    mPackageToOffer.getTariffPlan().getCommands().isEmpty() ||  mPackageToOffer.getTariffPlan().getCommands().getFirstCommandByCommandType(Command.TYPE_CHANGE_TARIFF_PLAN) == null ) {
                View enableTpContainer = root.findViewById(R.id.enableTpContainer);
                enableTpContainer.setVisibility(View.GONE);
            }

            TextView additionalServicesTitle = (TextView) root.findViewById(R.id.additional_services);
            additionalServicesTitle.setTypeface(Application.lightTypeface);

            if (mPackageToOffer.getServices() != null && !mPackageToOffer.getServices().isEmpty()) {
                ListView servicesList = (ListView) root.findViewById(R.id.additional_services_list);
                servicesList.setAdapter(new AdditionalServicesListAdapter(getActivity(), mPackageToOffer.getServices()));

                additionalServicesTitle.setOnClickListener(new collapsableViewClickListener(additionalServicesTitle, servicesList));
                setupCollapsableBlock(additionalServicesTitle, servicesList);
            } else {
                additionalServicesTitle.setVisibility(View.GONE);
            }
        } else {
            Button enableTp = (Button) root.findViewById(R.id.btnEnableTp);
            enableTp.setVisibility(View.GONE);

            TextView additionalServicesTitle = (TextView) root.findViewById(R.id.additional_services);
            additionalServicesTitle.setVisibility(View.GONE);

            Button btnBuyPro = (Button) root.findViewById(R.id.btnBuyPro);
            btnBuyPro.setTypeface(Application.lightTypeface);
            btnBuyPro.setVisibility(View.VISIBLE);
            btnBuyPro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Application.getInstance().buyProVersion(getActivity(), ResultTpFragment.this, mSku);
                    Application.getInstance().buyProVersion(getActivity(), ResultTpFragment.this, MainActivity.SKU_GENERAL_YEAR_SUBSCRIPTION);
                }
            });
        }

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPackageToOffer == null || mPackageToOffer.getTariffPlan() == null) {
            return;
        }
        String title =  Application.getInstance().getIsPremiumAccount()?mPackageToOffer.getTariffPlan().getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()):getString(R.string.tariff_plan_without_doublepoint);
        mListener.onFragmentChanged(TAG, title);
    }

    protected void showProgress() {
        if (mData != null) {
            mData.setVisibility(View.GONE);
        }
        if (mProgressControl != null) {
            mProgressControl.showProgressIndicator();
        }
    }

    protected void hideProgress() {
        mData.setVisibility(View.VISIBLE);
        mProgressControl.hideProgressIndicator();
    }

    private void enableTpButtonClicked() {
        if (mPackageToOffer.getTariffPlan().getCommands() == null) {
            return;
        }

        final Command command = mPackageToOffer.getTariffPlan().getCommands().getFirstCommandByCommandType(Command.TYPE_CHANGE_TARIFF_PLAN);

        if (command == null || command.getBaseCommands() == null || command.getBaseCommands().isEmpty()) {
            return;
        }

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent, getActivity().getTheme())));
        } else {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        }
        dialog.setContentView(R.layout.dialog_question_yes_no);

        TextView titleTextView = (TextView) dialog.findViewById(R.id.title);
        titleTextView.setTypeface(Application.lightTypeface);
        titleTextView.setText(getContext().getString(R.string.enable_tp));

        TextView infoTextView = (TextView) dialog.findViewById(R.id.info);
        infoTextView.setTypeface(Application.lightTypeface);
        String description = String.format(getContext().getString(R.string.enable_tp_info), mPackageToOffer.getTariffPlan().getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));
        infoTextView.setText(description);

        Button yesButton = (Button) dialog.findViewById(R.id.btnYes);
        yesButton.setTypeface(Application.lightTypeface);
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommandExecutionUtil.getInstance().executeOfflineCommand((BaseActivity) getActivity(), command, getString(R.string.ga_change_tp_action));
                dialog.dismiss();
            }
        });

        Button noButton = (Button) dialog.findViewById(R.id.btnNo);
        noButton.setTypeface(Application.lightTypeface);
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private class collapsableViewClickListener implements View.OnClickListener {

        private WeakReference<TextView> title;
        private WeakReference<View> collapsableView;

        public collapsableViewClickListener(TextView title, View description) {
            super();
            this.title = new WeakReference<>(title);
            this.collapsableView = new WeakReference<>(description);
        }

        @Override
        public void onClick(View view) {
            final TextView t = title.get();
            final View desc = collapsableView.get();
            if (t == null && desc == null)
                return;

            boolean open = setupCollapsableBlock(t, desc);

            if (open)
                ensureVisible(desc, 500);
        }

    }

    private boolean setupCollapsableBlock(final TextView title, final View collapsableView) {
        boolean open = collapsableView.getVisibility() == View.GONE;
        collapsableView.setVisibility(open ? View.VISIBLE : View.GONE);
        Drawable[] d = title.getCompoundDrawables();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            d[2] = title.getContext().getResources().getDrawable(open ? R.drawable.arrow_down : R.drawable.arrow_up, getActivity().getTheme());
        } else {
            d[2] = title.getContext().getResources().getDrawable(open ? R.drawable.arrow_down : R.drawable.arrow_up);
        }
        d[2].setBounds(0, 0, d[2].getIntrinsicWidth(), d[2].getIntrinsicHeight());

        collapsableView.setTag(open ? View.VISIBLE : View.GONE);

        title.setCompoundDrawables(d[0], d[1], d[2], d[3]);
        return open;
    }

    public void ensureVisible(final View view, long delay) {
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                ViewGroup parent = (ViewGroup) view.getParent();

                while (parent != null) {
                    if (parent instanceof ScrollView) {
                        ScrollView sv = (ScrollView) parent;

                        int[] viewLocation = {0, 0};
                        view.getLocationOnScreen(viewLocation);

                        int[] scrollViewLocation = {0, 0};
                        sv.getLocationOnScreen(scrollViewLocation);

                        int delta = view.getHeight()
                                - (scrollViewLocation[1] + sv.getHeight() - viewLocation[1]);

                        if (view.getHeight() > sv.getHeight()) {
                            delta = viewLocation[1] - scrollViewLocation[1];
                        }

                        if (delta > 0)
                            sv.smoothScrollBy(
                                    0,
                                    delta + Utils.convertDipToPixels(getActivity(), 10));

                        break;
                    }
                    parent = (ViewGroup) parent.getParent();
                }
            }
        }, delay);
    }

    public class AdditionalServicesListAdapter extends ArrayAdapter<ServiceToOffer> {
        private final Context mContext;
        private final ArrayList<ServiceToOffer> mListData;


        public AdditionalServicesListAdapter(Context context, ArrayList<ServiceToOffer> listData) {
            super(context, R.layout.statistic_list_item);
            this.mContext = context;
            this.mListData = listData;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.result_additional_service_item, parent, false);
            }

            final ServiceToOffer listItemData = mListData.get(position);

            ImageView imageView = (ImageView) convertView.findViewById(R.id.mobile_operator_image);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String description = listItemData.getService().getDescriptions().getStringByLanguage(Application.getInstance().getSelectedLanguageCode());

                    if (TextUtils.isEmpty(description)) {
                        return;
                    }

                    Application.getInstance().showInfoDialog(getContext(),description);
                }
            });

            TextView serviceLabel = (TextView) convertView.findViewById(R.id.service_label);
            serviceLabel.setTypeface(Application.lightTypeface);
            serviceLabel.setText(listItemData.getService().getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));

            Button enableButton = (Button) convertView.findViewById(R.id.btnEnable);
            enableButton.setTypeface(Application.lightTypeface);

            int visibility;
            if (listItemData.getService().getCommands() == null) {
                visibility = View.GONE;
            } else {
                final Command command = listItemData.getService().getCommands().getFirstCommandByCommandType(Command.TYPE_CHANGE_TARIFF_PLAN);

                if (command == null || command.getBaseCommands() == null || command.getBaseCommands().isEmpty()) {
                    visibility = View.GONE;
                } else {
                    visibility = View.VISIBLE;
                }
            }
            enableButton.setVisibility(visibility);
            enableButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    enableServiceClicked(listItemData);
                }
            });

            return convertView;
        }

        private void enableServiceClicked(final ServiceToOffer serviceToOffer) {

            if (serviceToOffer.getService() == null) {
                return;
            }

            final Command command = serviceToOffer.getService().getCommands().getFirstCommandByCommandType(Command.TYPE_CHANGE_TARIFF_PLAN);

            if (command == null || command.getBaseCommands() == null || command.getBaseCommands().isEmpty()) {
                return;
            }

            final Dialog dialog = new Dialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent, getActivity().getTheme())));
            } else {
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
            }
            dialog.setContentView(R.layout.dialog_question_yes_no);

            TextView titleTextView = (TextView) dialog.findViewById(R.id.title);
            titleTextView.setTypeface(Application.lightTypeface);
            titleTextView.setText(getContext().getString(R.string.enable_service));

            TextView infoTextView = (TextView) dialog.findViewById(R.id.info);
            infoTextView.setTypeface(Application.lightTypeface);
            String description = String.format(getContext().getString(R.string.enable_service_info), serviceToOffer.getService().getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));
            infoTextView.setText(description);

            Button yesButton = (Button) dialog.findViewById(R.id.btnYes);
            yesButton.setTypeface(Application.lightTypeface);
            yesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CommandExecutionUtil.getInstance().executeOfflineCommand((BaseActivity) getActivity(), command, getString(R.string.ga_enable_service));
                    dialog.dismiss();
                }
            });

            Button noButton = (Button) dialog.findViewById(R.id.btnNo);
            noButton.setTypeface(Application.lightTypeface);
            noButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }

        @Override
        public int getCount() {
            return mListData.size();
        }
    }
}
