package com.ct.tpcalculator.application.navigationControllers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.v4.app.FragmentTransaction;
import android.transition.Fade;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.activities.ResultActivity;
import com.ct.tpcalculator.application.fragments.AddNewOperatorFragment;
import com.ct.tpcalculator.application.fragments.ResultListFragment;
import com.ct.tpcalculator.application.fragments.ResultSettingsFragment;
import com.ct.tpcalculator.application.fragments.ResultTpFragment;
import com.ct.tpcalculator.calculator.data.MobileOperator;
import com.ct.tpcalculator.calculator.data.calculationData.CalculationResult;
import com.ct.tpcalculator.calculator.data.calculationData.PackageToOffer;

public class ResultNavigationController extends BroadcastReceiver {

    private static final IntentFilter sFilter = new IntentFilter();
    private ResultActivity mActivity;

    public final static String ACTION_SHOW_RESULT_SETTINGS = "com.ct.tpcalculator.ACTION_SHOW_RESULT_SETTINGS";
    public final static String ACTION_SHOW_RESULT_LIST = "com.ct.tpcalculator.ACTION_SHOW_RESULT_LIST";
    public final static String ACTION_SHOW_RESULT_TP = "com.ct.tpcalculator.ACTION_SHOW_RESULT_TP";


    static {
        sFilter.addAction(ACTION_SHOW_RESULT_SETTINGS);
        sFilter.addAction(ACTION_SHOW_RESULT_LIST);
        sFilter.addAction(ACTION_SHOW_RESULT_TP);
        sFilter.addAction(SetupNavigationController.ACTION_SHOW_ADD_NEW_OPERATOR);
    }

    public ResultNavigationController(ResultActivity activity) {
        super();
        mActivity = activity;
    }

    public IntentFilter getIntentFilter() {
        return sFilter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (ACTION_SHOW_RESULT_SETTINGS.equalsIgnoreCase(action)) {
            showResultSettings();
        } else if (ACTION_SHOW_RESULT_LIST.equalsIgnoreCase(action)) {
            CalculationResult data = (CalculationResult) intent.getSerializableExtra(ResultListFragment.ARG_PACKAGES_TO_OFFER);
            showResultListFragment(data);
        } else if (ACTION_SHOW_RESULT_TP.equalsIgnoreCase(action)) {
            MobileOperator mobileOperator = (MobileOperator) intent.getSerializableExtra(ResultTpFragment.ARG_MOBILE_OPERATOR);
            PackageToOffer packageToOffer = (PackageToOffer) intent.getSerializableExtra(ResultTpFragment.ARG_PACKAGE_TO_OFFER);
            String sku = intent.getStringExtra(ResultTpFragment.ARG_SELECTED_SKU);
            setActionShowResultTpFragment(mobileOperator, packageToOffer, sku);
        } else if (SetupNavigationController.ACTION_SHOW_ADD_NEW_OPERATOR.equalsIgnoreCase(action)){
            String countryName = intent.getStringExtra(AddNewOperatorFragment.ARG_COUNTRY_NAME);
            String regionName = intent.getStringExtra(AddNewOperatorFragment.ARG_REGION_NAME);
            String title = intent.getStringExtra(AddNewOperatorFragment.ARG_TITLE);
            showAddNewOperator(countryName, regionName, title);
        }
    }

    private void showAddNewOperator(String countryName, String regionName, String title) {
        AddNewOperatorFragment addNewOperatorFragment = AddNewOperatorFragment.newInstance(countryName, regionName, title);
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(AddNewOperatorFragment.TAG)
                .replace(R.id.container, addNewOperatorFragment, AddNewOperatorFragment.TAG)
                .commitAllowingStateLoss();
    }

    public void showResultSettings() {
        ResultSettingsFragment resultSettingsFragment = new ResultSettingsFragment();
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(ResultSettingsFragment.TAG)
                .replace(R.id.container, resultSettingsFragment, ResultSettingsFragment.TAG)
                .commitAllowingStateLoss();
    }

    private void showResultListFragment(CalculationResult data) {
        ResultListFragment resultListFragment = ResultListFragment.newInstance(data);
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(ResultListFragment.TAG)
                .replace(R.id.container, resultListFragment, ResultListFragment.TAG)
                .commitAllowingStateLoss();
    }

    private void setActionShowResultTpFragment(MobileOperator mobileOperator, PackageToOffer packageToOffer, String sku) {
        ResultTpFragment resultTpFragment = ResultTpFragment.newInstance(mobileOperator, packageToOffer, sku);
        FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_right, R.anim.exit_to_left);
        ft.addToBackStack(ResultTpFragment.TAG);
        ft.replace(R.id.container, resultTpFragment, ResultTpFragment.TAG);
        ft.commitAllowingStateLoss();
    }

}

