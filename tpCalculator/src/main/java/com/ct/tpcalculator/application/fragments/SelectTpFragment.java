package com.ct.tpcalculator.application.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.activities.SetupActivity;
import com.ct.tpcalculator.application.navigationControllers.SetupNavigationController;
import com.ct.tpcalculator.application.services.LoadFileService;
import com.ct.tpcalculator.application.views.ProgressIndicatorControl;
import com.ct.tpcalculator.application.views.SelectTpControl;
import com.ct.tpcalculator.calculator.data.Region;
import com.ct.tpcalculator.helpers.FileCache;
import com.ct.tpcalculator.helpers.GoogleAnalyticsHelper;
import com.ct.tpcalculator.helpers.Utils;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;

public class SelectTpFragment extends BaseMainFragment {

    public static String TAG = SelectTpFragment.class.getName();
    private static final String GA_PAGE_NAME = "SelectTpFragment";

    private static final String STATE_SELECTED_MOBILE_OPERATOR = "STATE_SELECTED_MOBILE_OPERATOR";
    private static final String STATE_SELECTED_TP = "STATE_SELECTED_TP";
    private static final String STATE_SELECTED_SERVICES = "STATE_SELECTED_SERVICES";

    private SelectTpControl mSelectTpControl;

    private ViewGroup mData;
    private ProgressIndicatorControl mProgressControl;

    public SelectTpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GoogleAnalyticsHelper.sendLog(GA_PAGE_NAME, null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_select_tp, container, false);

        mData = (ViewGroup) root.findViewById(R.id.data);
        mProgressControl = (ProgressIndicatorControl) root.findViewById(R.id.progress);
        mSelectTpControl = (SelectTpControl) root.findViewById(R.id.content);

        if (savedInstanceState == null) {
            mSelectTpControl.mobileOperatorChanged(false);
        } else {
            mSelectTpControl.setSelectedMobileOperatorId(savedInstanceState.getString(STATE_SELECTED_MOBILE_OPERATOR));
            mSelectTpControl.setSelectedTpId(savedInstanceState.getString(STATE_SELECTED_TP));
            mSelectTpControl.setSelectedServices((ArrayList<String>) savedInstanceState.getSerializable(STATE_SELECTED_SERVICES));
            mSelectTpControl.mobileOperatorChanged(true);
            mSelectTpControl.fillSelectedServicesListView();
        }

        Button btnNext = (Button) root.findViewById(R.id.btnNext);
        btnNext.setTypeface(Application.lightTypeface);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadOffer();
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.onFragmentChanged(TAG, getString(R.string.app_name));
    }

    protected void showProgress() {
        if (mData != null) {
            mData.setVisibility(View.GONE);
        }
        if (mProgressControl != null) {
           mProgressControl.showProgressIndicator();
        }
    }

    protected void hideProgress() {
        mData.setVisibility(View.VISIBLE);
        mProgressControl.hideProgressIndicator();
    }

    private void downloadOffer() {
        Region region = Application.getInstance().getRegion();
        if (region == null) {
            return;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String offerFilePath = preferences.getString(getString(R.string.preferences_offer_path, ""), "");
        Intent intent = new Intent(getActivity(), LoadFileService.class).
                putExtra(LoadFileService.PARAM_FILE_PATH, getString(R.string.server_url) + offerFilePath)
                .putExtra(SetupActivity.FileLoadReceiver.PARAM_DATA_TYPE, SetupActivity.FileLoadReceiver.DATA_TYPE_OFFER);
        getActivity().startService(intent);
        showProgress();
    }

    public void offerDataDownloadFinished(String data) {
        hideProgress();
        if (!TextUtils.isEmpty(data)) {
            try {
                FileCache.storeLicense(Application.getInstance().getSelectedLanguageCode(), data);
                showOfferFragment();

                Dictionary<Integer, String> customVariables = new Hashtable<>();
                customVariables.put(new Integer(GoogleAnalyticsHelper.CUSTOM_VARIABLE_COUNTRY), mSelectTpControl.getSelectedMobileOperatorName());
                customVariables.put(new Integer(GoogleAnalyticsHelper.CUSTOM_VARIABLE_TP), mSelectTpControl.getSelectedTpName());
                customVariables.put(new Integer(GoogleAnalyticsHelper.CUSTOM_VARIABLE_SERVICES), Utils.arrayListToString(mSelectTpControl.getSelectedServicesName()));

                GoogleAnalyticsHelper.sendLog(getString(R.string.ga_select_country_region_lang), getString(R.string.ga_select_action), customVariables);

            } catch (Exception e) {
                e.printStackTrace();
                showNoConnectionMessage();
            }
        } else {
            showOfferFragment();
        }
    }

    private void showOfferFragment() {
        if (mSelectTpControl == null){
            return;
        }
        Intent showOfferIntent = new Intent(SetupNavigationController.ACTION_SHOW_OFFER);
        showOfferIntent.putExtra(OfferFragment.ARG_MOBILE_OPERATOR_ID, mSelectTpControl.getSelectedMobileOperatorId());
        showOfferIntent.putExtra(OfferFragment.ARG_TP_ID, mSelectTpControl.getSelectedTpId());
        showOfferIntent.putExtra(OfferFragment.ARG_SERVICES, Utils.arrayListToString(mSelectTpControl.getSelectedServices()));

        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(showOfferIntent);
    }

    protected void showNoConnectionMessage() {
        Dialog d = new AlertDialog.Builder(getActivity())
                .setMessage(R.string.network_not_available)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        }).setCancelable(true)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                }).create();
        d.show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mSelectTpControl == null){
            return;
        }
        outState.putString(STATE_SELECTED_MOBILE_OPERATOR, mSelectTpControl.getSelectedMobileOperatorId());
        outState.putString(STATE_SELECTED_TP, mSelectTpControl.getSelectedTpId());
        outState.putSerializable(STATE_SELECTED_SERVICES, mSelectTpControl.getSelectedServices());
    }

}
