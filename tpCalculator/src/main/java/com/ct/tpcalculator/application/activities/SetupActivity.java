package com.ct.tpcalculator.application.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.fragments.AboutFragment;
import com.ct.tpcalculator.application.fragments.AddNewOperatorFragment;
import com.ct.tpcalculator.application.fragments.SelectCountryAndRegionFragment;
import com.ct.tpcalculator.application.fragments.SelectTpFragment;
import com.ct.tpcalculator.application.fragments.WelcomeFragment;
import com.ct.tpcalculator.application.listeners.OnFragmentChangedListener;
import com.ct.tpcalculator.application.navigationControllers.SetupNavigationController;
import com.ct.tpcalculator.helpers.CommandExecutionUtil;

public class SetupActivity extends BaseActivity implements OnFragmentChangedListener {

    public final static int ACTIVITY_SETUP = 1001;
    public final static int ACTIVITY_SETTINGS = 1002;

    public final static String SHOW_ABOUT_PAGE = "SHOW_ABOUT_PAGE";

    private final SetupNavigationController mSetupNavigationController = new SetupNavigationController(this);
    private final FileLoadReceiver mFileLoadReceiver = new FileLoadReceiver(SetupActivity.this);

    private TextView mTitle;
    private ImageView mBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        setContentView(R.layout.activity_setup);

        mTitle = (TextView) findViewById(R.id.title_text);
        mTitle.setTypeface(Application.lightTypeface);

        mBackButton = (ImageView) findViewById(R.id.back);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().popBackStack();
            }
        });
        if (savedInstanceState == null) {
            if (getIntent() != null && getIntent().getBooleanExtra(SHOW_ABOUT_PAGE, false)) {
                mSetupNavigationController.showAbout();
            } else {
                mSetupNavigationController.showSelectCountryAndRegionFragment();
            }
        }
        CommandExecutionUtil.requestAllPermissions(this);
    }

    @Override
    public void onBackPressed() {
        Fragment activeFragment = getActiveFragment();
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            SetupActivity.this.setResult(RESULT_CANCELED);
            finish();
        } else if (activeFragment instanceof WelcomeFragment || activeFragment instanceof SelectCountryAndRegionFragment || activeFragment instanceof AboutFragment) {
            SetupActivity.this.setResult(RESULT_CANCELED);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mSetupNavigationController);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mSetupNavigationController, mSetupNavigationController.getIntentFilter());
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(mFileLoadReceiver, new IntentFilter(FileLoadReceiver.ACTION_LOAD_FILE));
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mFileLoadReceiver);
        super.onStop();
    }

    public Fragment getActiveFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            return null;
        }

        String tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    @Override
    public void onFragmentChanged(String fragmentTag, String fragmentTitle) {
        mTitle.setText(fragmentTitle);
        if (fragmentTag.equalsIgnoreCase(AddNewOperatorFragment.TAG)) {
            mBackButton.setVisibility(View.VISIBLE);
            mTitle.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        } else {
            mBackButton.setVisibility(View.GONE);
            mTitle.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        }
    }

    public class FileLoadReceiver extends BroadcastReceiver {

        public final static String ACTION_LOAD_FILE = "com.ct.tpcalculator.ACTION_LOAD_FILE";
        public final static String PARAM_DATA = "com.ct.tpcalculator.PARAM_DATA";
        public final static String PARAM_DATA_TYPE = "com.ct.tpcalculator.DATA_TYPE";

        public final static int DATA_TYPE_COUNTRIES_REGIONS = 2001;
        public final static int DATA_TYPE_REGION = 2002;
        public final static int DATA_TYPE_OFFER = 2003;

        SetupActivity mContext;

        public FileLoadReceiver(SetupActivity context) {
            super();
            mContext = context;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equalsIgnoreCase(ACTION_LOAD_FILE)) {
                String data = intent.getStringExtra(PARAM_DATA);
//            int status = intent.getIntExtra(PARAM_STATUS, 0);
                int dataType = intent.getIntExtra(PARAM_DATA_TYPE, 0);

                Fragment currentFragment = mContext.getActiveFragment();
                if (currentFragment instanceof SelectCountryAndRegionFragment) {
                    if (dataType == DATA_TYPE_COUNTRIES_REGIONS) {
                        ((SelectCountryAndRegionFragment) currentFragment).countriesRegionsDataDownloadFinished(data);
                    } else if (dataType == DATA_TYPE_REGION) {
                        ((SelectCountryAndRegionFragment) currentFragment).regionDataDownloadFinished(data);
                    }
                } else if (currentFragment instanceof SelectTpFragment) {
                    if (dataType == DATA_TYPE_OFFER) {
                        ((SelectTpFragment) currentFragment).offerDataDownloadFinished(data);
                    }
                }
            }

        }
    }

}

