package com.ct.tpcalculator.application.views;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.helpers.Utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by marat on 10/2/2015.
 */
public class SelectPeriod extends LinearLayout {

    private TextView mStartDateTextView;
    private TextView mEndDateTextView;
    private SelectPeriodListener mSelectPeriodListener;

    private Date mStartDate;
    private Date mEndDate;

    public Date getEndDate() {
        return mEndDate;
    }

    public void setEndDate(Date endDate) {
        this.mEndDate = endDate;
        mEndDateTextView.setText(Utils.getFormattedDate_DayMonth(endDate));
    }

    public Date getStartDate() {
        return mStartDate;
    }

    public void setStartDate(Date startDate) {
        this.mStartDate = startDate;
        mStartDateTextView.setText(Utils.getFormattedDate_DayMonth(startDate));
    }

    public SelectPeriod(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LinearLayout rootView = (LinearLayout) inflater.inflate(R.layout.select_period, this, true);

        LinearLayout mainLayout = (LinearLayout) rootView.findViewById(R.id.main_layout);
        mainLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDate();
            }
        });

        TextView periodLabel = (TextView) rootView.findViewById(R.id.period_label);
        periodLabel.setTypeface(Application.condensedTypeface);

        mStartDateTextView = (TextView) rootView.findViewById(R.id.start_date);
        mStartDateTextView.setTypeface(Application.condensedTypeface);

        mEndDateTextView = (TextView) rootView.findViewById(R.id.end_date);
        mEndDateTextView.setTypeface(Application.condensedTypeface);

        TextView deleimeterLabel = (TextView) rootView.findViewById(R.id.delimeter_label);
        deleimeterLabel.setTypeface(Application.condensedTypeface);
    }

    private void selectDate() {
        final Dialog dialog = new Dialog(getContext(), R.style.Theme_SelectDate);
        dialog.setContentView(R.layout.select_date_range);
        TextView selectPeriodLabel = (TextView) dialog.findViewById(R.id.select_period);
        selectPeriodLabel.setTypeface(Application.lightTypeface);

        final DatePicker startDatePicker = (DatePicker) dialog.findViewById(R.id.dpStartDate);
        final DatePicker endDatePicker = (DatePicker) dialog.findViewById(R.id.dpEndDate);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(mStartDate);
        startDatePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        calendar.setTime(mEndDate);
        endDatePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        TextView ok_button = (TextView) dialog.findViewById(R.id.ok_button);
        ok_button.setTypeface(Application.lightTypeface);
        ok_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelectPeriodListener != null) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(startDatePicker.getYear(), startDatePicker.getMonth(), startDatePicker.getDayOfMonth(), 0, 0, 0);
                    Date startDate = calendar.getTime();
                    calendar.set(endDatePicker.getYear(), endDatePicker.getMonth(), endDatePicker.getDayOfMonth(), 23, 59, 59);
                    Date endDate = calendar.getTime();

                    if (startDate.getTime() > endDate.getTime()) {
                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                        alertDialog.setMessage(R.string.min_max_date_exception);
                        alertDialog.setTitle(R.string.exception);
                        alertDialog.show();
                        alertDialog.setNegativeButton(R.string.cancel, null);
                    } else {
                        mStartDate = startDate;
                        mEndDate = endDate;

                        mStartDateTextView.setText(Utils.getFormattedDate_DayMonth(startDate));
                        mEndDateTextView.setText(Utils.getFormattedDate_DayMonth(endDate));
                        mSelectPeriodListener.datesChanged(startDate, endDate);
                    }
                }
                dialog.dismiss();
            }
        });

        TextView cancel_button = (TextView) dialog.findViewById(R.id.cancel_button);
        cancel_button.setTypeface(Application.lightTypeface);
        cancel_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void setSelectPeriodListener(SelectPeriodListener listener) {
        mSelectPeriodListener = listener;
    }

    public interface SelectPeriodListener {
        void datesChanged(Date startDate, Date endDate);
    }
}
