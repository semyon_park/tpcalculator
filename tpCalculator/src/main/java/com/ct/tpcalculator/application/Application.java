package com.ct.tpcalculator.application;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ct.tpcalculator.BuildConfig;
import com.ct.tpcalculator.helpers.GoogleAnalyticsHelper;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.ct.tpcalculator.R;
import com.ct.tpcalculator.calculator.data.*;
import com.ct.tpcalculator.calculator.statisticAgent.callstat.AlarmCallReceiver;
import com.ct.tpcalculator.calculator.statisticAgent.exception.CrashHandler;
import com.ct.tpcalculator.calculator.statisticAgent.msgstat.AlarmMsgReceiver;
import com.ct.tpcalculator.calculator.statisticAgent.netstat.AlarmNetReceiver;
import com.ct.tpcalculator.helpers.FileCache;
import com.ct.tpcalculator.helpers.L;
import com.ct.tpcalculator.helpers.billing.IabHelper;
import com.ct.tpcalculator.helpers.billing.IabResult;
import com.ct.tpcalculator.helpers.billing.Purchase;
import com.ct.tpcalculator.helpers.billing.SkuDetails;
import com.ct.tpcalculator.sync.SyncAdapter;
//import org.apache.http.HttpVersion;
////import org.apache.http.client.HttpClient;
//import org.apache.http.conn.ClientConnectionManager;
//import org.apache.http.conn.scheme.PlainSocketFactory;
//import org.apache.http.conn.scheme.Scheme;
//import org.apache.http.conn.scheme.SchemeRegistry;
//import org.apache.http.conn.ssl.SSLSocketFactory;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
//import org.apache.http.params.BasicHttpParams;
//import org.apache.http.params.HttpParams;
//import org.apache.http.params.HttpProtocolParams;
//import org.apache.http.protocol.HTTP;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by USER on 26.08.2015.
 */
public class Application extends android.app.Application {

    private Tracker mTracker;

    private final static String TAG = Application.class.getName();

    private static Application instance;

    private boolean mIsPremiumAccount;

    public static Application getInstance() {
        return instance;
    }

    private MainData mMainData;

    public static Typeface lightTypeface;

    public static Typeface condensedTypeface;

    public static Typeface regularTypeface;

    private String version;

    private Map<String, SkuDetails> mSkuMap;

    // The helper object
    private IabHelper mIabHelper;

    public IabHelper getIabHelper() {
        return mIabHelper;
    }

    public void setIabHelper(IabHelper iabHelper) {
        mIabHelper = iabHelper;
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     *
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
//            analytics.enableAutoActivityReports(instance);
//            analytics.setLocalDispatchPeriod((int) TimeUnit.MINUTES.toSeconds(5));
//            analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
//            mTracker = analytics.newTracker(getInstance().getString(R.string.ga_tracking_id));
//            mTracker.setAppId(BuildConfig.APPLICATION_ID);
//            mTracker.enableAdvertisingIdCollection(true);


            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
//            GoogleAnalytics.getInstance(this).getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
//            mTracker = analytics.newTracker(getString(R.string.ga_tracking_id));
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

    public MainData getMainData() {
        return mMainData;
    }

    public void setMainData(MainData mainData) {
        this.mMainData = mainData;
    }

    public Map<String, SkuDetails> getSkuMap() {
        return mSkuMap;
    }

    public SkuDetails getSkuById(String id) {
        if (mSkuMap == null)
            return null;

        if (mSkuMap.containsKey(id)) {
            return mSkuMap.get(id);
        }
        return null;
    }

    public void setSkuMap(Map<String, SkuDetails> skuMap) {
        mSkuMap = skuMap;
    }
    // используем только один HttpClient для уменьшения занимаемых ресурсов
//    private HttpClient httpClient;

    public Region getRegion() {
        if (mMainData == null) {
            return null;
        }
        if (mMainData.getCountries() == null || mMainData.getCountries().size() == 0) {
            return null;
        }
        if (mMainData.getCountries().get(0).getRegions() == null || mMainData.getCountries().get(0).getRegions().size() == 0) {
            return null;
        }
        return mMainData.getCountries().get(0).getRegions().get(0);
    }

    public Country getCountry() {
        if (mMainData == null) {
            return null;
        }
        if (mMainData.getCountries() == null || mMainData.getCountries().size() == 0) {
            return null;
        }
        return mMainData.getCountries().get(0);
    }

    public MobileOperator getSelectedMobileOperator() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String mobileOperatorId = sharedPreferences.getString(getApplicationContext().getString(R.string.preferences_selected_mobile_operator_id), "");
        if (mMainData == null || mMainData.getCountries() == null || mMainData.getCountries().size() == 0 ||
                mMainData.getCountries().get(0).getRegions() == null || mMainData.getCountries().get(0).getRegions().size() == 0) {
            return null;
        }
        MobileOperator mobileOperator = mMainData.getCountries().get(0).getRegions().get(0).findMobileOperator(mobileOperatorId);
        if (mobileOperator == null)
            return null;

        return mobileOperator;
    }

    public TariffPlan getSelectedTariffPlan() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String tpId = sharedPreferences.getString(getApplicationContext().getString(R.string.preferences_selected_tp_id), "");

        MobileOperator mobileOperator = getSelectedMobileOperator();
        if (mobileOperator == null)
            return null;

        return mobileOperator.findTariffPlan(tpId);
    }

    public ArrayList<Service> getSelectedServices() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String[] services = sharedPreferences.getString(getApplicationContext().getString(R.string.preferences_selected_services), "").split(";");

        MobileOperator mobileOperator = getSelectedMobileOperator();
        if (mobileOperator == null)
            return null;

        ArrayList<Service> result = new ArrayList<>();
        for (String serviceId : services) {
            Service service = mobileOperator.findService(serviceId);
            if (service != null) {
                result.add(service);
            }
        }
        return result;
    }

    public String getSelectedLanguageCode() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        return sharedPreferences.getString(getApplicationContext().getString(R.string.preferences_selected_language_code), getString(R.string.default_locale));
    }

    public boolean getIsPremiumAccount() {
//        return false;
        return mIsPremiumAccount;
    }

    public void setIsPremiumAccount(boolean isPremiumAccount) {
        mIsPremiumAccount = isPremiumAccount;

        SharedPreferences preferences = getSharedPreferences(getString(R.string.private_preferences_file_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(getString(R.string.preferences_is_app_pro), isPremiumAccount);
        editor.apply();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        SharedPreferences preferences = getSharedPreferences(getString(R.string.private_preferences_file_name), Context.MODE_PRIVATE);
        mIsPremiumAccount = preferences.getBoolean(getString(R.string.preferences_is_app_pro), false);

        regularTypeface = Typeface.createFromAsset(getAssets(), "roboto_regular.ttf");
        lightTypeface = Typeface.createFromAsset(getAssets(), "roboto_light.ttf");
        condensedTypeface = Typeface.createFromAsset(getAssets(), "roboto_condensed_regular.ttf");

        Thread.setDefaultUncaughtExceptionHandler(new CrashHandler(getApplicationContext()));

        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(getApplicationContext()));

        AlarmCallReceiver.setAlarm(getApplicationContext());
        AlarmMsgReceiver.setAlarm(getApplicationContext());
        AlarmNetReceiver.setAlarm(getApplicationContext());

        instance = this;
        FileCache.initCache(this);
        mMainData = FileCache.getMainData();
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    /***
     * Возращает состояние сети - есть или нет доступ в Интернет
     *
     * @return true если есть доступ или невозможно определить
     */
    public static boolean isNetworkAvailable() {
        try {
            ConnectivityManager mConn = (ConnectivityManager) getInstance()
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            return mConn.getActiveNetworkInfo() != null
                    && mConn.getActiveNetworkInfo().isConnected();
        } catch (Throwable t) {
            Log.e(TAG, Log.getStackTraceString(t));
            return true;
        }
    }

//    /**
//     * Возвращает HTTP клиента Apache. При необходимости создает новый.
//     *
//     * @return http client
//     */
//    public HttpClient getHttpClient() {
//        if (httpClient == null)
//            httpClient = createHttpClient();
//        return httpClient;
//    }
//
//    /**
//     * Создаем клиента HTTP для обслуживания запросов. Чтобы запросы можно было
//     * отправлять одновремнно - используем многовходовый менеджер соединений.
//     *
//     * @return http client
//     */
//    private HttpClient createHttpClient() {
//
//        HttpParams params = new BasicHttpParams();
//        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
//        HttpProtocolParams.setContentCharset(params,
//                HTTP.DEFAULT_CONTENT_CHARSET);
//        HttpProtocolParams.setUseExpectContinue(params, true);
//
//        SchemeRegistry schReg = new SchemeRegistry();
//        schReg.register(new Scheme("http", PlainSocketFactory
//                .getSocketFactory(), 80));
//        schReg.register(new Scheme("https",
//                SSLSocketFactory.getSocketFactory(), 443));
//        ClientConnectionManager conMgr = new ThreadSafeClientConnManager(
//                params, schReg);
//
//        return new DefaultHttpClient(conMgr, params);
//    }

    /***
     * Выдает user agent, которым идентифицируется приложение при обращении к
     * серверу (можно использовать для статистики)ы
     *
     * @return
     */
    public String getUserAgent() {
        return getResources().getString(R.string.user_agent) + " " + version;
    }

    public Account getCurrentAccount() {
        final String accountType = getString(R.string.account_type_authority);
        AccountManager am = AccountManager.get(this);
        Account[] accounts = am.getAccountsByType(accountType);
        return accounts.length == 1 ? accounts[0] : null;
    }

    public void requestManualSync(String fileName) {
        if (!isNetworkAvailable()) {
            Toast.makeText(instance, R.string.network_not_available,
                    Toast.LENGTH_LONG).show();
            return;
        }

        Account account = getCurrentAccount();
        if (account != null) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
            bundle.putBoolean(SyncAdapter.ARG_FORCE_SYNC, true);
            bundle.putString(SyncAdapter.ARG_FILE_NAME, fileName);
            ContentResolver.requestSync(account, getString(R.string.account_type_authority), bundle);
        }
    }

    public void addAccount() {
        String username = getString(R.string.account_name);

        final String accountType = getString(R.string.account_type_authority);

        Account account = new Account(username, accountType);

        AccountManager am = AccountManager.get(this);
        Bundle options = new Bundle();
        if (am.addAccountExplicitly(account, "", options)) {
            ContentResolver.setIsSyncable(account, accountType, 1);
            long defaultInterval = 86400;
            if (defaultInterval > 0) {
                ContentResolver.addPeriodicSync(account, accountType, new Bundle(), defaultInterval);
            }
            ContentResolver.setSyncAutomatically(account, accountType, true);
        }
    }

    public boolean isAccountSetup() {
        return getCurrentAccount() != null;
    }

    public void buyProVersion(final FragmentActivity activity, final Fragment fragment, final String sku) {
        IabHelper billingHelper = getIabHelper();
        if (billingHelper == null) {
            return;
        }
        billingHelper.flagEndAsync();

        final String developerPayload = UUID.randomUUID().toString();


        GoogleAnalyticsHelper.sendLog(getString(R.string.ga_subscribe_app), sku);
//        billingHelper.launchPurchaseFlow(activity, sku, 555, new IabHelper.OnIabPurchaseFinishedListener() {
//            @Override
//            public void onIabPurchaseFinished(IabResult result, Purchase info) {
//                if (result.isFailure()) {
//                    L.e("Error purchasing: " + result);
//                    showInfoDialog(activity, result.getMessage());
//                    return;
//                } else if (info.getSku().equals(sku) && info.getDeveloperPayload().equalsIgnoreCase(developerPayload)) {
//                    Application.getInstance().setIsPremiumAccount(true);
//                    if (fragment != null) {
//                        activity.getSupportFragmentManager()
//                                .beginTransaction()
//                                .detach(fragment)
//                                .attach(fragment)
//                                .commit();
//                    }
//                }
//            }
//        }, developerPayload);

        billingHelper.launchPurchaseFlow(activity, sku, IabHelper.ITEM_TYPE_SUBS, null, 10001, new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                if (result.isFailure()) {
                    L.e("Error purchasing: " + result);
                    showInfoDialog(activity, String.format(getString(R.string.subscriptionConfirmFailed), result.getMessage()));
                    return;
                } else if (purchase.getSku().equals(sku) && purchase.getDeveloperPayload().equalsIgnoreCase(developerPayload)) {
                    showInfoDialog(activity, getString(R.string.subscriptionConfirmed));
                    Application.getInstance().setIsPremiumAccount(true);
                    if (fragment != null) {
                        activity.getSupportFragmentManager()
                                .beginTransaction()
                                .detach(fragment)
                                .attach(fragment)
                                .commit();
                    }
                }
                L.e("Item purchased: " + result);
            }
        }, developerPayload);
    }

    public void showInfoDialog(Context context, String message) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent, getTheme())));
        } else {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        }

        dialog.setContentView(R.layout.dialog_info);

        TextView infoTextView = (TextView) dialog.findViewById(R.id.info);
        infoTextView.setTypeface(Application.lightTypeface);
        infoTextView.setText(message);

        Button okButton = (Button) dialog.findViewById(R.id.btnOk);
        okButton.setTypeface(Application.lightTypeface);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
