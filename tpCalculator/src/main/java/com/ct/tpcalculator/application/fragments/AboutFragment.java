package com.ct.tpcalculator.application.fragments;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.navigationControllers.SetupNavigationController;
import com.ct.tpcalculator.helpers.GoogleAnalyticsHelper;

public class AboutFragment extends BaseMainFragment {

    public final static String TAG = AboutFragment.class.getName();

    public static final String ARG_SHOW_NEXT_BUTTON = "ARG_SHOW_NEXT_BUTTON";
    private static final String GA_PAGE_NAME = "AboutFragment";

    private boolean mShowNextButton;

    public static AboutFragment newInstance(boolean showNextButton) {
        AboutFragment fragment = new AboutFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_SHOW_NEXT_BUTTON, showNextButton);
        fragment.setArguments(args);
        return fragment;
    }

    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mShowNextButton = getArguments().getBoolean(ARG_SHOW_NEXT_BUTTON);
        }
        GoogleAnalyticsHelper.sendLog(GA_PAGE_NAME, null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_about, container, false);

        TextView appName = (TextView) root.findViewById(R.id.app_name);
        appName.setTypeface(Application.lightTypeface);

        TextView version = (TextView) root.findViewById(R.id.version);
        version.setTypeface(Application.lightTypeface);

        try {
            PackageInfo pi = Application.getInstance().getPackageManager()
                    .getPackageInfo(Application.getInstance().getPackageName(), 0);
            version.setText(getString(R.string.version, pi.versionName));
        } catch (PackageManager.NameNotFoundException e) {
            version.setText("");
        }

        TextView aboutApplication = (TextView) root.findViewById(R.id.about_application);
        aboutApplication.setTypeface(Application.lightTypeface);

        TextView firstMonth = (TextView) root.findViewById(R.id.first_month);
        firstMonth.setTypeface(Application.lightTypeface);

        TextView firstMonthText = (TextView) root.findViewById(R.id.first_month_text);
        firstMonthText.setTypeface(Application.lightTypeface);
        firstMonthText.setText(Html.fromHtml(getString(R.string.first_month_text)));


        TextView secondMonth = (TextView) root.findViewById(R.id.second_month);
        secondMonth.setTypeface(Application.lightTypeface);

        TextView secondMonthText = (TextView) root.findViewById(R.id.second_month_text);
        secondMonthText.setTypeface(Application.lightTypeface);
        secondMonthText.setText(Html.fromHtml(getString(R.string.second_month_text)));

        final Button continueButton = (Button) root.findViewById(R.id.btnNext);
        continueButton.setTypeface(Application.lightTypeface);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent showSelectCountryAndRegionIntent = new Intent(SetupNavigationController.ACTION_SHOW_SELECT_COUNTRY);
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(showSelectCountryAndRegionIntent);
            }
        });

        View nextContainer = root.findViewById(R.id.nextContainer);
        nextContainer.setVisibility(mShowNextButton ? View.VISIBLE : View.GONE);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener == null) {
            return;
        }
        mListener.onFragmentChanged(TAG, getString(R.string.about));
    }

}
