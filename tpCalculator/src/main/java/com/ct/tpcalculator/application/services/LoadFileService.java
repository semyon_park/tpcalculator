package com.ct.tpcalculator.application.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.ct.tpcalculator.application.activities.SetupActivity;
import com.ct.tpcalculator.loaders.FileLoader;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by marat on 8/28/2015.
 */
public class LoadFileService extends Service {

    private ExecutorService es;
    public static final String PARAM_FILE_PATH = "com.ct.tpcalculator.PARAM_FILE_PATH";

    public void onCreate() {
        super.onCreate();
        es = Executors.newFixedThreadPool(2);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return START_STICKY_COMPATIBILITY;
        }
        String path = intent.getStringExtra(PARAM_FILE_PATH);
        int dataType = intent.getIntExtra(SetupActivity.FileLoadReceiver.PARAM_DATA_TYPE,
                SetupActivity.FileLoadReceiver.DATA_TYPE_COUNTRIES_REGIONS);

        FileLoader mr = new FileLoader(this, path, dataType);
        es.execute(mr);

        return super.onStartCommand(intent, flags, startId);
    }

}
