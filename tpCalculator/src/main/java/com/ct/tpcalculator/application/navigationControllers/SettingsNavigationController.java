package com.ct.tpcalculator.application.navigationControllers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.activities.NotificationSettingsActivity;
import com.ct.tpcalculator.application.fragments.ResultListFragment;
import com.ct.tpcalculator.application.fragments.ResultSettingsFragment;
import com.ct.tpcalculator.application.fragments.ResultTpFragment;
import com.ct.tpcalculator.calculator.data.MobileOperator;
import com.ct.tpcalculator.calculator.data.calculationData.CalculationResult;
import com.ct.tpcalculator.calculator.data.calculationData.PackageToOffer;

public class SettingsNavigationController extends BroadcastReceiver {

    private static final IntentFilter sFilter = new IntentFilter();
    private NotificationSettingsActivity mActivity;

    public final static String ACTION_SHOW_GENERAL_SETTINGS = "com.ct.tpcalculator.ACTION_SHOW_GENERAL_SETTINGS";
    public final static String ACTION_SHOW_NOTIFICATION_SETTINGS = "com.ct.tpcalculator.ACTION_SHOW_NOTIFICATION_SETTINGS";
    public final static String ACTION_SHOW_SELECT_TP_SETTINGS = "com.ct.tpcalculator.ACTION_SHOW_SELECT_TP_SETTINGS";

    static {
        sFilter.addAction(ACTION_SHOW_GENERAL_SETTINGS);
        sFilter.addAction(ACTION_SHOW_NOTIFICATION_SETTINGS);
        sFilter.addAction(ACTION_SHOW_SELECT_TP_SETTINGS);
    }

    public SettingsNavigationController(NotificationSettingsActivity activity) {
        super();
        mActivity = activity;
    }

    public IntentFilter getIntentFilter() {
        return sFilter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (ACTION_SHOW_GENERAL_SETTINGS.equalsIgnoreCase(action)) {

        } else if (ACTION_SHOW_NOTIFICATION_SETTINGS.equalsIgnoreCase(action)) {

        } else if (ACTION_SHOW_SELECT_TP_SETTINGS.equalsIgnoreCase(action)) {

        }
    }

    public void showResultSettings() {
        ResultSettingsFragment resultSettingsFragment = new ResultSettingsFragment();
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(ResultSettingsFragment.TAG)
                .replace(R.id.container, resultSettingsFragment, ResultSettingsFragment.TAG)
                .commitAllowingStateLoss();
    }

    private void showResultListFragment(CalculationResult data) {
        ResultListFragment resultListFragment = ResultListFragment.newInstance(data);
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(ResultListFragment.TAG)
                .replace(R.id.container, resultListFragment, ResultListFragment.TAG)
                .commitAllowingStateLoss();
    }

    private void setActionShowResultTpFragment(MobileOperator mobileOperator, PackageToOffer packageToOffer, String sku) {
        ResultTpFragment resultTpFragment = ResultTpFragment.newInstance(mobileOperator, packageToOffer, sku);
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(ResultTpFragment.TAG)
                .replace(R.id.container, resultTpFragment, ResultTpFragment.TAG)
                .commitAllowingStateLoss();
    }

}

