package com.ct.tpcalculator.application.views;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.activities.BaseActivity;
import com.ct.tpcalculator.application.fragments.AddNewOperatorFragment;
import com.ct.tpcalculator.application.fragments.ResultListFragment;
import com.ct.tpcalculator.application.navigationControllers.ResultNavigationController;
import com.ct.tpcalculator.application.navigationControllers.SetupNavigationController;
import com.ct.tpcalculator.calculator.data.Command;
import com.ct.tpcalculator.calculator.data.Country;
import com.ct.tpcalculator.calculator.data.MobileOperator;
import com.ct.tpcalculator.calculator.data.Region;
import com.ct.tpcalculator.calculator.data.Service;
import com.ct.tpcalculator.calculator.data.TariffPlan;
import com.ct.tpcalculator.helpers.CommandExecutionUtil;
import com.ct.tpcalculator.helpers.Utils;

import java.util.ArrayList;

/**
 * Created by marat on 11/4/2015.
 */
public class SelectTpControl extends LinearLayout {

    private LinearLayout mControlView;
    private LinearLayout mServicesList;
    private TextView mMobileOperatorLabel;
    private LinearLayout mSelectMobileOperator;
    private TextView mTariffPlanLabel;
    private LinearLayout mSelectTariffPlan;
    private TextView mServicesLabel;
    private LinearLayout mSelectServices;

    private String mSelectedMobileOperatorId;
    private String mSelectedTpId;
    private ArrayList<String> mSelectedServices;

    public String getSelectedMobileOperatorId() {
        return mSelectedMobileOperatorId;
    }

    public void setSelectedMobileOperatorId(String selectedMobileOperatorId) {
        mSelectedMobileOperatorId = selectedMobileOperatorId;
    }

    public String getSelectedMobileOperatorName() {
        Region region = Application.getInstance().getRegion();
        if (region == null) {
            return "";
        }
        MobileOperator selectedMobileOperator = region.findMobileOperator(mSelectedMobileOperatorId);
        if (selectedMobileOperator == null){
            return "";
        }
        return  selectedMobileOperator.getTitles().getStringByLanguage("ru");
    }

    public String getSelectedTpId() {
        return mSelectedTpId;
    }

    public String getSelectedTpName(){
        Region region = Application.getInstance().getRegion();
        if (region == null) {
            return "";
        }
        MobileOperator selectedMobileOperator = region.findMobileOperator(mSelectedMobileOperatorId);
        if (selectedMobileOperator == null){
            return "";
        }

        TariffPlan tariffPlan = selectedMobileOperator.findTariffPlan(mSelectedTpId);
        if (tariffPlan == null){
            return "";
        }

        return tariffPlan.getTitles().getStringByLanguage("ru");
    }

    public void setSelectedTpId(String selectedTpId) {
        mSelectedTpId = selectedTpId;
    }

    public ArrayList<String> getSelectedServices() {
        return mSelectedServices;
    }

    public ArrayList<String> getSelectedServicesName(){
        ArrayList<String> services = new ArrayList<>();

        Region region = Application.getInstance().getRegion();
        if (region == null) {
            return services;
        }
        MobileOperator selectedMobileOperator = region.findMobileOperator(mSelectedMobileOperatorId);
        if (selectedMobileOperator == null){
            return services;
        }

        for (Service service :selectedMobileOperator.getServices()) {
            if (mSelectedServices.contains(service.getId())){
                services.add(service.getTitles().getStringByLanguage("ru"));
            }
        }
        return services;
    }

    public void setSelectedServices(ArrayList<String> selectedServices) {
        mSelectedServices = selectedServices;
    }

    public SelectTpControl(Context context, AttributeSet attrs) {
        super(context, attrs);

        mSelectedServices = new ArrayList<>();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mControlView = (LinearLayout) inflater.inflate(R.layout.select_tp_control, this, true);

        TextView mobileOperator = (TextView) mControlView.findViewById(R.id.mobile_operator);
        mobileOperator.setTypeface(Application.lightTypeface);

        TextView tp = (TextView) mControlView.findViewById(R.id.tp);
        tp.setTypeface(Application.lightTypeface);

        TextView services = (TextView) mControlView.findViewById(R.id.services);
        services.setTypeface(Application.lightTypeface);

        mServicesList = (LinearLayout) mControlView.findViewById(R.id.services_list);

        mMobileOperatorLabel = (TextView) mControlView.findViewById(R.id.mobile_operator_label);
        mMobileOperatorLabel.setTypeface(Application.lightTypeface);

        mSelectMobileOperator = (LinearLayout) mControlView.findViewById(R.id.select_mobile_operator);
        mSelectMobileOperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectMobileOperatorClicked();
            }
        });

        mTariffPlanLabel = (TextView) mControlView.findViewById(R.id.tp_label);
        mTariffPlanLabel.setTypeface(Application.lightTypeface);

        mSelectTariffPlan = (LinearLayout) mControlView.findViewById(R.id.select_tp);
        mSelectTariffPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTariffPlanClicked();
            }
        });

        mServicesLabel = (TextView) mControlView.findViewById(R.id.services_label);
        mServicesLabel.setTypeface(Application.lightTypeface);
        mSelectServices = (LinearLayout) mControlView.findViewById(R.id.select_services);
        mSelectServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectServicesClicked();
            }
        });

//        if (savedInstanceState == null) {
//            mobileOperatorChanged(false);
//        } else {
//            mobileOperatorChanged(true);
//            if (mSelectedServices != null) {
//                fillSelectedServicesListView();
//            }
//        }
    }

    public void mobileOperatorChanged(boolean isLoading) {
        Region region = Application.getInstance().getRegion();
        if (region == null) {
            return;
        }
        if (!isLoading) {
            mSelectedTpId = "";
            mSelectedServices.clear();
        }

        mMobileOperatorLabel.setText(R.string.selectMobileOperator);
        mTariffPlanLabel.setText(R.string.selectTariffPlan);
        mServicesLabel.setText(R.string.selectServices);
        mServicesList.removeAllViews();

        MobileOperator selectedMobileOperator = region.findMobileOperator(mSelectedMobileOperatorId);
        if (selectedMobileOperator == null) {
            if (region.getMobileOperators().size() == 0) {
                return;
            }
            selectedMobileOperator = region.getMobileOperators().get(0);
            mSelectedMobileOperatorId = selectedMobileOperator.getId();
        }

        mMobileOperatorLabel.setText(selectedMobileOperator.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));

        TariffPlan tariffPlan = selectedMobileOperator.findTariffPlan(mSelectedTpId);

        if (tariffPlan == null) {
            if (selectedMobileOperator.getTariffPlans().size() == 0) {
                return;
            }
            tariffPlan = selectedMobileOperator.getTariffPlans().get(0);
            mSelectedTpId = tariffPlan.getId();
        }

        mTariffPlanLabel.setText(tariffPlan.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));
    }

    public void fillSelectedServicesListView() {
        if (mSelectedServices == null) {
            return;
        }

        Region region = Application.getInstance().getRegion();
        if (region == null) {
            return;
        }


        MobileOperator mobileOperator = region.findMobileOperator(mSelectedMobileOperatorId);

        if (mobileOperator == null) {
            return;
        }

        TariffPlan tariffPlan = mobileOperator.findTariffPlan(mSelectedTpId);

        if (tariffPlan == null) {
            return;
        }

        final ArrayList<Service> availableServices = mobileOperator.getAvailableServices(tariffPlan.getAvailableServices());
        fillSelectedServicesListView(availableServices);
    }

    private void fillSelectedServicesListView(ArrayList<Service> availableServices) {
        if (mSelectedServices.size() > 0) {
            Service service = Utils.findServiceById(availableServices, mSelectedServices.get(0));
            if (service != null && service.getTitles() != null) {
                mServicesLabel.setText(service.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));
            }
            if (mSelectedServices.size() > 1) {

                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                for (int i = 1; i < mSelectedServices.size(); i++) {
                    service = Utils.findServiceById(availableServices, mSelectedServices.get(i));

                    TextView serviceItem = (TextView) inflater.inflate(R.layout.service_list_item, mServicesList, false);
                    serviceItem.setTypeface(Application.lightTypeface);
                    if (service != null && service.getTitles() != null) {
                        serviceItem.setText(service.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));
                    }
                    mServicesList.addView(serviceItem);
                }
            }
        }
    }

    private void selectMobileOperatorClicked() {
        Region region = Application.getInstance().getRegion();
        if (region == null) {
            return;
        }

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent, getContext().getTheme())));
        } else {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        }
        dialog.setContentView(R.layout.dialog_select_item);
        TextView titleTextView = (TextView) dialog.findViewById(R.id.title);
        titleTextView.setText(getContext().getString(R.string.selectMobileOperator));
        titleTextView.setTypeface(Application.lightTypeface);

        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.content);
        int i = 0;
        for (final MobileOperator mobileOperator : region.getMobileOperators()) {
            RadioButton rbtnMobileOperator = new RadioButton(getContext());
            rbtnMobileOperator.setText(mobileOperator.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));
            rbtnMobileOperator.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_seventeen));

            ColorStateList colorStateList;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                colorStateList = new ColorStateList(new int[][]{
                        new int[]{android.R.attr.state_pressed},
                        new int[]{}
                },
                        new int[]{
                                getResources().getColor(R.color.red_color, getContext().getTheme()),
                                getResources().getColor(R.color.text_color_white, getContext().getTheme())
                        });
            } else {
                colorStateList = new ColorStateList(new int[][]{
                        new int[]{android.R.attr.state_pressed},
                        new int[]{}
                },
                        new int[]{
                                getResources().getColor(R.color.red_color),
                                getResources().getColor(R.color.text_color_white)
                        });
            }
            rbtnMobileOperator.setTextColor(colorStateList);
            rbtnMobileOperator.setButtonDrawable(R.drawable.radio_selector);
            rbtnMobileOperator.setTypeface(Application.lightTypeface);
            rbtnMobileOperator.setTag(mobileOperator);
            rbtnMobileOperator.setId(i);

            RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.bottomMargin = Utils.convertDipToPixels(getContext(), getResources().getDimension(R.dimen.half_margin));

            radioGroup.addView(rbtnMobileOperator,layoutParams);

            if (mobileOperator.getId().equalsIgnoreCase(mSelectedMobileOperatorId) || i == 0) {
                rbtnMobileOperator.setChecked(true);
            }

            rbtnMobileOperator.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectedMobileOperatorId = mobileOperator.getId();
                    mobileOperatorChanged(false);
                    dialog.dismiss();
                }
            });

            i += 1;
        }

        addNewMobileOperator(dialog, radioGroup);

        ImageView cancelButton = (ImageView) dialog.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void addNewMobileOperator(final Dialog dialog, RadioGroup radioGroup){
        RadioButton rbtnMobileOperator = new RadioButton(getContext());
        rbtnMobileOperator.setText(getContext().getString(R.string.addNewMobileOperator));
        rbtnMobileOperator.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_seventeen));

        ColorStateList colorStateList;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            colorStateList = new ColorStateList(new int[][]{
                    new int[]{android.R.attr.state_pressed},
                    new int[]{}
            },
                    new int[]{
                            getResources().getColor(R.color.red_color, getContext().getTheme()),
                            getResources().getColor(R.color.text_color_white, getContext().getTheme())
                    });
        } else {
            colorStateList = new ColorStateList(new int[][]{
                    new int[]{android.R.attr.state_pressed},
                    new int[]{}
            },
                    new int[]{
                            getResources().getColor(R.color.red_color),
                            getResources().getColor(R.color.text_color_white)
                    });
        }
        rbtnMobileOperator.setTextColor(colorStateList);
        rbtnMobileOperator.setButtonDrawable(R.drawable.radio_selector);
        rbtnMobileOperator.setTypeface(Application.lightTypeface);

        RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.bottomMargin = Utils.convertDipToPixels(getContext(), getResources().getDimension(R.dimen.half_margin));

        radioGroup.addView(rbtnMobileOperator, layoutParams);

        rbtnMobileOperator.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                Country country = Application.getInstance().getCountry();
                if (country == null) {
                    return;
                }
                Region region = Application.getInstance().getRegion();
                if (region == null) {
                    return;
                }

                dialog.dismiss();

                Intent addNewOperatorIntent = new Intent(SetupNavigationController.ACTION_SHOW_ADD_NEW_OPERATOR);
                addNewOperatorIntent.putExtra(AddNewOperatorFragment.ARG_COUNTRY_NAME, country.getTitles().getStringByLanguage("ru"));
                addNewOperatorIntent.putExtra(AddNewOperatorFragment.ARG_REGION_NAME, region.getTitles().getStringByLanguage("ru"));
                addNewOperatorIntent.putExtra(AddNewOperatorFragment.ARG_TITLE, getContext().getString(R.string.addNewMobileOperator));
                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(addNewOperatorIntent);
            }
        });
    }

    private void selectTariffPlanClicked() {
        Region region = Application.getInstance().getRegion();
        if (region == null) {
            return;
        }

        final MobileOperator mobileOperator = region.findMobileOperator(mSelectedMobileOperatorId);

        if (mobileOperator == null) {
            return;
        }

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent, getContext().getTheme())));
        } else {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        }
        dialog.setContentView(R.layout.dialog_select_tp);
        TextView titleTextView = (TextView) dialog.findViewById(R.id.title);
        titleTextView.setTypeface(Application.lightTypeface);
        titleTextView.setText(getContext().getString(R.string.selectTariffPlan));

        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.content);
        int i = 0;

        for (final TariffPlan tariffPlan : mobileOperator.getTariffPlans()) {
            RadioButton rbtnTariffPlan = new RadioButton(getContext());
            rbtnTariffPlan.setText(tariffPlan.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));
            rbtnTariffPlan.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_seventeen));
            rbtnTariffPlan.setButtonDrawable(R.drawable.radio_selector);
//            rbtnTariffPlan.setPadding((int)getResources().getDimension(R.dimen.margin), rbtnTariffPlan.getPaddingTop(),
//                    rbtnTariffPlan.getPaddingRight(), rbtnTariffPlan.getPaddingBottom());
            ColorStateList colorStateList;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                colorStateList = new ColorStateList(new int[][]{
                        new int[]{android.R.attr.state_pressed},
                        new int[]{}
                },
                        new int[]{
                                getResources().getColor(R.color.red_color, getContext().getTheme()),
                                getResources().getColor(R.color.text_color_white, getContext().getTheme())
                        });
            } else {
                colorStateList = new ColorStateList(new int[][]{
                        new int[]{android.R.attr.state_pressed},
                        new int[]{}
                },
                        new int[]{
                                getResources().getColor(R.color.red_color),
                                getResources().getColor(R.color.text_color_white)
                        });
            }
            rbtnTariffPlan.setTextColor(colorStateList);
            rbtnTariffPlan.setTypeface(Application.lightTypeface);
            rbtnTariffPlan.setTag(tariffPlan);
            rbtnTariffPlan.setId(i);

            RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.bottomMargin = Utils.convertDipToPixels(getContext(), getResources().getDimension(R.dimen.half_margin));

            radioGroup.addView(rbtnTariffPlan, layoutParams);

            if (tariffPlan.getId().equalsIgnoreCase(mSelectedTpId) || i == 0) {
                rbtnTariffPlan.setChecked(true);
            }
            rbtnTariffPlan.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectedTpId = tariffPlan.getId();
                    tariffPlanChanged();
                    dialog.dismiss();
                }
            });
            i += 1;
        }

        Button recognizeTp = (Button) dialog.findViewById(R.id.btnRecognizeTp);
        recognizeTp.setTypeface(Application.lightTypeface);
        int recognizeTpVisibility;
        if (mobileOperator.getCommands() == null) {
            recognizeTpVisibility = View.GONE;
        } else {
            Command command = mobileOperator.getCommands().getFirstCommandByCommandType(Command.TYPE_GET_TARIFF_PLAN);
            if (command == null || command.getBaseCommands() == null || command.getBaseCommands().isEmpty()) {
                recognizeTpVisibility = View.GONE;
            } else {
                recognizeTpVisibility = View.VISIBLE;
            }
        }
        recognizeTp.setVisibility(recognizeTpVisibility);
        recognizeTp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mobileOperator.getCommands() == null || mobileOperator.getCommands().isEmpty()) {
                    return;
                }
                Command command = mobileOperator.getCommands().getFirstCommandByCommandType(Command.TYPE_GET_TARIFF_PLAN);
                if (command != null) {
                    recognizeUserTpClicked(command);
                }
            }
        });

        ImageView cancelButton = (ImageView) dialog.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void tariffPlanChanged() {
        Region region = Application.getInstance().getRegion();
        if (region == null) {
            return;
        }

        mTariffPlanLabel.setText("");
        mSelectedServices.clear();
        mServicesLabel.setText(R.string.selectServices);
        mServicesList.removeAllViews();

        MobileOperator selectedMobileOperator = region.findMobileOperator(mSelectedMobileOperatorId);
        if (selectedMobileOperator == null) {
            return;
        }

        TariffPlan tariffPlan = selectedMobileOperator.findTariffPlan(mSelectedTpId);
        if (tariffPlan == null) {
            return;
        }

        mTariffPlanLabel.setText(tariffPlan.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));
    }

    private void selectServicesClicked() {
        Region region = Application.getInstance().getRegion();
        if (region == null) {
            return;
        }

        MobileOperator mobileOperator = region.findMobileOperator(mSelectedMobileOperatorId);

        if (mobileOperator == null) {
            return;
        }

        TariffPlan tariffPlan = mobileOperator.findTariffPlan(mSelectedTpId);

        if (tariffPlan == null) {
            return;
        }

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent, getContext().getTheme())));
        } else {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        }
        dialog.setContentView(R.layout.dialog_select_items);
        TextView titleTextView = (TextView) dialog.findViewById(R.id.title);
        titleTextView.setTypeface(Application.lightTypeface);
        titleTextView.setText(getContext().getString(R.string.selectServices));

        final LinearLayout containerLayout = (LinearLayout) dialog.findViewById(R.id.content);
        int i = 0;

        final ArrayList<Service> availableServices = mobileOperator.getAvailableServices(tariffPlan.getAvailableServices());
        for (Service service : availableServices) {
            CheckBox chbService = new CheckBox(getContext());
            chbService.setText(service.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));
            chbService.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_seventeen));

//            chbService.setPadding((int) getResources().getDimension(R.dimen.margin), chbService.getPaddingTop(),
//                    chbService.getPaddingRight(), chbService.getPaddingBottom());
            ColorStateList colorStateList;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                colorStateList = new ColorStateList(new int[][]{
                        new int[]{android.R.attr.state_pressed},
                        new int[]{}
                },
                        new int[]{
                                getResources().getColor(R.color.red_color, getContext().getTheme()),
                                getResources().getColor(R.color.text_color_white, getContext().getTheme())
                        });
            } else {
                colorStateList = new ColorStateList(new int[][]{
                        new int[]{android.R.attr.state_pressed},
                        new int[]{}
                },
                        new int[]{
                                getResources().getColor(R.color.red_color),
                                getResources().getColor(R.color.text_color_white)
                        });
            }
            chbService.setTextColor(colorStateList);
            chbService.setTypeface(Application.lightTypeface);
            chbService.setButtonDrawable(R.drawable.check_box_selector);
            chbService.setTag(service);
            chbService.setId(i);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            int marginInPx =  Utils.convertDipToPixels(getContext(), getResources().getDimension(R.dimen.half_margin));
            layoutParams.bottomMargin = marginInPx;

            containerLayout.addView(chbService, layoutParams);

            if (mSelectedServices.contains(service.getId())) {
                chbService.setChecked(true);
            }
            i += 1;
        }

        Button okButton = (Button) dialog.findViewById(R.id.btnOk);
        okButton.setTypeface(Application.lightTypeface);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedServices.clear();
                mServicesList.removeAllViews();
                mServicesLabel.setText(R.string.selectServices);

                for (int i = 0; i < containerLayout.getChildCount(); i++) {
                    View childView = containerLayout.getChildAt(i);
                    if (childView instanceof CheckBox) {
                        Service service = (Service) childView.getTag();
                        if (((CheckBox) childView).isChecked()) {
                            mSelectedServices.add(service.getId());
                        }
                    }
                }

                fillSelectedServicesListView(availableServices);

                dialog.dismiss();
            }
        });

        ImageView cancelButton = (ImageView) dialog.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void recognizeUserTpClicked(final Command command) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent, getContext().getTheme())));
        } else {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        }
        dialog.setContentView(R.layout.dialog_question_yes_no);

        TextView titleTextView = (TextView) dialog.findViewById(R.id.title);
        titleTextView.setTypeface(Application.lightTypeface);
        titleTextView.setVisibility(GONE);

        TextView infoTextView = (TextView) dialog.findViewById(R.id.info);
        infoTextView.setTypeface(Application.lightTypeface);
        String description = getContext().getString(R.string.send_request_info);
        infoTextView.setText(description);

        Button yesButton = (Button) dialog.findViewById(R.id.btnYes);
        yesButton.setTypeface(Application.lightTypeface);
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommandExecutionUtil.getInstance().executeOfflineCommand((BaseActivity) getContext(), command, getContext().getString(R.string.ga_get_tp));
                dialog.dismiss();
            }
        });

        Button noButton = (Button) dialog.findViewById(R.id.btnNo);
        noButton.setTypeface(Application.lightTypeface);
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }
}
