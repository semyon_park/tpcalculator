package com.ct.tpcalculator.application.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.activities.NotificationSettingsActivity;
import com.ct.tpcalculator.application.activities.SetupActivity;
import com.ct.tpcalculator.helpers.GoogleAnalyticsHelper;

public class GeneralSettingsFragment extends BaseMainFragment {

    public final static String TAG = GeneralSettingsFragment.class.getName();

    private static final String GA_PAGE_NAME = "SettingsFragment";

    public static GeneralSettingsFragment newInstance() {
        return new GeneralSettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GoogleAnalyticsHelper.sendLog(GA_PAGE_NAME, null);
    }

    public GeneralSettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_settings_general, container, false);

        LinearLayout settingsNotification = (LinearLayout) root.findViewById(R.id.settings_notifications);
        settingsNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(getActivity(), NotificationSettingsActivity.class));
            }
        });

        TextView notificationSettingsTitle = (TextView)root.findViewById(R.id.settings_notifications_title);
        notificationSettingsTitle.setTypeface(Application.lightTypeface);

        LinearLayout selectTp = (LinearLayout)root.findViewById(R.id.settings_general);
        selectTp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SetupActivity.class);
                intent.putExtra("requestCode", SetupActivity.ACTIVITY_SETTINGS);
                getActivity().startActivityForResult(intent, SetupActivity.ACTIVITY_SETTINGS);
            }
        });

        TextView selectTpTitle = (TextView)root.findViewById(R.id.general_settings_title);
        selectTpTitle.setTypeface(Application.lightTypeface);
        return root;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mListener == null){
            return;
        }
        mListener.onFragmentChanged(TAG, getString(R.string.recommended_tp));
    }

}
