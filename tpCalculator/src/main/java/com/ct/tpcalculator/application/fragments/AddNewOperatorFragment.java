package com.ct.tpcalculator.application.fragments;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.helpers.GoogleAnalyticsHelper;

import java.util.Dictionary;
import java.util.Hashtable;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddNewOperatorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddNewOperatorFragment extends BaseMainFragment {

    public static String TAG = AddNewOperatorFragment.class.getName();

    public static final String ARG_COUNTRY_NAME = "country_name";
    public static final String ARG_REGION_NAME = "region_name";
    public static final String ARG_TITLE = "title";

    private String mCountryName;
    private String mRegionName;
    private String mTitle;

    private EditText mRegionEditText;
    private EditText mMobileOperatorEditText;

    public AddNewOperatorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param countryName Country name.
     * @return A new instance of fragment AddNewOperatorFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddNewOperatorFragment newInstance(String countryName, String regionName, String title) {
        AddNewOperatorFragment fragment = new AddNewOperatorFragment();
        Bundle args = new Bundle();
        args.putString(ARG_COUNTRY_NAME, countryName);
        args.putString(ARG_REGION_NAME, regionName);
        args.putString(ARG_TITLE, title);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCountryName = getArguments().getString(ARG_COUNTRY_NAME);
            mRegionName = getArguments().getString(ARG_REGION_NAME);
            mTitle = getArguments().getString(ARG_TITLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_add_new_operator, container, false);

        TextView descriptionTextView = (TextView) root.findViewById(R.id.description);
        descriptionTextView.setTypeface(Application.lightTypeface);

        TextView regionTextView = (TextView) root.findViewById(R.id.region_label);
        regionTextView.setTypeface(Application.lightTypeface);

        mRegionEditText = (EditText) root.findViewById(R.id.region_title);
        mRegionEditText.setTypeface(Application.lightTypeface);
        mRegionEditText.setText(mRegionName);

        TextView mobileOperatorTextView = (TextView) root.findViewById(R.id.mobile_operator_label);
        mobileOperatorTextView.setTypeface(Application.lightTypeface);

        mMobileOperatorEditText = (EditText) root.findViewById(R.id.mobile_operator);
        mMobileOperatorEditText.setTypeface(Application.lightTypeface);

        Button sendButton = (Button) root.findViewById(R.id.send_button);
        sendButton.setTypeface(Application.lightTypeface);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String region = mRegionEditText.getText().toString();
                String mobileOperator = mMobileOperatorEditText.getText().toString();

                if (TextUtils.isEmpty(region) || TextUtils.isEmpty(mobileOperator)) {
                    Application.getInstance().showInfoDialog(getContext(), getString(R.string.region_and_operator_cannot_be_null));
                } else {
                    Dictionary<Integer, String> customVariables = new Hashtable<>();
                    customVariables.put(new Integer(GoogleAnalyticsHelper.CUSTOM_VARIABLE_COUNTRY), mCountryName);
                    customVariables.put(new Integer(GoogleAnalyticsHelper.CUSTOM_VARIABLE_REGION), region);
                    customVariables.put(new Integer(GoogleAnalyticsHelper.CUSTOM_VARIABLE_OPERATOR), mobileOperator);

                    GoogleAnalyticsHelper.sendLog(getString(R.string.ga_add_region_operator), getString(R.string.ga_send_action), customVariables);


                    final Dialog dialog = new Dialog(getContext());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent, getActivity().getTheme())));
                    } else {
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
                    }

                    dialog.setContentView(R.layout.dialog_info);

                    TextView infoTextView = (TextView) dialog.findViewById(R.id.info);
                    infoTextView.setTypeface(Application.lightTypeface);
                    infoTextView.setText(getString(R.string.thank_you_for_response));

                    Button okButton = (Button) dialog.findViewById(R.id.btnOk);
                    okButton.setTypeface(Application.lightTypeface);
                    okButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    });
                    dialog.show();
                }

            }
        });
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.onFragmentChanged(TAG, mTitle);
    }
}
