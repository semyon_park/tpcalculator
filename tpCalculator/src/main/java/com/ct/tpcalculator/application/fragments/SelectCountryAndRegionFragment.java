package com.ct.tpcalculator.application.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.activities.SetupActivity;
import com.ct.tpcalculator.application.navigationControllers.SetupNavigationController;
import com.ct.tpcalculator.application.services.LoadFileService;
import com.ct.tpcalculator.application.views.ProgressIndicatorControl;
import com.ct.tpcalculator.application.views.RotateLayout;
import com.ct.tpcalculator.calculator.data.Country;
import com.ct.tpcalculator.calculator.data.Language;
import com.ct.tpcalculator.calculator.data.Languages;
import com.ct.tpcalculator.calculator.data.MainData;
import com.ct.tpcalculator.calculator.data.Region;
import com.ct.tpcalculator.helpers.FileCache;
import com.ct.tpcalculator.helpers.GoogleAnalyticsHelper;
import com.ct.tpcalculator.helpers.Utils;
import com.ct.tpcalculator.sync.SyncAdapter;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * A placeholder fragment containing a simple view.
 */
public class SelectCountryAndRegionFragment extends BaseMainFragment {

    public static String TAG = SelectCountryAndRegionFragment.class.getName();
    private static final String GA_PAGE_NAME = "SelectCountryAndRegionFragment";

    private static final String STATE_SELECTED_COUNTRY = "STATE_SELECTED_COUNTRY";
    private static final String STATE_SELECTED_REGION = "STATE_SELECTED_REGION";
    private static final String STATE_SELECTED_LANGUAGE = "STATE_SELECTED_LANGUAGE";

    private ViewGroup mData;
    private ProgressIndicatorControl mProgressControl;

    private TextView mCountryLabel;
    private TextView mRegionLabel;
    private TextView mLanguageLabel;

    private LinearLayout mSelectCountry;
    private LinearLayout mSelectRegion;
    private LinearLayout mSelectLanguage;

    private Button mNextButton;

    private static MainData mCountriesRegions;

    private String mSelectedCountryId = "";
    private String mSelectedRegionId = "";
    private String mSelectedLanguageId = "";

    private boolean isLoading;

    private String mDefaultLocale;

    public SelectCountryAndRegionFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDefaultLocale = getString(R.string.default_locale);
        if (savedInstanceState != null) {
            mSelectedCountryId = savedInstanceState.getString(STATE_SELECTED_COUNTRY);
            mSelectedRegionId = savedInstanceState.getString(STATE_SELECTED_REGION);
            mSelectedLanguageId = savedInstanceState.getString(STATE_SELECTED_LANGUAGE);
        }
        GoogleAnalyticsHelper.sendLog(GA_PAGE_NAME, null);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCountriesRegions == null) {
            downloadCountryRegionsLanguages();
        } else {
            isLoading = true;
            showData(mCountriesRegions);
            isLoading = false;
            hideProgress();
        }
        mListener.onFragmentChanged(TAG, getString(R.string.app_name));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_country_region, container, false);

        mData = (ViewGroup) root.findViewById(R.id.data);
        mProgressControl = (ProgressIndicatorControl) root.findViewById(R.id.progress);

        mNextButton = (Button) root.findViewById(R.id.btnNext);
        mNextButton.setTypeface(Application.lightTypeface);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCountriesRegions == null) {
                    return;
                }

                Country country = mCountriesRegions.getCountryById(mSelectedCountryId);
                if (country == null) {
                    return;
                }

                Region region = country.getRegionById(mSelectedRegionId);
                if (region == null) {
                    return;
                }

                downloadRegionData(region.getFileName());
            }
        });

        TextView countryTitle = (TextView) root.findViewById(R.id.country_title);
        countryTitle.setTypeface(Application.lightTypeface);

        mCountryLabel = (TextView) root.findViewById(R.id.country_label);
        mCountryLabel.setTypeface(Application.lightTypeface);

        mSelectCountry = (LinearLayout) root.findViewById(R.id.select_country);
        mSelectCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectCountryButtonClicked();
            }
        });

        TextView regionTitle = (TextView) root.findViewById(R.id.region_title);
        regionTitle.setTypeface(Application.lightTypeface);

        mRegionLabel = (TextView) root.findViewById(R.id.region_label);
        mRegionLabel.setTypeface(Application.lightTypeface);

        mSelectRegion = (LinearLayout) root.findViewById(R.id.select_region);
        mSelectRegion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectRegionButtonClicked();
            }
        });

        TextView languageTitle = (TextView) root.findViewById(R.id.language_title);
        languageTitle.setTypeface(Application.lightTypeface);

        mLanguageLabel = (TextView) root.findViewById(R.id.language_label);
        mLanguageLabel.setTypeface(Application.lightTypeface);

        mSelectLanguage = (LinearLayout) root.findViewById(R.id.select_language);
        mSelectLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectLanguageButtonClicked();
            }
        });

        TextView progressMessage = (TextView) root.findViewById(R.id.progressMessage);
        progressMessage.setTypeface(Application.lightTypeface);

        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        IntentFilter filter = new IntentFilter();
        filter.addAction(SyncAdapter.ACTION_SYNC_COMPLETE);
        getActivity().registerReceiver(mReceiver, filter);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getActivity().unregisterReceiver(mReceiver);
    }

    private void selectCountryButtonClicked() {
        if (mCountriesRegions == null || mCountriesRegions.getCountries() == null) {
            return;
        }

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent, getActivity().getTheme())));
        } else {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        }

        dialog.setContentView(R.layout.dialog_select_item);
        TextView titleTextView = (TextView) dialog.findViewById(R.id.title);
        titleTextView.setTypeface(Application.lightTypeface);
        titleTextView.setText(getString(R.string.selectCountry));

        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.content);
        int i = 0;
        for (final Country country : mCountriesRegions.getCountries()) {
            RadioButton rbtnCountry = new RadioButton(getActivity());
            rbtnCountry.setText(country.getTitles().getStringByLanguage(mDefaultLocale));
//            rbtnCountry.setPadding((int) getResources().getDimension(R.dimen.margin), rbtnCountry.getPaddingTop(),
//                    rbtnCountry.getPaddingRight(), rbtnCountry.getPaddingBottom());
            rbtnCountry.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_seventeen));
            ColorStateList colorStateList;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                colorStateList = new ColorStateList(new int[][]{
                        new int[]{android.R.attr.state_pressed},
                        new int[]{}
                },
                        new int[]{
                                getResources().getColor(R.color.red_color, getActivity().getTheme()),
                                getResources().getColor(R.color.text_color_white, getActivity().getTheme())
                        });
            } else {
                colorStateList = new ColorStateList(new int[][]{
                        new int[]{android.R.attr.state_pressed},
                        new int[]{}
                },
                        new int[]{
                                getResources().getColor(R.color.red_color),
                                getResources().getColor(R.color.text_color_white)
                        });
            }

            rbtnCountry.setTextColor(colorStateList);
            rbtnCountry.setTypeface(Application.lightTypeface);
            rbtnCountry.setButtonDrawable(R.drawable.radio_selector);
            rbtnCountry.setTag(country);
            rbtnCountry.setId(i);

            RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.bottomMargin = Utils.convertDipToPixels(getContext(), getResources().getDimension(R.dimen.half_margin));

            radioGroup.addView(rbtnCountry,layoutParams);

            if (i == 0 && !isLoading) {
                rbtnCountry.setChecked(true);
            } else if (country.getId().equalsIgnoreCase(mSelectedCountryId)) {
                rbtnCountry.setChecked(true);
            }
            rbtnCountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectedCountryId = country.getId();
                    selectedCountryChanged();
                    dialog.dismiss();
                }
            });
            i += 1;
        }
        ImageView cancelButton = (ImageView) dialog.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void selectRegionButtonClicked() {
        if (mCountriesRegions == null) {
            return;
        }
        Country country = mCountriesRegions.getCountryById(mSelectedCountryId);

        if (country == null) {
            return;
        }

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent, getActivity().getTheme())));
        } else {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        }
        dialog.setContentView(R.layout.dialog_select_item);
        TextView titleTextView = (TextView) dialog.findViewById(R.id.title);
        titleTextView.setTypeface(Application.lightTypeface);
        titleTextView.setText(getString(R.string.selectRegion));

        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.content);
        int i = 0;

        for (final Region region : country.getRegions()) {
            RadioButton rbtnRegion = new RadioButton(getActivity());
            rbtnRegion.setText(region.getTitles().getStringByLanguage(mDefaultLocale));
            rbtnRegion.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_seventeen));
//            rbtnRegion.setPadding((int) getResources().getDimension(R.dimen.margin), rbtnRegion.getPaddingTop(),
//                    rbtnRegion.getPaddingRight(), rbtnRegion.getPaddingBottom());
            ColorStateList colorStateList;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                colorStateList = new ColorStateList(new int[][]{
                        new int[]{android.R.attr.state_pressed},
                        new int[]{}
                },
                        new int[]{
                                getResources().getColor(R.color.red_color, getActivity().getTheme()),
                                getResources().getColor(R.color.text_color_white, getActivity().getTheme())
                        });
            } else {
                colorStateList = new ColorStateList(new int[][]{
                        new int[]{android.R.attr.state_pressed},
                        new int[]{}
                },
                        new int[]{
                                getResources().getColor(R.color.red_color),
                                getResources().getColor(R.color.text_color_white)
                        });
            }
            rbtnRegion.setTextColor(colorStateList);
            rbtnRegion.setTypeface(Application.lightTypeface);
            rbtnRegion.setButtonDrawable(R.drawable.radio_selector);
            rbtnRegion.setTag(region);
            rbtnRegion.setId(i);

            RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.bottomMargin = Utils.convertDipToPixels(getContext(), getResources().getDimension(R.dimen.half_margin));

            radioGroup.addView(rbtnRegion, layoutParams);
            if (i == 0 && !isLoading) {
                rbtnRegion.setChecked(true);
            } else if (region.getId().equalsIgnoreCase(mSelectedRegionId)) {
                rbtnRegion.setChecked(true);
            }
            rbtnRegion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectedRegionId = region.getId();
                    selectedRegionChanged();
                    dialog.dismiss();
                }
            });
            i += 1;
        }

        addNewMobileOperator(dialog, radioGroup);

        ImageView cancelButton = (ImageView) dialog.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void addNewMobileOperator(final Dialog dialog, RadioGroup radioGroup) {
        RadioButton rbtnMobileOperator = new RadioButton(getContext());
        rbtnMobileOperator.setText(getContext().getString(R.string.addNewRegion));
        rbtnMobileOperator.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_seventeen));

        ColorStateList colorStateList;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            colorStateList = new ColorStateList(new int[][]{
                    new int[]{android.R.attr.state_pressed},
                    new int[]{}
            },
                    new int[]{
                            getResources().getColor(R.color.red_color, getContext().getTheme()),
                            getResources().getColor(R.color.text_color_white, getContext().getTheme())
                    });
        } else {
            colorStateList = new ColorStateList(new int[][]{
                    new int[]{android.R.attr.state_pressed},
                    new int[]{}
            },
                    new int[]{
                            getResources().getColor(R.color.red_color),
                            getResources().getColor(R.color.text_color_white)
                    });
        }
        rbtnMobileOperator.setTextColor(colorStateList);
        rbtnMobileOperator.setButtonDrawable(R.drawable.radio_selector);
        rbtnMobileOperator.setTypeface(Application.lightTypeface);

        RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.bottomMargin = Utils.convertDipToPixels(getContext(), getResources().getDimension(R.dimen.half_margin));

        radioGroup.addView(rbtnMobileOperator, layoutParams);

        rbtnMobileOperator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Country country = Application.getInstance().getCountry();
                if (country == null) {
                    return;
                }
                Region region = Application.getInstance().getRegion();
                if (region == null) {
                    return;
                }

                dialog.dismiss();

                Intent addNewOperatorIntent = new Intent(SetupNavigationController.ACTION_SHOW_ADD_NEW_OPERATOR);
                addNewOperatorIntent.putExtra(AddNewOperatorFragment.ARG_COUNTRY_NAME, country.getTitles().getStringByLanguage("ru"));
                addNewOperatorIntent.putExtra(AddNewOperatorFragment.ARG_REGION_NAME, "");
                addNewOperatorIntent.putExtra(AddNewOperatorFragment.ARG_TITLE, getString(R.string.addNewRegion));
                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(addNewOperatorIntent);
            }
        });
    }

    private void selectLanguageButtonClicked() {
        if (mCountriesRegions == null) {
            return;
        }

        Country country = mCountriesRegions.getCountryById(mSelectedCountryId);

        if (country == null) {
            return;
        }

        Region region = country.getRegionById(mSelectedRegionId);

        if (region == null) {
            return;
        }

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent, getActivity().getTheme())));
        } else {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        }
        dialog.setContentView(R.layout.dialog_select_item);
        TextView titleTextView = (TextView) dialog.findViewById(R.id.title);
        titleTextView.setTypeface(Application.lightTypeface);
        titleTextView.setText(getString(R.string.selectLanguage));

        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.content);
        int i = 0;

        for (final Language language : mCountriesRegions.getLanguages().getAvailableLanguages(region.getAvailableLanguages())) {
            RadioButton rbtnLanguage = new RadioButton(getActivity());
//            rbtnLanguage.setPadding((int) getResources().getDimension(R.dimen.margin), rbtnLanguage.getPaddingTop(),
//                    rbtnLanguage.getPaddingRight(), rbtnLanguage.getPaddingBottom());
            rbtnLanguage.setText(language.getName());
            rbtnLanguage.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_seventeen));
            ColorStateList colorStateList;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                colorStateList = new ColorStateList(new int[][]{
                        new int[]{android.R.attr.state_pressed},
                        new int[]{}
                },
                        new int[]{
                                getResources().getColor(R.color.red_color, getActivity().getTheme()),
                                getResources().getColor(R.color.text_color_white, getActivity().getTheme())
                        });
            } else {
                colorStateList = new ColorStateList(new int[][]{
                        new int[]{android.R.attr.state_pressed},
                        new int[]{}
                },
                        new int[]{
                                getResources().getColor(R.color.red_color),
                                getResources().getColor(R.color.text_color_white)
                        });
            }
            rbtnLanguage.setTextColor(colorStateList);
            rbtnLanguage.setButtonDrawable(R.drawable.radio_selector);
            rbtnLanguage.setTypeface(Application.lightTypeface);

            rbtnLanguage.setTag(language);
            rbtnLanguage.setId(i);

            RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.bottomMargin = Utils.convertDipToPixels(getContext(), getResources().getDimension(R.dimen.half_margin));

            radioGroup.addView(rbtnLanguage,layoutParams);
            if (language.getLocale().equalsIgnoreCase(mSelectedLanguageId)) {
                rbtnLanguage.setChecked(true);
            }

            rbtnLanguage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectedLanguageId = language.getLocale();
                    selectedLanguageChanged();
                    dialog.dismiss();
                }
            });
            i += 1;
        }

        ImageView cancelButton = (ImageView) dialog.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void selectedCountryChanged() {
        if (!isLoading) {
            mSelectedRegionId = "";
            mSelectedLanguageId = "";
        }

        mCountryLabel.setText(R.string.selectCountry);
        mRegionLabel.setText(R.string.selectRegion);
        mLanguageLabel.setText(R.string.selectLanguage);

        Country country = mCountriesRegions.getCountryById(mSelectedCountryId);

        if (country == null) {
            return;
        }

        mCountryLabel.setText(country.getTitles().getStringByLanguage(mDefaultLocale));

        if (country.getRegions() != null && country.getRegions().size() > 0) {
            Region region;
            if (isLoading) {
                region = country.getRegionById(mSelectedRegionId);
            } else {
                region = country.getRegions().get(0);
            }
            if (region == null) {
                return;
            }

            mSelectedRegionId = region.getId();
            mRegionLabel.setText(region.getTitles().getStringByLanguage(mDefaultLocale));

            Languages languages = mCountriesRegions.getLanguages().getAvailableLanguages(region.getAvailableLanguages());

            if (languages != null && languages.size() > 0) {
                Language language;
                if (isLoading) {
                    language = mCountriesRegions.getLanguages().findLanguage(mSelectedLanguageId);
                } else {
                    language = languages.get(0);
                }
                mSelectedLanguageId = language.getLocale();
                mLanguageLabel.setText(language.getName());
            }

        }
    }

    private void selectedRegionChanged() {
        mSelectedLanguageId = "";

        mRegionLabel.setText(R.string.selectRegion);
        mLanguageLabel.setText(R.string.selectLanguage);

        Country country = mCountriesRegions.getCountryById(mSelectedCountryId);

        if (country == null) {
            return;
        }

        if (country.getRegions() != null && country.getRegions().size() > 0) {
            Region region = country.getRegionById(mSelectedRegionId);
            mRegionLabel.setText(region.getTitles().getStringByLanguage(mDefaultLocale));

            Languages languages = mCountriesRegions.getLanguages().getAvailableLanguages(region.getAvailableLanguages());

            if (languages != null && languages.size() > 0) {
                Language language = languages.get(0);
                mSelectedLanguageId = language.getLocale();
                mLanguageLabel.setText(language.getName());
            }
        }
    }

    private void selectedLanguageChanged() {
        mLanguageLabel.setText(R.string.selectLanguage);

        Country country = mCountriesRegions.getCountryById(mSelectedCountryId);

        if (country == null) {
            return;
        }

        Language language = mCountriesRegions.getLanguages().findLanguage(mSelectedLanguageId);

        if (language == null) {
            return;
        }

        mLanguageLabel.setText(language.getName());
    }

    protected void showProgress() {
        if (mData != null) {
            mData.setVisibility(View.GONE);
        }
        if (mProgressControl != null) {
            mProgressControl.showProgressIndicator();
        }
    }

    protected void hideProgress() {
        mData.setVisibility(View.VISIBLE);
        mProgressControl.hideProgressIndicator();
    }

    private void downloadCountryRegionsLanguages() {
        Intent intent = new Intent(getActivity(), LoadFileService.class).putExtra(LoadFileService.PARAM_FILE_PATH, getString(R.string.server_url) + getString(R.string.country_region_file_name))
                .putExtra(SetupActivity.FileLoadReceiver.PARAM_DATA_TYPE, SetupActivity.FileLoadReceiver.DATA_TYPE_COUNTRIES_REGIONS);
        // стартуем сервис
        getActivity().startService(intent);
        showProgress();
    }

    private void downloadRegionData(String fileName) {
//        Intent intent = new Intent(getActivity(), LoadFileService.class).putExtra(LoadFileService.PARAM_FILE_PATH, getString(R.string.server_url) + fileName)
//                .putExtra(SetupActivity.FileLoadReceiver.PARAM_DATA_TYPE, SetupActivity.FileLoadReceiver.DATA_TYPE_REGION);
//        // стартуем сервис
//        getActivity().startService(intent);

        Application.getInstance().requestManualSync(fileName);

        showProgress();
    }

    public void countriesRegionsDataDownloadFinished(String data) {
        hideProgress();
        if (!TextUtils.isEmpty(data)) {
            try {
                XmlPullParser parser = XmlPullParserFactory.newInstance()
                        .newPullParser();
                parser.setInput(new ByteArrayInputStream(data.getBytes()), null);

                MainData countries_regions = new MainData(parser);

                showData(countries_regions);

                FileCache.setContent(FileCache.COUNTRIES_REGIONS_FILE_KEY, data.getBytes(), FileCache.NEVER_EXPIRES);
            } catch (Exception e) {
                e.printStackTrace();
                showNoConnectionMessage();
            }
        } else {
            showWelcomeFragment();
//            showNoConnectionMessage();
        }
    }

    public void regionDataDownloadFinished(String data) {
        hideProgress();
        if (!TextUtils.isEmpty(data)) {
            try {
                XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
                parser.setInput(new ByteArrayInputStream(data.getBytes()), null);
                MainData mainData = new MainData(parser);
                Application.getInstance().setMainData(mainData);
                FileCache.setContent(FileCache.REGION_FILE_KEY, data.getBytes(), FileCache.NEVER_EXPIRES);
                saveSelectedRegionLanguageAndOfferPath();


                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(SetupNavigationController.ACTION_SHOW_SELECT_TP));
            } catch (Exception e) {
                e.printStackTrace();
                showNoConnectionMessage();
            }
        } else {
            showWelcomeFragment();
//            showNoConnectionMessage();
        }
    }

    private void showWelcomeFragment() {
        Intent showWelcomeIntent = new Intent(SetupNavigationController.ACTION_SHOW_WELCOME);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(showWelcomeIntent);
    }

    protected void showNoConnectionMessage() {
        Dialog d = new AlertDialog.Builder(getActivity())
                .setMessage(R.string.network_not_available)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        }).setCancelable(true)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                }).create();
        d.show();
    }

    private void showData(MainData countries_regions) {
        mCountriesRegions = countries_regions;
        if (mCountriesRegions != null && mCountriesRegions.getCountries() != null && mCountriesRegions.getCountries().size() > 0) {
            if (!isLoading) {
                Country country = mCountriesRegions.getCountries().get(0);
                mSelectedCountryId = country.getId();
            }
            selectedCountryChanged();
        }
    }

    private void saveSelectedRegionLanguageAndOfferPath() {
        if (mCountriesRegions == null) {
            return;
        }

        Country country = mCountriesRegions.getCountryById(mSelectedCountryId);
        if (country == null) {
            return;
        }

        Region region = country.getRegionById(mSelectedRegionId);
        if (region == null) {
            return;
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(getString(R.string.preferences_selected_country_region_file_name), region.getFileName());
        editor.putString(getString(R.string.preferences_selected_language_code), mSelectedLanguageId);
        editor.putString(getString(R.string.preferences_offer_path), region.getOffers().getStringByLanguage(mSelectedLanguageId));
        editor.apply();

        Dictionary<Integer, String> customVariables = new Hashtable<>();
        customVariables.put(new Integer(GoogleAnalyticsHelper.CUSTOM_VARIABLE_COUNTRY), country.getTitles().getStringByLanguage("ru"));
        customVariables.put(new Integer(GoogleAnalyticsHelper.CUSTOM_VARIABLE_REGION), region.getTitles().getStringByLanguage("ru"));
        customVariables.put(new Integer(GoogleAnalyticsHelper.CUSTOM_VARIABLE_LANG), mSelectedLanguageId);

        GoogleAnalyticsHelper.sendLog(getString(R.string.ga_select_country_region_lang), getString(R.string.ga_select_action), customVariables);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_SELECTED_COUNTRY, mSelectedCountryId);
        outState.putString(STATE_SELECTED_REGION, mSelectedRegionId);
        outState.putString(STATE_SELECTED_LANGUAGE, mSelectedLanguageId);
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (SyncAdapter.ACTION_SYNC_COMPLETE.equals(intent.getAction())) {
//                boolean success = intent.getBooleanExtra(SyncAdapter.ARG_EXTRA_RESULT_IS_COMPLETE, false);
                String data = intent.getStringExtra(SyncAdapter.ARG_EXTRA_RESULT_DATA);
                regionDataDownloadFinished(data);
            }
        }

    };
}
