package com.ct.tpcalculator.application.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.widget.CompoundButton;

import com.ct.tpcalculator.R;

public class Switch extends CompoundButton {
    private final static String TAG = Switch.class.getName();

    private static final int TOUCH_MODE_IDLE = 0;
    private static final int TOUCH_MODE_DOWN = 1;
    private static final int TOUCH_MODE_DRAGGING = 2;
    private static final int TOUCH_MODE_ANIMATE = 3;

    private Drawable mThumbDrawable;
    private Drawable mThumbDrawable2;
    private Drawable mTrackDrawable;
    private Drawable mBackgroundOn;

    private int mSwitchMinWidth;
    private int mSwitchPadding;

    private int mTouchMode;
    private float mTouchX;
    private float mTouchY;
    private VelocityTracker mVelocityTracker = VelocityTracker.obtain();
    private int mMinFlingVelocity;
    private int mTouchSlop;

    private int mThumbWidth;
    private int mInternalTrackWidth;
    private int mSwitchWidth;
    private int mSwitchHeight;
    private float mThumbPosition;
    private float mTargetPosition;
    private boolean mTargetState;

    private int mSwitchLeft;
    private int mSwitchTop;
    private int mSwitchRight;
    private int mSwitchBottom;

    @SuppressWarnings("hiding")
    private final Rect mTempRect = new Rect();

    private static final int[] CHECKED_STATE_SET = {android.R.attr.state_checked};

    public Switch(Context context) {
        this(context, null);
    }

    public Switch(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.switchStyle);
    }

    public Switch(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.iSwitch, defStyle, 0);

        mThumbDrawable = a.getDrawable(R.styleable.iSwitch_ithumb);
        mThumbDrawable2 = a.getDrawable(R.styleable.iSwitch_ithumb);
        mTrackDrawable = a.getDrawable(R.styleable.iSwitch_itrack);
        mBackgroundOn = a.getDrawable(R.styleable.iSwitch_ibackgroundOn);

        mSwitchMinWidth = a.getDimensionPixelSize(R.styleable.iSwitch_iswitchMinWidth, 0);
        mSwitchPadding = a.getDimensionPixelSize(R.styleable.iSwitch_iswitchPadding, 0);

        a.recycle();

        ViewConfiguration config = ViewConfiguration.get(context);
        mTouchSlop = config.getScaledTouchSlop();
        mMinFlingVelocity = config.getScaledMinimumFlingVelocity();

        // Refresh display with current params
        refreshDrawableState();
        setChecked(isChecked());
    }

    public void setSwitchPadding(int pixels) {
        mSwitchPadding = pixels;
        requestLayout();
    }

    public int getSwitchPadding() {
        return mSwitchPadding;
    }

    public void setSwitchMinWidth(int pixels) {
        mSwitchMinWidth = pixels;
        requestLayout();
    }

    public int getSwitchMinWidth() {
        return mSwitchMinWidth;
    }

    public void setTrackDrawable(Drawable track) {
        mTrackDrawable = track;
        requestLayout();
    }

    public void setTrackResource(int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setTrackDrawable(getContext().getResources().getDrawable(resId, getContext().getTheme()));
        } else {
            setTrackDrawable(getContext().getResources().getDrawable(resId));
        }
    }

    public Drawable getTrackDrawable() {
        return mTrackDrawable;
    }

    public void setThumbDrawable(Drawable thumb) {
        mThumbDrawable = thumb;
        requestLayout();
    }

    public void setThumbResource(int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setThumbDrawable(getContext().getResources().getDrawable(resId, getContext().getTheme()));
        } else {
            //noinspection deprecation
            setThumbDrawable(getContext().getResources().getDrawable(resId));
        }
    }

    public Drawable getThumbDrawable() {
        return mThumbDrawable;
    }


    public void setThumbDrawable2(Drawable thumb) {
        mThumbDrawable2 = thumb;
        requestLayout();
    }

    public void setThumbResource2(int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setThumbDrawable2(getContext().getResources().getDrawable(resId, getContext().getTheme()));
        } else {
            //noinspection deprecation
            setThumbDrawable2(getContext().getResources().getDrawable(resId));
        }
    }

    public Drawable getThumbDrawable2() {
        return mThumbDrawable2;
    }

    public void setBackgroundOnDrawable(Drawable backgroundOn) {
        mBackgroundOn = backgroundOn;
        requestLayout();
    }

    public void setBackgroundOnResource(int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setBackgroundOnDrawable(getContext().getResources().getDrawable(resId, getContext().getTheme()));
        } else {
            //noinspection deprecation
            setBackgroundOnDrawable(getContext().getResources().getDrawable(resId));
        }
    }

    public Drawable getBackgroundOnDrawable() {
        return mBackgroundOn;
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        mThumbWidth = 0;
        int thumbHeight = 0;
        int maxThumbWidth = 0;

        if (mTargetState) {
            if (mThumbDrawable != null) {
                mThumbDrawable.getPadding(mTempRect);
                mThumbWidth = mThumbDrawable.getIntrinsicWidth() - mTempRect.left - mTempRect.right;

                maxThumbWidth = Math.max(mThumbWidth + mTempRect.left, mThumbWidth + mTempRect.right);
                thumbHeight = mThumbDrawable.getIntrinsicHeight() - (mTempRect.top + mTempRect.bottom);
            }
        } else {
            if (mThumbDrawable2 != null) {
                mThumbDrawable2.getPadding(mTempRect);
                mThumbWidth = mThumbDrawable2.getIntrinsicWidth() - mTempRect.left - mTempRect.right;

                maxThumbWidth = Math.max(mThumbWidth + mTempRect.left, mThumbWidth + mTempRect.right);
                thumbHeight = mThumbDrawable2.getIntrinsicHeight() - (mTempRect.top + mTempRect.bottom);
            }
        }


        int switchHeight = 0;
        mInternalTrackWidth = 0;
        mSwitchWidth = 0;
        mSwitchHeight = 0;

        if (mTrackDrawable != null) {
            mTrackDrawable.getPadding(mTempRect);
            mInternalTrackWidth = Math.max(maxThumbWidth, mTrackDrawable.getIntrinsicWidth()
                    - (mTempRect.left + mTempRect.right));
            final int switchWidth = Math.max(mSwitchMinWidth, mTempRect.left + mInternalTrackWidth + mTempRect.right);
            int internalTrackHeight = Math.max(
                    mTrackDrawable.getIntrinsicHeight() - (mTempRect.top + mTempRect.bottom), thumbHeight);
            switchHeight = Math.max(mTrackDrawable.getIntrinsicHeight(), internalTrackHeight + mTempRect.top
                    + mTempRect.bottom);

            mSwitchWidth = switchWidth;
            mSwitchHeight = switchHeight;

            mInternalTrackWidth = mSwitchWidth - (mTempRect.left + mTempRect.right);
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        final int measuredHeight = getMeasuredHeight();
        if (measuredHeight < switchHeight) {
            setMeasuredDimension(MeasureSpec.makeMeasureSpec(getMeasuredWidth(), widthMeasureSpec), switchHeight);
        }
    }

    private void animateThumbToCheckedState(boolean newCheckedState) {
        mTargetPosition = newCheckedState ? getThumbScrollRange() : 0;
        mTargetState = newCheckedState;
        mTouchMode = TOUCH_MODE_ANIMATE;
        invalidate();
    }

    private void animateNextPosition() {
        if (Math.abs(mTargetPosition - mThumbPosition) > mTouchSlop) {
            mThumbPosition += (mThumbPosition > mTargetPosition) ? -mTouchSlop : mTouchSlop;
            invalidate();
        } else {
            stopAnimation();
        }
    }

    private void stopAnimation() {
        mThumbPosition = mTargetPosition;
        mTouchMode = TOUCH_MODE_IDLE;
        setChecked(mTargetState);
    }

    @Override
    public void toggle() {
        animateThumbToCheckedState(!isChecked());
    }

    private boolean getTargetCheckedState() {
        return mThumbPosition >= getThumbScrollRange() / 2;
    }

    private void setThumbPosition(boolean checked) {
        mThumbPosition = checked ? getThumbScrollRange() : 0;
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        setThumbPosition(isChecked());
        invalidate();
    }

    private int getThumbScrollRange() {
        if (mTrackDrawable == null) {
            return 0;
        }
        return mInternalTrackWidth - mThumbWidth;
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isChecked()) {
            mergeDrawableStates(drawableState, CHECKED_STATE_SET);
        }
        return drawableState;
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();

        int[] myDrawableState = getDrawableState();
        // Set the state of the Drawable
        // Drawable may be null when checked state is set from XML, from super constructor
        if (mThumbDrawable != null) mThumbDrawable.setState(myDrawableState);
        if (mThumbDrawable2 != null) mThumbDrawable2.setState(myDrawableState);
        if (mTrackDrawable != null) mTrackDrawable.setState(myDrawableState);
        if (mBackgroundOn != null) mBackgroundOn.setState(myDrawableState);

        invalidate();
    }

    @Override
    protected boolean verifyDrawable(Drawable who) {
        return super.verifyDrawable(who) || who == mThumbDrawable || who == mThumbDrawable2 || who == mTrackDrawable;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        setThumbPosition(isChecked());

        int switchRight;
        int switchLeft;

        switchRight = getWidth() - getPaddingRight();
        switchLeft = switchRight - mSwitchWidth;

        int switchTop = 0;
        int switchBottom = 0;
        switch (getGravity() & Gravity.VERTICAL_GRAVITY_MASK) {
            default:
            case Gravity.TOP:
                switchTop = getPaddingTop();
                switchBottom = switchTop + mSwitchHeight;
                break;

            case Gravity.CENTER_VERTICAL:
                switchTop = (getPaddingTop() + getHeight() - getPaddingBottom()) / 2 - mSwitchHeight / 2;
                switchBottom = switchTop + mSwitchHeight;
                break;

            case Gravity.BOTTOM:
                switchBottom = getHeight() - getPaddingBottom();
                switchTop = switchBottom - mSwitchHeight;
                break;
        }

        mSwitchLeft = switchLeft;
        mSwitchTop = switchTop;
        mSwitchBottom = switchBottom;
        mSwitchRight = switchRight;
    }

    @Override
    public int getCompoundPaddingRight() {
        int padding = super.getCompoundPaddingRight() + mSwitchWidth;
        if (!TextUtils.isEmpty(getText())) {
            padding += mSwitchPadding;
        }
        return padding;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int switchLeft = mSwitchLeft;
        int switchTop = mSwitchTop;
        int switchRight = mSwitchRight;
        int switchBottom = mSwitchBottom;

//        int alpha = (int) (mThumbPosition / getThumbScrollRange() * (float) 0xff);

        if (mBackgroundOn != null) {
            mBackgroundOn.setBounds(switchLeft, switchTop, switchRight, switchBottom);
//            mBackgroundOn.setAlpha(alpha);
            mBackgroundOn.draw(canvas);
        }

        if (mTrackDrawable != null) {
            mTrackDrawable.setBounds(switchLeft, switchTop, switchRight, switchBottom);
            mTrackDrawable.draw(canvas);
            canvas.save();

            mTrackDrawable.getPadding(mTempRect);
            int switchInnerLeft = switchLeft + mTempRect.left;
            int switchInnerTop = switchTop + mTempRect.top;
            int switchInnerRight = switchRight - mTempRect.right;
            int switchInnerBottom = switchBottom - mTempRect.bottom;
            canvas.clipRect(switchInnerLeft, switchTop, switchInnerRight, switchBottom);

            if (mTargetState) {
                if (mThumbDrawable != null) {
                    mThumbDrawable.getPadding(mTempRect);
                    final int thumbPos = (int) (mThumbPosition + 0.5f);
                    int thumbLeft = switchInnerLeft - mTempRect.left + thumbPos;
                    int thumbRight = switchInnerLeft + thumbPos + mThumbWidth + mTempRect.right;
                    int thumbTop = switchInnerTop - mTempRect.top;
                    int thumbBottom = switchInnerBottom + mTempRect.bottom;

                    mThumbDrawable.setBounds(thumbLeft, thumbTop, thumbRight, thumbBottom);
                    mThumbDrawable.draw(canvas);

                }
            } else {
                if (mThumbDrawable2 != null) {
                    mThumbDrawable2.getPadding(mTempRect);
                    final int thumbPos = (int) (mThumbPosition + 0.5f);
                    int thumbLeft = switchInnerLeft - mTempRect.left + thumbPos;
                    int thumbRight = switchInnerLeft + thumbPos + mThumbWidth + mTempRect.right;
                    int thumbTop = switchInnerTop - mTempRect.top;
                    int thumbBottom = switchInnerBottom + mTempRect.bottom;

                    mThumbDrawable2.setBounds(thumbLeft, thumbTop, thumbRight, thumbBottom);
                    mThumbDrawable2.draw(canvas);

                }
            }

            canvas.restore();

        }

        if (mTouchMode == TOUCH_MODE_ANIMATE) {
            animateNextPosition();
        }
    }

    /**
     * @return true if (x, y) is within the target area of the switch thumb
     */
    private boolean hitThumb(float x, float y) {
        if (mTargetState) {
            if (mThumbDrawable != null) {
                mThumbDrawable.getPadding(mTempRect);
                final int thumbTop = mSwitchTop - mTouchSlop;
                final int thumbLeft = mSwitchLeft + (int) (mThumbPosition + 0.5f) - mTouchSlop;
                final int thumbRight = thumbLeft + mThumbWidth + mTempRect.left + mTempRect.right + mTouchSlop;
                final int thumbBottom = mSwitchBottom + mTouchSlop;
                return x > thumbLeft && x < thumbRight && y > thumbTop && y < thumbBottom;
            }
        } else {
            if (mThumbDrawable2 != null) {
                mThumbDrawable2.getPadding(mTempRect);
                final int thumbTop = mSwitchTop - mTouchSlop;
                final int thumbLeft = mSwitchLeft + (int) (mThumbPosition + 0.5f) - mTouchSlop;
                final int thumbRight = thumbLeft + mThumbWidth + mTempRect.left + mTempRect.right + mTouchSlop;
                final int thumbBottom = mSwitchBottom + mTouchSlop;
                return x > thumbLeft && x < thumbRight && y > thumbTop && y < thumbBottom;
            }
        }

        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        mVelocityTracker.addMovement(ev);
        final int action = ev.getActionMasked();
        switch (action) {
            case MotionEvent.ACTION_DOWN: {
                final float x = ev.getX();
                final float y = ev.getY();
                if (isEnabled() && hitThumb(x, y)) {
                    mTouchMode = TOUCH_MODE_DOWN;
                    mTouchX = x;
                    mTouchY = y;
                }
                break;
            }

            case MotionEvent.ACTION_MOVE: {
                switch (mTouchMode) {
                    case TOUCH_MODE_IDLE:
                        // Didn't target the thumb, treat normally.
                        break;

                    case TOUCH_MODE_DOWN: {
                        final float x = ev.getX();
                        final float y = ev.getY();
                        if (Math.abs(x - mTouchX) > mTouchSlop || Math.abs(y - mTouchY) > mTouchSlop) {
                            mTouchMode = TOUCH_MODE_DRAGGING;
                            getParent().requestDisallowInterceptTouchEvent(true);
                            mTouchX = x;
                            mTouchY = y;
                            return true;
                        }
                        break;
                    }

                    case TOUCH_MODE_DRAGGING: {
                        final float x = ev.getX();
                        final float dx = x - mTouchX;
                        float newPos = Math.max(0, Math.min(mThumbPosition + dx, getThumbScrollRange()));
                        if (newPos != mThumbPosition) {
                            mThumbPosition = newPos;
                            mTouchX = x;
                            invalidate();
                        }
                        return true;
                    }
                }
                break;
            }

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                if (mTouchMode == TOUCH_MODE_DRAGGING) {
                    stopDrag(ev);
                    return true;
                }
                mTouchMode = TOUCH_MODE_IDLE;
                mVelocityTracker.clear();
                break;
            }
        }

        return super.onTouchEvent(ev);
    }

    private void stopDrag(MotionEvent ev) {
        mTouchMode = TOUCH_MODE_IDLE;
        // Up and not canceled, also checks the switch has not been disabled during the drag
        boolean commitChange = ev.getAction() == MotionEvent.ACTION_UP && isEnabled();

        cancelSuperTouch(ev);

        if (commitChange) {
            boolean newState;
            mVelocityTracker.computeCurrentVelocity(1000);
            float xvel = mVelocityTracker.getXVelocity();
            if (Math.abs(xvel) > mMinFlingVelocity) {
                newState = (xvel > 0);
            } else {
                newState = getTargetCheckedState();
            }
            animateThumbToCheckedState(newState);
        } else {
            animateThumbToCheckedState(isChecked());
        }
    }

    private void cancelSuperTouch(MotionEvent ev) {
        MotionEvent cancel = MotionEvent.obtain(ev);
        cancel.setAction(MotionEvent.ACTION_CANCEL);
        super.onTouchEvent(cancel);
        cancel.recycle();
    }
}
