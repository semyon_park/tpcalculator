package com.ct.tpcalculator.application.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.ct.tpcalculator.application.listeners.OnFragmentChangedListener;

/**
 * Created by marat on 11/23/2015.
 */
public class BaseMainFragment extends Fragment {

    protected OnFragmentChangedListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentChangedListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnFragmentChangedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
