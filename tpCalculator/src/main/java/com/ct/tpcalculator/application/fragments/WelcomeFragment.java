package com.ct.tpcalculator.application.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.navigationControllers.SetupNavigationController;
import com.ct.tpcalculator.helpers.GoogleAnalyticsHelper;

public class WelcomeFragment extends BaseMainFragment {

    public static String TAG = WelcomeFragment.class.getName();
    private static final String GA_PAGE_NAME = "WelcomeFragment";

    public static WelcomeFragment newInstance() {
        return new WelcomeFragment();
    }

    public WelcomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GoogleAnalyticsHelper.sendLog(GA_PAGE_NAME, null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_welocme, container, false);
        TextView welcomeText = (TextView) root.findViewById(R.id.welcome_text);
        welcomeText.setTypeface(Application.lightTypeface);

        TextView btnDownload = (TextView) root.findViewById(R.id.btnLoad);
        btnDownload.setTypeface(Application.lightTypeface);
        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent showSelectCountryAndRegionIntent = new Intent(SetupNavigationController.ACTION_SHOW_SELECT_COUNTRY);
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(showSelectCountryAndRegionIntent);
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.onFragmentChanged(TAG, getString(R.string.app_name));
    }
}
