package com.ct.tpcalculator.application.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;

/**
 * Created by marat on 11/19/2015.
 */
public class ProgressIndicatorControl extends LinearLayout {

    private LinearLayout mControlView;
    private ImageView mCircle;
    private Animation mRotation;
    private TextView mProgressText;

    public ProgressIndicatorControl(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mControlView = (LinearLayout) inflater.inflate(R.layout.progress_indicator_control, this, true);

        mCircle = (ImageView) mControlView.findViewById(R.id.circle);

        mProgressText = (TextView) mControlView.findViewById(R.id.progressMessage);
        mProgressText.setTypeface(Application.lightTypeface);
    }

    public void setProgressText(String progressText) {
        if (mProgressText == null) {
            return;
        }
        mProgressText.setText(progressText);
    }

    public void showProgressIndicator() {

        if (mControlView != null) {
            mControlView.setVisibility(View.VISIBLE);
        }

        if (mCircle != null) {
            mRotation = AnimationUtils.loadAnimation(getContext(), R.anim.rotation);
            mRotation.setRepeatCount(Animation.INFINITE);
            mCircle.startAnimation(mRotation);
        }
    }

    public void hideProgressIndicator() {
        mControlView.setVisibility(View.GONE);

        if (mRotation != null) {
            mRotation.cancel();
            mRotation = null;
        }
    }
}
