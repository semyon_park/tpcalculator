package com.ct.tpcalculator.application.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.navigationControllers.SetupNavigationController;
import com.ct.tpcalculator.helpers.FileCache;
import com.ct.tpcalculator.helpers.GoogleAnalyticsHelper;

public class OfferFragment extends BaseMainFragment {

    public static final String TAG = OfferFragment.class.getName();
    private static final String GA_PAGE_NAME = "OfferFragment";


    public static final String ARG_MOBILE_OPERATOR_ID = "MOBILE_OPERATOR_ID";
    public static final String ARG_TP_ID = "TP_ID";
    public static final String ARG_SERVICES = "SERVICES";

    private String mTpId;
    private String mMobileOperatorId;
    private String mServices;

    private TextView mOfferTextView;
    private TextView mAgreeButton;

    public static OfferFragment newInstance(String mobileOperatorId, String tpId, String services) {
        OfferFragment fragment = new OfferFragment();
        Bundle args = new Bundle();
        args.putString(ARG_MOBILE_OPERATOR_ID, mobileOperatorId);
        args.putString(ARG_TP_ID, tpId);
        args.putString(ARG_SERVICES, services);
        fragment.setArguments(args);
        return fragment;
    }

    public OfferFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTpId = getArguments().getString(ARG_TP_ID);
            mMobileOperatorId = getArguments().getString(ARG_MOBILE_OPERATOR_ID);
            mServices = getArguments().getString(ARG_SERVICES);
        }
        GoogleAnalyticsHelper.sendLog(GA_PAGE_NAME, null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_offer, container, false);
        mOfferTextView = (TextView) root.findViewById(R.id.offer);
        mOfferTextView.setTypeface(Application.lightTypeface);

        String license = FileCache.getLicense(Application.getInstance().getSelectedLanguageCode());

        mOfferTextView.setText(license);

        TextView licenseTitle = (TextView) root.findViewById(R.id.offer_title);
        licenseTitle.setTypeface(Application.lightTypeface);

        mAgreeButton = (TextView) root.findViewById(R.id.agree);
        mAgreeButton.setTypeface(Application.lightTypeface);
        mAgreeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offerConfirmedClicked();
            }
        });
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.onFragmentChanged(TAG, getString(R.string.app_name));
    }

    private void offerConfirmedClicked() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getActivity().getString(R.string.preferences_selected_mobile_operator_id), mMobileOperatorId);
        editor.putString(getActivity().getString(R.string.preferences_selected_tp_id), mTpId);
        editor.putString(getActivity().getString(R.string.preferences_selected_services), mServices);

        editor.apply();

        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(SetupNavigationController.ACTION_OFFER_CONFIRMED));
    }

}
