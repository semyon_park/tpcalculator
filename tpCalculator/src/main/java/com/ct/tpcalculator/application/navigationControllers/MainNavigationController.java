package com.ct.tpcalculator.application.navigationControllers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.activities.MainActivity;
import com.ct.tpcalculator.application.fragments.AboutFragment;
import com.ct.tpcalculator.application.fragments.GeneralSettingsFragment;
import com.ct.tpcalculator.application.fragments.MainFragment;
import com.ct.tpcalculator.testapplication.fragments.*;

public class MainNavigationController extends BroadcastReceiver {

    private static final IntentFilter sFilter = new IntentFilter();
    private MainActivity mActivity;

    public final static String ACTION_HOME = "com.ct.tpcalculator.ACTION_HOME";
    public final static String ACTION_COUNTRY_REGION_LANGUAGE = "com.ct.tpcalculator.COUNTRY_REGION_LANGUAGE";
    public final static String ACTION_SELECT_MOBILE_OPERATORS = "com.ct.tpcalculator.SELECT_MOBILE_OPERATORS";
    public final static String ACTION_SELECT_TP_SERVICES = "com.ct.tpcalculator.SELECT_TP_SERVICES";
    public final static String ACTION_SHOW_RESULT = "com.ct.tpcalculator.SHOW_RESULT";
    public final static String ACTION_SHOW_TEST = "com.ct.tpcalculator.SHOW_TEST";
    public final static String ACTION_SHOW_SETTINGS = "com.ct.tpcalculator.ACTION_SHOW_SETTINGS";
    public final static String ACTION_SHOW_SHARE = "com.ct.tpcalculator.SHOW_SHARE";
    public final static String ACTION_SHOW_ABOUT = "com.ct.tpcalculator.SHOW_ABOUT";
    public final static String ACTION_SHOW_BUY_PRO = "com.ct.tpcalculator.SHOW_BUY_PRO";

    public final static String EXTRA_RESULT = "extra_result";
    public final static String EXTRA_FRAGMENT = "extra_fragment";

    static {
        sFilter.addAction(ACTION_HOME);
        sFilter.addAction(ACTION_COUNTRY_REGION_LANGUAGE);
        sFilter.addAction(ACTION_SELECT_MOBILE_OPERATORS);
        sFilter.addAction(ACTION_SELECT_TP_SERVICES);
        sFilter.addAction(ACTION_SHOW_RESULT);
        sFilter.addAction(ACTION_SHOW_SETTINGS);
        sFilter.addAction(ACTION_SHOW_SHARE);
        sFilter.addAction(ACTION_SHOW_ABOUT);
    }

    public MainNavigationController(MainActivity activity) {
        super();
        mActivity = activity;
    }

    public IntentFilter getIntentFilter() {
        return sFilter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (ACTION_HOME.equalsIgnoreCase(action)) {
            showHome();
        } else if (ACTION_SHOW_TEST.equalsIgnoreCase(action)) {
            showTest();
        } else if (ACTION_SHOW_SETTINGS.equalsIgnoreCase(action)) {
            showSettings();
        } else if (ACTION_SHOW_SHARE.equalsIgnoreCase(action)) {
            showShare();
        }else if (ACTION_SHOW_ABOUT.equalsIgnoreCase(action)){
            showAbout();
        }else if (ACTION_SHOW_BUY_PRO.equalsIgnoreCase(action)){
//            Fragment fragment = (Fragment) intent.getSerializableExtra(EXTRA_FRAGMENT);
//            Application.getInstance().buyProVersion(mActivity, fragment, MainActivity.SKU_PROFESSIONAL);
        }
    }

    private void showHome() {
        mActivity.getSupportFragmentManager().popBackStackImmediate(MainFragment.TAG,
                FragmentManager.POP_BACK_STACK_INCLUSIVE);

        MainFragment f = new MainFragment();
        f.setChartChangedListener(new MainFragment.OnChartChangedListener() {
            @Override
            public void OnChartChanged(int selectedFragment, int selectedChart) {
                mActivity.onChartChanged(selectedFragment, selectedChart);
            }
        });
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(MainFragment.TAG)
                .replace(R.id.container, f, MainFragment.TAG)
                .commitAllowingStateLoss();
    }

    private void showTest() {
        TestFragment f = new TestFragment();
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.container, f, TestFragment.TAG)
                .commitAllowingStateLoss();
    }

    private void showShare() {
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, mActivity.getString(R.string.application_share));

        mActivity.startActivity(Intent.createChooser(intent, mActivity.getString(R.string.share_command_title)));
    }

    private void showSettings() {
        GeneralSettingsFragment f = GeneralSettingsFragment.newInstance();
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("SETTINGS")
                .replace(R.id.container, f, GeneralSettingsFragment.TAG)
                .commitAllowingStateLoss();
    }

    private void showAbout() {
        AboutFragment f = AboutFragment.newInstance(false);
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("ABOUT")
                .replace(R.id.container, f, AboutFragment.TAG)
                .commitAllowingStateLoss();
    }
}