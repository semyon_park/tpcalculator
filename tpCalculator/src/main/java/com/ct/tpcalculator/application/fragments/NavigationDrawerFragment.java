package com.ct.tpcalculator.application.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.*;
import android.widget.*;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.navigationControllers.MainNavigationController;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends Fragment {

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
    private static final String STATE_SELECTED_ACTION = "selected_navigation_drawer_action";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks mCallbacks;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerListView;
    private View mFragmentContainerView;

    private int mCurrentSelectedPosition = 0;
    private String mCurrentSelectedAction = MainNavigationController.ACTION_HOME;

    private boolean mFromSavedInstanceState;

    public NavigationDrawerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mCurrentSelectedAction = savedInstanceState.getString(STATE_SELECTED_ACTION);
        } else {
            mCurrentSelectedPosition = 0;
            mCurrentSelectedAction = MainNavigationController.ACTION_HOME;
//             Select either the default item (0) or the last selected item.
            selectItem(mCurrentSelectedPosition, mCurrentSelectedAction);
        }

        mFromSavedInstanceState = savedInstanceState != null;


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mDrawerListView = (ListView) inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position, (String) view.getTag());
            }
        });


//        if (Application.getInstance().getIsPremiumAccount()) {
            mDrawerListView.setAdapter(new NavigationArrayAdapter(getActivity(), new String[]{
                    getString(R.string.main),
                    getString(R.string.share),
                    getString(R.string.settings),
                    getString(R.string.about)
            }));
//        } else {
//            mDrawerListView.setAdapter(new NavigationArrayAdapter(getActivity(), new String[]{
//                    getString(R.string.main),
//                    getString(R.string.share),
//                    getString(R.string.settings),
//                    getString(R.string.about),
//                    getString(R.string.buyPro)
//            }));
//        }
        mDrawerListView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position, (String) view.getTag());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);
        return mDrawerListView;
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    public void openCloseDrawer() {
        if (isDrawerOpen()) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        } else {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
//        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
//            mDrawerLayout.openDrawer(mFragmentContainerView);
//        }
    }

    private void selectItem(int position, String action) {
        mCurrentSelectedPosition = position;
        mCurrentSelectedAction = action;
        if (mDrawerListView != null) {
            mDrawerListView.setItemChecked(position, true);
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(action);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallbacks = (NavigationDrawerCallbacks) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
        outState.putString(STATE_SELECTED_ACTION, mCurrentSelectedAction);
        super.onSaveInstanceState(outState);
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(String action);
    }

    public class NavigationArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final String[] values;

        public NavigationArrayAdapter(Context context, String[] values) {
            super(context, R.layout.navigation_item, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.navigation_item, parent, false);
            }
            ((TextView) convertView).setTypeface(Application.condensedTypeface);
            ((TextView) convertView).setText(values[position]);
            if (position == 0) {
                convertView.setTag(MainNavigationController.ACTION_HOME);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((TextView) convertView).setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.menu_main_selector, getActivity().getTheme()), null, null);
                } else {
                    ((TextView) convertView).setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.menu_main_selector), null, null);
                }
            } else if (position == 1) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((TextView) convertView).setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.menu_share_selector, getActivity().getTheme()), null, null);
                } else {
                    ((TextView) convertView).setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.menu_share_selector), null, null);
                }
                convertView.setTag(MainNavigationController.ACTION_SHOW_SHARE);
            } else if (position == 2) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((TextView) convertView).setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.menu_settings_selector, getActivity().getTheme()), null, null);
                } else {
                    ((TextView) convertView).setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.menu_settings_selector), null, null);
                }
                convertView.setTag(MainNavigationController.ACTION_SHOW_SETTINGS);
            } else if (position == 3) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((TextView) convertView).setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.menu_about_selector, getActivity().getTheme()), null, null);
                } else {
                    ((TextView) convertView).setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.menu_about_selector), null, null);
                }
                convertView.setTag(MainNavigationController.ACTION_SHOW_ABOUT);
            } else if (position == 4) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((TextView) convertView).setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.menu_buypro_selector, getActivity().getTheme()), null, null);
                } else {
                    ((TextView) convertView).setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.menu_buypro_selector), null, null);
                }
                convertView.setTag(MainNavigationController.ACTION_SHOW_BUY_PRO);
            }
            return convertView;
        }
    }
}