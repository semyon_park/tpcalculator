package com.ct.tpcalculator.application.listeners;

/**
 * Created by marat on 11/23/2015.
 */
public interface OnFragmentChangedListener {
    void onFragmentChanged(String fragmentTag, String fragmentTitle);
}
