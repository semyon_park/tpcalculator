package com.ct.tpcalculator.application.fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.activities.ResultActivity;
import com.ct.tpcalculator.application.views.SlidingTabLayout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainFragment extends BaseMainFragment implements Serializable {


    public final static String TAG = MainFragment.class.getName();

    private ViewPager mChartsView;
    private ChartsViewAdapter mChartsViewAdapter;
    private SlidingTabLayout mSlidingTabLayout;
    private ArrayList<ChartBaseFragment> mChartViews;
    private OnChartChangedListener mChartChangedListener;

    public MainFragment() {
        // Required empty public constructor

        mChartViews = new ArrayList<>();
        mChartViews.add(new ChartCallsFragment());
        mChartViews.add(new ChartMessagesFragment());
        mChartViews.add(new ChartInternetFragment());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_main, container, false);

        mChartsView = (ViewPager) root.findViewById(R.id.charts_view);
        mChartsViewAdapter = new ChartsViewAdapter(getChildFragmentManager(), mChartViews);
        mChartsView.setAdapter(mChartsViewAdapter);
        mChartsView.addOnPageChangeListener(
                new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        ChartBaseFragment chartBaseFragment = mChartViews.get(mChartsView.getCurrentItem());
                        int selectedFragment = ChartBaseFragment.CHART_FRAGMENT_CALLS;
                        if (chartBaseFragment instanceof ChartMessagesFragment) {
                            selectedFragment = ChartBaseFragment.CHART_FRAGMENT_MESSAGES;
                        } else if (chartBaseFragment instanceof ChartCallsFragment) {
                            selectedFragment = ChartBaseFragment.CHART_FRAGMENT_CALLS;
                        } else if (chartBaseFragment instanceof ChartInternetFragment) {
                            selectedFragment = ChartBaseFragment.CHART_FRAGMENT_INTERNET;
                        }
                        int selectedChartType = chartBaseFragment.getSelectedChartType();
                        if (mChartChangedListener != null) {
                            mChartChangedListener.OnChartChanged(selectedFragment, selectedChartType);
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                }
        );

        mSlidingTabLayout = (SlidingTabLayout) root.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mChartsView);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.text_color_white, getActivity().getTheme()));
            mSlidingTabLayout.setDividerColors(getResources().getColor(R.color.action_bar_bg, getActivity().getTheme()));
        } else {
            mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.text_color_white));
            mSlidingTabLayout.setDividerColors(getResources().getColor(R.color.action_bar_bg));
        }
        Button calculate_button = (Button) root.findViewById(R.id.calculate_tp_button);
        calculate_button.setTypeface(Application.lightTypeface);
        calculate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), ResultActivity.class);
                startActivity(myIntent);

//                new AsyncTask<Void, Void, ArrayList<PackagesToOffer>>() {
//
//                    @Override
//                    protected void onPreExecute() {
//                    }
//
//                    @Override
//                    protected ArrayList<PackagesToOffer> doInBackground(Void... params) {
//                        Country country = Application.getInstance().getCountry();
//                        ArrayList<String> countryPrefixes = new ArrayList<>(Arrays.asList(country.getNumberPrefix().split(";")));
//
//                        Region region = Application.getInstance().getRegion();
//                        ArrayList<String> regionPrefixes = new ArrayList<>(Arrays.asList(region.getNumberPrefix().split(";")));
//
//                        Calendar c = Calendar.getInstance();
//                        Date endDate = c.getTime();
//
//                        c.add(Calendar.MONTH, -1);
//                        Date startDate = c.getTime();
//
//                        StatisticLogs statisticLogs = StatisticData.getInstance().getStatisticData(countryPrefixes, regionPrefixes, region.getMinDigitsCount(),
//                                region.getMaxDigitsCount(), startDate, endDate, getActivity());
//
//                        TariffPlan tariffPlan = Application.getInstance().getSelectedTariffPlan();
//                        if (tariffPlan == null) {
//                            return null;
//                        }
//                        ArrayList<Pair<Service, Date>> selectedServices = new ArrayList<>();
//                        ArrayList<Service> services = Application.getInstance().getSelectedServices();
//                        for (Service service : services) {
//                            selectedServices.add(new Pair<>(service, startDate));
//                        }
//                        double price = ExpenseController.getInstance().calculateExpense(Utils.getStartOfDay(startDate),
//                                Utils.getEndOfDay(endDate), tariffPlan, selectedServices, statisticLogs);
//
//                        ArrayList<PackagesToOffer> result = ExpenseController.getInstance().calculatePackagesToOffer(Utils.getStartOfDay(startDate), Utils.getEndOfDay(endDate), price, statisticLogs);
//
//                        return result;
//                    }
//
//                    @Override
//                    protected void onPostExecute(ArrayList<PackagesToOffer> packagesToOffers) {
//                        if (packagesToOffers != null) {
//                            Intent myIntent = new Intent(getActivity(), ResultActivity.class);
//                            myIntent.putExtra(ResultActivity.ARG_PACKAGES_TO_OFFER, packagesToOffers);
//                            startActivity(myIntent);
//                        }
//                    }
//                }.execute();
            }
        });

        return root;
    }

    public void setChartChangedListener(OnChartChangedListener listener) {
        mChartChangedListener = listener;
    }

    private class ChartsViewAdapter extends FragmentPagerAdapter {

        private List<ChartBaseFragment> mViews;

        public ChartsViewAdapter(FragmentManager fragmentManager, List<ChartBaseFragment> views) {
            super(fragmentManager);
            mViews = views;
        }

        @Override
        public Fragment getItem(int position) {
            return mViews.get(position);
//            if (fragmentName.equalsIgnoreCase(ChartCallsFragment.TAG)) {
//                return new ChartCallsFragment();
//            } else if (fragmentName.equalsIgnoreCase(ChartMessagesFragment.TAG)) {
//                return new ChartMessagesFragment();
//            } else if (fragmentName.equalsIgnoreCase(ChartInternetFragment.TAG)) {
//                return new ChartInternetFragment();
//            }
//            return null;
        }

        @Override
        public int getCount() {
            return mViews.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.calls);
                case 1:
                    return getResources().getString(R.string.messages);
                default:
                    return getResources().getString(R.string.internet);
            }
        }
    }

    public void redrawChart(int chartType) {
        ChartBaseFragment chartBaseFragment = mChartViews.get(mChartsView.getCurrentItem());
        chartBaseFragment.showChart(chartType);
    }

    public int getSelectedChartFragmentType() {
        ChartBaseFragment chartBaseFragment = mChartViews.get(mChartsView.getCurrentItem());
        if (chartBaseFragment instanceof ChartMessagesFragment) {
            return ChartBaseFragment.CHART_FRAGMENT_MESSAGES;
        } else if (chartBaseFragment instanceof ChartCallsFragment) {
            return ChartBaseFragment.CHART_FRAGMENT_CALLS;
        } else {
            return ChartBaseFragment.CHART_FRAGMENT_INTERNET;
        }
    }

    public void setSelectedChartFragmentType(int chartFragmentType) {
        if (mChartsView == null) {
            return;
        }

        mChartsView.setCurrentItem(chartFragmentType);
    }

    public int getSelectedChartType() {
        ChartBaseFragment chartBaseFragment = mChartViews.get(mChartsView.getCurrentItem());
        return chartBaseFragment.getSelectedChartType();
    }

    public interface OnChartChangedListener {
        void OnChartChanged(int selectedFragment, int selectedChart);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mListener == null) {
            return;
        }
        mListener.onFragmentChanged(TAG, getString(R.string.statistics));
    }
}
