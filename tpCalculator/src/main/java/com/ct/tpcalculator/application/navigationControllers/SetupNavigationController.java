package com.ct.tpcalculator.application.navigationControllers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.FragmentManager;
import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.activities.SetupActivity;
import com.ct.tpcalculator.application.fragments.AboutFragment;
import com.ct.tpcalculator.application.fragments.AddNewOperatorFragment;
import com.ct.tpcalculator.application.fragments.OfferFragment;
import com.ct.tpcalculator.application.fragments.SelectCountryAndRegionFragment;
import com.ct.tpcalculator.application.fragments.SelectTpFragment;
import com.ct.tpcalculator.application.fragments.WelcomeFragment;

public class SetupNavigationController extends BroadcastReceiver {

    private static final IntentFilter sFilter = new IntentFilter();
    private SetupActivity mActivity;

    public final static String ACTION_SHOW_SELECT_COUNTRY = "com.ct.tpcalculator.ACTION_SHOW_SELECT_COUNTRY";
    public final static String ACTION_SHOW_SELECT_TP = "com.ct.tpcalculator.ACTION_SHOW_SELECT_TP";
    public static final String ACTION_SHOW_OFFER = "com.ct.tpcalculator.ACTION_SHOW_OFFER";
    public static final String ACTION_OFFER_CONFIRMED = "com.ct.tpcalculator.ACTION_OFFER_CONFIRMED";
    public static final String ACTION_SHOW_WELCOME = "com.ct.tpcalculator.ACTION_SHOW_WELCOME";
    public static final String ACTION_SHOW_ABOUT = "com.ct.tpcalculator.ACTION_SHOW_ABOUT";
    public final static String ACTION_SHOW_ADD_NEW_OPERATOR = "com.ct.tpcalculator.ACTION_SHOW_ADD_NEW_OPERATOR";


    static {
        sFilter.addAction(ACTION_SHOW_SELECT_COUNTRY);
        sFilter.addAction(ACTION_SHOW_SELECT_TP);
        sFilter.addAction(ACTION_SHOW_OFFER);
        sFilter.addAction(ACTION_OFFER_CONFIRMED);
        sFilter.addAction(ACTION_SHOW_WELCOME);
        sFilter.addAction(ACTION_SHOW_ABOUT);
        sFilter.addAction(ACTION_SHOW_ADD_NEW_OPERATOR);
    }

    public SetupNavigationController(SetupActivity activity) {
        super();
        mActivity = activity;
    }

    public IntentFilter getIntentFilter() {
        return sFilter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (ACTION_SHOW_SELECT_TP.equalsIgnoreCase(action)) {
            showSelectTp();
        } else if (ACTION_SHOW_OFFER.equalsIgnoreCase(action)) {
            String operatorId = intent.getStringExtra(OfferFragment.ARG_MOBILE_OPERATOR_ID);
            String tpId = intent.getStringExtra(OfferFragment.ARG_TP_ID);
            String services = intent.getStringExtra(OfferFragment.ARG_SERVICES);

            showOffer(operatorId, tpId, services);
        } else if (ACTION_OFFER_CONFIRMED.equalsIgnoreCase(action)) {
            mActivity.setResult(Activity.RESULT_OK);
            mActivity.finish();
        }else if (ACTION_SHOW_WELCOME.equalsIgnoreCase(action)){
            showWelcomeFragment();
        }else if (ACTION_SHOW_SELECT_COUNTRY.equalsIgnoreCase(action)){
            showSelectCountryAndRegionFragment();
        }else if(ACTION_SHOW_ABOUT.equalsIgnoreCase(action)){
            showAbout();
        }else if (ACTION_SHOW_ADD_NEW_OPERATOR.equalsIgnoreCase(action)){
            String countryName = intent.getStringExtra(AddNewOperatorFragment.ARG_COUNTRY_NAME);
            String regionName = intent.getStringExtra(AddNewOperatorFragment.ARG_REGION_NAME);
            String title = intent.getStringExtra(AddNewOperatorFragment.ARG_TITLE);
            showAddNewOperator(countryName, regionName, title);
        }
    }

    private void showAddNewOperator(String countryName, String regionName, String title) {
        AddNewOperatorFragment addNewOperatorFragment = AddNewOperatorFragment.newInstance(countryName, regionName, title);
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(AddNewOperatorFragment.TAG)
                .replace(R.id.container, addNewOperatorFragment, AddNewOperatorFragment.TAG)
                .commitAllowingStateLoss();
    }

    private void showSelectTp() {
        SelectTpFragment selectTpFragment = new SelectTpFragment();
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(SelectTpFragment.TAG)
                .replace(R.id.container, selectTpFragment, SelectTpFragment.TAG)
                .commitAllowingStateLoss();
    }

    private void showWelcomeFragment() {
        mActivity.getSupportFragmentManager().popBackStackImmediate(WelcomeFragment.TAG,
                FragmentManager.POP_BACK_STACK_INCLUSIVE);

        WelcomeFragment welcomeTpFragment = new WelcomeFragment();
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(WelcomeFragment.TAG)
                .replace(R.id.container, welcomeTpFragment, WelcomeFragment.TAG)
                .commitAllowingStateLoss();
    }

    public void showSelectCountryAndRegionFragment() {
        mActivity.getSupportFragmentManager().popBackStackImmediate(SelectCountryAndRegionFragment.TAG,
                FragmentManager.POP_BACK_STACK_INCLUSIVE);

        SelectCountryAndRegionFragment f = new SelectCountryAndRegionFragment();
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(SelectCountryAndRegionFragment.TAG)
                .replace(R.id.container, f, SelectCountryAndRegionFragment.TAG)
                .commitAllowingStateLoss();
    }

    public void showAbout() {
        AboutFragment f = AboutFragment.newInstance(true);
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(AboutFragment.TAG)
                .replace(R.id.container, f, AboutFragment.TAG)
                .commitAllowingStateLoss();
    }

    private void showOffer(String operatorId, String tpId, String services) {
        OfferFragment offerFragment = OfferFragment.newInstance(operatorId, tpId, services);
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(OfferFragment.TAG)
                .replace(R.id.container, offerFragment, OfferFragment.TAG)
                .commitAllowingStateLoss();
    }
}

