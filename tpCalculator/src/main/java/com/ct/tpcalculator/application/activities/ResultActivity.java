package com.ct.tpcalculator.application.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.*;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.application.listeners.OnFragmentChangedListener;
import com.ct.tpcalculator.application.navigationControllers.ResultNavigationController;
import com.ct.tpcalculator.helpers.L;

public class ResultActivity extends BaseActivity implements OnFragmentChangedListener {

    private final ResultNavigationController mResultNavigationController = new ResultNavigationController(this);

    private TextView mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);        // Set up your ActionBar
        final ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        mTitle = (TextView)findViewById(R.id.title_text);
        mTitle.setTypeface(Application.lightTypeface);

        ImageView backCommand = (ImageView) findViewById(R.id.back);
        backCommand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                    finish();
                } else {
                    getSupportFragmentManager().popBackStack();
                }
            }
        });

        if (savedInstanceState == null) {
            mResultNavigationController.showResultSettings();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mResultNavigationController);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mResultNavigationController, mResultNavigationController.getIntentFilter());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        L.e("onActivityResult(" + requestCode + "," + resultCode + "," + data);

        // Pass on the activity result to the helper for handling
        if (!Application.getInstance().getIabHelper().handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            L.e("onActivityResult handled by IABUtil.");
        }
    }

    @Override
    public void onFragmentChanged(String fragmentTag, String fragmentTitle) {
        if (mTitle == null){
            return;
        }
        mTitle.setText(fragmentTitle);
    }
}