package com.ct.tpcalculator.loaders;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.ct.tpcalculator.application.activities.SetupActivity;
import com.ct.tpcalculator.helpers.FileCache;
import com.ct.tpcalculator.helpers.L;

//import org.apache.http.HttpEntity;
//import org.apache.http.HttpResponse;
//import org.apache.http.HttpStatus;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpRequestBase;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by USER on 26.08.2015.
 */
public class FileLoader implements Runnable {

    private static final String TAG = FileLoader.class.getName();

    private final String mFilePath;
    private final int mDataType;
    private final Context mContext;

    public FileLoader(Context context, String filePath, int dataType) {
        super();
        mContext = context;
        mFilePath = filePath;
        mDataType = dataType;
    }

//    public static boolean isGZiped(HttpEntity entity) {
//        return entity.getContentType() != null
//                && "application/x-gzip".equalsIgnoreCase(entity.getContentType().getValue());
//    }

    @Override
    public void run() {
        byte[] data = null;

        // нет сети - нет данных
        if (!com.ct.tpcalculator.application.Application.isNetworkAvailable()) {
            sendDataDownloadFailed();
            return;
        }

        try {
             URL url = new URL(mFilePath);
            URLConnection urlConnection = url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            try {
                data = FileCache.readFull(in);
            } finally {
                in.close();
            }
        } catch (MalformedURLException ex) {

        } catch (IOException ex) {

        }

        if (data != null) {
            Log.d(TAG, String.format("Data: %s", new String(data)));

            String result = null;
            try {
                result = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                L.e(Log.getStackTraceString(e));
            }

            Log.d(TAG, String.format("Result: %s", result));

            Intent intent = new Intent(SetupActivity.FileLoadReceiver.ACTION_LOAD_FILE);
            intent.putExtra(SetupActivity.FileLoadReceiver.PARAM_DATA_TYPE, mDataType);
            intent.putExtra(SetupActivity.FileLoadReceiver.PARAM_DATA, result);

            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
        } else {
            sendDataDownloadFailed();
        }
    }

    private void sendDataDownloadFailed() {
        Intent intent = new Intent(SetupActivity.FileLoadReceiver.ACTION_LOAD_FILE);
        intent.putExtra(SetupActivity.FileLoadReceiver.PARAM_DATA_TYPE, mDataType);
        intent.putExtra(SetupActivity.FileLoadReceiver.PARAM_DATA, "");

        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }
}
