package com.ct.tpcalculator.testapplication.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.calculator.data.MobileOperator;
import com.ct.tpcalculator.helpers.Storage;
import com.ct.tpcalculator.helpers.Utils;
import com.ct.tpcalculator.testapplication.NavigationController;

public class SelectMobileOperatorFragment extends Fragment {

    private MobileOperator selectedMobileOperator;

    private RadioGroup rgrpSelectMobileOperator;

    public SelectMobileOperatorFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_mobile_operator, container, false);

        rgrpSelectMobileOperator = (RadioGroup)view.findViewById(R.id.rgrpMobileOperator);
        rgrpSelectMobileOperator.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                selectedMobileOperator = Storage.getInstance().getMobileOperators().getCountries().get(0).getRegions().get(0).getMobileOperators().get(checkedId);
            }
        });
        fillRadioGroup();

        Button btnNext = (Button)view.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedMobileOperator == null){
                    return;
                }
                showTariffPlanAndServices();
            }
        });
        return view;
    }

    private void showTariffPlanAndServices(){
        Storage.getInstance().setMobileOperator(selectedMobileOperator);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
                new Intent(NavigationController.ACTION_SELECT_TP_SERVICES));
    }

    private void fillRadioGroup(){
        int i = 0;
        for (MobileOperator mobileOperator : Storage.getInstance().getMobileOperators().getCountries().get(0).getRegions().get(0).getMobileOperators()) {
            RadioButton rbtnMobileOperators = new RadioButton(getActivity());
            rbtnMobileOperators.setText(mobileOperator.getTitles().getStringByLanguage("ru"));
            rbtnMobileOperators.setTag(mobileOperator);
            rbtnMobileOperators.setId(i);
            rgrpSelectMobileOperator.addView(rbtnMobileOperators,
                    new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            Utils.convertDipToPixels(48, getResources())));
            i += 1;
        }
    }
}
