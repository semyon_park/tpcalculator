package com.ct.tpcalculator.testapplication.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.calculator.data.Country;
import com.ct.tpcalculator.calculator.data.Language;
import com.ct.tpcalculator.calculator.data.Languages;
import com.ct.tpcalculator.calculator.data.MainData;
import com.ct.tpcalculator.calculator.data.Region;
import com.ct.tpcalculator.helpers.Storage;
import com.ct.tpcalculator.helpers.Utils;
import com.ct.tpcalculator.testapplication.NavigationController;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by USER on 23.06.2015.
 */
public class SelectCountryRegionLanguagesFragment extends Fragment {

    public final static String TAG = SelectCountryRegionLanguagesFragment.class.getName();

    private RadioGroup rgrpSelectCountry;
    private RadioGroup rgrpSelectRegions;
    private RadioGroup rgrpSelectLanguages;
    private Button btnDownload;

    public SelectCountryRegionLanguagesFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_country_region, container, false);

        rgrpSelectCountry = (RadioGroup) view.findViewById(R.id.rgrpSelectCountry);
        rgrpSelectCountry.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) group.getChildAt(checkedId);
                Country country = (Country) radioButton.getTag();
                fillRegions(country);
            }
        });

        rgrpSelectRegions = (RadioGroup) view.findViewById(R.id.rgrpSelectRegion);
        rgrpSelectRegions.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) group.getChildAt(checkedId);
                Region region = (Region) radioButton.getTag();
                fillLanguages(region);
            }
        });

        rgrpSelectLanguages = (RadioGroup) view.findViewById(R.id.rgrpSelectLanguage);
        rgrpSelectLanguages.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

            }
        });
        fillCountries();

        btnDownload = (Button) view.findViewById(R.id.btnDownloadMobileOperators);
        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DownloadFile().execute(v.getContext());
            }
        });
        return view;
    }

    private void fillCountries() {
        int i = 0;
        for (Country country : Storage.getInstance().getCountries_regions_langs().getCountries()) {
            RadioButton rbtnCountry = new RadioButton(getActivity());
            rbtnCountry.setPadding((int) getResources().getDimension(R.dimen.margin), rbtnCountry.getPaddingTop(),
                    rbtnCountry.getPaddingRight(), rbtnCountry.getPaddingBottom());
            rbtnCountry.setText(country.getTitles().getStringByLanguage("ru"));
            rbtnCountry.setTag(country);
            rbtnCountry.setId(i);
            rgrpSelectCountry.addView(rbtnCountry,
                    new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            Utils.convertDipToPixels(48, getResources())));
            i += 1;
        }
    }

    private void fillRegions(Country country) {
        rgrpSelectRegions.removeAllViews();
        int i = 0;
        for (Region region : country.getRegions()) {
            RadioButton rbtnRegion = new RadioButton(getActivity());
            rbtnRegion.setText(region.getTitles().getStringByLanguage("ru"));
            rbtnRegion.setTag(region);
            rbtnRegion.setId(i);
            rgrpSelectRegions.addView(rbtnRegion,
                    new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            Utils.convertDipToPixels(48, getResources())));
            i += 1;
        }
    }

    private void fillLanguages(Region region) {
        rgrpSelectLanguages.removeAllViews();
        Languages languages = Storage.getInstance().getCountries_regions_langs().getLanguages().getAvailableLanguages(region.getAvailableLanguages());
        int i = 0;
        for (Language language : languages) {
            RadioButton rbtnLanguage = new RadioButton(getActivity());
            rbtnLanguage.setText(language.getName());
            rbtnLanguage.setTag(language);
            rbtnLanguage.setId(i);
            rgrpSelectLanguages.addView(rbtnLanguage,
                    new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            Utils.convertDipToPixels(48, getResources())));
            i += 1;
        }
    }

    private void ShowSelectMobileOperators(){
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(NavigationController.ACTION_SELECT_MOBILE_OPERATORS));
    }

    private class DownloadFile extends AsyncTask<Context, Void, Void> {

        @Override
        protected Void doInBackground(Context... params) {
            AssetManager assetManager = params[0].getAssets();
            try {
                InputStream is = assetManager.open("XML_data_sample_uz.xml");
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser xpp = factory.newPullParser();
                xpp.setInput(is, "utf-8");

                MainData mobileOperators = new MainData(xpp);
                Storage.getInstance().setMobileOperators(mobileOperators);

            } catch (IOException e) {

            } catch (XmlPullParserException ex) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ShowSelectMobileOperators();
        }

        @Override
        protected void onPreExecute() {
        }
    }
}
