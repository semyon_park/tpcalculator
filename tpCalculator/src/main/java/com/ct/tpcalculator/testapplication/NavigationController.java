package com.ct.tpcalculator.testapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.testapplication.activities.TestActivity;
import com.ct.tpcalculator.testapplication.fragments.TestFragment;
import com.ct.tpcalculator.testapplication.fragments.ResultFragment;
import com.ct.tpcalculator.testapplication.fragments.SelectCountryRegionLanguagesFragment;
import com.ct.tpcalculator.testapplication.fragments.SelectMobileOperatorFragment;
import com.ct.tpcalculator.testapplication.fragments.SelectTariffPlanAndServicesFragment;


public class NavigationController extends BroadcastReceiver {

    private static final IntentFilter sFilter  = new IntentFilter();
    private TestActivity mActivity;

    public final static String ACTION_HOME = "com.ct.tpcalculator.ACTION_HOME";
    public final static String ACTION_COUNTRY_REGION_LANGUAGE = "com.ct.tpcalculator.COUNTRY_REGION_LANGUAGE";
    public final static String ACTION_SELECT_MOBILE_OPERATORS = "com.ct.tpcalculator.SELECT_MOBILE_OPERATORS";
    public final static String ACTION_SELECT_TP_SERVICES = "com.ct.tpcalculator.SELECT_TP_SERVICES";
    public final static String ACTION_SHOW_RESULT = "com.ct.tpcalculator.SHOW_RESULT";

    public final static String EXTRA_RESULT = "extra_result";

    static {
        sFilter.addAction(ACTION_HOME);
        sFilter.addAction(ACTION_COUNTRY_REGION_LANGUAGE);
        sFilter.addAction(ACTION_SELECT_MOBILE_OPERATORS);
        sFilter.addAction(ACTION_SELECT_TP_SERVICES);
        sFilter.addAction(ACTION_SHOW_RESULT);
    }

    public NavigationController(TestActivity activity) {
        super();
        mActivity = activity;
    }

    public IntentFilter getIntentFilter() {
        return sFilter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (ACTION_HOME.equalsIgnoreCase(action)) {
            showHome();
        }else if (ACTION_COUNTRY_REGION_LANGUAGE.equalsIgnoreCase(action)){
            showCalculator();
        }else if (ACTION_SELECT_MOBILE_OPERATORS.equalsIgnoreCase(action)){
            showMobileOperators();
        }else if (ACTION_SELECT_TP_SERVICES.equalsIgnoreCase(action)){
            showSelectTpAndServices();
        }else if (ACTION_SHOW_RESULT.equalsIgnoreCase(action)){
            String result = intent.getStringExtra(EXTRA_RESULT);
            showResult(result);
        }
    }

    private void back() {
        mActivity.getSupportFragmentManager().popBackStack();
    }

    private void showHome() {
        mActivity.getSupportFragmentManager().popBackStack(TestFragment.TAG, 0);
    }

    private void showCalculator() {
        SelectCountryRegionLanguagesFragment f = new SelectCountryRegionLanguagesFragment();
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.main, f)
                .commitAllowingStateLoss();
    }

    private void showMobileOperators(){
        SelectMobileOperatorFragment f = new SelectMobileOperatorFragment();
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.main, f)
                .commitAllowingStateLoss();
    }

    private void showSelectTpAndServices(){
        SelectTariffPlanAndServicesFragment f = SelectTariffPlanAndServicesFragment.newInstance();
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.main, f)
                .commitAllowingStateLoss();
    }

    private void showResult(String result){
        ResultFragment f = ResultFragment.newInstance(result);
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.main, f)
                .commitAllowingStateLoss();
    }
}

