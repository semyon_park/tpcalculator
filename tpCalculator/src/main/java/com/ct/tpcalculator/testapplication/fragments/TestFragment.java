package com.ct.tpcalculator.testapplication.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.calculator.data.MainData;
import com.ct.tpcalculator.helpers.Storage;
import com.ct.tpcalculator.testapplication.NavigationController;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

/**
 * Created by USER on 22.06.2015.
 */
public class TestFragment extends Fragment {

    public final static String TAG = TestFragment.class.getName();

    private TextView txtResult;

    public TestFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_test, container, false);

        txtResult = (TextView) view.findViewById(R.id.txtResult);
        Button btnCallLogs = (Button) view.findViewById(R.id.btnCallLogs);
        btnCallLogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCallLogs();
            }
        });

        Button smsLogs = (Button) view.findViewById(R.id.btnSmsLogs);
        smsLogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSmsLogs();
            }
        });

        Button mmsLogs = (Button) view.findViewById(R.id.btnMmsLogs);
        mmsLogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMmsLogs();
            }
        });

        Button btnInternetLogs = (Button) view.findViewById(R.id.btnInternet);
        btnInternetLogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInternetLogs();
            }
        });

        Button btnCalculator = (Button) view.findViewById(R.id.btnCalculator);
        btnCalculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalculator();
            }
        });

        return view;
    }

    private void showCallLogs() {
        StringBuffer stringBuffer = new StringBuffer();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Cursor cursor = getActivity().getContentResolver().query(CallLog.Calls.CONTENT_URI, null, "", null, CallLog.Calls.DATE + " DESC");
        int number = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = cursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = cursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = cursor.getColumnIndex(CallLog.Calls.DURATION);

        while (cursor.moveToNext()) {
            String phNumber = cursor.getString(number);
            String callType = cursor.getString(type);
            String callDate = cursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = cursor.getString(duration);

            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;
                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
            stringBuffer.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- "
                    + dir + " \nCall Date:--- " + callDayTime
                    + " \nCall duration in sec :--- " + callDuration);
            stringBuffer.append("\n----------------------------------");
        }
        cursor.close();
        txtResult.setText(stringBuffer.toString());
    }

    private void showSmsLogs() {
        final String[] projection = null;
        final String selection = "";
        final String selectionArgs[] = null;
        final String sortOrder = null;
        Cursor cursor = null;

        StringBuffer stringBuffer = new StringBuffer();

        cursor = getActivity().getContentResolver().query(
                Uri.parse("content://sms"),
                projection,
                selection,
                selectionArgs,
                sortOrder);

        if (cursor.moveToFirst() && cursor.isNull(cursor.getColumnIndex("date")) == false &&
                cursor.isNull(cursor.getColumnIndex("address")) == false) {

            do {

                int date = cursor.getColumnIndex("date");
                int address = cursor.getColumnIndex("address");

                String callDate = cursor.getString(date);
                Date callDayTime = new Date(Long.valueOf(callDate));

                String phoneNumber = cursor.getString(address);

                stringBuffer.append("\nPhone Number:--- " + phoneNumber + " \nSMS Date:--- " + callDayTime + " \nAddress:--- " + address);
                stringBuffer.append("\n----------------------------------");

            } while (cursor.moveToNext());

        }
        txtResult.setText(stringBuffer);
        cursor.close();
    }

    private void showMmsLogs(){
        final String[] projection = null;
        final String selection = "";
        final String selectionArgs[] = null;
        final String sortOrder = null;
        Cursor cursor = null;

        StringBuffer stringBuffer = new StringBuffer();

        cursor = getActivity().getContentResolver().query(
                Uri.parse("content://mms"),
                projection,
                selection,
                selectionArgs,
                sortOrder);

        if (cursor.moveToFirst() && cursor.isNull(cursor.getColumnIndex("date")) == false &&
                cursor.isNull(cursor.getColumnIndex("address")) == false) {

            do {

                int date = cursor.getColumnIndex("date");
                int address = cursor.getColumnIndex("address");

                String callDate = cursor.getString(date);
                Date callDayTime = new Date(Long.valueOf(callDate));

                String phoneNumber = cursor.getString(address);

                stringBuffer.append("\nPhone Number:--- " + phoneNumber + " \nMMS Date:--- " + callDayTime + " \nAddress:--- " + address);
                stringBuffer.append("\n----------------------------------");

            } while (cursor.moveToNext());

        }
        txtResult.setText(stringBuffer);
        cursor.close();
    }

    private void showInternetLogs() {
        StringBuffer stringBuffer = new StringBuffer();

        long totalRecievedBytes = TrafficStats.getTotalRxBytes();
        long totalTransmittedBytes = TrafficStats.getTotalTxBytes();
        stringBuffer.append("\nAll traffic:--- " + (((totalRecievedBytes + totalTransmittedBytes) / 1024) / 1024) + "MB");

        long mobileRecievedBytes = TrafficStats.getMobileRxBytes();
        long mobileTransmittedBytes = TrafficStats.getMobileTxBytes();
        stringBuffer.append("\nMobile traffic:--- " + (((mobileRecievedBytes + mobileTransmittedBytes) / 1024) / 1024) + "MB");

        txtResult.setText(stringBuffer);

        Process proc = null;
        try {
            proc = Runtime.getRuntime().exec(new String[]{"cat", "/proc/net/dev"});
            BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            String line = null;

            while ((line = reader.readLine()) != null) {
                stringBuffer.append("\n\n" + line);
                // parse netstat output here
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        txtResult.setText(stringBuffer);
    }

    private void showCalculator() {
        DownloadFile downloadFile = new DownloadFile();
        downloadFile.execute(this.getActivity());
    }

    private void showSelectCountryRegionAndLanguage() {

        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(NavigationController.ACTION_COUNTRY_REGION_LANGUAGE));
    }

    private class DownloadFile extends AsyncTask<Context, Void, Void> {

        @Override
        protected Void doInBackground(Context... params) {
            AssetManager assetManager = params[0].getAssets();
            try {
                InputStream is = assetManager.open("countries_regions_languages.xml");
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser xpp = factory.newPullParser();
                xpp.setInput(is, "utf-8");

                MainData countries_regions = new MainData(xpp);
                Storage.getInstance().setCountriesRegionsLanguages(countries_regions);

            } catch (IOException e) {

            } catch (XmlPullParserException ex) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            txtResult.setText("Loaded!...");
            showSelectCountryRegionAndLanguage();
        }

        @Override
        protected void onPreExecute() {
            txtResult.setText("Loading");
        }
    }
}