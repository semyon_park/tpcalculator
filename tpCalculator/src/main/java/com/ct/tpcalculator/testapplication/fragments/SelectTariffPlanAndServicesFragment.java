package com.ct.tpcalculator.testapplication.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.application.Application;
import com.ct.tpcalculator.calculator.ExpenseController;
import com.ct.tpcalculator.calculator.data.Country;
import com.ct.tpcalculator.calculator.data.Region;
import com.ct.tpcalculator.calculator.data.Service;
import com.ct.tpcalculator.calculator.data.TariffPlan;
import com.ct.tpcalculator.calculator.data.calculationData.PackageToOffer;
import com.ct.tpcalculator.calculator.data.calculationData.PackagesToOffer;
import com.ct.tpcalculator.calculator.data.calculationData.ServiceToOffer;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticInternetTraffic;
import com.ct.tpcalculator.calculator.data.statisticData.StatisticLogs;
import com.ct.tpcalculator.helpers.StatisticData;
import com.ct.tpcalculator.helpers.Storage;
import com.ct.tpcalculator.helpers.Utils;
import com.ct.tpcalculator.testapplication.NavigationController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link SelectTariffPlanAndServicesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SelectTariffPlanAndServicesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_MOBILE_OPERATOR = "MOBILE_OPERATOR";

    // TODO: Rename and change types of parameters
    private RadioGroup rgrpSelectTariffPlans;
    private LinearLayout services;
    private TariffPlan tariffPlan;
    private EditText txtInternetTraffic;
    private final ArrayList<Pair<Service, Date>> selectedServices = new ArrayList<>();

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SelectTariffPlanAndServicesFragment.
     */
    public static SelectTariffPlanAndServicesFragment newInstance() {
        return new SelectTariffPlanAndServicesFragment();
    }

    public SelectTariffPlanAndServicesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_tariff_plan_and_services, container, false);

        rgrpSelectTariffPlans = (RadioGroup) view.findViewById(R.id.rgrpTariffPlans);
        rgrpSelectTariffPlans.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                tariffPlan = Storage.getInstance().getMobileOperator().getTariffPlans().get(checkedId);
                fillServices();
            }
        });

        txtInternetTraffic = (EditText) view.findViewById(R.id.txtInternetTraffic);

        Button btnCalculate = (Button) view.findViewById(R.id.btnCalculate);
        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Country country = Storage.getInstance().getMobileOperators().getCountries().get(0);
                ArrayList<String> countryPrefixes = new ArrayList<>(Arrays.asList(country.getNumberPrefix().split(";")));

                Region region = country.getRegions().get(0);
                ArrayList<String> regionPrefixes = new ArrayList<>(Arrays.asList(region.getNumberPrefix().split(";")));

                Calendar c = Calendar.getInstance();
                Date endDate = c.getTime();

                c.add(Calendar.MONTH, -1);
                Date startDate = c.getTime();

                StatisticLogs statisticLogs = StatisticData.getInstance().getStatisticData(countryPrefixes, regionPrefixes, region.getMinDigitsCount(),
                        region.getMaxDigitsCount(), startDate, endDate, v.getContext());

                StatisticInternetTraffic internetTraffic = new StatisticInternetTraffic(StatisticInternetTraffic.TYPE_MOBILE);
                internetTraffic.setDate(startDate);
                internetTraffic.setReceivedBytes(Integer.parseInt(txtInternetTraffic.getText().toString()));
                statisticLogs.getInternetTraffics().getMobileInternetTraffic().add(internetTraffic);

                double price = ExpenseController.getInstance().calculateExpense(Utils.getStartOfDay(startDate),
                        Utils.getEndOfDay(endDate), tariffPlan, selectedServices, statisticLogs);

                String result = "Ваши текущии расходы = " + price;
                showResultForm(result);
            }
        });
        Button btnCalculatePackagesToOffer = (Button) view.findViewById(R.id.btnGetPackagesToOffer);
        btnCalculatePackagesToOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Country country = Storage.getInstance().getMobileOperators().getCountries().get(0);
                ArrayList<String> countryPrefixes = new ArrayList<>(Arrays.asList(country.getNumberPrefix().split(";")));

                Region region = country.getRegions().get(0);
                ArrayList<String> regionPrefixes = new ArrayList<>(Arrays.asList(region.getNumberPrefix().split(";")));

                Calendar c = Calendar.getInstance();
                Date endDate = c.getTime();

                c.add(Calendar.MONTH, -1);
                Date startDate = c.getTime();

                StatisticLogs statisticLogs = StatisticData.getInstance().getStatisticData(countryPrefixes, regionPrefixes, region.getMinDigitsCount(),
                        region.getMaxDigitsCount(), startDate, endDate, v.getContext());

                StatisticInternetTraffic internetTraffic = new StatisticInternetTraffic(StatisticInternetTraffic.TYPE_MOBILE);
                internetTraffic.setDate(startDate);
                internetTraffic.setReceivedBytes(Integer.parseInt(txtInternetTraffic.getText().toString()));
                statisticLogs.getInternetTraffics().getMobileInternetTraffic().add(internetTraffic);

                double price = ExpenseController.getInstance().calculateExpense(Utils.getStartOfDay(startDate),
                        Utils.getEndOfDay(endDate), tariffPlan, selectedServices, statisticLogs);

                ArrayList<PackagesToOffer> result = ExpenseController.getInstance().calculatePackagesToOffer(Utils.getStartOfDay(startDate), Utils.getEndOfDay(endDate), price, statisticLogs);

                StringBuilder stringResult = new StringBuilder();
                stringResult.append("\t\t\t");
                stringResult.append("Ваши текущии расходы = ");
                stringResult.append(price);
                stringResult.append("\n\n");
                stringResult.append("Предлагаемые ТП и услуги:");

                for (PackagesToOffer p : result) {
                    stringResult.append("\n\n");
                    stringResult.append("\t");
                    stringResult.append(p.getMobileOperator().getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));
                    stringResult.append("\n");

                    for (PackageToOffer packageToOffer : p.getPackagesToOffer()) {
                        stringResult.append("\n\n");
                        stringResult.append("\t\t");
                        stringResult.append(packageToOffer.getTariffPlan().getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));
                        stringResult.append(" - ");
                        stringResult.append(packageToOffer.getSumOfCost());
                        for (ServiceToOffer serviceToOffer : packageToOffer.getServices()) {
                            stringResult.append("\n");
                            stringResult.append("\t\t\t");
                            stringResult.append(serviceToOffer.getService().getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));
                        }
                    }
                }

                showResultForm(stringResult.toString());

            }
        });

        fillTariffPlans();
        services = (LinearLayout) view.findViewById(R.id.services);

        return view;
    }

    private void showResultForm(String result) {
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(NavigationController.ACTION_SHOW_RESULT).putExtra(NavigationController.EXTRA_RESULT, result));
    }

    private void fillTariffPlans() {
        int i = 0;
        for (TariffPlan tariffPlan : Storage.getInstance().getMobileOperator().getTariffPlans()) {
            RadioButton rbtnTariffPlan = new RadioButton(getActivity());
            rbtnTariffPlan.setText(tariffPlan.getTitles().getStringByLanguage("ru"));
            rbtnTariffPlan.setTag(tariffPlan);
            rbtnTariffPlan.setId(i);
            rgrpSelectTariffPlans.addView(rbtnTariffPlan,
                    new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            Utils.convertDipToPixels(48, getResources())));
            i += 1;
        }
    }

    private void fillServices() {
        selectedServices.clear();
        this.services.removeAllViews();

        ArrayList<Service> services = Storage.getInstance().getMobileOperator().getAvailableServices(tariffPlan.getAvailableServices());
        int i = 0;
        for (final Service service : services) {
            final CheckBox chbService = new CheckBox(getActivity());
            chbService.setText(service.getTitles().getStringByLanguage(Application.getInstance().getSelectedLanguageCode()));
            chbService.setId(i);
            chbService.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        Calendar c = Calendar.getInstance();
                        c.add(Calendar.MONTH, -1);
                        Date endDate = c.getTime();
                        selectedServices.add(new Pair<>(service, endDate));
                    } else if (selectedServices.contains(service)) {
                        selectedServices.remove(service);
                    }
                }
            });

            this.services.addView(chbService,
                    new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            Utils.convertDipToPixels(48, getResources())));

            i += 1;
        }
    }

}
