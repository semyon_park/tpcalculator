package com.ct.tpcalculator.testapplication.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;

import com.ct.tpcalculator.R;
import com.ct.tpcalculator.calculator.statisticAgent.callstat.AlarmCallReceiver;
import com.ct.tpcalculator.calculator.statisticAgent.msgstat.AlarmMsgReceiver;
import com.ct.tpcalculator.calculator.statisticAgent.netstat.AlarmNetReceiver;
import com.ct.tpcalculator.helpers.FileCache;
import com.ct.tpcalculator.testapplication.NavigationController;
import com.ct.tpcalculator.testapplication.fragments.TestFragment;


public class TestActivity extends FragmentActivity {

    private NavigationController navigationController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FileCache.initCache(getApplicationContext());
        setContentView(R.layout.activity_test);
        navigationController = new NavigationController(this);
        showHome();

        AlarmCallReceiver.setAlarm(getApplicationContext());
        AlarmMsgReceiver.setAlarm(getApplicationContext());
        AlarmNetReceiver.setAlarm(getApplicationContext());
    }

    private void showHome() {
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(TestFragment.TAG)
                .replace(R.id.main, new TestFragment())
                .commit();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(navigationController);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(navigationController, navigationController.getIntentFilter());
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            finish();
        }

    }
}
